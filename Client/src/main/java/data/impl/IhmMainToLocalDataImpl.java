package data.impl;

import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IhmMainToLocalData;
import data.crud.OwnedChannelCRUD;
import data.crud.SubscriptionCRUD;
import data.crud.UserCRUD;
import data.utils.PasswordHasher;

import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * The IhmMainToLocalData Interface Implementation
 *
 * Implements all the methods from the IhmMainToLocalData Interface
 */
public class IhmMainToLocalDataImpl implements IhmMainToLocalData {
    private UserCRUD userCRUD;
    private OwnedChannelCRUD ownedChannelCRUD;
    private SubscriptionCRUD subscriptionCRUD;

    /**
     * Instantiates a new Ihm main to local data.
     *
     * @param subscriptionFilePath the subscription file path
     * @param ownedChannelFilePath the owned channel file path
     * @param userFilePath         the user file path
     * @param avatarDirectoryPath  the avatar directory path
     */
    public IhmMainToLocalDataImpl(String subscriptionFilePath,String ownedChannelFilePath,String userFilePath, String avatarDirectoryPath) {
        userCRUD = new UserCRUD(userFilePath, avatarDirectoryPath);
        ownedChannelCRUD = new OwnedChannelCRUD(ownedChannelFilePath);
        subscriptionCRUD = new SubscriptionCRUD(subscriptionFilePath);
    }

    @Override
    public User saveNewAccount(String login, String password, String firstName, String lastName, Date birthDate, BufferedImage avatar) {
        User u = userCRUD.readWithLogin(login);
        if(u == null){
            // create uuid
            String id = UUID.randomUUID().toString();
            // hash password
            String hashedPassword = PasswordHasher.hashPassword(password);
            u = new User(id, login, hashedPassword, firstName, lastName, birthDate, avatar);
            userCRUD.create(u);
            return u;
        }
        return null;
    }

    @Override
    public User getUserData(String id, String login) {
        return userCRUD.read(id);
    }

    @Override
    public User authenticate(String login, String password) {
        List<User> userList = userCRUD.readAll();

        if(userList == null){ return new User();}

        for(User u : userList){
            if(u.getLogin().equals(login) && u.getPassword().equals(PasswordHasher.hashPassword(password))){
                    return u;
            }
        }

        return new User();
    }
    @Override
    public OwnedChannel createOwnedChannel(String name, String desc, boolean isPrivate, User owner) {
        String id =UUID.randomUUID().toString();
        OwnedChannel newOwnedChannel = new OwnedChannel(id, name, desc, isPrivate, owner);
        return ownedChannelCRUD.create(newOwnedChannel);
    }
    @Override
    public Subscription addSubscription(String idChannel, User user, Subscription.Role role){
        OwnedChannel channel = this.ownedChannelCRUD.read(idChannel);

        if (channel != null && this.subscriptionCRUD.readByIdChannelAndIdUser(idChannel, user.getId()) == null){
            String nickname = user.getLogin();

            if (user.getLastName() != null && user.getFirstName() != null){
                nickname = user.getLastName() + " " + user.getFirstName();
            }

            Subscription s = new Subscription(channel.getId() + user.getId(), user, channel, nickname, role);
            this.subscriptionCRUD.create(s);
            return s;

        }

        return null;

    }

    @Override
    public User updateUser(String idUser, String firstName, String lastName, Date birthDate, BufferedImage avatar){
        User u = userCRUD.read(idUser);

        if(u != null) {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setBirthDate(birthDate);
            u.setAvatar(avatar);

            userCRUD.update(u);

            return u;

        }

        return null;
    }
}
