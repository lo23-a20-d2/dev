package data.impl;

import CommonClasses.*;
import Interfaces.CommToLocalData;
import data.crud.MessageCRUD;
import data.crud.OwnedChannelCRUD;
import data.crud.SubscriptionCRUD;

import java.util.*;

/**
 * The CommToLocalData Interface Implementation
 *
 * Implements all the methods from the CommToLocalData Interface
 *
 * @author Benjamin Vaysse
 */
public class CommToLocalDataImpl implements CommToLocalData {
    private MessageCRUD messageCRUD;
    private OwnedChannelCRUD ownedChannelCRUD;
    private SubscriptionCRUD subscriptionCRUD;

    /**
     * Instantiates a new Comm to local data.
     *
     * @param messagePath        the message path
     * @param ownedChannel       the owned channel
     * @param subscriptionPath   the subscription path
     * @param imageDirectoryPath the image directory path
     */
    public CommToLocalDataImpl(String messagePath, String ownedChannel, String subscriptionPath, String imageDirectoryPath) {
        messageCRUD = new MessageCRUD(messagePath);
        ownedChannelCRUD = new OwnedChannelCRUD(ownedChannel);
        subscriptionCRUD = new SubscriptionCRUD(subscriptionPath);
    }

    @Override
    public TextMessage modifyMessage(String newContent, String messageId){
        Message message = this.messageCRUD.read(messageId);
        message.setEdited(true);
        if (message instanceof TextMessage) {
            ((TextMessage) message).setTextContent(newContent);
            this.messageCRUD.update(message);
            return (TextMessage) message;
        }
        else {
            return null;
        }
    }

    @Override
    public void deleteMessage(String messageId) {
        Message message = this.messageCRUD.read(messageId);
        message.setErased(true);
        this.messageCRUD.update(message);
    }

    @Override
    public void deleteOwnedChannel(String channelId){
        List<Message> msgs = this.messageCRUD.readByIdChannel(channelId);
        List<Subscription> subs = this.subscriptionCRUD.readByIdChannel(channelId);
        for(Message m : msgs){
            this.messageCRUD.delete(m.getId());
        }
        for(Subscription s : subs){
            this.subscriptionCRUD.delete(s.getId());
        }
        this.ownedChannelCRUD.delete(channelId);
    }

    @Override
    public List<OwnedChannel> getAllOwnedChannel(String idUser) {
        List<OwnedChannel> list = ownedChannelCRUD.readByUserId(idUser);
        list.forEach(c -> {
            List<Subscription> subscriptions = subscriptionCRUD.readByIdChannel(c.getId());
            subscriptions.forEach(s ->{
                s.setChannel(c);
                c.getWhiteList().add(s);
            });
        });
        return list;
    }

    @Override
    public void updateAdmList(String idUser, String idChannel) {
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setRole(Subscription.Role.ADMIN);
        s.setChannel(ownedChannelCRUD.read(idChannel));
        subscriptionCRUD.update(s);
    }

    @Override
    public Subscription modifyNickname(String nickname, String userId, String channelId) {
        OwnedChannel c = ownedChannelCRUD.read(channelId);
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(channelId, userId);
        if (s != null) {
            s.setChannel(c);
            s.setNickname(nickname);
            subscriptionCRUD.update(s);
        }
        return s;
    }

    @Override
    public TextMessage addTextMessage(String messageContent, String senderId, String channelId) {
        Date date = new Date();
        Subscription subscription = subscriptionCRUD.readByIdChannelAndIdUser(channelId,senderId);
        String id = UUID.randomUUID().toString();
        TextMessage message = new TextMessage(id,id,date,false,false,subscription.getId(),channelId,messageContent);
        return (TextMessage) messageCRUD.create(message);
    }

    @Override
    public FileMessage addFileMessage(String messageContent, String senderId, String channelId) {
        return null;
    }

    @Override
    public void updateLikersList(String messageId, String senderId, String channelId) {
        Message m = messageCRUD.read(messageId);
        if (m.getLikersList() == null) {
            m.setLikersList(new ArrayList<>());
        }
        if (m.getLikersList().contains(senderId)) {
            m.getLikersList().remove(senderId);
        } else {
            m.getLikersList().add(senderId);
        }
        messageCRUD.update(m);
    }

    @Override
    public TextMessage addTextReply(String messageContent, String senderId, String channelId, String threadId) {
        String id = UUID.randomUUID().toString();
        Date date = new Date();
        Subscription subscription = subscriptionCRUD.readByIdChannelAndIdUser(channelId,senderId);
        TextMessage message = new TextMessage(id,threadId,date,false,false,subscription.getId(),channelId,messageContent);
        return (TextMessage) messageCRUD.create(message);
    }

    @Override
    public FileMessage addFileReply(String messageContent, String senderId, String channelId, String threadId) {
        return null;
    }


    @Override
    public List<Message> getThreadContents(String channelId, String threadId) {
        List<Message> thread = new ArrayList<>();
        this.messageCRUD.readByIdChannel(channelId).forEach(c ->{
            if(c.getIdThread().equals(threadId)){
                thread.add(c);
            }
        });
        return thread;
    }

    @Override
    public User getChannelOwner(String channelId) {
        OwnedChannel channel = ownedChannelCRUD.read(channelId);
        return channel == null ? null : channel.getOwner();
    }

    @Override
    public OwnedChannel getOwnedChannel(String idChannel){
        OwnedChannel channel = ownedChannelCRUD.read(idChannel);
        if(channel != null){
            List<Subscription> subscriptions = subscriptionCRUD.readByIdChannel(idChannel);
            subscriptions.forEach(s -> s.setChannel(channel));
            channel.setWhiteList(new ArrayList<>(subscriptions));
        }
        return channel;
    }


    @Override
    public List<Message> getMessages(String idChannel) {
        return this.messageCRUD.readByIdChannel(idChannel);
    }

    @Override
    public Message getMessage(String idChannel, String idMessage){
        List<Message> messages = messageCRUD.readByIdChannel(idChannel);
        Optional<Message> message = messages.stream().filter(m -> m.getId().equals(idMessage)).findFirst();
        return message.orElse(null);
    }

    @Override
    public Subscription addSubscription(String idChannel, User user, Subscription.Role role){
        OwnedChannel channel = this.ownedChannelCRUD.read(idChannel);
        if (channel != null && this.subscriptionCRUD.readByIdChannelAndIdUser(idChannel, user.getId()) == null){
            String nickname = user.getLogin();
            if (user.getLastName() != null && user.getFirstName() != null){
                nickname = user.getLastName() + " " + user.getFirstName();
            }
            Subscription s = new Subscription(channel.getId() + user.getId(), user, channel, nickname, role);
            this.subscriptionCRUD.create(s);
            return s;
        }
        return null;
    }

    @Override
    public void removeFromUserList(String idUser, String idChannel) {

        //Set Subscription isBanned attribute to TRUE
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setIsBanned(true);
        s.setEndDate(0); //Simple kick = 0
        subscriptionCRUD.update(s);

    }

    @Override
    public void cancelKick(String idUser, String idChannel){
        //Set Subscription isBanned attribute to FALSE
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setIsBanned(false);
        s.setEndDate(0);
        subscriptionCRUD.update(s);
    }

    @Override
    public void blockUserChannel(String idChannel, String idUserKicked) {
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUserKicked);
        s.setEndDate(-1); //Permanent kick = -1
        subscriptionCRUD.update(s);
    }

    @Override
    public void suspendMemberChannel(String idChannel, String idUserKicked, Long endDate) {
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUserKicked);
        s.setEndDate(endDate);
        subscriptionCRUD.update(s);
    }
}
