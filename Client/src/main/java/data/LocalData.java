package data;

import Interfaces.CommToLocalData;
import Interfaces.IhmMainToLocalData;
import data.impl.CommToLocalDataImpl;
import data.impl.IhmMainToLocalDataImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates all local files if they don't yet exist
 * Instantiates interfaces with Com and IhmMain.
 *
 * The data stored locally is :
 * - all users (each user stores his own user data locally)
 * - owned channels are its related data (subscription and messages)
 *
 * @author Tom Bourg
 * @author Kenza Rifki
 * @author Rami Jerbaka
 * @author Benjamin Vaysse
 */
public class LocalData {
    private final String STORAGE_DIRECTORY_NAME = "storage";
    private final String AVATAR_DIRECTORY_NAME = "img";
    private final String USER_FILENAME = "users.json";
    private final String OWNED_CHANNELS_FILENAME = "own_channels.json";
    private final String MESSAGE_FILENAME = "messages.json";
    private final String SUBSCRIPTION_FILENAME = "subscriptions.json";
    private File storageDirectory;
    private File avatarDirectory;
    private Map<String,File> files;

    private CommToLocalData commToLocalData;
    private IhmMainToLocalData ihmMainToLocalData;

    /**
     * Constructor of the class : instantiates LocalData, creating or getting all storage files.
     * The interfaces are then created with the paths to the useful storage files.
     */
    public LocalData() {
        //Storage Directory Creation
        String storageDirectoryPath = this.getClass().getResource("").getPath()+STORAGE_DIRECTORY_NAME;
        storageDirectory = new File(storageDirectoryPath);
        if(! storageDirectory.exists()){
            storageDirectory.mkdir();
        }

        //Avatar Directory Creation
        String avatarDirectoryPath = storageDirectoryPath +File.separator + AVATAR_DIRECTORY_NAME;
        avatarDirectory = new File(avatarDirectoryPath);
        if(! avatarDirectory.exists()){
            avatarDirectory.mkdir();
        }

        files = new HashMap();
        files.put("users",new File( storageDirectoryPath + File.separator + USER_FILENAME));
        files.put("messages",new File( storageDirectoryPath + File.separator + MESSAGE_FILENAME));
        files.put("own_channels",new File( storageDirectoryPath + File.separator + OWNED_CHANNELS_FILENAME));
        files.put("subscriptions",new File( storageDirectoryPath + File.separator + SUBSCRIPTION_FILENAME));
        files.values().forEach(file -> {
            if(!file.exists()){
                try {
                    file.createNewFile();
                    FileWriter myWriter = new FileWriter(file);
                    myWriter.write("[]");
                    myWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        ihmMainToLocalData = new IhmMainToLocalDataImpl(
                files.get("subscriptions").getPath(),
                files.get("own_channels").getPath(),
                files.get("users").getPath(),
                avatarDirectoryPath
        );
        commToLocalData = new CommToLocalDataImpl(
                files.get("messages").getPath(),
                files.get("own_channels").getPath(),
                files.get("subscriptions").getPath(),
                avatarDirectoryPath
        );
    }

    /**
     * Gets the storage directory.
     *
     * @return the storage directory
     */
    public File getStorageDirectory() {
        return storageDirectory;
    }

    /**
     * Gets all the files (
     *
     * @return the files
     */
    public Map<String, File> getFiles() {
        return files;
    }

    /**
     * Gets comm to local data interface
     *
     * @return CommToLocalDatainterface
     */
    public CommToLocalData getCommToLocalData() {
        return commToLocalData;
    }

    /**
     * Gets ihm main to local data interface
     *
     * @return IhmMainToLocalData
     */
    public IhmMainToLocalData getIhmMainToLocalData() {
        return ihmMainToLocalData;
    }
}
