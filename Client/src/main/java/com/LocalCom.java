package com;

import Interfaces.*;
import message.client.ClientMessage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class LocalCom implements ILocalCom {
    // Network
    private Socket socket;
    private ObjectOutputStream output;
    private ServerListener listener;

    //Interfaces
    private CommToLocalData dataInterface;
    private LocalComToIhmChannel ihmChannelInterface;
    private LocalComToIhmMain ihmMainInterface;

    private IhmMainToLocalCom interfaceForMain;
    private IhmChannelToLocalCom interfaceForChannel;

    //Server connexion
    String host;
    static final int PORT = 1996;

    //Getters + Setters
    public Socket getSocket() {
        return socket;
    }

    public LocalComToIhmChannel getIhmChannelInterface() {
        return ihmChannelInterface;
    }

    public CommToLocalData getDataInterface() {
        return dataInterface;
    }

    public LocalComToIhmMain getIhmMainInterface() {
        return ihmMainInterface;
    }

    public IhmMainToLocalCom getInterfaceForMain() {
        return interfaceForMain;
    }

    public IhmChannelToLocalCom getInterfaceForChannel() {
        return interfaceForChannel;
    }


    //Constructor
    public LocalCom() {
        this.interfaceForMain = new IhmMainToLocalComImpl(this);
        this.interfaceForChannel = new IhmChannelToLocalComImpl(this);
    }

    public void init(LocalComToIhmChannel ihmChannelInterface, CommToLocalData dataInterface, LocalComToIhmMain ihmMainInterface) {
        this.ihmChannelInterface = ihmChannelInterface;
        this.dataInterface = dataInterface;
        this.ihmMainInterface = ihmMainInterface;
    }

    //Methods
    /**
     * Initialization of the connection between client and server 
     * @param host server's adress
     * @throws IOException  if an error occurs during the initialization of the communication socket
     */
    public void connectToServer(String host) throws IOException {
        this.host = host;
        socket = new Socket(host, PORT);
        output = new ObjectOutputStream(socket.getOutputStream());
        listener = new ServerListener(this);
        listener.start();
    }

    public void disconnectFromServer() throws IOException {
        output.close();
        socket.close();
        ihmMainInterface.quitServer("");
    }

    /**
     * Sending of a client message to the server
     * @param msg message to send
     * @throws IOException  if an error occurs during the sending of the message via the communication socket
     */
    public void sendMessage(ClientMessage msg) throws IOException {
        output.writeObject(msg);
    }
}
