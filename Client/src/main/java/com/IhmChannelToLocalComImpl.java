package com;

import CommonClasses.Subscription;
import Interfaces.IhmChannelToLocalCom;
import message.client.*;

import java.io.File;
import java.io.IOException;

public class IhmChannelToLocalComImpl implements IhmChannelToLocalCom {
    private LocalCom com;

    public IhmChannelToLocalComImpl(LocalCom com) {
        this.com = com;
    }
    
    /**
     * Pick nickname
     * @param nickname the nickname
     * @param idUser user's id
     * @param idChannel channel's id
     */
    @Override
    public void changeNickname(String nickname, String idUser, String idChannel) {
    	try {
    		//Message initialization
    		NewNicknameClientMessage msg = new NewNicknameClientMessage(nickname,idUser,idChannel);
    		//Sending the message to the server
    		com.sendMessage(msg);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }

    /**
     * Send new text message
     * @param msgContent the message
     * @param idSender sender's id
     * @param idChannel channel's id
     */
    @Override
    public void sendNewTextMsg(String msgContent, String idSender, String idChannel)
    {
        try {
            //Message initialization
            NewTextMsgClientMessage msg = new NewTextMsgClientMessage(msgContent, idSender, idChannel);
            //Sending the message to the server
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendNewFileMsg(File fileContent, String idSender, String idChannel){}

    /**
     * Send modified message
     * @param newContent the modified message
     * @param idMsg message's id
     * @param idChannel channel's id
     */
    @Override
    public void sendModifiedMsg(String newContent, String idMsg, String idChannel){
        // Message initialization
        ModifyMsgClientMessage msg = new ModifyMsgClientMessage(newContent, idMsg, idChannel);

        // Sending the message to the server
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete message
     * @param idMsg id of the message
     * @param idChannel id of the channel
     */
    @Override
    public void sendDeletedMsg(String idMsg, String idChannel){
        //Message initialization
        try {
            DeleteMsgClientMessage msg = new DeleteMsgClientMessage(idMsg, idChannel);
            //Sending the message to the server
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

    /**
     * Like a message
     * @param idMsg     message's id
     * @param idSender  id of the person who liked the message
     * @param idChannel channel's id
     * @author Benoit RAMS
     */
    @Override
    public void sendLike(String idMsg, String idSender, String idChannel) {
        //Message initialization
        try {
            NewLikeClientMessage msg = new NewLikeClientMessage(idMsg, idSender, idChannel);
            //Sending the message to the server
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Send new text reply
     * @param msgContent the message
     * @param idSender   id of the person who is answering the message
     * @param idChannel  channel's id
     * @param idThread   thread's id
     * @author Benoit RAMS
     */
    @Override
    public void sendNewTextReply(String msgContent, String idSender, String idChannel, String idThread) {
        NewTextReplyClientMessage msg = new NewTextReplyClientMessage(msgContent, idSender, idChannel, idThread);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendNewFileReply(File fileContent, String idSender, String idChannel, String idThread){}

    /**
     * Request thread contents
     * @param idChannel channel's id
     * @param idThread  thread's id
     * @author Benoit RAMS
     */
    @Override
    public void requestThreadContents(String idChannel, String idThread) {
        RequestThreadContentsClientMessage msg = new RequestThreadContentsClientMessage(idChannel, idThread);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Invite a user to a channel
     * @param idChannel channel's id
     * @param idUser user's id
     * @param role user's role
     */
    @Override
    public void inviteUserToChannel(String idChannel, String idUser, Subscription.Role role) {
        try {
            com.sendMessage(new InviteToChannelClientMessage(idUser, idChannel, role));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Kick a user from a channel
     * @param idUser       user's id (the one who kick)
     * @param idChannel    channel's id
     * @param idUserKicked user's id (the one kicked)
     * @param endDate      kick duration
     *                     if -1 then permanent, else if 0 then normal, else temporary
     * @author Benoit RAMS
     */
    @Override
    public void kickMember(String idUser, String idChannel, String idUserKicked, long endDate) {
        KickMemberClientMessage msg = new KickMemberClientMessage(idUser, idChannel, idUserKicked, endDate);
        try {
            //Sending the message to the server
            com.sendMessage(msg);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    } // Proposed modification: int endData -> Date endData

    /**
     * Promote an user to admin
     * @param idUser user's id
     * @param idChannel channel's id
     */
    @Override
    public void promoteToAdmin(String idUser, String idChannel) {
    	try {
    		//Message initialization
    		RequestPromotionClientMessage msg = new RequestPromotionClientMessage(idUser, idChannel);
    		//Sending the message to the server
    		com.sendMessage(msg);
    	} catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cancel a kick
     * @param idChannel channel's id
     * @param idUser    user's id
     * @author Benoit RAMS
     */
    @Override
    public void cancelKick(String idUser, String idChannel) {
        //Message initialization
        ReqCancelKickClientMessage msg = new ReqCancelKickClientMessage(idUser, idChannel);
        try {
            //Sending the message to the server
            com.sendMessage(msg);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * Leave a channel
     * @param idChannel channel's id
     * @param idUser    user's id
     */
    @Override
    public void quitChannel(String idUser, String idChannel) {
        //Message initialization
    	ReqDeconnexionClientMessage msg = new ReqDeconnexionClientMessage(idUser, idChannel);
        try{
            //Sending the message to the server
        	com.sendMessage(msg);
        }catch (IOException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    /**
     * Delete a channel
     * @param idChannel channel's id
     */
    @Override
    public void deleteChannel(String idChannel){
    	//Message initialization
    	SendDeleteChannelClientMessage msg = new SendDeleteChannelClientMessage(idChannel);
        try{
            //Sending the message to the server
        	com.sendMessage(msg);
        }catch (IOException ex){
            System.err.println(ex.getMessage());
        }
    }

}
