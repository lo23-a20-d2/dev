package com;

import CommonClasses.Category;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IhmMainToLocalCom;
import message.client.*;

import java.io.IOException;

public class IhmMainToLocalComImpl implements IhmMainToLocalCom {
    private LocalCom com;

    public IhmMainToLocalComImpl(LocalCom com) {
        this.com = com;
    }
    
    /**
     * Create a channel
     * @param name channel's name
     * @param description channel's description 
     * @param isPrivate if the channel is private
     * @param isShared if the channel is shared 
     */
    @Override
    public void createChannel(String name, String description, boolean isPrivate, boolean isShared) {
        NewChannelClientMessage msg = new NewChannelClientMessage(name, description, isPrivate);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a owned channel
     *
     * @param channel the owned channel
     */
    @Override
    public void unsharedChannelCreated(OwnedChannel channel, Subscription sub) {
        NotifyNewUnsharedChannelClientMessage msg = new NotifyNewUnsharedChannelClientMessage(channel, sub);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Join a new channel 
     * @param idChannel channel'id  
     * @param idUser user's id 
     */
    @Override
    public void joinNewChannel(String idChannel, String idUser) throws IOException {
        // Message initialization
        NewSubscriptionClientMessage msg = new NewSubscriptionClientMessage(idChannel, idUser);

        // Sending of the message 
        com.sendMessage(msg);
    }
    
    /**
     * Enter a channel 
     * @param idChannel channel'id  
     * @param idUser user's id 
     */
    @Override
    public void enterChannel(String idChannel, String idUser) {
        // Message initialization
        JoinChannelClientMessage msg = new JoinChannelClientMessage(idChannel, idUser);

        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Send an account 
     * @param u the user 
     * @param ipAddress the ip adress 
     */
    @Override
    public void sendAccount(User u, String ipAddress) throws IOException {
        //Client connection to the server 
        com.connectToServer(ipAddress);

        //Sending the client account to the server 
        NotifyNewConnectionClientMessage msg = new NotifyNewConnectionClientMessage(u, com.getDataInterface().getAllOwnedChannel(u.getId()));
        com.sendMessage(msg);
    }

    @Override
    public void reqServerDisconnection() throws IOException {
        com.disconnectFromServer();
    }
    
    /**
     * Search a channel 
     * @param input the search 
     * @param category the category 
     */
    @Override
    public void searchChannel(String input, Category category) {
        SearchChannelClientMessage msg = new SearchChannelClientMessage(input, category);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void sendUpdateUser(User u) {
        try{
            SendModifyUserClientMessage msg=new SendModifyUserClientMessage(u);
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }


}
