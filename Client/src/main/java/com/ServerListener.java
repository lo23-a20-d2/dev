package com;

import message.server.ServerMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;

public class ServerListener extends Thread {
    // Network
    private ObjectInputStream input;

    private LocalCom com;

    public ServerListener(LocalCom com){
        this.com = com;
    }

    public void run() {
        try{
            input = new ObjectInputStream(com.getSocket().getInputStream());

            while(true) {
                ServerMessage msg = (ServerMessage) input.readObject();
                System.out.println("Nouveau message reçu : " + msg.toString());
                msg.handle(com);
            }
        }
        catch (ClassNotFoundException exc){
            exc.printStackTrace();
            return;
        }
        catch (IOException ex){
            System.out.println("Déconnexion du serveur.");
//            ex.printStackTrace(); //Debug
        }
    }

}
