import com.LocalCom;
import data.LocalData;
import ihmChannel.IhmChannel;
import ihmMain.IhmMain;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class MainClient extends Application {
    private LocalData data;
    private LocalCom com;
    private IhmMain ihmMain;
    private IhmChannel ihmChannel;

    @Override
    public void init() throws Exception {
        super.init();
        System.setProperty("javafx.sg.warn", "true"); // Necessary for IDE Debug

        this.data = new LocalData();
        this.com = new LocalCom();
        this.ihmMain = new IhmMain();
        this.ihmMain.loadIhmChannel();
        this.ihmChannel = ihmMain.getIhmChannel();

        this.com.init(ihmChannel.getInterfaceForLocalCom(), data.getCommToLocalData(), ihmMain.getInterfaceForCom());
        this.ihmMain.init(data.getIhmMainToLocalData(), com.getInterfaceForMain(), ihmChannel.getInterfaceForIhmMain());
        this.ihmChannel.init(com.getInterfaceForChannel(),ihmMain.getInterfaceForChannel());
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        // Chargement de la vue de IhmMain
        try{
            ihmMain.start(primaryStage);
        } catch (IOException ex){
            System.out.println("error");
        }
    }

    @Override
    public void stop() throws Exception {
        if (this.ihmMain.userIsConnected()){
            this.ihmMain.getInterfaceToCom().reqServerDisconnection();
        }

        super.stop();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
