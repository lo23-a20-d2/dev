package ihmMain;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;

/**
 * Standalone launcher for the IHM Main Module
 * Used before v1 when the 4 submodules weren't integrated together
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 * @deprecated use {@link main.MainClient} instead
 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        // Main stage set up
        Locale.setDefault(Locale.FRANCE);
        Parent root = FXMLLoader.load(getClass().getResource("views/mainView.fxml"));
        Scene scene = new Scene(root);

        primaryStage.setTitle("Nom de l'application");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(700);
        primaryStage.setMinWidth(1000);

        primaryStage.show();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
