package ihmMain;

public class Constants {
    private Constants() { throw new IllegalStateException("Utility class"); }

    // User input conditions
    public static final int MAX_LOGIN_LENGTH = 20;
    public static final int MIN_PSW_LENGTH = 6;
    public static final int MAX_PSW_LENGTH = 100;
    public static final int AVATAR_WIDTH = 100; // Avatar width (and height because it's square) in pixels
    public static final int MAX_AVATAR_SIZE = 2 * (1024 * 1024); // Maximum avatar size in bits
    public static final int MAX_NAME_LENGTH = 50; // Maximum number of characters in first and last name
    public static final int MAX_CHANNEL_DESCRIPTION_LENGTH = 200; // Maximum number of characters in channel description
    public static final int MAX_CHANEL_NAME_LENGTH = 50; // Maximum number of characters in channel name
    public static final int MIN_SEARCH_INPUT_LENGTH = 2;
}
