package ihmMain;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.*;
import ihmChannel.IhmChannel;
import ihmMain.controllers.MainView;
import ihmMain.controllers.common.EndKick;
import ihmMain.interfaces.impl.IhmChannelToIhmMainImpl;
import ihmMain.interfaces.impl.LocalComToIhmMainImpl;
import ihmMain.interfaces.impl.LocalDataToIhmMainImpl;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;

/**
 * Main controller for the IhmMain module
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class IhmMain {
    private IhmChannel ihmChannel;
    private VBox ihmChannelView;

    // Application state
    private String idConnectedUser;
    private User connectedUser;
    private String idCurrentChannel;

    private Map<String, EndKick> scheduledKicks = new HashMap<>();
    private ObservableList<Channel> kickedFromChannels = FXCollections.observableArrayList();
    private ObservableList<Channel> observableListChannel = FXCollections.observableArrayList();

    private ObservableList<Channel> observableUserListChannelPublic = FXCollections.observableArrayList();
    private ObservableList<Channel> observableUserListChannelPv = FXCollections.observableArrayList();

    private ObservableList<Subscription> observableUserSubscription = FXCollections.observableArrayList();


    private IhmMainToLocalData dataInterface;
    private IhmMainToLocalCom comInterface;
    private IhmMainToIhmChannel ihmChannelInterface;

    private LocalComToIhmMain interfaceForCom;
    private IhmChannelToIhmMain interfaceForChannel;
    private LocalDataToIhmMain interfaceForData;

    private MainView mainController;

    private Timer kickTimer;

    /**
     * IhmMain's default constructor.
     * Creates interfaces for the other modules. Loads IhmChannel's view.
     */
    public IhmMain() {
        this.idConnectedUser = "";
        this.idCurrentChannel = null;
        this.connectedUser = new User();
        this.kickTimer = new Timer();

        this.interfaceForCom = new LocalComToIhmMainImpl(this);
        this.interfaceForChannel = new IhmChannelToIhmMainImpl(this);
        this.interfaceForData = new LocalDataToIhmMainImpl(this);
    }

    /**
     * Loads IhmChannel view
     */
    public void loadIhmChannel(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ihmChannel/views/windowView.fxml"));
        try {
            ihmChannelView = loader.load();
            this.ihmChannelView.setVisible(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ihmChannel = loader.getController();
    }

    /**
     * Initializes the interfaces to other modules.
     *
     * @param data the interface to LocalData
     * @param com  the interface to LocalCom
     * @param ihmChannel the interface to IhmChannel
     */
    public void init(IhmMainToLocalData data, IhmMainToLocalCom com, IhmMainToIhmChannel ihmChannel){
        dataInterface = data;
        comInterface = com;
        ihmChannelInterface = ihmChannel;
    }

    /**
     * Start method of the ihmMain module
     * Loads the main view and initializes the main controller.
     *
     * @param primaryStage Displayed stage
     * @throws IOException
     */
    public void start(Stage primaryStage) throws IOException {
        Locale.setDefault(Locale.FRANCE);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/mainView.fxml"));
        Parent root = loader.load();
        this.mainController = loader.getController();
        this.mainController.init(this);

        int width = 1100;
        int height = 720;
        final Scene scene = new Scene(root, width, height);

        // Center the window
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth() - width) / 2);
        primaryStage.setY((screenBounds.getHeight() - height) / 2);

        primaryStage.setTitle("Nom de l'application");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(height);
        primaryStage.setMinWidth(width);
        primaryStage.show();
    }

    /**
     * Get interface for LocalCom
     *
     * @return the interface for local com
     */
    public LocalComToIhmMain getInterfaceForCom() { return interfaceForCom; }

    /**
     * Get interface for IhmChannel
     *
     * @return the interface for ihm channel
     */
    public IhmChannelToIhmMain getInterfaceForChannel() { return interfaceForChannel; }

    /**
     * Get interface for LocalData
     *
     * @return the interface for local data
     */
    public LocalDataToIhmMain getInterfaceForData(){ return this.interfaceForData; }

    /**
     * Get ihm channel module's main controller
     *
     * @return the ihm channel's main controller
     */
    public IhmChannel getIhmChannel() { return ihmChannel; }

    /**
     * Get interface to LocalData
     *
     * @return the interface to local data
     */
    public IhmMainToLocalData getInterfaceToData(){ return this.dataInterface; }

    /**
     * Get interface to LocalCom
     *
     * @return the interface to local com
     */
    public IhmMainToLocalCom getInterfaceToCom(){ return this.comInterface; }

    /**
     * Get interface to IhmChannel
     *
     * @return the interface to to ihm channel
     */
    public IhmMainToIhmChannel getInterfaceToChannel(){return this.ihmChannelInterface; }

    /**
     * Get the main controller
     *
     * @return the main controller
     */
    public MainView getMainController(){ return this.mainController;}

    /**
     * Get the connected user
     *
     * @return the connected user
     */
    public User getConnectedUser(){ return this.connectedUser; }

    /**
     * Set the application's connected user
     *
     * @param user the connected user
     */
    public void setConnectedUser(User user){
        this.connectedUser = user;
        this.idConnectedUser = user.getId();
    }

    /**
     * Reset the connected user to none
     */
    public void resetConectedUser(){
        this.connectedUser = new User();
        this.idConnectedUser = "";
    }

    /**
     * Check if a user is connected to the application
     *
     * @return true if idConnectedUser is defined
     */
    public boolean userIsConnected(){
        return !idConnectedUser.equals("");
    }

    /**
     * Get id of the connected user
     *
     * @return the id of the connected user
     */
    public String getIdConnectedUser(){
        return this.idConnectedUser;
    }

    /**
     * Sets the id of current channel
     * Also selects current channel in ListChannel and toggles visibility if new current channel is null
     *
     * @param channel the channel
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void setIdCurrentChannel(Channel channel){
        if(channel == null) { // Hide ihmChannel
            this.ihmChannelView.setVisible(false);
            if(this.mainController.getHome() != null) {
                this.mainController.getHome().getSidebar().getListChannel().getPublicChannelsList().getSelectionModel().clearSelection();
                this.mainController.getHome().getSidebar().getListChannel().getPrivateChannelsList().getSelectionModel().clearSelection();
            }
            this.idCurrentChannel = null;
        } else {
            if (this.idCurrentChannel == null) { // ihmChannel was hidden
                this.ihmChannelView.setVisible(true);
            }

            this.getInterfaceToCom().enterChannel(channel.getId(), this.getIdConnectedUser());
            this.idCurrentChannel = channel.getId();

            Platform.runLater(() -> {
                // Select channel in ListChannel
                if (channel.isPrivate()) {
                    this.mainController.getHome().getSidebar().getListChannel().getPublicChannelsList().getSelectionModel().clearSelection();
                    for (Channel c : this.mainController.getHome().getSidebar().getListChannel().getPrivateChannelsList().getItems()){
                        if (c.getId().equals(channel.getId())){
                            this.mainController.getHome().getSidebar().getListChannel().getPrivateChannelsList().getSelectionModel().select(c);
                            break;
                        }
                    }
                    this.mainController.getHome().getSidebar().getListChannel().getPrivateChannelsList().refresh();
                } else {
                    this.mainController.getHome().getSidebar().getListChannel().getPrivateChannelsList().getSelectionModel().clearSelection();
                    for (Channel c : this.mainController.getHome().getSidebar().getListChannel().getPublicChannelsList().getItems()){
                        if (c.getId().equals(channel.getId())){
                            this.mainController.getHome().getSidebar().getListChannel().getPublicChannelsList().getSelectionModel().select(c);
                            break;
                        }
                    }
                    this.mainController.getHome().getSidebar().getListChannel().getPublicChannelsList().refresh();
                }
            });
        }
    }

    /**
     * Get the current channel's id
     *
     * @return the current channel's id
     */
    public String getIdCurrentChannel(){
        return this.idCurrentChannel;
    }

    /**
     * Get IhmChannel module's view
     *
     * @return the ihmChannel's view
     */
    public VBox getIhmChannelView(){ return this.ihmChannelView;}

    /**
     * Gets list of channels the user got kicked from
     *
     * @return the kicked from channels
     */
    public ObservableList<Channel> getKickedFromChannels() { return kickedFromChannels; }

    /**
     * Get all channels from the server
     *
     * @return an observable list of channels
     */
    public ObservableList<Channel> getObservableListChannel() {return this.observableListChannel;}

    /**
     * Get all public channels joined or created by the connected user
     *
     * @return an observable list of channels
     */
    public ObservableList<Channel> getObservableUserListChannelPublic() {return this.observableUserListChannelPublic;}

    /**
     * Get all private channels joined or created by the connected user
     *
     * @return an observable list of channels
     */
    public ObservableList<Channel> getObservableUserListChannelPv() {return this.observableUserListChannelPv;}

    /**
     * Get all subscriptions of the connected user
     *
     * @return an observable list of subscriptions
     */
    public ObservableList<Subscription> getObservableUserSubscription() {return this.observableUserSubscription;}

    /**
     * Updates the connected user's public and private channels
     */
    public void setUserListChannel() {
        this.observableUserListChannelPv.clear();
        this.observableUserListChannelPublic.clear();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        long now = cal.getTimeInMillis();

        this.observableUserSubscription.forEach(subscription -> {
            Channel channel = subscription.getChannel();
            if (channel.isPrivate())
                this.observableUserListChannelPv.add(channel);
            else
                this.observableUserListChannelPublic.add(channel);

            if (subscription.getIsBanned() && (subscription.getEndDate() == -1 || subscription.getEndDate() > now)){
                boolean alreadyKicked = false;
                for (Channel c : this.getKickedFromChannels()){
                    if (c.getId().equals(channel.getId()))
                        alreadyKicked = true;
                }
                if (!alreadyKicked)
                    this.kickedFromChannels.add(channel);
            }
        });
    }

    /**
     * Add a new channel to the user
     * @param channel the channel to add
     */
    public void addNewChannelToUser(Channel channel, Subscription subscription){
        if (!this.observableUserSubscription.contains(subscription))
            this.observableUserSubscription.add(subscription);
        if(channel.isPrivate() && !this.observableUserListChannelPv.contains(channel)){
            this.observableUserListChannelPv.add(channel);
        } else if (!channel.isPrivate() && !this.observableUserListChannelPublic.contains(channel)) {
            this.observableUserListChannelPublic.add(channel);
        }
    }

    /**
     * Add a public channel to the server
     * @param channel the channel to add
     */
    public void addNewPublicChannel(Channel channel){
        if (!channel.isPrivate() && !this.observableListChannel.contains(channel)){
            this.observableListChannel.add(channel);
        }
    }

    /**
     * Reset all the user's channels list
     */
    public void clearChannelsList(){
        this.observableUserListChannelPublic.clear();
        this.observableUserListChannelPv.clear();
        this.observableListChannel.clear();
        this.observableUserSubscription.clear();
        this.kickedFromChannels.clear();
        for (EndKick task : scheduledKicks.values())
            task.cancel();
        this.scheduledKicks.clear();
    }

    /**
     * Get the timer to schedule a kick
     * @return the timer
     */
    public Timer getKickTimer(){ return this.kickTimer;}

    /**
     * Get all scheduled kicks
     * @return a Map with all the user's scheduled kicks
     */
    public Map<String, EndKick> getScheduledKicks() { return this.scheduledKicks;}

    /**
     * Stop a scheduled kick
     * @param idChannel channel's id the kick was canceled
     */
    public void stopScheduledKick(String idChannel) {
        if(this.scheduledKicks.containsKey(idChannel)){
            this.scheduledKicks.get(idChannel).cancel();
        }
    }
}
