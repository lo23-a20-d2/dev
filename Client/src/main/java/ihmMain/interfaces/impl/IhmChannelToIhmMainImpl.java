package ihmMain.interfaces.impl;

import ihmMain.IhmMain;
import Interfaces.IhmChannelToIhmMain;

/**
 * IhmMain's interface for IhmChannel.
 * Contains set of methods needed by ihmChannel.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class IhmChannelToIhmMainImpl implements IhmChannelToIhmMain {
    private IhmMain ihmMain;

    public IhmChannelToIhmMainImpl(IhmMain main){
        this.ihmMain = main;
    }

    /**
     * Getter for the connected user's id
     * @return String the connected user's id
     */
    @Override
    public String getIdConnectedUser() {
        return ihmMain.getIdConnectedUser();
    }
}
