package ihmMain.interfaces.impl;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import Interfaces.LocalComToIhmMain;
import ihmMain.IhmMain;
import ihmMain.controllers.common.EndKick;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.Alert;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Implementation of ihmMain's interface for LocalCom.
 * Overrides set of methods needed by localCom.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class LocalComToIhmMainImpl implements LocalComToIhmMain {
    private IhmMain ihmMain;

    public LocalComToIhmMainImpl(IhmMain main){
        this.ihmMain = main;
    }

    /**
     * Getter for the connected user's id
     *
     * @return String the connected user's id
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr
     */
    @Override
    public String getIdConnectedUser() {
        return ihmMain.getIdConnectedUser();
    }

    /**
     * Display the user's search result
     *
     * @param channelList query's result
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Override
    public void displaySearchResult(List<Channel> channelList) {
        Platform.runLater( () -> {
            channelList.removeIf(Channel::isPrivate);
            ihmMain.getMainController().getHome().getSidebar().showResults(channelList);
        });
    }

    /**
     * Add a channel to the user ListChannel when the connected User join it
     *
     * @param channel channel joined by the user
     * @param subscription user's subscription to the new channel
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    @Override
    public void displayNewChannel(Channel channel, Subscription subscription) {
        Platform.runLater( () -> {
            this.ihmMain.addNewChannelToUser(channel, subscription);
            this.ihmMain.setIdCurrentChannel(channel);
        });
    }

    /**
     * Add a channel to the user after being invited into it.
     *
     * @param channel the channel the user was invited to
     * @param subscription user's subscription to the new channel
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Override
    public void newInvitation(Channel channel, Subscription subscription){
        Platform.runLater( () -> {
            this.ihmMain.addNewChannelToUser(channel, subscription);
            this.ihmMain.getMainController().getHome().alertUser(Alert.AlertType.INFORMATION, channel, "Nouvelle invitation", "Vous avez été invité dans le channel " + channel.getName(), "Voulez-vous ouvrir ce channel ?");
        });
    }

    /**
     * Called when a new public channel was created on the server
     *
     * @param channel the public channel created
     */
    @Override
    public void newPublicChannel(Channel channel) {
        Platform.runLater(() -> this.ihmMain.addNewPublicChannel(channel));
    }

    /**
     * Notify the user he's been kicked from a channel.
     *
     * @param idUserKicked kicked user's id
     * @param channel the channel the user has been kicked from
     * @param endDate
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Override
    public void notifyKickedChannel(String idUserKicked, Channel channel, long endDate) {
        String alertMessage = "Vous avez été banni(e) du channel " + channel.getName();
        String alertContent = "";

        // Close the channel if it's open
        if (ihmMain.getIdCurrentChannel() != null && ihmMain.getIdCurrentChannel().equals(channel.getId()))
            ihmMain.setIdCurrentChannel(null);

        if (endDate != 0 && !ihmMain.getKickedFromChannels().contains(channel)) { // Timed kick
            ihmMain.getKickedFromChannels().add(channel);

            if (endDate == -1)
                alertContent = "Vous pourrez revenir sur le channel après annulation de celui-ci";
            else {
                Timestamp stamp = new Timestamp(endDate);
                Date date = new Date(stamp.getTime());
                String pattern = "dd/MM/yyyy à HH:mm:ss";
                DateFormat df = new SimpleDateFormat(pattern);
                alertContent = "Vous pourrez revenir sur le channel le " + df.format(date);

                Date currentDate = new Date();
                long currentTimestamp = currentDate.getTime();

                EndKick scheduledKick = new EndKick(this.ihmMain, channel);
                this.ihmMain.getKickTimer().schedule(scheduledKick, endDate - currentTimestamp);
                this.ihmMain.getScheduledKicks().put(channel.getId(), scheduledKick);
            }
        }

        String finalAlertContent = alertContent;
        Platform.runLater(() -> this.ihmMain.getMainController().getHome().alertUser(Alert.AlertType.WARNING, channel, "Kick", alertMessage, finalAlertContent));
    }

    /**
     * Notify the user his kick has been canceled
     *
     * @param idUser kicked user's id
     * @param channel the channel id the user used to be kicked from
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Override
    public void notifyKickCanceled(String idUser, Channel channel) {
        this.ihmMain.getKickedFromChannels().removeIf(kickedChannel -> channel.getId().equals(kickedChannel.getId()));
        this.ihmMain.stopScheduledKick(channel.getId());
        Platform.runLater(() -> ihmMain.getMainController().getHome().alertUser(Alert.AlertType.INFORMATION, channel, "Ban annulé", "Votre ban du channel " + channel.getName() + " a été annulé", "Voulez-vous ouvrir ce channel ?"));
    }

    /**
     * Quit the current server
     * Reset the connected user and current channel to none. Show authentication page.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Override
    public void quitServer(String message) {
        if(!message.equals(""))
            Platform.runLater(() -> ihmMain.getMainController().getAuth().getConnectToServer().getErrorLabel().setText(message));
        else {
            this.ihmMain.resetConectedUser();
            this.ihmMain.setIdCurrentChannel(null);
            this.ihmMain.clearChannelsList();
            this.ihmMain.getKickTimer().purge();
            this.ihmMain.getMainController().showAuthentication();
        }
    }

    /**
     * Called on connexion, initiates all the user's channels and subscriptions
     *
     * @param channelList       server's channels
     * @param subscriptionList  user's subscriptions
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Override
    public void initConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList){
        Platform.runLater(() -> {
            ihmMain.getMainController().getAuth().showHome();
            ihmMain.getObservableUserSubscription().addAll(subscriptionList);
            ihmMain.getObservableListChannel().addAll(channelList);

            subscriptionList.forEach(subscription ->
            {
                Channel channel = subscription.getChannel();
                Date currentDate = new Date();
                long currentTimestamp = currentDate.getTime();

                if (subscription.getIsBanned() && subscription.getEndDate() > currentTimestamp) {
                    EndKick scheduledKick = new EndKick(this.ihmMain, channel);
                    this.ihmMain.getKickTimer().schedule(scheduledKick, subscription.getEndDate() - currentTimestamp);
                    this.ihmMain.getScheduledKicks().put(channel.getId(), scheduledKick);
                }
            });

            ihmMain.setUserListChannel();
        });
    }

    /**
     * Add all channels and subscriptions to observables
     * Set userListChannel
     *
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    @Override
    public void updateConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList) {
        Platform.runLater(() -> {
            ihmMain.getMainController().getAuth().showHome();
            ihmMain.getObservableUserSubscription().addAll(subscriptionList);
            ihmMain.getObservableListChannel().addAll(channelList);

            ihmMain.setUserListChannel();
        });
    }

    /**
     * A temporary method used for removing owned channnels when the creator of channel disconnect from server
     *
     * @param ownedChannelsId identifiants of owned channels to remove from current list
     * @author Yiwen WANG
     */
    @Override
    public void removeOwnedChannels(List<String> ownedChannelsId) {
        Platform.runLater(() -> {
            ihmMain.getObservableListChannel().removeIf(c -> ownedChannelsId.contains(c.getId()));
            ihmMain.getObservableUserSubscription().removeIf(s -> ownedChannelsId.contains(s.getChannel().getId()));
            if (ownedChannelsId.contains(ihmMain.getIdCurrentChannel())) {
                ihmMain.setIdCurrentChannel(null);
            }

            ihmMain.setUserListChannel();
        });

    }

    /**
     * Delete channel from user and global channel lists
     * Hides ihmChannel if the deleted channel was the open one
     * Opens next channel if existant
     *
     * @param idChannel the id of the channel to delete
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void deleteChannel(String idChannel) {
        Channel newChannel = null;

        if(ihmMain.getIdCurrentChannel() != null && ihmMain.getIdCurrentChannel().equals(idChannel)) { // If deleted channel is open
            ObservableList<Channel> privateChannels = ihmMain.getObservableUserListChannelPv();
            for (Channel channel : privateChannels) { // Find channel in private user channels list
                if (channel.getId().equals(idChannel)) {
                    // If other private channel exist, go to next private channel
                    if (privateChannels.size() > 1) {
                        int index = privateChannels.indexOf(channel);
                        // if current channel is last, go back to first
                        if (index == privateChannels.size() - 1) newChannel = privateChannels.get(0);
                        else newChannel = privateChannels.get(index + 1);
                    }
                }
            }

            ObservableList<Channel> publicChannels = ihmMain.getObservableUserListChannelPublic();
            for (Channel channel : publicChannels) {
                if (channel.getId().equals(idChannel)) { // Find channel in public user channels list
                    // If other public channel exist, go to next public channel
                    if (publicChannels.size() > 1) {
                        int index = publicChannels.indexOf(channel);
                        // if current channel is last, go back to first
                        if (index == publicChannels.size() - 1) newChannel = publicChannels.get(0);
                        else newChannel = publicChannels.get(index + 1);
                    }
                }
            }

            ihmMain.setIdCurrentChannel(newChannel);
        }

        // Remove channel from subscriptions and kicked, global and user channel lists
        Platform.runLater(() -> {
            ihmMain.getObservableListChannel().removeIf(channel -> channel.getId().equals(idChannel));
            ihmMain.getObservableUserSubscription().removeIf(sub -> sub.getChannel().getId().equals(idChannel));
            ihmMain.getKickedFromChannels().removeIf(channel -> channel.getId().equals(idChannel));
            ihmMain.stopScheduledKick(idChannel);
            ihmMain.setUserListChannel();
        });
    }
}
