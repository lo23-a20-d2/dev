package ihmMain.interfaces.impl;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import ihmMain.IhmMain;
import Interfaces.LocalDataToIhmMain;

import java.util.List;

/**
 * Implementation of ihmMain's interface for LocalData.
 * Overrides set of methods needed by LocalData.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class LocalDataToIhmMainImpl implements LocalDataToIhmMain {
    private IhmMain ihmMain;

    public LocalDataToIhmMainImpl(IhmMain main){
        this.ihmMain = main;
    }

    /**
     * Add all channels and subscriptions to observables
     * Set userListChannel
     *
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    @Override
    public void updateConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList) {
        subscriptionList.forEach(sub -> {
            if(!ihmMain.getObservableUserSubscription().contains(sub))
                ihmMain.getObservableUserSubscription().add(sub);
        });
        channelList.forEach(channel -> {
            if(!ihmMain.getObservableListChannel().contains(channel))
                ihmMain.getObservableListChannel().add(channel);
        });
    }
}
