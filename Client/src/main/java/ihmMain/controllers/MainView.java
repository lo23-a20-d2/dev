package ihmMain.controllers;

import ihmMain.IhmMain;
import ihmMain.controllers.authentication.Authentication;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;

import java.io.IOException;

/**
 * The Main View controller
 * Manages the home and authentication submodules
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class MainView {
    // FXML element
    @FXML private StackPane mainView;

    // Owner controller
    private IhmMain root;

    // Child controllers
    private Authentication auth;
    private Home home;

    /**
     * Sets the owner and root controller IhmMain
     *
     * @param ihmMain the controller
     */
    public void init(IhmMain ihmMain){
        this.root = ihmMain;
    }

    /**
     * Get owner and root controller IhmMain.
     *
     * @return the controller
     */
    public IhmMain getRoot(){ return this.root; }

    /**
     * Shows authentication submodule once the root StackPane container is loaded
     * Sets Authentication parent controller to this controller
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void initialize() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/authentication/authentication.fxml"));
            mainView.getChildren().add(loader.load());
            auth = loader.getController();
            auth.setMain(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows authentication submodule.
     * Clears root StackPane container, loads Authentication submodule and shows it in root container. Sets Authentication's parent controller to this.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void showAuthentication() {
        mainView.getChildren().remove(0);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/authentication/authentication.fxml"));
            mainView.getChildren().add(loader.load());
            Authentication authController = loader.getController();
            authController.setMain(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clears root StackPane container, loads Home submodule and shows it on root StackPane container
     * Sets Home parent controller to this controller
     * Tells Home to load IhmChannel and Sidebar (not done in constructor to wait for Home to have loaded and for its parent controller to be set)
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showHome() {
        mainView.getChildren().remove(0);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/home.fxml"));
            mainView.getChildren().add(loader.load());
            home = loader.getController();
            home.setMain(this);
            home.loadIhmChannel();
            home.loadSidebar();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets home.
     *
     * @return the home
     */
    public Home getHome() { return home; }

    /**
     * Gets auth.
     *
     * @return the auth
     */
    public Authentication getAuth() { return auth; }
}
