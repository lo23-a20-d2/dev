package ihmMain.controllers.popups;

import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import ihmMain.controllers.Home;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import static ihmMain.Constants.MAX_CHANEL_NAME_LENGTH;
import static ihmMain.Constants.MAX_CHANNEL_DESCRIPTION_LENGTH;

/**
 * Create channel.
 *
 * @author Saad Bennis <saad.bennis@etu.utc.fr>
 */

public class CreateChannelForm {
    @FXML private TextField channelName;
    @FXML private TextArea channelDescription;
    @FXML final ToggleGroup visibility = new ToggleGroup();
    @FXML private RadioButton vPublic;
    @FXML private RadioButton vPrivate;
    @FXML final ToggleGroup type = new ToggleGroup();
    @FXML private RadioButton tPartage;
    @FXML private RadioButton tProprietaire;

    @FXML private Label errorChanelDesc;
    @FXML private Label errorChanelName;

    private Home home;
    private Stage stage;
    
    /**
     * Creates choice boxes groups
     * Hide errors labels
     * Add a listener to chanel name's and description's input for validation
     *
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    public void initialize() {
        this.errorChanelDesc.setVisible(false);
        this.errorChanelName.setVisible(false);
        this.vPublic.setToggleGroup(this.visibility);
        this.vPrivate.setToggleGroup(this.visibility);
        this.vPrivate.fire();
        this.tPartage.setToggleGroup(this.type);
        this.tProprietaire.setToggleGroup(this.type);
        this.tProprietaire.fire();
        this.channelName.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty() && this.channelName.getLength() < MAX_CHANEL_NAME_LENGTH) {
                this.channelName.setStyle("-fx-border-width: 0px ; -fx-prompt-text-fill: red; ");
            } else if(newValue.isEmpty()){
                channelName.setPromptText("Veuillez saisir un nom de canal!");
                channelName.setStyle(" -fx-prompt-text-fill: red; -fx-text-box-border: red ; ");
            }

            if(this.channelName.getLength() > MAX_CHANEL_NAME_LENGTH){
                this.channelName.setStyle(" -fx-text-box-border: red ; ");
                this.errorChanelName.setStyle("-fx-text-fill: red");
                this.errorChanelName.setText("Le nom ne peut pas dépasser " + MAX_CHANEL_NAME_LENGTH + " caractères");
                this.errorChanelName.setVisible(true);
            } else {
                this.errorChanelName.setVisible(false);
            }
        });
        channelDescription.textProperty().addListener((observable, oldValue, newValue) -> {
            if(this.channelDescription.getLength() > MAX_CHANNEL_DESCRIPTION_LENGTH){
                channelDescription.setStyle(" -fx-text-box-border: red ; ");
                this.errorChanelDesc.setStyle("-fx-text-fill: red");
                this.errorChanelDesc.setText("La description ne peut pas dépasser " + MAX_CHANNEL_DESCRIPTION_LENGTH + " caractères");
                this.errorChanelDesc.setVisible(true);
            } else {
                this.channelDescription.setStyle("-fx-border-width: 0px");
                this.errorChanelDesc.setVisible(false);
            }
        });
    }
    
    /**
     * Create Channel if there is no validation problem
     * Changes style if there is any validation errors
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    public void onCreateChanel() {
        if (!this.verifyForm()) {
            this.channelName.setPromptText("Veuillez saisir un nom de canal !");
            this.channelName.setStyle(" -fx-prompt-text-fill: red; -fx-text-box-border: red ; ");
        } else {

            RadioButton selectedVisibility = (RadioButton)this.visibility.getSelectedToggle();
            RadioButton selectedType = (RadioButton)this.type.getSelectedToggle();
            boolean isPrivate = selectedVisibility.getText().equals("Prive");
            boolean isShared = selectedType.getText().equals("Partage");

            if (isShared){
                this.home.getMain().getRoot().getInterfaceToCom().createChannel(this.channelName.getText(), this.channelDescription.getText(), isPrivate, true);
                this.stage.close();
            }else {
                User user = this.home.getMain().getRoot().getConnectedUser();

                // Creating the channel in local database with localData
                OwnedChannel newChannel = this.home.getMain().getRoot().getInterfaceToData().createOwnedChannel(this.channelName.getText(), this.channelDescription.getText(), isPrivate, user);

                // Creating the user's subscription to the new channel and sending it to ihmChannel
                Subscription subscription = this.home.getMain().getRoot().getInterfaceToData().addSubscription(newChannel.getId(), user, Subscription.Role.ADMIN);
                this.home.getMain().getRoot().getInterfaceToCom().unsharedChannelCreated(newChannel, subscription);

                List<Subscription> subscriptionList = FXCollections.observableArrayList();
                subscriptionList.add(subscription);
                this.home.getMain().getRoot().addNewChannelToUser(newChannel, subscription);
                this.home.getMain().getRoot().getInterfaceToChannel().displayChannel(newChannel, new ArrayList<>(), subscriptionList);

                // Setting the current channel to the one just created and notifying localCom
                this.home.getMain().getRoot().setIdCurrentChannel(newChannel);

                this.stage.close();
            }
        }

    }
    /**
     * Verify if the chanel name's input is filled
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    private boolean verifyForm() {
        return !this.channelName.getText().isEmpty();
    }

    /**
     * Set the selected visibility toggle button
     * @param isPublic wanted visibility
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void setToggleSelectedButton(boolean isPublic){
        if (isPublic){
            this.vPublic.setSelected(true);
        } else {
            this.vPrivate.setSelected(true);
        }
    }

    /**
     * Initiates the controllers needed by this pop up
     * @param home the Home controller
     * @param dialogStage the pop up stage
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void init(Home home, Stage dialogStage){
        this.home = home;
        this.stage = dialogStage;
    }
}

