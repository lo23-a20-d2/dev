package ihmMain.controllers.popups;

import CommonClasses.User;
import ihmMain.Constants;
import ihmMain.controllers.Home;
import ihmMain.controllers.sidebar.ProfileSummary;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Edit profil controller
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class EditProfile {

    private File avatar;
    private BufferedImage croppedAvatarImage;

    @FXML private TextField firstNameLabel;
    @FXML private TextField lastNameLabel;
    @FXML private DatePicker birthdatePicker;
    @FXML private Button editButton;
    @FXML private Label errorLabel;
    @FXML private ImageView avatarView;
    @FXML private Label noFileLabel;

    private ProfileSummary profilSummary;
    private Home home;
    private User connectedUser;
    private Stage stage;

    /**
     * Initializes the view after it is loaded
     * Creates and applies circular mask on avatar view
     * Binds inputs content to disabled property on the validation button
     * Binds error and "no file imported" labels content to visbility properties
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void initialize() {
        // Creates and applies circular mask on default avatar image
        final Circle mask = new Circle(Constants.AVATAR_WIDTH /2, Constants.AVATAR_WIDTH/2, Constants.AVATAR_WIDTH/2);
        avatarView.setClip(mask);
        // Binds visibility and layout management of the label next to the avatar import button
        // Useful to hide the label in one line in chooseAvatarFile()
        noFileLabel.managedProperty().bind(noFileLabel.visibleProperty());

        // Binds visible property of error label to custom boolean detectors for empty text
        // Show error label only when filled
        BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorLabel.getText().length()>0, errorLabel.textProperty());
        errorLabel.visibleProperty().bind(errorLabelFilled);

        // Binds disabled property of validate button to custom boolean detectors for empty inputs
        // Enables validate button only when at least one of the inputs is filled
        BooleanBinding firstNameLabelFilled = Bindings.createBooleanBinding(() -> firstNameLabel.getLength()>0, firstNameLabel.textProperty());
        BooleanBinding lastNameLabelFilled = Bindings.createBooleanBinding(() -> lastNameLabel.getLength()>0, lastNameLabel.textProperty());
        BooleanBinding birthdayFilled = Bindings.createBooleanBinding(() -> birthdatePicker.getValue() != null, birthdatePicker.valueProperty());
        BooleanBinding avatarImported = Bindings.createBooleanBinding(() -> avatar != null, avatarView.imageProperty());
        editButton.disableProperty().bind(firstNameLabelFilled.not().and(lastNameLabelFilled.not()).and(birthdayFilled.not()).and(avatarImported.not()));
    }

    /**
     * Initiates controllers needed by this pop up
     *
     * @param profilSummary the profilSummary controller
     * @param home the home controller
     * @param dialogStage the pop up stage
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void initControllers(ProfileSummary profilSummary, Home home, Stage dialogStage){
        this.profilSummary = profilSummary;
        this.home = home;
        this.connectedUser = this.home.getMain().getRoot().getConnectedUser();
        this.stage = dialogStage;
    }

    /**
     * Initiates the form values with the user's
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void initFieldValues(){
        if (this.connectedUser.getFirstName() != null)
            this.firstNameLabel.textProperty().setValue(this.connectedUser.getFirstName());
        if (this.connectedUser.getLastName() != null)
            this.lastNameLabel.textProperty().setValue(this.connectedUser.getLastName());
        if (this.connectedUser.getBirthDate() != null)
            this.birthdatePicker.setValue(this.connectedUser.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        if (this.connectedUser.getAvatar() != null){
            avatarView.setImage(SwingFXUtils.toFXImage(this.connectedUser.getAvatar(), null));
            noFileLabel.setVisible(false);
        }
    }

    /**
     * Crop the avatar provided by the user to a centered 100*100px square
     *
     * @throws IOException if there was an error reading the image
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @return
     */
    public BufferedImage cropImage(File avatar) throws IOException {
        // Read and buffer image from file to enable manipulation
        BufferedImage bufferedImage = ImageIO.read(avatar);

        // Scale the image so that the smallest side has avatar size
        // To do so, create a new buffered image with the new calculated dimensions
        double width = bufferedImage.getWidth();
        double height = bufferedImage.getHeight();
        double minSide = Math.min(width, height);
        int newWidth, newHeight, cropX, cropY;
        if(minSide==width) { // Smallest side is width
            newWidth = Constants.AVATAR_WIDTH;
            // Height is calculated so that the new image keeps the same aspect ratio as the original
            newHeight = (int) Math.round(height * Constants.AVATAR_WIDTH / width);
            cropX = 0;
            // Top of image will be offset by cropping on vertical axis to center the image
            cropY = (newHeight-Constants.AVATAR_WIDTH)/2;
        } else { // Smallest side is height
            newWidth = (int) Math.round(width * Constants.AVATAR_WIDTH / height);
            newHeight = Constants.AVATAR_WIDTH;
            // Left of image will be offset by cropping on horizontal axis to center the image
            cropX = (newWidth-Constants.AVATAR_WIDTH)/2;
            cropY = 0;
        }
        BufferedImage scaledBufferedImage = new BufferedImage(newWidth, newHeight, bufferedImage.getType());

        // Paint the image on the new blank buffered image
        Graphics2D g = scaledBufferedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newWidth, newHeight, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
        g.dispose();

        // Crop the image to a centered square thanks to crop values calculated previously
        return scaledBufferedImage.getSubimage(cropX, cropY,Constants.AVATAR_WIDTH,Constants.AVATAR_WIDTH);
    }

    /**
     * Choose avatar file
     * Triggered when the user clicks on the 'Importer avatar' button
     * Opens dialog to choose the avatar file then reads it to show a preview before validating
     * Checks if image is too large before accepting it (size limit is one of the static class attributes)
     *
     * @param event event triggered when the "Importer avatar" button has been clicked, used to link the FileChooser to its parent, the main stage
     * @throws IOException if there was an error reading the image
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void chooseAvatarFile(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();

        // Only enable jpg and png images to be imported
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        chooser.getExtensionFilters().add(imageFilter);

        // Linking the main stage to be the parent of the FileChooser stage
        Node source = (Node) event.getSource();
        Window ownerStage = source.getScene().getWindow();
        avatar = chooser.showOpenDialog(ownerStage);

        // Processing the user input
        // (only read after the FileChooser stage is closed)
        if(avatar != null) {
            // Check if image is too large
            if (avatar.length() > Constants.MAX_AVATAR_SIZE) {
                errorLabel.setText("L'image est trop lourde, importez une image de moins de 2 Mb");
            } else {
                // Empty the error label if it was set by an "image too large" error
                if(errorLabel.getText().equals("L'image est trop lourde, importez une image de moins de 2 Mb"))
                    errorLabel.setText("");

                // Crop the image
                croppedAvatarImage = cropImage(this.avatar);

                // Create a JavaFX Image that ImageView can render from the cropped Java buffered image
                Image croppedImage = SwingFXUtils.toFXImage(croppedAvatarImage, null);

                // Show preview of avatar and hide 'no file imported' label
                avatarView.setImage(croppedImage);
                noFileLabel.setVisible(false);
            }
        }
    }

    /**
     * Edit the user
     * Triggered when the user clicks on the 'Modifier' button
     * Checks if the first and last name are too long for database storage
     * Checks if birthday is set after the date of input
     * If valid , updates the user
     * If invalid, displays appropriate error
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void handleEditAction(){
        // First and last names length checks
        if(lastNameLabel.getLength()> Constants.MAX_NAME_LENGTH)
            errorLabel.setText("Le nom entré est trop long");
        else if(firstNameLabel.getLength()>Constants.MAX_NAME_LENGTH)
            errorLabel.setText("Le prénom entré est trop long");

        else if(birthdatePicker.getValue() != null && birthdatePicker.getValue().isAfter(new Date().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()))
            errorLabel.setText("Votre anniversaire ne peut pas être après la date d'aujourd'hui");

        else {
            // Empty error label if user entered invalid data previously
            errorLabel.setText("");

            // Conversion from LocalDate to Date for use by the User Common Class
            LocalDate birthdayValue = birthdatePicker.getValue();
            Date birthdayConverted = null;
            if(birthdayValue != null) {
                birthdayConverted = java.util.Date.from(birthdayValue.atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
            }

            User updatedUser = this.home.getMain().getRoot().getInterfaceToData().updateUser(connectedUser.getId(), firstNameLabel.getText(), lastNameLabel.getText(), birthdayConverted, croppedAvatarImage);
            if (updatedUser != null || updatedUser.getId().equals("")){
                this.home.getMain().getRoot().getInterfaceToCom().sendUpdateUser(updatedUser);
                this.home.getMain().getRoot().setConnectedUser(updatedUser);
                this.home.getSidebar().getProfileSummary().updateInfo();
                this.stage.close();
            }else {
                errorLabel.setText("Une erreur est survenue");
            }
        }
    }
}
