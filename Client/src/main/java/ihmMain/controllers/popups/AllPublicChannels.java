package ihmMain.controllers.popups;

import CommonClasses.Channel;
import ihmMain.IhmMain;
import ihmMain.controllers.common.ChannelCell;
import ihmMain.controllers.common.NoSelectionModel;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * allPublicChannels controller
 * Manages the popup showing all the public channels a user can join
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
*/
public class AllPublicChannels  {
    //FXML element
    @FXML private ListView<Channel> allPublicChannelsListView;

    /**
     * Initialize controller for allPublicChannelPopup
     * Creates an FilteredList containing all public channels the user hasn't joined yet
     * The list will automatically evolve whenever a new publicChannel is created/deleted/joined
     * Then calls (through setCellFactory) ChannelCell responsible for rendering each public channel in the popup
     *
     * @param root is the parent window
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     * @see ChannelCell
     */
    public void init(IhmMain root) {
        allPublicChannelsListView.setSelectionModel(new NoSelectionModel<>());
        FilteredList<Channel> publicNotJoinedChannels = new FilteredList<>(root.getObservableListChannel(), channel -> !channel.isPrivate() && root.getObservableUserListChannelPublic().stream().noneMatch(pc -> (pc.getId().equals(channel.getId()))));
        root.getObservableUserListChannelPublic().addListener((ListChangeListener<Channel>) c -> {
            while (c.next())
                if (c.wasAdded())
                    Platform.runLater(() -> publicNotJoinedChannels.setPredicate(channel -> !channel.isPrivate() && root.getObservableUserListChannelPublic().stream().noneMatch(pc -> (pc.getId().equals(channel.getId())))));
        });
        allPublicChannelsListView.setItems(publicNotJoinedChannels);
        allPublicChannelsListView.setCellFactory(publicChannelsListView -> new ChannelCell(root));
    }
}
