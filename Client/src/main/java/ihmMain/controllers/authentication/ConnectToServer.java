package ihmMain.controllers.authentication;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;

import java.io.IOException;

/**
 * Controller for the IP Address form
 * Reads and verifies the format of the IP Address before connecting to server
 *
 * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
 */
public class ConnectToServer {
    // FXML elements
    @FXML private TextField userInput;
    @FXML private Label errorLabel;
    @FXML private Button connectButton;

    // User input conditions
    private static final int minInputLength = 7; // At least one number in each block 1.1.1.1

    // Owner controller
    private Authentication auth;

    /**
     * Initialize
     *
     * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
     */
    public void initialize() {
        // Binds disabled property of connect button to custom boolean detectors for empty input
        // Enables connect button only when the input is filled
        BooleanBinding userInputFilled = Bindings.createBooleanBinding(() -> userInput.getText().length() > 0, userInput.textProperty());
        connectButton.disableProperty().bind(userInputFilled.not());
        // Binds visible property of error label to custom boolean detectors for empty text
        // Shows error label only when filled
        BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorLabel.getText().length() > 0, errorLabel.textProperty());
        errorLabel.visibleProperty().bind(errorLabelFilled);
    }

    /**
     * Checks if the format of the IP Address is valid
     * Triggered when the user clicks on the 'Se connecter' button
     * If valid, the connection to the server is established and the chat interface is displayed
     * If not, an error message is displayed
     *
     * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
     */
    public void connect() {
        final boolean valid = checkIfValid(userInput.getText());
        if (valid) {
            try {
                this.auth.getMain().getRoot().getInterfaceToCom().sendAccount(this.auth.getMain().getRoot().getConnectedUser(), userInput.getText());
            } catch (IOException e) {
                errorLabel.setText(e.getMessage());
            }
        } else{
            errorLabel.setText("Le format de l'adresse IP saisie n'est pas valide");
        }
    }

    /**
     * Checks if user input is valid
     * Compares the format of the IP Address to a pattern x.x.x.x
     *
     * @param text user input (IP Adress)
     * @return true if user input matchs pattern
     * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
     */
    public static boolean checkIfValid(String text) {
        String pattern = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
        return text.matches(pattern);
    }

    /**
     * Get min input length
     *
     * @return the length
     */
    public static int getMinInputLength(){
        return minInputLength;
    }

    /**
     * Sets owner controller.
     *
     * @param controller the controller
     */
    public void setAuthentification(Authentication controller) {
        auth=controller;
    }

    /**
     * Gets error label.
     *
     * @return the error label
     */
    public Label getErrorLabel() { return errorLabel; }
}
