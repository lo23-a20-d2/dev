package ihmMain.controllers.authentication;

import CommonClasses.User;
import ihmMain.Constants;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Controller for the complete registration form
 * Reads and validates additional registration data from user then asks Local Data Interface to create new user and sends result back to authentification controller
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 * @see Authentication
 * @see Interfaces.IhmMainToLocalData
 */
public class CompleteRegistrationForm {
    // Avatar data
    private File avatar;
    private BufferedImage croppedAvatarImage;

    // RegistrationForm data
    private String login;
    private String pwd;

    // FXML elements
    @FXML private TextField firstNameLabel;
    @FXML private TextField lastNameLabel;
    @FXML private DatePicker birthday;
    @FXML private Label noFileLabel;
    @FXML private Label errorLabel;
    @FXML private ImageView avatarView;
    @FXML private Button validateButton;

    // Owner controller
    private Authentication auth;

    /**
     * Initializes the view after it is loaded
     * Creates and applies circular mask on avatar view
     * Binds inputs content to disabled property on the validation button
     * Binds error and "no file imported" labels content to visbility properties
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void initialize(){
        // Creates and applies circular mask on default avatar image
        final Circle mask = new Circle(Constants.AVATAR_WIDTH /2, Constants.AVATAR_WIDTH/2, Constants.AVATAR_WIDTH/2);
        avatarView.setClip(mask);
        // Binds visibility and layout management of the label next to the avatar import button
        // Useful to hide the label in one line in chooseAvatarFile()
        noFileLabel.managedProperty().bind(noFileLabel.visibleProperty());

        // Binds visible property of error label to custom boolean detectors for empty text
        // Show error label only when filled
        BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorLabel.getText().length()>0, errorLabel.textProperty());
        errorLabel.visibleProperty().bind(errorLabelFilled);

        // Binds disabled property of validate button to custom boolean detectors for empty inputs
        // Enables validate button only when at least one of the inputs is filled
        BooleanBinding firstNameLabelFilled = Bindings.createBooleanBinding(() -> firstNameLabel.getLength()>0, firstNameLabel.textProperty());
        BooleanBinding lastNameLabelFilled = Bindings.createBooleanBinding(() -> lastNameLabel.getLength()>0, lastNameLabel.textProperty());
        BooleanBinding birthdayFilled = Bindings.createBooleanBinding(() -> birthday.getValue() != null, birthday.valueProperty());
        BooleanBinding avatarImported = Bindings.createBooleanBinding(() -> avatar != null, avatarView.imageProperty());
        validateButton.disableProperty().bind(firstNameLabelFilled.not().and(lastNameLabelFilled.not()).and(birthdayFilled.not()).and(avatarImported.not()));
    }

    /**
     * Choose avatar file
     * Triggered when the user clicks on the 'Importer avatar' button
     * Opens dialog to choose the avatar file then reads it to show a preview before validating
     * Checks if image is too large before accepting it (size limit is one of the static class attributes)
     *
     * @param event event triggered when the "Importer avatar" button has been clicked, used to link the FileChooser to its parent, the main stage
     * @throws IOException if there was an error reading the image
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void chooseAvatarFile(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();

        // Only enable jpg and png images to be imported
        FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        chooser.getExtensionFilters().add(imageFilter);

        // Linking the main stage to be the parent of the FileChooser stage
        Node source = (Node) event.getSource();
        Window ownerStage = source.getScene().getWindow();
        avatar = chooser.showOpenDialog(ownerStage);

        // Processing the user input
        // (only read after the FileChooser stage is closed)
        if(avatar != null) {
            // Check if image is too large
            if (avatar.length() > Constants.MAX_AVATAR_SIZE) {
                errorLabel.setText("L'image est trop lourde, importez une image de moins de 2 Mb");
            } else {
                // Empty the error label if it was set by an "image too large" error
                if(errorLabel.getText().equals("L'image est trop lourde, importez une image de moins de 2 Mb"))
                    errorLabel.setText("");

                // Crop the image
                croppedAvatarImage = cropImage(this.avatar);

                // Create a JavaFX Image that ImageView can render from the cropped Java buffered image
                Image croppedImage = SwingFXUtils.toFXImage(croppedAvatarImage, null);

                // Show preview of avatar and hide 'no file imported' label
                avatarView.setImage(croppedImage);
                noFileLabel.setVisible(false);
            }
        }
    }

    /**
     * Crop the avatar provided by the user to a centered 100*100px square
     *
     * @throws IOException if there was an error reading the image
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @return
     */
    public BufferedImage cropImage(File avatar) throws IOException {
        // Read and buffer image from file to enable manipulation
        BufferedImage bufferedImage = ImageIO.read(avatar);

        // Scale the image so that the smallest side has avatar size
        // To do so, create a new buffered image with the new calculated dimensions
        double width = bufferedImage.getWidth();
        double height = bufferedImage.getHeight();
        double minSide = Math.min(width, height);
        int newWidth, newHeight, cropX, cropY;
        if(minSide==width) { // Smallest side is width
            newWidth = Constants.AVATAR_WIDTH;
            // Height is calculated so that the new image keeps the same aspect ratio as the original
            newHeight = (int) Math.round(height * Constants.AVATAR_WIDTH / width);
            cropX = 0;
            // Top of image will be offset by cropping on vertical axis to center the image
            cropY = (newHeight-Constants.AVATAR_WIDTH)/2;
        } else { // Smallest side is height
            newWidth = (int) Math.round(width * Constants.AVATAR_WIDTH / height);
            newHeight = Constants.AVATAR_WIDTH;
            // Left of image will be offset by cropping on horizontal axis to center the image
            cropX = (newWidth-Constants.AVATAR_WIDTH)/2;
            cropY = 0;
        }
        BufferedImage scaledBufferedImage = new BufferedImage(newWidth, newHeight, bufferedImage.getType());

        // Paint the image on the new blank buffered image
        Graphics2D g = scaledBufferedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newWidth, newHeight, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
        g.dispose();

        // Crop the image to a centered square thanks to crop values calculated previously
        return scaledBufferedImage.getSubimage(cropX, cropY,Constants.AVATAR_WIDTH,Constants.AVATAR_WIDTH);
    }

    /**
     * Validate
     * Triggered when the user clicks on the 'Valider' button
     * Checks if the first and last name are too long for database storage
     * Checks if birthday is set after the date of input
     * If valid, sends the data back to the authentification controller
     * If invalid, displays appropriate error
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void validate() {
        // First and last names length checks
        if(lastNameLabel.getLength()>Constants.MAX_NAME_LENGTH)
            errorLabel.setText("Le nom entré est trop long");
        else if(firstNameLabel.getLength()>Constants.MAX_NAME_LENGTH)
            errorLabel.setText("Le prénom entré est trop long");

        // Birthday date before today check
        else if(birthday.getValue() != null && birthday.getValue().isAfter(new Date().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()))
            errorLabel.setText("Votre anniversaire ne peut pas être après la date d'aujourd'hui");
        else {
            // Empty error label if user entered invalid data previously
            errorLabel.setText("");

            // Conversion from LocalDate to Date for use by the User Common Class
            LocalDate birthdayValue = birthday.getValue();
            Date birthdayConverted = null;
            if(birthdayValue != null) {
                birthdayConverted = java.util.Date.from(birthdayValue.atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
            }

            // Call to LocalData interface to create new user with complete data and
            User connectedUser = this.auth.getMain().getRoot().getInterfaceToData().saveNewAccount(login, pwd, firstNameLabel.getText(), lastNameLabel.getText(), birthdayConverted,  croppedAvatarImage);
            if (connectedUser != null){
                // Sets new user to be the connected user
                this.auth.getMain().getRoot().setConnectedUser(connectedUser);
                // Asks owner controller to show the server address form
                auth.showServerAddressForm();
            } else {
                errorLabel.setText("Erreur lors de l'inscription");
            }

        }
    }

    /**
     * Skip the step
     * Asks Local Data Interface for user creation with all the information gathered
     * Sets new user to be the connected user
     * Asks owner controller to show the server address form
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void skip() {
        User connectedUser = this.auth.getMain().getRoot().getInterfaceToData().saveNewAccount(login, pwd, "", "", null,  croppedAvatarImage);
        if (connectedUser != null){
            // Sets new user to be the connected user
            this.auth.getMain().getRoot().setConnectedUser(connectedUser);
            // Asks owner controller to show the server address form
            auth.showServerAddressForm();
        } else {
            errorLabel.setText("Erreur lors de l'inscription");
        }
    }

    /**
     * Sets initial data gathered by Registration form
     *
     * @param l the login
     * @param p the password
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void setInitialData(String l, String p) {
        login=l;
        pwd=p;
    }

    /**
     * Sets owner authentification controller
     *
     * @param controller the controller
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void setAuthentification(Authentication controller) {
        auth=controller;
    }
}
