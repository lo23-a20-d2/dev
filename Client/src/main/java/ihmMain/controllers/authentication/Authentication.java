package ihmMain.controllers.authentication;

import ihmMain.controllers.MainView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.IOException;

/**
 * Manages authentification steps : connection, minimal and complete registration and server ip
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class Authentication {
    // FXML elements
    @FXML private GridPane container;

    // Child nodes
    private Node connectionForm;
    private Node registrationForm;

    // Child controllers
    private ConnectToServer connectToServer;

    // Owner controller
    private MainView main;

    /**
     * Show connection step by default after main GridPane is setup
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void initialize() {
        showConnection();
    }

    /**
     * Load and show connection form
     * Set ConnectionForm parent controller to this controller
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showConnection() {
        container.getChildren().remove(registrationForm);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../views/authentication/connectionForm.fxml"));
            connectionForm = loader.load();
            // Added to second column for split layout between form and image
            container.add(connectionForm, 1, 0);
            ConnectionForm connectionFormController = loader.getController();
            connectionFormController.setAuthentification(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load and show registration form
     * Set RegistrationForm parent controller to this controller
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showRegistration() {
        container.getChildren().remove(connectionForm);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../views/authentication/registrationForm.fxml"));
            registrationForm = loader.load();
            // Added to second column for split layout between form and image
            container.add(registrationForm, 1, 0);
            RegistrationForm registrationFormController = loader.getController();
            registrationFormController.setAuthentification(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show complete registration.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @param login the login
     * @param pwd   the pwd
     */
    public void showCompleteRegistration(String login, String pwd) {
        container.getChildren().clear();
        container.getColumnConstraints().clear();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../views/authentication/completeRegistrationForm.fxml"));
            container.add(loader.load(), 0, 0);
            CompleteRegistrationForm completeRegistrationFormController = loader.getController();
            completeRegistrationFormController.setInitialData(login, pwd);
            completeRegistrationFormController.setAuthentification(this);
            ColumnConstraints fillWidth = new ColumnConstraints();
            fillWidth.setHgrow(Priority.ALWAYS);
            container.getColumnConstraints().add(fillWidth);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show server address form.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showServerAddressForm(){
        container.getChildren().clear();
        container.getColumnConstraints().clear();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../views/authentication/connectToServer.fxml"));
            container.add(loader.load(), 0, 0);
            connectToServer = loader.getController();
            connectToServer.setAuthentification(this);
            ColumnConstraints fillWidth = new ColumnConstraints();
            fillWidth.setHgrow(Priority.ALWAYS);
            container.getColumnConstraints().add(fillWidth);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Asks parent controller to show homepage
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showHome() {
        main.showHome();
    }

    /**
     * Set owner controller MainView
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @param mainViewController the main view controller
     */
    public void setMain(MainView mainViewController) { main= mainViewController; }

    /**
     * Get owner controller MainView
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @return the main view controller
     */
    public MainView getMain(){ return this.main; }

    /**
     * Gets connect to server.
     *
     * @return the connect to server
     */
    public ConnectToServer getConnectToServer() { return connectToServer; }
}
