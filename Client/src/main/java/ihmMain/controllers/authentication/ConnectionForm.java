package ihmMain.controllers.authentication;

import CommonClasses.User;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import static ihmMain.Constants.*;

/**
 * Controller for connection form
 * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
 * The user fill the login and password input, and click on the button connect
 * If the user is not registrated yet, he will be redirected to Registration form
 */
public class ConnectionForm {
    // FXML Elements
    @FXML private TextField login;
    @FXML private PasswordField password;
    @FXML private Button connect;
    @FXML private Label errorLabel;

    // Owner controller
    private Authentication auth;

    /**
     * Initialize
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     *  The connect button is disable until both of the input are filled
     */
    public void initialize(){
        BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorLabel.getText().length()>0, errorLabel.textProperty());
        errorLabel.visibleProperty().bind(errorLabelFilled);

        BooleanBinding loginFilled = Bindings.createBooleanBinding(() -> login.getLength()>0, login.textProperty());
        BooleanBinding passwordFilled = Bindings.createBooleanBinding(() -> password.getLength()>0, password.textProperty());
        connect.disableProperty().bind(loginFilled.not().or(passwordFilled.not()));
    }

    /**
     * onConnect
     *  @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Triggered when the user clicks on the 'Se connecter' button
     * Checks if the login and password are too long
     * If not there is an error message
     * It prints login and password on the console
     */
    public void onConnect() {
        if(login.getText().isEmpty() && password.getText().isEmpty()) {
            errorLabel.setText("Veuillez renseigner votre login et votre mot de passe");
        } else {
            if(login.getLength()>MAX_LOGIN_LENGTH)
                errorLabel.setText("Le login est trop long");
            else if(password.getLength()>MAX_PSW_LENGTH)
                errorLabel.setText("Le mot de passe est trop long");
            else if(password.getLength()<MIN_PSW_LENGTH)
                errorLabel.setText("Le mot de passe est trop court, saisissez un mot de passe avec plus de 6 caractères");
            else {
                errorLabel.setText("");

                User connectedUser = this.auth.getMain().getRoot().getInterfaceToData().authenticate(login.getText(), password.getText());
                if (!connectedUser.getId().equals("")){
                    this.auth.getMain().getRoot().setConnectedUser(connectedUser);
                    auth.showServerAddressForm();
                } else {
                    errorLabel.setText("Login ou mot de passe incorrect, avez-vous bien un compte un chez nous ?");
                }
            }
        }
    }

    /**
     * onRegistration
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Send the user to registration form if he's not registered yet
     */
    public void onRegistration() {
        auth.showRegistration();
    }

    /**
     * setAuthentification
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Sets owner authentification controller
     */
    public void setAuthentification(Authentication controller) {
        auth=controller;
    }
}

