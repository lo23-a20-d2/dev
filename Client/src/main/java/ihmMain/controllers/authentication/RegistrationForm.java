package ihmMain.controllers.authentication;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;

import static ihmMain.Constants.*;


/**
 * Controller for the registration form
 * Collect and validates compulsory information for user's registration (login,password)
 *
 * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
 * @see Authentication
 */
public class RegistrationForm {
	// Parent controller
	private Authentication auth;
	
	//FXML elements for Registration window
	@FXML private TextField login;
	@FXML private PasswordField psw;
	@FXML private PasswordField pswConfirmation;
	@FXML private Label errorDisplay;
	@FXML private Button registrationButton;

	/**
	 * Initializes the form when the view is loaded
	 * Creates an error field to display error message, invisible if no error
	 * Binds input fields to allow click on Registration button only when all inputs are filled
	 *
	 * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
	 */
	public void initialize() {
		// Binds visible property of error label to custom boolean detectors for empty text
		// Show error label only when filled
		BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorDisplay.getText().length()>0, errorDisplay.textProperty());
		errorDisplay.visibleProperty().bind(errorLabelFilled);

		BooleanBinding loginLabelFilled = Bindings.createBooleanBinding(() -> login.getLength()>0, login.textProperty());
		BooleanBinding pswLabelFilled = Bindings.createBooleanBinding(() -> psw.getLength()>0, psw.textProperty());
		BooleanBinding pswConfirmationLabelFilled = Bindings.createBooleanBinding(() -> pswConfirmation.getLength()>0, pswConfirmation.textProperty());
	    registrationButton.disableProperty().bind(loginLabelFilled.not().or(pswLabelFilled.not()).or(pswConfirmationLabelFilled.not()));
	}


	/**
	 * Sets owner controller authentification
	 *
	 * @param controller the controller
	 */
	public void setAuthentification(Authentication controller){
		auth=controller;
	}


	/**
	 * Validation of the registration inputs (login and password)
	 * Triggered when a user clicks on registrationButton
	 * Check password and login length and that password confirmation is identical with password
	 * If an error occurs display error message
	 * Else sends the validated data to the next page:complete registration form
	 *
	 * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
	 */
	public void validate() {
		//Get data from the form
		String loginText = login.getText();
		String pswText = psw.getText();

		//Check login and password length
		if(login.getLength()> MAX_LOGIN_LENGTH)
			errorDisplay.setText("Le login est trop long");
		else if(psw.getLength()> MAX_PSW_LENGTH || psw.getLength()< MIN_PSW_LENGTH)
			errorDisplay.setText("Le mot de passe doit être entre " + MIN_PSW_LENGTH +" et " + MAX_PSW_LENGTH + " caractères");

		//Check whether password and password confirmation are identical
		else if(! psw.getText().equals(pswConfirmation.getText()))
			errorDisplay.setText("Le mot de passe est différent de la confirmation");

		else {
			//TODO : valider données par controller
			
			// If everything is okay
			auth.showCompleteRegistration(loginText, pswText);
		}

		//TODO :Check password format ?->For now no limitations were defined
			
		//TODO : Check login format (no number, special caracter,....)?->For now no limitations were defined
		
		//TODO : Is there a function to escape html or inserted code before sending it to the next page or the database?
		
	}

	/**
	 * Redirect user to login pages
	 * Triggered when user clicks on "Se connecter" hyperlink
	 *
	 * @author Anis Mikou <anis.mikou@etu.utc.fr>
	 */
	public void redirect() {
		auth.showConnection();
	}
	
}
