package ihmMain.controllers.sidebar;

import CommonClasses.Channel;
import ihmMain.controllers.Home;
import ihmMain.controllers.sidebar.defaultSidebar.ListChannel;
import ihmMain.controllers.sidebar.defaultSidebar.Search;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.List;

/**
 * Main sidebar controller
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class MainSidebar {
    // FXML elements
    @FXML private VBox container;
    @FXML private VBox innerContainer;

    // Owner controller
    private Home home;

    // Child controllers
    private ListChannel listChannel;
    private ProfileSummary profileSummary;

    /**
     * Load profile summary and set its parent controller to this controller
     * Also pass on user data and update display
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void loadProfileSummary() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/profileSummary.fxml"));
            Node childNode = loader.load();
            container.getChildren().add(childNode);
            childNode.toBack();
            profileSummary = loader.getController();
            profileSummary.setSidebar(this);
            profileSummary.updateInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load searchbar and set its parent controller to this controller
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void loadSearchbar() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/defaultSidebar/search.fxml"));
            innerContainer.getChildren().add(loader.load());
            Search search = loader.getController();
            search.setSidebar(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load sidebar and set its parent controller to this controller
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void loadChannelLists() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/defaultSidebar/listChannel.fxml"));
            innerContainer.getChildren().add(loader.load());
            listChannel = loader.getController();
            listChannel.setSidebar(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clear sidebar container and show default sidebar
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showDefaultSidebar() {
        innerContainer.getChildren().clear();
        loadSearchbar();
        loadChannelLists();
    }

    /**
     * Hide default sidebar and show search results
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void showResults(List<Channel> channelList) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/searchResults.fxml"));
            innerContainer.getChildren().clear();
            innerContainer.getChildren().add(loader.load());
            SearchResults searchResults = loader.getController();
            searchResults.setSidebar(this);
            searchResults.init(this.home.getMain().getRoot(), channelList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get owner controller Home.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @return the controller
     */
    public Home getHome(){ return this.home; }

    /**
     * Sets owner controller Home.
     * Also updates user info
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @param home the controller
     */
    public void setHome(Home home) { this.home = home; }

    /**
     * Gets list channel.
     *
     * @return the list channel
     */
    public ListChannel getListChannel() { return listChannel; }

    /**
     * Gets profile summary.
     *
     * @return the profile summary
     */
    public ProfileSummary getProfileSummary() { return profileSummary; }
}
