package ihmMain.controllers.sidebar.defaultSidebar;

import CommonClasses.Channel;
import ihmMain.controllers.common.DisableableCell;
import ihmMain.controllers.popups.CreateChannelForm;
import ihmMain.controllers.popups.AllPublicChannels;
import ihmMain.controllers.sidebar.MainSidebar;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * List channel.
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 * @author Saad Bennis <saad.bennis@etu.utc.fr>
 */
public class ListChannel {
    // FXML elements
    @FXML private ListView<Channel> publicChannelsList;
    @FXML private ListView<Channel> privateChannelsList;

    // Owner controller
    private MainSidebar sidebar;

    /**
     * Sets sidebar parent controller and observable channel lists to ListViews
     *
     * @param mainSidebar the main sidebar
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    public void setSidebar(MainSidebar mainSidebar) {
        sidebar=mainSidebar;

        // Custom ListCell that disables the access to the channel when kicked
        publicChannelsList.setCellFactory(callback -> new DisableableCell(sidebar));
        privateChannelsList.setCellFactory(callback -> new DisableableCell(sidebar));

        // Link ListView to IhmMain's observable channel lists
        publicChannelsList.setItems(this.sidebar.getHome().getMain().getRoot().getObservableUserListChannelPublic());
        privateChannelsList.setItems(this.sidebar.getHome().getMain().getRoot().getObservableUserListChannelPv());
    }

    /**
     * Display the selected public channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void displayPublicChannel(){
        Channel selectedChannel = publicChannelsList.getSelectionModel().getSelectedItem();

        if (selectedChannel != null && !selectedChannel.getId().equals(this.sidebar.getHome().getMain().getRoot().getIdCurrentChannel()))
            this.sidebar.getHome().getMain().getRoot().setIdCurrentChannel(selectedChannel);
    }

    /**
     * Display the selected private  channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void displayPrivateChannel(){
        Channel selectedChannel = privateChannelsList.getSelectionModel().getSelectedItem();

        if (selectedChannel != null && !selectedChannel.getId().equals(this.sidebar.getHome().getMain().getRoot().getIdCurrentChannel()))
            this.sidebar.getHome().getMain().getRoot().setIdCurrentChannel(selectedChannel);
    }

    /**
     * Show all public channels in a pop up
     * Opens the popup with all public channels when 'Tous' is cliked in main window
     * Sets all parameter to correctly show the popup, give it the right dimensions,..
     *
     * @throws IOException if the popup cant be loaded
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void showAllPublicChannels() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/popups/allPublicChannels.fxml"));

            Scene allPublicChannelsScene = new Scene(loader.load());
            Stage allPublicChannelsStage = new Stage();
            allPublicChannelsStage.initModality(Modality.WINDOW_MODAL);
            allPublicChannelsStage.initOwner(sidebar.getHome().getContainer().getScene().getWindow());

            AllPublicChannels publicChannelsController = loader.getController();
            publicChannelsController.init(this.sidebar.getHome().getMain().getRoot());

            allPublicChannelsStage.setScene(allPublicChannelsScene);
            allPublicChannelsStage.setTitle("Tous les channels");
            allPublicChannelsStage.setWidth(600);
            allPublicChannelsStage.setHeight(600);
            allPublicChannelsStage.setResizable(false);

            allPublicChannelsStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open create channel popup.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void openCreateChannelPopup(Event event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/popups/createChannelForm.fxml"));

            Scene createChannelScene = new Scene(loader.load());
            Stage createChannelStage = new Stage();
            createChannelStage.initModality(Modality.WINDOW_MODAL);
            createChannelStage.initOwner(sidebar.getHome().getContainer().getScene().getWindow());

            CreateChannelForm controller = loader.getController();
            controller.init(this.sidebar.getHome(), createChannelStage);
            controller.setToggleSelectedButton(((Hyperlink) event.getSource()).getId().equals("createPublic"));

            createChannelStage.setScene(createChannelScene);
            createChannelStage.setTitle("Créer un nouveau channel");
            createChannelStage.setWidth(600);
            createChannelStage.setHeight(600);
            createChannelStage.setResizable(false);


            createChannelStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ListView<Channel> getPrivateChannelsList() {
        return privateChannelsList;
    }

    public ListView<Channel> getPublicChannelsList() {
        return publicChannelsList;
    }
}
