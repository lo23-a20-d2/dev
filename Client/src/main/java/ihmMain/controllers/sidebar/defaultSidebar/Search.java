package ihmMain.controllers.sidebar.defaultSidebar;

import CommonClasses.Category;
import ihmMain.controllers.sidebar.MainSidebar;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import static ihmMain.Constants.MIN_SEARCH_INPUT_LENGTH;

/**
 * Controller for Search form
 * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
 *
 */
public class Search {
    @FXML public Button searchButton;
    @FXML public ComboBox<Category> searchOption;
    @FXML public TextField searchInput;
    @FXML private Label errorLabel;

    // Parent controller
    MainSidebar sidebar;

    /**
     * Initialize
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Fill the combo Box with "Nom channel", "Description", "Utilisateur"
     */
    public void initialize(){
        searchOption.getItems().setAll(Category.values());
        searchOption.setValue(Category.CHANNEL_NAME);

        BooleanBinding errorLabelFilled = Bindings.createBooleanBinding(() -> errorLabel.getText().length()>0, errorLabel.textProperty());
        errorLabel.visibleProperty().bind(errorLabelFilled);
    }

    /**
     * onSearch
     *  @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     *  Calls verify form
     *  Checks if the search input is too long
     *  Calls searchResults with the searchOption and the searchInput
     */
    public void onSearch() {
        if(searchInput.getText().isEmpty()) {
            errorLabel.setText("Remplissez le champ de recherche");
        } else {
            if(searchInput.getLength()<= MIN_SEARCH_INPUT_LENGTH)
                errorLabel.setText("Le champ de recherche est trop court");
            else {
                errorLabel.setText("");
                sidebar.getHome().getMain().getRoot().getInterfaceToCom().searchChannel(searchInput.getText(), searchOption.getValue());
            }
        }
    }

    /**
     * Sets sidebar.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @param mainSidebar the main sidebar
     */
    public void setSidebar(MainSidebar mainSidebar) { this.sidebar = mainSidebar; }
}
