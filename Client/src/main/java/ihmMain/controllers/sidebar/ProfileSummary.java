package ihmMain.controllers.sidebar;

import CommonClasses.User;
import ihmMain.controllers.popups.EditProfile;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

/**
 * Shows user avatar and login and link to manage profile
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class ProfileSummary {
    // FXML elements
    @FXML private ImageView avatar;
    @FXML private Label userPseudo;
    @FXML private VBox menu;
    @FXML private Hyperlink showMenuButton;
    @FXML private HBox info;

    // Parent controller
    MainSidebar sidebar;

    /**
     * Initialize view after it has been loaded
     * Creates and applies circular clipping mask to avatar
     * Sets up menu animation
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void initialize() {
        // set a circular mask on avatar
        double avatarWidth = avatar.getFitWidth();
        final Circle mask = new Circle(avatarWidth/2, avatarWidth/2, avatarWidth/2);
        avatar.setClip(mask);

        // show info on top of every other element so that the menu transition doesn't hide it
        info.setViewOrder(-1);

        // Binds visibility and layout management of the menu
        menu.managedProperty().bind(menu.visibleProperty());

        // Hide menu by default
        menu.setVisible(false);

        // the showMenuButton will trigger an animation to show/hide the menu
        showMenuButton.setOnAction(actionEvent -> {
            // create an animation to hide menu.
            final double startHeight = menu.getHeight();
            final Animation hideMenu = new Transition() {
                { setCycleDuration(Duration.millis(250)); }
                protected void interpolate(double frac) {
                    final double curHeight = startHeight * (1.0 - frac);
                    menu.setTranslateY(-startHeight + curHeight);
                }
            };
            hideMenu.onFinishedProperty().set(actionEventFinished -> menu.setVisible(false));

            // create an animation to show menu.
            final Animation showMenu = new Transition() {
                { setCycleDuration(Duration.millis(250)); }
                protected void interpolate(double frac) {
                    final double curHeight = startHeight * frac;
                    menu.setTranslateY(-startHeight + curHeight);
                }
            };

            // if all animations are done (so that we don't play multiple animations together if user spams the menu button)
            if (showMenu.statusProperty().get() == Animation.Status.STOPPED && hideMenu.statusProperty().get() == Animation.Status.STOPPED) {
                // if menu is visible we hide it, else we show it, all with the animation
                if (menu.isVisible()) {
                    hideMenu.play();
                } else {
                    menu.setVisible(true);
                    showMenu.play();
                }
            }
        });
    }

    /**
     * Update displayed user info.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void updateInfo() {
        User user = sidebar.getHome().getMain().getRoot().getConnectedUser();
        userPseudo.setText(user.getLogin());
        if(user.getAvatar() != null)
            avatar.setImage(SwingFXUtils.toFXImage(user.getAvatar(), null));
    }

    /**
     * Disconnect the user.
     * Called on menuItem "Déconnexion" action.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void disconnection(){
        try {
            this.sidebar.getHome().getMain().getRoot().getInterfaceToCom().reqServerDisconnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open edit profile pop up
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void openEditProfilePopUp(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/popups/editProfile.fxml"));

            Scene editProfileScene = new Scene(loader.load());
            Stage editProfileStage = new Stage();
            editProfileStage.initModality(Modality.WINDOW_MODAL);
            editProfileStage.initOwner(sidebar.getHome().getContainer().getScene().getWindow());

            EditProfile controller = loader.getController();
            controller.initControllers(this, this.sidebar.getHome(), editProfileStage);
            controller.initFieldValues();

            editProfileStage.setScene(editProfileScene);
            editProfileStage.setTitle("Modifier votre profil");
            editProfileStage.setWidth(600);
            editProfileStage.setHeight(650);
            editProfileStage.setResizable(false);

            editProfileStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets sidebar.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @param mainSidebar the main sidebar
     */
    public void setSidebar(MainSidebar mainSidebar) { this.sidebar = mainSidebar; }

}
