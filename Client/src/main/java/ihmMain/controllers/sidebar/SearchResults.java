package ihmMain.controllers.sidebar;

import CommonClasses.Channel;
import ihmMain.IhmMain;
import ihmMain.controllers.common.ChannelCell;
import ihmMain.controllers.common.NoSelectionModel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import java.util.List;

/**
 * Class responsible for showing the search results
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class SearchResults {
    // FXML Elements
    @FXML private ListView<Channel> searchResultsListView;

    // Parent controller
    private MainSidebar sidebar;

    /**
     * Initialize controller
     *
     * @author Saad Bennis <saad.bennis@etu.utc.fr>
     */
    public void init(IhmMain root, List<Channel> channelList) {
        searchResultsListView.setSelectionModel(new NoSelectionModel<>());
        ObservableList<Channel> channels = FXCollections.observableArrayList(channelList);

        FilteredList<Channel> notJoinedChannels = new FilteredList<>(channels,
                channel -> root.getObservableUserListChannelPublic().stream().noneMatch(pc -> (pc.getId().equals(channel.getId()))));

        root.getObservableUserListChannelPublic().addListener((ListChangeListener<Channel>) c -> {
            while (c.next()) {
                if (c.wasAdded() || c.wasRemoved())
                    Platform.runLater(() -> notJoinedChannels.setPredicate(channel -> root.getObservableUserListChannelPublic().stream().noneMatch(pc -> (pc.getId().equals(channel.getId())))));
            }
        });

        searchResultsListView.setItems(notJoinedChannels);
        searchResultsListView.setCellFactory(publicChannelsListView -> new ChannelCell(root));
    }


    /**
     * Go back to default sidebar
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void backToDefaultSidebar() { sidebar.showDefaultSidebar(); }

    /**
     * Sets sidebar.
     *
     * @param mainSidebar the main sidebar
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void setSidebar(MainSidebar mainSidebar) { this.sidebar = mainSidebar; }
}
