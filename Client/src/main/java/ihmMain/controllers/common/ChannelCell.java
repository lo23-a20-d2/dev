package ihmMain.controllers.common;

import CommonClasses.Channel;
import ihmMain.IhmMain;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * Class used to display a channel in a ListView
 * Renders a ListCell containing channel name, description and join button
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
 * @see ihmMain.controllers.popups.AllPublicChannels
 */
public class ChannelCell extends ListCell<Channel>  {
    // Names of CSS classes used to set styles on the child elements
    private static final String ROBOTO_BOLD_CLASS = "roboto-bold";
    private static final String ELEMENT_FONT_CLASS = "elementFont";
    private static final String NAME_FONT_CLASS = "nameFont";
    private static final String ELEMENT_CLASS = "element";
    private static final String BUTTON_CLASS = "button";
    private static final String BOX_CLASS = "box";

    // JavaFX elements
    private VBoxChannel channelBox = null;
    private VBoxChannel emptyBox = new VBoxChannel(null);

    private static IhmMain ihmMainRoot;

    /**
     * Instantiates a channel cell
     * Creates all the JavaFX elements needed to display the channel's properties and join button
     *
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     */
    public ChannelCell(IhmMain root){
        ihmMainRoot = root;
        this.setStyle("-fx-padding: 0px;");
    }


    /**
     * Used to customize the visual of a public channel cell
     * Set channel and empty properties right with super.updateItem
     * If there is no data, the list displayed is empty
     * Else creates a public channel cell calling VBoxChannel()
     *
     * @param channel is the Channel we want to display in one cell
     * @param empty indicates whether the channel is a data from the list or an empty row
     * @see VBoxChannel
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     */
    @Override
    protected void updateItem(Channel channel, boolean empty) {
        super.updateItem(channel,empty);

        if (empty || channel == null) {
            setText(null);
            setGraphic(emptyBox);
        } else {
            channelBox = new VBoxChannel(channel);

            // TODO : get accessibility information on channel (for example kicked channels are disabled)
            boolean isDisabled = false;
            if(isDisabled)
                channelBox.getStyleClass().add("disabled");
            setGraphic(channelBox);
        }
    }


    /**
     * Class to display a channel in PublicChannelPopup
     * Specifies how to organize the visual elements (HBoxChannel and the channel's description)
     *
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     */
    public static class VBoxChannel extends VBox {
        /**
         * Instantiates a VBoxChannel
         * Sets visual properties for display (calling HBoxChannel for the name and button)
         *
         * @param channel          the Channel to be displayed
         * @see HBoxChannel
         * @author Anis Mikou <anis.mikou@etu.utc.fr>
         * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
         */
        VBoxChannel(Channel channel) {
            super();

            this.setMaxWidth(562);
            this.getStyleClass().add(BOX_CLASS);

            // Create, style and fill description label
            Label descriptionLabel = new Label();
            VBox.setVgrow(descriptionLabel, Priority.ALWAYS);
            descriptionLabel.setWrapText(true);
            descriptionLabel.getStyleClass().add(ELEMENT_CLASS);
            descriptionLabel.getStyleClass().add(ELEMENT_FONT_CLASS);
            if(channel==null)
                descriptionLabel.setText("");
            else
                descriptionLabel.setText(channel.getDescription());

            this.getChildren().addAll(new HBoxChannel(channel), descriptionLabel);
        }
    }

    /**
     * Class to display a channel's name and join button
     * Specifies how to organize the visual elements
     *
     * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
     */
    public static class HBoxChannel extends HBox {

        /**
         * Instantiates an HBoxChannel
         * Sets visual properties for channel's name and join button
         * Region is used as a spacer to put the name at the left and the join button at the right of the window
         *
         * @param channel the channel to display
         * @author Anis Mikou <anis.mikou@etu.utc.fr>
         * @author Elsa Bataille <elsa.bataille@etu.utc.fr>
         */
        HBoxChannel(Channel channel){
            super();

            // Create and style name label
            Label nameLabel = new Label();
            nameLabel.setWrapText(true);
            nameLabel.getStyleClass().add(ELEMENT_CLASS);
            nameLabel.getStyleClass().add(NAME_FONT_CLASS);
            nameLabel.getStyleClass().add(ROBOTO_BOLD_CLASS);

            if(channel != null) {
                // Fill name label
                nameLabel.setText(channel.getName());

                // Create spacer region
                Region spacer = new Region();
                HBox.setHgrow(spacer, Priority.ALWAYS);

                // Create, style and link join button
                Button joinButton = new Button();
                joinButton.setText("Rejoindre");
                joinButton.getStyleClass().add(BUTTON_CLASS);
                joinButton.getStyleClass().add(ELEMENT_CLASS);
                joinButton.getStyleClass().add(ELEMENT_FONT_CLASS);
                joinButton.setOnAction(e -> {
                    try {
                        ihmMainRoot.getInterfaceToCom().joinNewChannel(channel.getId(), ihmMainRoot.getIdConnectedUser());
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });

                this.getChildren().addAll(nameLabel, spacer, joinButton);
            } else
                this.getChildren().add(nameLabel);
        }
    }
}
