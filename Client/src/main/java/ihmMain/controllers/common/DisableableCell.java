package ihmMain.controllers.common;

import CommonClasses.Channel;
import ihmMain.controllers.sidebar.MainSidebar;
import javafx.collections.ListChangeListener;
import javafx.scene.control.ListCell;

/**
 * Custom ListCell that disables the access to the channel when kicked
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class DisableableCell extends ListCell<Channel> {
    private MainSidebar sidebar;

    /**
     * Instantiates a new Disableable cell.
     * Instantiates kicked channels list and disabled property listeners
     *
     * @param sidebar the sidebar
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public DisableableCell(MainSidebar sidebar) {
        this.sidebar = sidebar;

        // Everytime there's a change in the kicked channels, update the disable property of the appropriate cell
        this.sidebar.getHome().getMain().getRoot().getKickedFromChannels().addListener((ListChangeListener<Channel>) change -> {
            Channel item = this.getItem();
            while (change.next()) {
                if(item!=null) {
                    if (change.wasAdded() && change.getAddedSubList().get(0).getId().equals(item.getId()))
                        this.setDisable(true);
                    else if (change.wasRemoved() && change.getRemoved().get(0).getId().equals(item.getId())) {
                        this.setDisable(false);
                }
            }
        }});

        // Strike through text with CSS class when cell is disabled
        this.disabledProperty().addListener(((observable, oldValue, newValue) -> {
            if(Boolean.TRUE.equals(newValue)) this.getStyleClass().add("strikethrough");
            else this.getStyleClass().remove("strikethrough");
        }));
    }

    @Override
    protected void updateItem(Channel item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item==null)
            setText(null);
        else {
            setText(item.getName());

            for (Channel channel : sidebar.getHome().getMain().getRoot().getKickedFromChannels()){
                if (channel.getId().equals(item.getId()))
                    this.setDisable(true);
            }
        }
    }
}
