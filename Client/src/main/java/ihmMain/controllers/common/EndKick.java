package ihmMain.controllers.common;

import CommonClasses.Channel;
import ihmMain.IhmMain;
import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.util.TimerTask;

/**
 * Task to execute at the end of a timed kick.
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class EndKick extends TimerTask {

    IhmMain ihmMain;
    Channel kickedChannel;

    /**
     * EndKick's constructor.
     * Initiates the needed attributes
     *
     * @param ihmMain ihmMain module's main controller
     * @param channel the channel the user was kicked from
     */
    public EndKick(IhmMain ihmMain, Channel channel){
        this.ihmMain = ihmMain;
        this.kickedChannel = channel;
    }

    /**
     * Called at the end of a timed kick.
     */
    @Override
    public void run() {
        this.ihmMain.getKickedFromChannels().removeIf(channel -> channel.getId().equals(kickedChannel.getId()));
        this.ihmMain.getScheduledKicks().remove(kickedChannel.getId());
        Platform.runLater(() -> this.ihmMain.getMainController().getHome().alertUser(Alert.AlertType.INFORMATION, kickedChannel, "Kick terminé","Le channel " + kickedChannel.getName() + " vous est à nouveau accessible.", "Voulez-vous ouvrir ce channel ?"));
    }
}
