package ihmMain.controllers;

import CommonClasses.Channel;
import ihmMain.controllers.sidebar.MainSidebar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;

import java.io.IOException;

/**
 * Home controller
 * Loads and displays sidebar and channel
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
public class Home {
    // FXML elements
    @FXML private GridPane container;

    // Owner controller
    private MainView main;

    // Child controllers
    private MainSidebar sidebar;

    /**
     * Load sidebar and set its parent controller to this controller
     * Also triggers sidebar to load its child nodes
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    public void loadSidebar() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/mainSidebar.fxml"));
            container.add(loader.load(), 0, 0);
            sidebar = loader.getController();
            sidebar.setHome(this);

            // Load child nodes
            sidebar.loadProfileSummary();
            sidebar.showDefaultSidebar();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show an alert to open the channel after the user was invited into it or his kick was removed.
     *
     * @param channel the channel the user was invited to
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void alertUser(Alert.AlertType type, Channel channel, String alertTitle, String alertMessage, String alertContent) {
        Alert alert = new Alert(type);
        alert.setTitle(alertTitle);
        alert.setHeaderText(alertMessage);
        alert.setContentText(alertContent);

        if (type == Alert.AlertType.INFORMATION){
            alert.getButtonTypes().add(ButtonType.NO);
            alert.getDialogPane().lookupButton(ButtonType.OK).addEventFilter(ActionEvent.ACTION, event -> {
                this.main.getRoot().setIdCurrentChannel(channel);
            });
        }

        alert.showAndWait();
    }

    /**
     * Gets main grid container
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     * @return the container
     */
    public Node getContainer() { return container; }

    /**
     * Gets owner controller main
     *
     * @return the controller
     */
    public MainView getMain() { return main; }

    /**
     * Sets owner controller main
     *
     * @param mainViewController the main view controller
     */
    public void setMain(MainView mainViewController) { main = mainViewController; }

    /**
     * Load and add ihm channel view to the main grid
     */
    public void loadIhmChannel() {
        GridPane parent = (GridPane) this.main.getRoot().getIhmChannelView().getParent();
        if(parent != null) parent.getChildren().clear();
        container.add(this.main.getRoot().getIhmChannelView(), 1, 0);
    }

    /**
     * Gets sidebar.
     *
     * @return the sidebar
     */
    public MainSidebar getSidebar() { return sidebar; }
}
