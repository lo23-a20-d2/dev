package ihmChannel.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The type TextMessage.
 * @author Guillaume Renaud
 */
public class TextMessage extends Message {
    private StringProperty content;
    private BooleanProperty edited;

    /**
     * Instantiates a new Text message.
     */
    public TextMessage() {
    	this.init();
    }

    /**
     * Instantiates a new Text message.
     *
     * @param content the content
     * @param edited  the edited value
     */
    public TextMessage(String content, boolean edited) {
    	this.init();
        this.content.set(content);
        this.edited.set(edited);
    }

    /**
     * Instantiates a new Text message.
     *
     * @param id         the id
     * @param date       the date
     * @param deleted    the deleted value
     * @param author     the author
     * @param likersList the likers list
     * @param content    the content
     * @param edited     the edited value
     */
    public TextMessage(String id, LocalDateTime date, boolean deleted, User author, List<User> likersList, String content, boolean edited) {
        super(id, date, deleted, author, likersList);
        this.init();
        this.content.set(content);
        this.edited.set(edited);
    }

    private void init() {
    	this.content = new SimpleStringProperty(this, "contentProperty", "");
    	this.edited = new SimpleBooleanProperty(this, "editedProperty", false);
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content.get();
    }

    /**
     * Content property string property.
     *
     * @return the string property
     */
    public StringProperty contentProperty() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content.set(content);
    }

    /**
     * Is edited boolean.
     *
     * @return the boolean
     */
    public boolean isEdited() {
        return edited.get();
    }

    /**
     * Edited property boolean property.
     *
     * @return the boolean property
     */
    public BooleanProperty editedProperty() {
        return edited;
    }

    /**
     * Sets edited.
     *
     * @param edited the edited
     */
    public void setEdited(boolean edited) {
        this.edited.set(edited);
    }
}
