package ihmChannel.models;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDateTime;
import java.util.List;

/**
 * The type Message.
 *
 * @author Guillaume Renaud
 */
public class Message {
    private String id;
    private LocalDateTime date;
    private String idThread;
    private BooleanProperty deleted;
    private ObjectProperty<User> author;
    private ObservableList<User> likersList;
    private ObservableList<Message> repliesList;

    /**
     * Instantiates a new Message.
     */
    public Message(){
    	this.init();
    	// test. à supprimer après
    	date = LocalDateTime.now();
    }

    /**
     * Instantiates a new Message.
     *
     * @param id         the id
     * @param date       the date
     * @param deleted    the deleted value
     * @param author     the author
     * @param likersList the likers list
     */
    public Message(String id, LocalDateTime date, boolean deleted, User author, List<User> likersList) {
        this.id = id;
        this.idThread = id;
        this.date = date;

        this.init();
        this.deleted.set(deleted);
    	this.author.set(author);
    	this.likersList.addAll(likersList);
    }

    private void init() {
    	this.deleted = new SimpleBooleanProperty(this, "deletedProperty", false);
    	this.author = new SimpleObjectProperty<User>(this, "deletedProperty");
    	this.likersList = FXCollections.observableArrayList();
    	this.repliesList = FXCollections.observableArrayList();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * Gets id thread.
     *
     * @return the id thread
     */
    public String getIdThread() {
        return idThread;
    }

    /**
     * Sets id thread.
     *
     * @param idThread the id thread
     */
    public void setIdThread(String idThread) {
        this.idThread = idThread;
    }

    /**
     * Is deleted boolean.
     *
     * @return the boolean
     */
    public boolean isDeleted() {
        return deleted.get();
    }

    /**
     * Deleted property boolean property.
     *
     * @return the boolean property
     */
    public BooleanProperty deletedProperty() {
        return deleted;
    }

    /**
     * Sets deleted.
     *
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted.set(deleted);
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public User getAuthor() {
        return author.get();
    }

    /**
     * Author property object property.
     *
     * @return the object property
     */
    public ObjectProperty<User> authorProperty() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(User author) {
        this.author.set(author);
    }

    /**
     * Gets likers list.
     *
     * @return the likers list
     */
    public ObservableList<User> getLikersList() {
        return likersList;
    }

    /**
     * Sets likers list.
     *
     * @param likersList the likers list
     */
    public void setLikersList(List<User> likersList) {
        this.likersList.setAll(likersList);
    }

    /**
     * Gets replies list.
     *
     * @return the replies list
     */
    public ObservableList<Message> getRepliesList() {
        return repliesList;
    }

    /**
     * Sets replies list.
     *
     * @param repliesList the replies list
     */
    public void setRepliesList(ObservableList<Message> repliesList) {
        this.repliesList = repliesList;
    }
}
