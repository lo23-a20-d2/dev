package ihmChannel.models;

import CommonClasses.Subscription;
import javafx.beans.property.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import java.awt.image.BufferedImage;

/**
 * The type User.
 *
 * @author Guillaume Renaud
 */
public class User {
    private String id;
    private String idSubscription;
    private String login;
    private String firstName;
    private String lastName;
    private StringProperty nickName;
    private ObjectProperty<Subscription.Role> role;
    private BooleanProperty connected;
    private BooleanProperty member;
    private ObjectProperty<Image> avatar;
    private LongProperty banEndDate;   // 0 if user is not banned, -1 if permaban, >0 if temporary ban

    /**
     * Instantiates a new User.
     */
    public User() {
        init();
    }

    /**
     * Instantiates a new User.
     *
     * @param id             the id
     * @param idSubscription the id subscription
     * @param login          the login
     * @param firstName      the first name
     * @param lastName       the last name
     * @param nickName       the nick name
     * @param role           the role
     * @param connected      the connected value
     * @param member         the member value
     * @param avatar         the avatar
     * @param banEndDate     the ban end date
     */
    public User(String id,
                String idSubscription,
                String login,
                String firstName,
                String lastName,
                String nickName,
                Subscription.Role role,
                boolean connected,
                boolean member,
                BufferedImage avatar,
                long banEndDate) {
        init();
        this.id = id;
        this.idSubscription = idSubscription;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName.set(nickName);
        this.role.set(role);
        this.connected.set(connected);
        this.member.set(member);
        this.setAvatar(avatar);
        this.banEndDate.set(banEndDate);
    }

    private void init() {
        this.nickName = new SimpleStringProperty(this, "nickNameProperty", "");
        this.role = new SimpleObjectProperty<>(this, "roleProperty", Subscription.Role.USER);
        this.connected = new SimpleBooleanProperty(this, "connectedProperty", false);
        this.member = new SimpleBooleanProperty(this, "memberProperty", false);
        this.avatar = new SimpleObjectProperty<>(this, "avatarProperty", null);
        this.banEndDate = new SimpleLongProperty(this, "banEndDateProperty", 0);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets id subscription.
     *
     * @return the id subscription
     */
    public String getIdSubscription() {
        return idSubscription;
    }

    /**
     * Sets id subscription.
     *
     * @param idSubscription the id subscription
     */
    public void setIdSubscription(String idSubscription) {
        this.idSubscription = idSubscription;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() { return login; }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) { this.login = login; }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets nick name.
     *
     * @return the nick name
     */
    public String getNickName() {
        return nickName.get();
    }

    /**
     * Sets nick name.
     *
     * @param nickName the nick name
     */
    public void setNickName(String nickName) {
        this.nickName.set(nickName);
    }

    /**
     * Nick name property string property.
     *
     * @return the string property
     */
    public StringProperty nickNameProperty() {
        return nickName;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Subscription.Role getRole() {
        return role.get();
    }

    /**
     * Role property object property.
     *
     * @return the object property
     */
    public ObjectProperty<Subscription.Role> roleProperty() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Subscription.Role role) {
        this.role.set(role);
    }

    /**
     * Is connected boolean.
     *
     * @return the boolean
     */
    public boolean isConnected() {
        return connected.get();
    }

    /**
     * Sets connected.
     *
     * @param connected the connected
     */
    public void setConnected(boolean connected) {
        this.connected.set(connected);
    }

    /**
     * Connected property boolean property.
     *
     * @return the boolean property
     */
    public BooleanProperty connectedProperty() {
        return connected;
    }

    /**
     * Is member boolean.
     *
     * @return the boolean
     */
    public boolean isMember() {
        return member.get();
    }

    /**
     * Member property boolean property.
     *
     * @return the boolean property
     */
    public BooleanProperty memberProperty() {
        return member;
    }

    /**
     * Sets member.
     *
     * @param member the member
     */
    public void setMember(boolean member) {
        this.member.set(member);
    }

    /**
     * Gets avatar.
     *
     * @return the avatar
     */
    public Image getAvatar() {
        if (avatar == null) return null;
        else return avatar.get();
    }

    /**
     * Sets avatar.
     *
     * @param avatar the avatar
     */
    public void setAvatar(Image avatar) {
        this.avatar.set(avatar);
    }

    /**
     * Sets avatar.
     *
     * @param avatar the avatar
     */
    public void setAvatar(BufferedImage avatar) {
        if (avatar != null) {
            this.avatar.set(SwingFXUtils.toFXImage(avatar, null));
        } else {
            this.avatar = null;
        }
    }

    /**
     * Avatar property object property.
     *
     * @return the object property
     */
    public ObjectProperty<Image> avatarProperty() {
        return avatar;
    }

    /**
     * Gets ban end date.
     *
     * @return the ban end date
     */
    public long getBanEndDate() {
        return banEndDate.get();
    }

    /**
     * Ban end date property long property.
     *
     * @return the long property
     */
    public LongProperty banEndDateProperty() {
        return banEndDate;
    }

    /**
     * Sets ban end date.
     *
     * @param banEndDate the ban end date
     */
    public void setBanEndDate(long banEndDate) {
        this.banEndDate.set(banEndDate);
    }
}
