package ihmChannel.models;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The type FileMessage model.
 * @author Guillaume Renaud
 */
public class FileMessage extends Message {
    private File content;

    /**
     * Instantiates a new FileMessage.
     */
    public FileMessage(){}

    /**
     * Instantiates a new FileMessage.
     *
     * @param content the content
     */
    public FileMessage(File content) {
        this.content = content;
    }

    /**
     * Instantiates a new File message.
     *
     * @param id         the id
     * @param date       the date
     * @param deleted    the deleted value
     * @param author     the author
     * @param likersList the likers list
     * @param content    the content
     */
    public FileMessage(String id, LocalDateTime date, boolean deleted, User author, List<User> likersList, File content) {
        super(id, date, deleted, author, likersList);
        this.content = content;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public File getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(File content) {
        this.content = content;
    }
}
