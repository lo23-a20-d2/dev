package ihmChannel.models;

import CommonClasses.Subscription;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type Model.
 *
 * @author Guillaume Renaud
 */
public class Model {
    private StringProperty idChannel;
    private StringProperty idChannelCreator;
    private StringProperty channelName;
    private StringProperty channelDesc;
    private ObservableList<Message> messageList;
    private Map<String, Message> rootMessagesMap; // Maps root msg id with the corresponding Message object
    private ObservableList<User> userList;
    private IntegerProperty connectedMembersCount;

    /**
     * Instantiates a new Model.
     */
    public Model() {
    }

    /**
     * Init.
     */
    public void init() {
        // Initialize ObservableLists
        this.messageList = FXCollections.observableArrayList();
        this.rootMessagesMap = new HashMap<>();
        this.userList = FXCollections.observableArrayList(item -> new Observable[] {item.connectedProperty(), item.memberProperty()});

        // Initialize ObjectProperties
        this.idChannel = new SimpleStringProperty(this, "idChannelProperty", "");
        this.idChannelCreator = new SimpleStringProperty(this, "idChannelCreatorProperty", "");
        this.channelName = new SimpleStringProperty(this, "channelNameProperty", "");
        this.channelDesc = new SimpleStringProperty(this, "channelDescProperty", "");
        this.connectedMembersCount = new SimpleIntegerProperty(this, "connectedMembersCountProperty", 0);
    }

    /**
     * Gets id channel.
     *
     * @return the id channel
     */
    public String getIdChannel() {
        return idChannel.get();
    }

    /**
     * Id channel property string property.
     *
     * @return the string property
     */
    public StringProperty idChannelProperty() {
        return idChannel;
    }

    /**
     * Sets id channel.
     *
     * @param idChannel the id channel
     */
    public void setIdChannel(String idChannel) {
        this.idChannel.set(idChannel);
    }

    /**
     * Gets id channel creator.
     *
     * @return the id channel creator
     */
    public String getIdChannelCreator() {
        return idChannelCreator.get();
    }

    /**
     * Id channel creator property string property.
     *
     * @return the string property
     */
    public StringProperty idChannelCreatorProperty() {
        return idChannelCreator;
    }

    /**
     * Sets id channel creator.
     *
     * @param idChannelCreator the id channel creator
     */
    public void setIdChannelCreator(String idChannelCreator) {
        this.idChannelCreator.set(idChannelCreator);
    }

    /**
     * Gets channel name.
     *
     * @return the channel name
     */
    public String getChannelName() {
        return channelName.get();
    }

    /**
     * Sets channel name.
     *
     * @param channelName the channel name
     */
    public void setChannelName(String channelName) {
        this.channelName.set(channelName);
    }

    /**
     * Channel name property string property.
     *
     * @return the string property
     */
    public StringProperty channelNameProperty() {
        return channelName;
    }

    /**
     * Gets channel description.
     *
     * @return the channel desc
     */
    public String getChannelDesc() {
        return channelDesc.get();
    }

    /**
     * Sets channel description.
     *
     * @param channelDesc the channel desc
     */
    public void setChannelDesc(String channelDesc) {
        this.channelDesc.set(channelDesc);
    }

    /**
     * Channel description property string property.
     *
     * @return the string property
     */
    public StringProperty channelDescProperty() {
        return channelDesc;
    }

    /**
     * Gets message list.
     *
     * @return the message list
     */
    public ObservableList<Message> getMessageList() {
        return messageList;
    }

    /**
     * Sets message list.
     *
     * @param messageList the message list
     */
    public void setMessageList(List<Message> messageList) {
        this.messageList.setAll(messageList);
    }

    /**
     * Gets root messages map.
     *
     * @return the root messages map
     */
    public Map<String, Message> getRootMessagesMap() {
        return rootMessagesMap;
    }

    /**
     * Sets root messages map.
     *
     * @param rootMessagesMap the root messages map
     */
    public void setRootMessagesMap(Map<String, Message> rootMessagesMap) {
        this.rootMessagesMap = rootMessagesMap;
    }

    /**
     * Gets user list.
     *
     * @return the user list
     */
    public ObservableList<User> getUserList() {
        return userList;
    }

    /**
     * Sets user list.
     *
     * @param userList the user list
     */
    public void setUserList(List<User> userList) {
        this.userList.setAll(userList);
    }

    /**
     * Gets connected members count.
     *
     * @return the connected members count
     */
    public int getConnectedMembersCount() {
        return connectedMembersCount.get();
    }

    /**
     * Connected members count property integer property.
     *
     * @return the integer property
     */
    public IntegerProperty connectedMembersCountProperty() {
        return connectedMembersCount;
    }

    /**
     * Sets connected members count.
     *
     * @param connectedMembersCount the connected members count
     */
    public void setConnectedMembersCount(int connectedMembersCount) {
        this.connectedMembersCount.set(connectedMembersCount);
    }

    /**
     * Increment connected members count.
     */
    public void incrementConnectedMembersCount() {
        this.connectedMembersCount.set(connectedMembersCount.get() + 1);
    }

    /**
     * Decrement connected members count.
     */
    public void decrementConnectedMembersCount() {
        if (this.connectedMembersCount.get() > 0)
            this.connectedMembersCount.set(connectedMembersCount.get() - 1);
        else
            System.err.println("Error : tried to decrement connected members count but count was already 0");
    }


    /**
     * Clears all the attributes
     */
    public void clearChannel() {
        idChannel.set("");
        idChannelCreator.set("");
        channelName.set("");
        channelDesc.set("");
        this.messageList.clear();
	    this.rootMessagesMap.clear();
        connectedMembersCount.set(0);

        List<User> disconnectedUsers = new ArrayList<>();
        userList.forEach(user -> {
            if (!user.isConnected()) {
                // Remove disconnected users
                disconnectedUsers.add(user);
            } else {
                // Clean connected users
                user.setIdSubscription("");
                user.setNickName("");
                user.setRole(Subscription.Role.USER);
                user.setMember(false);
            }
        });
        userList.removeAll(disconnectedUsers);
    }

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    public User getUserById(String id) {
        for (User user: userList){
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Gets user by subscription.
     *
     * @param idSubscription the id subscription
     * @return the user by subscription
     */
    public User getUserBySubscription(String idSubscription) {
        for (User user: userList){
            if (user.getIdSubscription().equals(idSubscription)) {
                return user;
            }
        }
        return null;
    }

}
