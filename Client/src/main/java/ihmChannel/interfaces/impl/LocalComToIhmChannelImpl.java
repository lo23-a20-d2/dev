package ihmChannel.interfaces.impl;

import CommonClasses.Channel;
import CommonClasses.OwnedChannel;
import CommonClasses.SharedChannel;
import CommonClasses.Subscription;
import Interfaces.LocalComToIhmChannel;
import ihmChannel.IhmChannel;
import ihmChannel.models.User;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The LocalComToIhmChannel implementation.
 * @author Guillaume Renaud
 */
public class LocalComToIhmChannelImpl implements LocalComToIhmChannel {

    private IhmChannel root;

    /**
     * Instantiates a new LocalComToIhmChannel.
     *
     * @param root the root
     */
    public LocalComToIhmChannelImpl(IhmChannel root) {
        this.root = root;
    }

    @Override
    public void displayNewNickname(String nickname, String idUser, String idChannel) {
        Platform.runLater(() -> {
            if (idChannel.equals(this.root.getModel().getIdChannel())) {
                User user = root.getModel().getUserById(idUser);
                user.setNickName(nickname);
            }
        });
    }

    @Override
    public void displayNewTextMsg(CommonClasses.TextMessage textMsg) {
        Platform.runLater(() -> {
            //verify that the messsage is send to current channel
            if (textMsg.getIdChannel().equals(this.root.getModel().getIdChannel())) {
                List<User> likersList = new ArrayList<>();
                ihmChannel.models.TextMessage newTextMessage = new ihmChannel.models.TextMessage(
                        textMsg.getId(),
                        // convert Date to LocalDateTime
                        textMsg.getDatetime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                        textMsg.isErased(),
                        root.getModel().getUserBySubscription(textMsg.getIdSubscription()),
                        likersList,
                        textMsg.getTextContent(),
                        textMsg.isEdited()
                );
                ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
                messageList.add(newTextMessage);
            }
        });
    }

    @Override
    public void displayNewFileMsg(CommonClasses.FileMessage fileMsg) {

    }

    @Override
    public void displayModifiedMsg(CommonClasses.TextMessage textMsg) {
        Platform.runLater(() -> {
            ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
            // find the message in the list and modify its content and if it is edited
            for (ihmChannel.models.Message message : messageList) {
                if (message.getId() != null && message.getId().equals(textMsg.getId())) {
                    ((ihmChannel.models.TextMessage) message).setContent(textMsg.getTextContent());
                    ((ihmChannel.models.TextMessage) message).setEdited(textMsg.isEdited());
                    break;
                }
            }
        });
    }

    @Override
    public void displayDeletedMsg(String idMsg, String idChannel) {
        Platform.runLater(() -> {
            ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
            // find the message and delete its content and set his deleted property to true
            for (ihmChannel.models.Message message: messageList) {
                if (message.getId() != null && message.getId().equals(idMsg)) {
                    if (message instanceof ihmChannel.models.TextMessage)
                        ((ihmChannel.models.TextMessage) message).setContent("");
                    message.setDeleted(true);
                    break;
                }
            }
        });
    }

    @Override
    public void displayLikes(String idMsg, String idUser, String idChannel) {
        Platform.runLater(() -> {
            if (this.root.getModel().getIdChannel().equals(idChannel)) {
                ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
                // find the message and update the likers list
                for (ihmChannel.models.Message message : messageList) {
                    if (message.getId() != null && message.getId().equals(idMsg)) {
                        ObservableList<User> likerList = message.getLikersList();
                        User user = this.root.getModel().getUserById(idUser);
                        boolean find = false;
                        for (User liker : likerList) {
                            if (liker.getId().equals(idUser)) {
                                find = true;
                                break;
                            }
                        }
                        if (find) {
                            likerList.remove(user);
                        } else {
                            likerList.add(user);
                        }
                        break;
                    }
                }
                ;
            }
        });
    }

    @Override
    public void displayTextReply(CommonClasses.TextMessage textMsg) {
        Platform.runLater(() -> {
            System.out.println("Reply returned : id = " + textMsg.getId() + " idThread = " + textMsg.getIdThread());
            List<User> likersList = new ArrayList<>();
            ihmChannel.models.TextMessage newTextMessage = new ihmChannel.models.TextMessage(
                    textMsg.getId(),
                    // convert Date to LocalDateTime
                    textMsg.getDatetime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                    textMsg.isErased(),
                    root.getModel().getUserBySubscription(textMsg.getIdSubscription()),
                    likersList,
                    textMsg.getTextContent(),
                    textMsg.isEdited()
            );
            newTextMessage.setIdThread(textMsg.getIdThread());
            this.root.getModel().getMessageList().add(newTextMessage);

            Map<String, ihmChannel.models.Message> rootMessagesMap = this.root.getModel().getRootMessagesMap();
            ihmChannel.models.Message rootMsg = null;
            if (rootMessagesMap.containsKey(newTextMessage.getIdThread())) {
                rootMsg = rootMessagesMap.get(newTextMessage.getIdThread());
            } else {
                for (ihmChannel.models.Message rm : this.root.getModel().getMessageList()) {
                    if (rm.getId().equals(newTextMessage.getIdThread())) {
                        rootMsg = rm;
                        rootMessagesMap.put(newTextMessage.getIdThread(), rm);
                        break;
                    }
                }
            }
            if (rootMsg == null) {
                System.err.println("The root message of msg " + newTextMessage.getId() + " was not found. Ignoring Message " + newTextMessage.getId() + ".");
            } else {
                rootMsg.getRepliesList().add(newTextMessage);
            }
        });
    }

    @Override
    public void displayFileReply(CommonClasses.FileMessage fileMsg) {

    }

    @Override
    public void displayThread(String idChannel, List<CommonClasses.Message> threadContents) {

    }

    @Override
    public void displayChannel(Channel channel, List<CommonClasses.Message> messages, List<Subscription> subscriptions) {
        Platform.runLater(() -> {
            // Remove all messages from UI
            root.getCentralController().clearAllMessages();

            // Close thread if one was open
            if (root.getCentralController().getMsgOpenedInThread() != null) {
                root.getCentralThreadContainer().getChildren().remove(root.getCentralThreadContainer().getChildren().size() - 1);
                root.getCentralController().setMsgOpenedInThread(null);
            }

            // Clean up model
            root.getModel().clearChannel();

            // Populate model
            root.getModel().setIdChannel(channel.getId());
            if (channel instanceof SharedChannel) {
                root.getModel().setIdChannelCreator(((SharedChannel) channel).getCreator().getId());
            } else { // instanceof OwnedChannel
                root.getModel().setIdChannelCreator(((OwnedChannel) channel).getOwner().getId());
            }
            root.getModel().setChannelName(channel.getName());
            root.getModel().setChannelDesc(channel.getDescription());

            // Get list of users from model
            ObservableList<ihmChannel.models.User> ihmUsers = this.root.getModel().getUserList();

            subscriptions.forEach(sub -> {
                ihmChannel.models.User user = root.getModel().getUserById(sub.getUser().getId());

                boolean disconnectedUser = false;
                // User will be null if they are not connected
                if (user == null) {
                    disconnectedUser = true;
                    user = new ihmChannel.models.User();
                    user.setId(sub.getUser().getId());
                    user.setConnected(false);
                } else {
                    this.root.getModel().incrementConnectedMembersCount();
                }
                user.setIdSubscription(sub.getId());
                user.setNickName(sub.getNickname());
                user.setRole(sub.getRole());
                user.setMember(true);
                if(sub.getUser().getAvatar() != null)   //TODO : Temporaire ?- Ajouté par Gauthier C (com) pour régler une exception lors de l'affichage de channels...
                user.setAvatar(sub.getUser().getAvatar());

                if(sub.getIsBanned()) {
                    user.setBanEndDate(sub.getEndDate());
                } else {
                    user.setBanEndDate(0);
                }

                // Add user to the list if they are not connected (but are part a member of the channel)
                if (disconnectedUser) {
                    ihmUsers.add(user);
                }
            });

            List<ihmChannel.models.Message> ihmMessages = new ArrayList<>();
            List<ihmChannel.models.Message> rootMessages = new ArrayList<>();
            List<ihmChannel.models.Message> replyMessages = new ArrayList<>();
            messages.forEach(msg -> {
                // Find users who liked the message
                List<ihmChannel.models.User> likersList = new ArrayList<>();
                if (msg.getLikersList() != null) {
                    for (String id : msg.getLikersList()) {
                        User u = this.root.getModel().getUserById(id);
                        if (u != null) {
                            likersList.add(u);
                        } else {
                            System.err.println("Error : user " + id + " who liked message " + msg.getId() + " not found in channel users.");
                        }
                    }
                }

                ihmChannel.models.Message ihmMsg = null;
                if (msg instanceof CommonClasses.TextMessage) {
                    ihmChannel.models.User author = root.getModel().getUserBySubscription(msg.getIdSubscription());
                    if (author == null) {
                        System.err.println("The owner of Subscription " + msg.getIdSubscription() + " was not found. Ignoring Message " + msg.getId() + ".");
                    } else {
                        ihmMsg = new ihmChannel.models.TextMessage(
                                msg.getId(),
                                LocalDateTime.ofInstant(msg.getDatetime().toInstant(), ZoneId.systemDefault()),
                                msg.isErased(),
                                author,
                                likersList,
                                ((CommonClasses.TextMessage) msg).getTextContent(),
                                msg.isEdited());
                    }
                } else {
                    // Later version : FileMessage handling
                }

                if (ihmMsg != null) {
                    ihmMsg.setIdThread(msg.getIdThread());
                    if (!ihmMsg.getId().equals(ihmMsg.getIdThread())) {
                        replyMessages.add(ihmMsg);
                    } else {
                        rootMessages.add(ihmMsg);
                    }
                    ihmMessages.add(ihmMsg);
                }
            });
            Map<String, ihmChannel.models.Message> rootMessagesMap = this.root.getModel().getRootMessagesMap();
            replyMessages.forEach(msg -> {
                ihmChannel.models.Message rootMsg = null;
                if (rootMessagesMap.containsKey(msg.getIdThread())) {
                    rootMsg = rootMessagesMap.get(msg.getIdThread());
                } else {
                    for (ihmChannel.models.Message rm : rootMessages) {
                        if (rm.getId().equals(msg.getIdThread())) {
                            rootMsg = rm;
                            rootMessagesMap.put(msg.getIdThread(), rm);
                            break;
                        }
                    }
                }

                if (rootMsg == null) {
                    System.err.println("The root message of msg " + msg.getId() + " was not found. Ignoring Message " + msg.getId() + ".");
                } else {
                    rootMsg.getRepliesList().add(msg);
                }
            });

            root.getModel().setMessageList(ihmMessages);
        });
    }

    @Override
    public void displayNewMemberGreeting(Subscription sub) {
        Platform.runLater(() -> {
            ihmChannel.models.TextMessage greetingMessage = new ihmChannel.models.TextMessage();
            ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();

            if (sub.getUser().getId().equals(root.getInterfaceToIhmMain().getIdConnectedUser())) {
                greetingMessage.setContent("Vous avez rejoint le canal");
            } else {
                greetingMessage.setContent(sub.getNickname() + " a rejoint le canal");
            }

            ihmChannel.models.User ihmUser = this.root.getModel().getUserById(sub.getUser().getId());
            ihmUser.setIdSubscription(sub.getId());
            ihmUser.setNickName(sub.getNickname());
            ihmUser.setRole(sub.getRole());
            ihmUser.setMember(true);
            this.root.getModel().incrementConnectedMembersCount();

            greetingMessage.setAuthor(null); // Messages with null author are interpreted as system messages
            messageList.add(greetingMessage);
        });
    }

    @Override
    public void displayNewMember(String idChannel, CommonClasses.User newUser) {

    }

    @Override
    public void notifyChannelKick(String idUserKicked, String idChannel, long endDate) {
        Platform.runLater(() -> {
            // Only if the user is currently in the channel
            if (idChannel.equals(root.getModel().getIdChannel())) {
                ihmChannel.models.TextMessage kickMessage = new ihmChannel.models.TextMessage();
                ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
                User userKicked = this.root.getModel().getUserById(idUserKicked);
                String username = userKicked.getNickName();

                if (endDate == -1) {
                    kickMessage.setContent(username + " a été banni(e) du channel");
                } else if (endDate == 0) {
                    kickMessage.setContent(username + " a été kické(e) du channel");
                } else {
                    Timestamp stamp = new Timestamp(endDate);
                    Date date = new Date(stamp.getTime());
                    String pattern = "MM/dd/yyyy HH:mm:ss";
                    DateFormat df = new SimpleDateFormat(pattern);
                    kickMessage.setContent(username + " a été banni(e) du channel jusqu'à " + df.format(date));
                }

                userKicked.setBanEndDate(endDate);

                // Members count a décrémenter ? en cas de kick ?
                kickMessage.setAuthor(null); // Messages with null author are interpreted as system messages
                messageList.add(kickMessage);
            }
        });
    }

    @Override
    public void newAdmin(String idUser, String idChannel) {
        Platform.runLater(() -> {
            this.root.getModel().getUserById(idUser).setRole(Subscription.Role.ADMIN);

            ihmChannel.models.TextMessage promotionMessage = new ihmChannel.models.TextMessage();
            ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
            String username = this.root.getModel().getUserById(idUser).getNickName();
            promotionMessage.setContent(username + " a été promu administrateur du channel");

            promotionMessage.setAuthor(null);
            messageList.add(promotionMessage);
        });
    }

    @Override
    public void notifyKickCanceled(String idUser, String idChannel) {
        Platform.runLater(() -> {
            // Only if the user is currently in the channel
            if (idChannel.equals(root.getModel().getIdChannel())) {
                ihmChannel.models.TextMessage unBanMessage = new ihmChannel.models.TextMessage();
                ObservableList<ihmChannel.models.Message> messageList = this.root.getModel().getMessageList();
                User user = this.root.getModel().getUserById(idUser);
                String username = user.getNickName();

                user.setBanEndDate(0);


                unBanMessage.setContent("Le ban de " + username + " a été levé");
                messageList.add(unBanMessage);
            }
        });
    }

    @Override
    public void sendConnectedUsersServerList(List<CommonClasses.User> connectedUsers) {
        List<ihmChannel.models.User> ihmUsers = new ArrayList<>();

        connectedUsers.forEach(user -> {
            ihmChannel.models.User ihmUser = new User(
                    user.getId(),
                    null,
                    user.getLogin(),
                    user.getFirstName(),
                    user.getLastName(),
                    null,
                    null,
                    true,
                    false,
                    user.getAvatar(),
                    0);
            ihmUsers.add(ihmUser);
        });

        root.getModel().setUserList(ihmUsers);
    }

    @Override
    public void sendNewUserConnectedToServer(CommonClasses.User user) {
        Platform.runLater(() -> {
            ihmChannel.models.User existingIhmUser = this.root.getModel().getUserById(user.getId());

            // user is a member of the channel
            if (existingIhmUser != null) {
                existingIhmUser.setLogin(user.getLogin());
                existingIhmUser.setFirstName(user.getFirstName());
                existingIhmUser.setLastName(user.getLastName());
                existingIhmUser.setConnected(true);
                this.root.getModel().incrementConnectedMembersCount();
            } else {
                ihmChannel.models.User newIhmUser = new User(
                        user.getId(),
                        null,
                        user.getLogin(),
                        user.getFirstName(),
                        user.getLastName(),
                        null,
                        null,
                        true,
                        false,
                        user.getAvatar(),
                        0
                );
                root.getModel().getUserList().add(newIhmUser);
            }
        });
    }

    @Override
    public void notifyQuitChannel(String idUser) {

    }

    @Override
    public void sendUserDisconnectedFromServer(String idUser) {
        Platform.runLater(() -> {
            User u = this.root.getModel().getUserById(idUser);
            if (u.isMember()) {
                u.setConnected(false);
                this.root.getModel().decrementConnectedMembersCount();
            } else {
                this.root.getModel().getUserList().remove(u);
            }
        });
    }

    @Override
    public void updateConnectedUser(CommonClasses.User updatedUser) {
        Platform.runLater(() -> {
            ihmChannel.models.User ihmUser = this.root.getModel().getUserById(updatedUser.getId());

            if (ihmUser == null) { // user not found
                System.err.println("User " + updatedUser.getId() + " not found in IhmChannel users. Ignoring user update.");
            } else {
                ihmUser.setFirstName(updatedUser.getFirstName());
                ihmUser.setLastName(updatedUser.getLastName());
                ihmUser.setAvatar(updatedUser.getAvatar());
            }
        });
    }
}
