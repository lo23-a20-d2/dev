package ihmChannel.interfaces.impl;

import CommonClasses.Channel;
import CommonClasses.Message;
import CommonClasses.Subscription;
import Interfaces.IhmMainToIhmChannel;
import Interfaces.LocalComToIhmChannel;

import java.util.List;

/**
 * The IhmMainToIhmChannel implementation.
 * @author Guillaume Renaud
 */
public class IhmMainToIhmChannelImpl implements IhmMainToIhmChannel {
    private LocalComToIhmChannel localComToIhmChannel;

    /**
     * Instantiates a new implementation of IhmMainToIhmChannel.
     *
     * @param localComToIhmChannel the local com to ihm channel
     */
    public IhmMainToIhmChannelImpl(LocalComToIhmChannel localComToIhmChannel) {
        this.localComToIhmChannel = localComToIhmChannel;
    }

    // The displayChannel interfaces for LocalCom and IhmMain are identical
    @Override
    public void displayChannel(Channel channel, List<Message> messages, List<Subscription> subscriptions) {
        localComToIhmChannel.displayChannel(channel, messages, subscriptions);
    }
}
