package ihmChannel.controllers;

import CommonClasses.Subscription;
import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import ihmChannel.models.TextMessage;
import ihmChannel.models.User;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * The type Message container.
 */
public class MessageContainer extends VBox {
    private static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("EE dd MMM YYYY - HH:mm");
    private IhmChannel root;

    private HBox mainBox;               // Leftbox + rightbox (avatar, header, content)
    private HBox leftBox;               // Avatar & connection state
    private VBox rightBox;              // Header & content
    private VBox contentBox;            // Content & state (if needed)
    private HBox headerBox;             // Username & date
    private HBox hoverBox;              // Hover icons
    private HBox likeBox;               // Likes (icon & number)
    private ImageView avatarView;
    private ImageView connectionView;
    private ImageView modifyView;
    private ImageView deleteView;
    private ImageView replyView;
    private ImageView likeView;
    private ImageView miniLikeView;
    private Image likeImage;
    private Image unlikeImage;
    private Label likeLabel;
    private Label nameLabel;
    private Label dateLabel;
    private Label contentLabel;
    private Label stateLabel;
    private HBox repliesBox;            // Under everything (in 'this', after mainBox)
    private Label repliesNumberLabel;
    private Label lastReplyDateLabel;
    private float avatarRadius = 15f;
    private float hoverIconSize = 25f;
    private boolean isInThread = false; // Indicates if the message is displayed in the context of a thread

    /**
     * Instantiates a new Message container and fills it with the provided Message.
     *
     * @param msg  the Message object to display
     * @param root the root
     */
    public MessageContainer(Message msg, IhmChannel root) {
        this.root = root;
        this.setFillWidth(true);
        init(msg);
        fill(msg);
    }

    /**
     * Instantiates a new Message container and fills it with the provided Message.
     *
     * @param msg        the Message object to display
     * @param root       the root
     * @param isInThread whether the message is displayed in a thread or not
     */
    public MessageContainer(Message msg, IhmChannel root, boolean isInThread) {
        this.root = root;
        this.isInThread = isInThread;
        this.setFillWidth(true);
        init(msg);
        fill(msg);
    }

    /**
     * Initializes the components of the container.
     */
    private void init(Message msg) {
        // Left part (Avatar & connection state)
        avatarView = new ImageView();
        avatarView.setFitHeight(avatarRadius * 2);
        avatarView.setFitWidth(avatarRadius * 2);
        // Circle clip to emulate the classic profile picture display
        avatarView.setClip(new Circle(avatarRadius, avatarRadius, avatarRadius));
        connectionView = new ImageView();
        connectionView.setFitHeight(10);
        connectionView.setFitWidth(10);
        leftBox = new HBox(avatarView, connectionView);

        // Right part (Author, date & content)
        nameLabel = new Label();
        nameLabel.setStyle("-fx-font-weight: bold;-fx-text-fill:#000000;-fx-font-size:16px;");
        dateLabel = new Label();
        dateLabel.setStyle("-fx-text-fill:#000000;-fx-font-size:11px;");
        headerBox = new HBox(nameLabel, dateLabel);
        headerBox.setSpacing(20);
        headerBox.setAlignment(Pos.CENTER_LEFT);
        contentLabel = new Label();
        contentLabel.setStyle("-fx-text-fill:#000000;-fx-font-size:14px;");
        contentLabel.setWrapText(true);
        contentLabel.setMinHeight(USE_PREF_SIZE);
        contentBox = new VBox(contentLabel);
        stateLabel = new Label();
        stateLabel.setStyle("-fx-text-fill:#000000;-fx-font-size:11px;");
        rightBox = new VBox(headerBox, contentBox);
        rightBox.setMinHeight(USE_PREF_SIZE);

        // Likes
        miniLikeView = new ImageView();
        miniLikeView.setFitWidth(10);
        miniLikeView.setFitHeight(10);
        miniLikeView.setImage(new Image("resources/icons/like.png"));
        likeLabel = new Label();
        likeLabel.setFont(new Font(16));
        likeLabel.setStyle("-fx-text-fill:#000000; -fx-font-size:11px; -fx-font-weight: bold");
        likeLabel.setText("0");
        likeBox = new HBox(miniLikeView, likeLabel);

        // Replies
        repliesNumberLabel = new Label();
        repliesNumberLabel.setStyle("-fx-text-fill:#1a237e;-fx-font-size:14px;");
        lastReplyDateLabel = new Label();
        lastReplyDateLabel.setStyle("-fx-text-fill:#000000;-fx-font-size:12px;");
        repliesBox = new HBox(repliesNumberLabel, lastReplyDateLabel);
        repliesBox.setStyle("-fx-background-color: #b0bec5; -fx-padding: 1px 10px 1px 10px");
        repliesBox.setPrefWidth(400);
        repliesBox.setAlignment(Pos.CENTER_LEFT);
        repliesBox.setSpacing(20);

        // Main box (avatar, header, content, likes)
        mainBox = new HBox(leftBox, rightBox);
        mainBox.setSpacing(10);
        HBox.setHgrow(mainBox, Priority.ALWAYS);

        // MessageContainer (mainBox + replies)
        this.getChildren().add(mainBox);

        // TextMessage
        if (msg instanceof TextMessage) {
            TextMessage textMsg = (TextMessage) msg;

            // Listener on text content
            textMsg.contentProperty().addListener((obs, oldVal, newVal) -> {
                if (!newVal.equals(oldVal)) {
                    contentLabel.setText(newVal);
                }
            });

            // Listener on edited
            textMsg.editedProperty().addListener((obs, oldVal, newVal) -> {
                if (!newVal.equals(oldVal) && newVal == true) {
                    this.setEdited();
                }
            });
        }

        // For messages that are not system messages
        if (msg.getAuthor() != null) {
            // Listener on nickname
            msg.getAuthor().nickNameProperty().addListener((obs, oldVal, newVal) -> {
                if (newVal != null && !newVal.equals(oldVal)) {
                    nameLabel.setText(newVal);
                }
            });

            // Listener on avatar
            if (msg.getAuthor().avatarProperty() != null) {
                msg.getAuthor().avatarProperty().addListener((obs, oldVal, newVal) -> {
                    if (newVal != null) {
                        avatarView.setImage(newVal);
                    } else {
                        avatarView.setImage(new Image("resources/icons/user.png")); // Default avatar
                    }
                });
            }
        }

        // Lister on deleted
        msg.deletedProperty().addListener((obs, oldVal, newVal) -> {
            if (!newVal.equals(oldVal) && newVal == true) {
                this.setDeleted();
            }
        });

        // Initialize replies
        if (!msg.getRepliesList().isEmpty()) {
            updateRepliesBox(msg);
        }

        // Listener on replies list
        ListChangeListener<Message> g = change -> {
            change.next();
            if (change.wasAdded()) {
                updateRepliesBox(msg);
            }
        };
        msg.getRepliesList().addListener(g);

        repliesBox.setOnMouseClicked(event -> {
            openThread(msg);
        });
    }

    private void updateLikesBox(Message msg) {
        int likes = msg.getLikersList().size();
        if (likes >= 1 && !rightBox.getChildren().contains(likeBox)) {
            rightBox.getChildren().add(likeBox);
        } else if (rightBox.getChildren().contains(likeBox) && likes == 0) {
            rightBox.getChildren().remove(likeBox);
        }

        likeLabel.setText(String.valueOf(likes));
    }

    private void updateRepliesBox(Message msg) {
        int repliesNb = msg.getRepliesList().size();
        String lastReplyDate = "Dernière réponse : "
                + DT_FORMATTER.format(msg.getRepliesList().get(msg.getRepliesList().size() - 1).getDate());
        repliesNumberLabel.setText(repliesNb + " réponse");
        if (repliesNb > 1)
            repliesNumberLabel.setText(repliesNumberLabel.getText() + "s");
        lastReplyDateLabel.setText(lastReplyDate);
        if (!isInThread && !this.getChildren().contains(repliesBox)) {
            this.getChildren().add(repliesBox);
        }
    }

    /**
     * Fills the container with the contents of a Message.
     *
     * @param msg the Message used to fill the container
     */
    public void fill(Message msg) {
        // hover (delete, modify, like, reply)
        modifyView = new ImageView();
        modifyView.setFitHeight(hoverIconSize);
        modifyView.setFitWidth(hoverIconSize);
        modifyView.setImage(new Image("resources/icons/edit.png"));
        deleteView = new ImageView();
        deleteView.setFitHeight(hoverIconSize);
        deleteView.setFitWidth(hoverIconSize);
        deleteView.setImage(new Image("resources/icons/delete.png"));
        replyView = new ImageView();
        replyView.setFitHeight(hoverIconSize);
        replyView.setFitWidth(hoverIconSize);
        replyView.setImage(new Image("resources/icons/reply.png"));
        // LikeView
        likeImage = new Image("resources/icons/like.png");
        unlikeImage = new Image("resources/icons/unlike.png");
        likeView = new ImageView(likeImage);
        likeView.setFitHeight(hoverIconSize);
        likeView.setFitWidth(hoverIconSize);
        hoverBox = new HBox();
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        this.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                mainBox.getChildren().addAll(spacer, hoverBox);
            }
        });

        this.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                mainBox.getChildren().removeAll(spacer, hoverBox);
            }
        });

        // Initialize likes
        if (!msg.getLikersList().isEmpty()) {
            updateLikesBox(msg);
            if (msg.getLikersList().contains(root.getModel().getUserById(root.getInterfaceToIhmMain().getIdConnectedUser()))) {
                likeView.setImage(unlikeImage);
            }
        }

        // Listener on likers list
        ListChangeListener<User> f = change -> {
            change.next();
            if (change.wasAdded()) {
                List<?extends User> addedList = change.getAddedSubList();
                if (addedList.contains(root.getModel().getUserById(root.getInterfaceToIhmMain().getIdConnectedUser()))) {
                    likeView.setImage(unlikeImage);
                }
            }
            if (change.wasRemoved()) {
                List<?extends User> removedList = change.getRemoved();
                if (removedList.contains(root.getModel().getUserById(root.getInterfaceToIhmMain().getIdConnectedUser()))) {
                    likeView.setImage(likeImage);
                }
            }
            updateLikesBox(msg);
        };
        msg.getLikersList().addListener(f);

        if (msg != null) {
            User author = msg.getAuthor();

            // Author null == system message, always TextMessage
            if (author == null) {
                nameLabel.setText("Système");
                dateLabel.setText(DT_FORMATTER.format(msg.getDate()));
                contentLabel.setText(((TextMessage) msg).getContent());
            }
            // Author not null == real message
            else {
                hoverBox.getChildren().add(likeView);

                if (author.getAvatar() != null) {
                    avatarView.setImage(author.getAvatar());
                } else {
                    avatarView.setImage(new Image("resources/icons/user.png")); // Default avatar
                }

                // Display a checkmark next to the avatar if the user is connected
                if (author.isConnected())
                    connectionView.setImage(new Image("resources/icons/connected.png"));

                // The displayed name is the nickname (specific to the channel)
                nameLabel.setText(msg.getAuthor().getNickName());

                dateLabel.setText(DT_FORMATTER.format(msg.getDate()));

                // Like hover button
                likeView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        root.getInterfaceToLocalCom().sendLike(msg.getId(), root.getInterfaceToIhmMain().getIdConnectedUser(), root.getModel().getIdChannel());
                    }
                });

                if (msg instanceof TextMessage) {
                    contentLabel.setText(((TextMessage) msg).getContent());
                    if (((TextMessage) msg).isEdited())
                        this.setEdited();

                    // Modification hover button
                    if (!msg.isDeleted() && author.getId().equals(root.getInterfaceToIhmMain().getIdConnectedUser())) {
                        hoverBox.getChildren().add(modifyView);
                        modifyView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent t) {
                                try {
                                    FXMLLoader modifyLoader = new FXMLLoader(getClass().getResource("../views/modifyView.fxml"));
                                    Parent modifyRoot = modifyLoader.load();
                                    ModifyMessageController modifyMessageController = modifyLoader.getController();
                                    modifyMessageController.init(root, msg);
                                    Stage stage = new Stage();
                                    stage.setScene(new Scene(modifyRoot));
                                    stage.setTitle("Modifier un message");
                                    stage.initModality(Modality.WINDOW_MODAL);
                                    stage.initOwner(
                                            ((Node) t.getSource()).getScene().getWindow());
                                    stage.show();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }
                }

                if (msg.isDeleted()) {
                    this.setDeleted();
                } else {
                    // Deletion hover button
                    if (author.getId().equals(root.getInterfaceToIhmMain().getIdConnectedUser())
                            || root.getModel().getUserById(root.getInterfaceToIhmMain().getIdConnectedUser()).getRole() == Subscription.Role.ADMIN) {
                        hoverBox.getChildren().add(deleteView);
                        deleteView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent t) {
                                try {
                                    FXMLLoader deleteLoader = new FXMLLoader(getClass().getResource("../views/deleteView.fxml"));
                                    Parent deleteRoot = deleteLoader.load();
                                    DeleteMessageController deleteMessageController = deleteLoader.getController();
                                    deleteMessageController.init(root, msg, hoverBox);
                                    Stage stage = new Stage();
                                    stage.setScene(new Scene(deleteRoot));
                                    stage.setTitle("Supprimer un message");
                                    stage.initModality(Modality.WINDOW_MODAL);
                                    stage.initOwner(
                                            ((Node) t.getSource()).getScene().getWindow());
                                    stage.show();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }

                    // Reply hover button
                    if (!isInThread) {
                        hoverBox.getChildren().add(replyView);
                        replyView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent t) {
                                openThread(msg);
                            }
                        });
                    }
                }
            }
        }
    }

    private void openThread(Message msg) {
        try {
            if (root.getCentralController().getMsgOpenedInThread() != msg) {
                FXMLLoader threadLoader = new FXMLLoader(getClass().getResource("../views/threadView.fxml"));
                Parent threadRoot = threadLoader.load();
                ThreadController threadController = threadLoader.getController();
                threadController.init(root, msg);

                // A thread was already open
                if (root.getCentralController().getMsgOpenedInThread() != null) {
                    root.getCentralThreadContainer().getChildren().remove(root.getCentralThreadContainer().getChildren().size() - 1);
                }

                root.getCentralController().setMsgOpenedInThread(msg);
                root.getCentralThreadContainer().getChildren().add(threadRoot);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Displays the message as edited
     */
    public void setEdited() {
        if (!contentBox.getChildren().contains(stateLabel))
            contentBox.getChildren().add(stateLabel);
        stateLabel.setText("(modifié)");
    }

    /**
     * Displays the message as deleted
     */
    public void setDeleted() {
        contentBox.getChildren().remove(contentLabel);
        if (!contentBox.getChildren().contains(stateLabel))
            contentBox.getChildren().add(stateLabel);
        hoverBox.getChildren().clear();
        stateLabel.setText("(supprimé)");
    }
}
