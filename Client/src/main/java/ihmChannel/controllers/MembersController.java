package ihmChannel.controllers;

import CommonClasses.Subscription;
import ihmChannel.IhmChannel;
import ihmChannel.models.User;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Members view controller
 */
public class MembersController {
    private static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("EE dd MMM YYYY - HH:mm");

    private IhmChannel root;
    private User currentUser;

    @FXML
    private ListView<User> currentMembersListView;
    @FXML
    private ListView<User> addMembersListView;
    @FXML
    private Accordion membersAccordion;
    @FXML
    private TitledPane currentMembersPane;
    @FXML
    private TextField searchUsersField;

    private FilteredList<User> currentMembersFilteredList;
    private FilteredList<User> addMembersFilteredList;

    /**
     * Initializes the controller:
     * - Assigns a cell factory for each users list
     * - Sets up listeners in case users connect or disconnect
     * - Initializes the lists with model data
     *
     * @param root the root channel controller
     */
    public void init(IhmChannel root) {
        this.root = root;
        this.currentUser = root.getModel().getUserById(root.getInterfaceToIhmMain().getIdConnectedUser());

        membersAccordion.setExpandedPane(currentMembersPane);

        // Assign cell factory to currentMembersListView
        currentMembersListView.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {
            @Override
            public ListCell<User> call(ListView<User> currentMembersListView) {
                    return new MemberCustomListCell();
                }
        });

        // Assign cell factory to addMembersListView
        addMembersListView.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {
            @Override
            public ListCell<User> call(ListView<User> addMembersListView) {
                return new NonMemberCustomListCell();
            }
        });

        // Use filtered lists to decide which users to display in each list
        currentMembersFilteredList = new FilteredList<>(this.root.getModel().getUserList(), this::currentMemberPredicate);
        currentMembersListView.setItems(currentMembersFilteredList);
        addMembersFilteredList = new FilteredList<>(this.root.getModel().getUserList(), this::addMemberPredicate);
        addMembersListView.setItems(addMembersFilteredList);

        /* Filter the users shown in addMembersListView.
         * The filter is updated after every keystroke.
         * Leaving the field empty displays the whole list (default behaviour).
         */
        this.searchUsersField.textProperty().addListener((obs, oldVal, newVal) -> {
            String filter = newVal.trim().toLowerCase();
            // Check if the text field is empty
            if (filter.equals("")) {
                addMembersFilteredList.setPredicate(this::addMemberPredicate);
            } else {
                // Display only the users whose login or name matches the filter
                Predicate<User> filteringPredicate = (u -> {
                    if (u.getLogin().toLowerCase().contains(filter))
                        return true;
                    else if (u.getFirstName() != null && u.getFirstName().toLowerCase().contains(filter))
                        return true;
                    else if (u.getLastName() != null && u.getLastName().toLowerCase().contains(filter))
                        return true;
                    else
                        return false;
                });
                addMembersFilteredList.setPredicate(filteringPredicate.and(this::addMemberPredicate));
            }
        });
    }

    // Returns true if the user should be displayed in currentMembersListView
    private boolean currentMemberPredicate(User u) {
        return u.isMember() && u.isConnected() && u.getId() != null && !u.getId().equals(currentUser.getId());
    }

    // Returns true if the user should be displayed in addMembersListView
    private boolean addMemberPredicate(User u) {
        return !u.isMember() && u.isConnected()  && u.getId() != null && !u.getId().equals(currentUser.getId());
    }

    /**
     * Abstract custom cell used to display users
     */
    private abstract class CustomListCell extends ListCell<User> {
        private HBox mainBox;
        private ImageView avatarView;
        private Label nameLabel;
        private HBox spacerBox;
        private float avatarRadius = 15f;
        private String idUserSelected;

        /**
         * Instantiates a new Custom list cell.
         */
        public CustomListCell() {
            super();
            avatarView = new ImageView();
            avatarView.setFitHeight(avatarRadius * 2);
            avatarView.setFitWidth(avatarRadius * 2);
            avatarView.setClip(new Circle(avatarRadius, avatarRadius, avatarRadius));
            nameLabel = new Label();
            nameLabel.setFont(new Font(15));
            spacerBox = new HBox();
            HBox.setHgrow(spacerBox, Priority.ALWAYS);

            mainBox = new HBox(avatarView, nameLabel, spacerBox);
            mainBox.setSpacing(10);
        }

        /**
         * Updates the display when the data of a User changes.
         *
         * @param item  Changed User
         * @param empty True if the item is empty
         */
        @Override
        protected void updateItem(User item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null && !empty) { // Test for null item or empty parameter
                if (item.getAvatar() != null) {
                    avatarView.setImage(item.getAvatar());
                } else {
                    avatarView.setImage(new Image("resources/icons/user.png")); // Default avatar
                }

                String name = item.getLogin();
                if (item.getFirstName() != null || item.getLastName() != null) {
                    name = name.concat(" (" + item.getFirstName() + " " + item.getLastName() + ")");
                }
                nameLabel.setText(name);
                idUserSelected = item.getId();

                setGraphic(mainBox);
            } else {
                setGraphic(null);
            }
        }

        /**
         * Gets id user selected.
         *
         * @return the id user selected
         */
        public String getIdUserSelected() {
            return idUserSelected;
        }


        /**
         * Display a confirmation popup
         *
         * @param header  the header
         * @param content the content
         * @return an optional that can be used to get the result
         */
        protected Optional<ButtonType> displayConfirmationPopup(String header, String content) {
            Stage stage = (Stage) currentMembersPane.getScene().getWindow();
            Alert.AlertType type = Alert.AlertType.CONFIRMATION;
            Alert alert = new Alert(type, "");

            alert.initModality(Modality.APPLICATION_MODAL);
            alert.initOwner(stage);

            alert.getDialogPane().setHeaderText(header);
            alert.getDialogPane().setContentText(content);
            return alert.showAndWait();
        }
    }

    /**
     * Specialized custom cell for members
     */
    private class MemberCustomListCell extends CustomListCell {
        private HBox actionsBox;
        private Label roleLabel;
        private MenuButton actionsDropDown;

        /**
         * Instantiates a new Member custom list cell.
         */
        public MemberCustomListCell() {
            super();
            roleLabel = new Label();
            roleLabel.setFont(new Font(15));
            actionsBox = new HBox();
        }

        @Override
        protected void updateItem(User item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null && !empty) { // Test for null item or empty parameter
                // Get current time in epoch format
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                long now = cal.getTimeInMillis();

                // Change display depending on whether or not the user to display is banned
                if (item.getBanEndDate() == 0 || (item.getBanEndDate() != -1 && item.getBanEndDate() < now)) {
                    displayNormalUser(getRoleString(item));
                } else {
                    displayBannedUser();
                }

                // Listener on ban to dynamically update display if a user is banned/unbanned
                item.banEndDateProperty().addListener(
                        (obs, oldVal, newVal) -> {
                            if (newVal.longValue() == 0) {
                                displayNormalUser(getRoleString(item));
                            } else {
                                displayBannedUser();
                            }
                        }
                );

                if (!super.mainBox.getChildren().contains(actionsBox))
                    super.mainBox.getChildren().add(actionsBox);
            }
        }

        private String getRoleString(User user) {
            String role = "Undefined";
            if (user.getRole() != null) {
                switch (user.getRole()) {
                    case USER -> role = "Utilisateur";
                    case ADMIN -> role = "Administrateur";
                }
            }
            return role;
        }

        private void displayNormalUser(String role) {
            roleLabel.setText(role);

            // Display a different actions box depending on current user role & listed user role
            if (currentUser.getRole() == Subscription.Role.ADMIN
                    && root.getModel().getUserById(getIdUserSelected()).getRole() != Subscription.Role.ADMIN) {
                showAdminActionsBox();
            } else {
                showUserActionsBox();
            }

            // Listener on current user role to update actions box
            currentUser.roleProperty().addListener(
                    (obs, oldVal, newVal) -> {
                        if (newVal == Subscription.Role.ADMIN
                                && root.getModel().getUserById(getIdUserSelected()).getRole() != Subscription.Role.ADMIN) {
                            showAdminActionsBox();
                        } else {
                            showUserActionsBox();
                        }
                    }
            );

            // Listener on listed user role to update actions box
            root.getModel().getUserById(getIdUserSelected()).roleProperty().addListener(
                    (obs, oldVal, newVal) -> {
                        if (newVal != Subscription.Role.ADMIN
                                && currentUser.getRole() == Subscription.Role.ADMIN) {
                            showAdminActionsBox();
                        } else {
                            showUserActionsBox();
                        }
                    }
            );
        }

        private void displayBannedUser() {
            roleLabel.setText("Banni");
            if (currentUser.getRole() == Subscription.Role.USER) {
                showUserActionsBox();
            } else {
                showAdminActionsBoxForBanned();
            }
        }

        private void showAdminActionsBox() {
            // Clear actions box before adding elements to it (to avoid duplication)
            actionsBox.getChildren().clear();

            MenuItem menuItemKick = new MenuItem("Kicker/Bannir");
            MenuItem menuItemPromoteToAdmin = new MenuItem("Donner les droits d'administration");
            actionsDropDown = new MenuButton("", null, menuItemKick, menuItemPromoteToAdmin);
            actionsBox.getChildren().addAll(roleLabel, actionsDropDown);
            actionsBox.setSpacing(10);
            menuItemKick.setOnAction((event) -> {
                FXMLLoader kickLoader = new FXMLLoader(getClass().getResource("../views/kickMemberView.fxml"));
                try {
                    Parent kickRoot = kickLoader.load();
                    KickMemberController kickMemberController = kickLoader.getController();
                    kickMemberController.init(root, getIdUserSelected()); // passer un idUser
                    Stage stage = new Stage();
                    stage.setScene(new Scene(kickRoot));
                    stage.setTitle("Kicker/Bannir");
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.initOwner(actionsDropDown.getScene().getWindow());
                    stage.show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            menuItemPromoteToAdmin.setOnAction((event) -> {
                String headerText = "Êtes-vous sûr de vouloir donner les droits d'administrateur à " + root.getModel().getUserById(getIdUserSelected()).getLogin() + " ?";
                String contentText = "Cette action est irréversible.";
                Optional<ButtonType> result = displayConfirmationPopup(headerText, contentText);

                if (result.isPresent() && result.get() == ButtonType.OK) {
                    root.getInterfaceToLocalCom().promoteToAdmin(getIdUserSelected(), root.getModel().getIdChannel());
                }
            });
        }

        private void showUserActionsBox() {
            // Clear actions box before adding elements to it (to avoid duplication)
            actionsBox.getChildren().clear();

            actionsBox.getChildren().add(roleLabel);
        }

        private void showAdminActionsBoxForBanned() {
            // Clear actions box before adding elements to it (to avoid duplication)
            actionsBox.getChildren().clear();

            Button cancelBanButton = new Button("Annuler le ban");
            actionsBox.getChildren().addAll(roleLabel, cancelBanButton);
            actionsBox.setSpacing(10);

            User bannedUser = root.getModel().getUserById(getIdUserSelected());

            cancelBanButton.setOnAction((event) -> {
                String headerText = "Êtes-vous sûr de vouloir annuler le ban de " + bannedUser.getLogin() + " ?";
                String contentText = "Cet utilisateur a été banni du canal ";

                if (bannedUser.getBanEndDate() == -1) {
                    contentText = contentText.concat("jusqu'à annulation.");
                } else {
                    LocalDateTime endDate = Instant.ofEpochMilli(bannedUser.getBanEndDate()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                    contentText = contentText.concat("jusqu'au " + DT_FORMATTER.format(endDate) +".");
                }

                Optional<ButtonType> result = displayConfirmationPopup(headerText, contentText);

                if (result.isPresent() && result.get() == ButtonType.OK) {
                    root.getInterfaceToLocalCom().cancelKick(getIdUserSelected(), root.getModel().getIdChannel());
                }
            });
        }
    }

    /**
     * Specialized custom cell for non-members
     */
    private class NonMemberCustomListCell extends CustomListCell {
        private Button addButton;
        private ImageView addIcon;
        private float addIconSize = 15f;

        /**
         * Instantiates a new Non member custom list cell.
         */
        public NonMemberCustomListCell() {
            super();
            addIcon = new ImageView("resources/icons/add.png");
            addIcon.setFitHeight(addIconSize);
            addIcon.setFitWidth(addIconSize);
            addButton = new Button();
            addButton.setGraphic(addIcon);

            super.mainBox.getChildren().add(addButton);
        }

        @Override
        protected void updateItem(User item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null && !empty) {
                addButton.setOnAction(e -> {
                    root.getInterfaceToLocalCom().inviteUserToChannel(root.getModel().getIdChannel(), item.getId(), Subscription.Role.USER);
                });
            }
        }
    }
}
