package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import ihmChannel.models.TextMessage;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.util.Date;

public class ModifyMessageController {
    private IhmChannel root;
    private Message msg;
    @FXML
    TextArea messageContent;
    @FXML
    Button cancelButton;
    @FXML
    Button saveButton;

    @FXML
    public void onCancel(ActionEvent ae) {
        cancelButton.getScene().getWindow().hide();
    }

    @FXML
    public void onSave(ActionEvent ae) {
        String newContent = this.messageContent.getText();
        String idMessage = msg.getId();
        String idChannel = root.getModel().getIdChannel();
        root.getInterfaceToLocalCom().sendModifiedMsg(newContent, idMessage, idChannel);
        saveButton.getScene().getWindow().hide();
    }

    public void init(IhmChannel root, Message msg) {
        this.root = root;
        this.msg = msg;
        this.messageContent.setText(((TextMessage)msg).getContent());
    }
}
