package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.User;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

/**
 * Menu bar controller
 */
public class MenuController {
    private IhmChannel root;

    @FXML
    private Label channelNameLabel;
    @FXML
    private Label channelDescLabel;
    @FXML
    private Button membersButton;
	@FXML
	private MenuItem deleteChannelItem;
	@FXML
	private HBox menuContainer;
	@FXML
	private MenuItem changeNicknameItem;

	/**
	 * Initializes the controller by setting up event listeners
	 *
	 * @param root the root channel controller
	 */
	public void init(IhmChannel root) {
    	this.root = root;

		channelNameLabel.setText("");
		channelDescLabel.setText("");

    	// Listeners for channelName and channelDesc
    	this.root.getModel().channelNameProperty().addListener(
    			(obs, oldVal, newVal) -> { channelNameLabel.setText(newVal); }
    	);
    	this.root.getModel().channelDescProperty().addListener(
    			(obs, oldVal, newVal) -> { channelDescLabel.setText(newVal); }
    	);

    	// Display the delete channel menu item only to the creator of the channel
		this.root.getModel().idChannelCreatorProperty().addListener(
				(obs, oldVal, newVal) -> {
					if (newVal != null
							&& newVal.equals(this.root.getInterfaceToIhmMain().getIdConnectedUser())) {
						deleteChannelItem.setVisible(true);
					} else {
						deleteChannelItem.setVisible(false);
					}
				}
		);

    	// Listener for number of members
		this.root.getModel().connectedMembersCountProperty().addListener(
				(obs, oldVal, newVal) -> { membersButton.setText(String.valueOf(newVal)); }
		);

    	// Open members window when members button is clicked
    	membersButton.setOnAction((event) -> {
    		FXMLLoader membersLoader = new FXMLLoader(getClass().getResource("../views/membersView.fxml"));
    		try {
    			Parent membersRoot = membersLoader.load();
    			Scene membersScene = new Scene(membersRoot);
    			MembersController membersController = membersLoader.getController();
    			membersController.init(this.root);

    			Stage membersStage = new Stage();
    			membersStage.initOwner(membersButton.getScene().getWindow());
    			membersStage.initModality(Modality.WINDOW_MODAL);
    			membersStage.setScene(membersScene);
    			membersStage.show();
    		} catch (IOException e) {
    			throw new RuntimeException(e);
    		}
    	});

    	// Clear the channel when item menu is clicked
		deleteChannelItem.setOnAction((event) -> {
			Stage stage = (Stage) menuContainer.getScene().getWindow();
			Alert.AlertType type = Alert.AlertType.CONFIRMATION;
			Alert alert = new Alert(type, "");

			alert.initModality(Modality.APPLICATION_MODAL);
			alert.initOwner(stage);

			alert.getDialogPane().setHeaderText("Êtes-vous sûr de vouloir supprimer le channel ?");
			alert.getDialogPane().setContentText("Cette action est irréversible");
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				this.root.getInterfaceToLocalCom().deleteChannel(this.root.getModel().getIdChannel());
				this.root.getModel().clearChannel();
			}
		});

		changeNicknameItem.setOnAction((event) -> {
			Stage stage = (Stage) menuContainer.getScene().getWindow();
			String userId = this.root.getInterfaceToIhmMain().getIdConnectedUser();
			TextInputDialog d = new TextInputDialog(this.root.getModel().getUserById(userId).getNickName());
			d.initModality(Modality.APPLICATION_MODAL);
			d.initOwner(stage);

			d.setTitle("Choisir un surnom");
			d.setContentText("Veuillez entrer votre surnom:");
			Optional<String> result = d.showAndWait();

			if (result.isPresent()
					&& !result.get().equals("")
					&& !result.get().equals(this.root.getModel().getUserById(userId).getNickName())) {
				this.root.getInterfaceToLocalCom().changeNickname(result.get(), userId, this.root.getModel().getIdChannel());
			}
		});
    }
}
