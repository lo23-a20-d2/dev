package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;

import java.util.List;


/**
 * Central view controller
 */
public class CentralController {
    private IhmChannel root;

    @FXML
    private TextArea inputMessageField;
    @FXML
    private ScrollPane messagesScrollPane;
    @FXML
    private VBox messagesBox;

    private Message msgOpenedInThread;

    /**
     * Gets msg opened in thread.
     *
     * @return the msg opened in thread
     */
    public Message getMsgOpenedInThread() {
        return msgOpenedInThread;
    }

    /**
     * Sets msg opened in thread.
     *
     * @param msgOpenedInThread the msg opened in thread
     */
    public void setMsgOpenedInThread(Message msgOpenedInThread) {
        this.msgOpenedInThread = msgOpenedInThread;
    }

    /**
     * Initializes the controller by setting up event listeners
     *
     * @param root the root channel controller
     */
    public void init(IhmChannel root) {
        this.root = root;

        // Handles the sending of a new message whose content is written in inputMessageField
        inputMessageField.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER)  {
                String value = inputMessageField.getText();

                // Check if the inputField is empty
                if (!value.trim().equals("")) {
                    // Get sender and channel id
                    String idSender = root.getInterfaceToIhmMain().getIdConnectedUser();
                    String idChannel = root.getModel().getIdChannel();

                    // Send message to LocalCom
                    root.getInterfaceToLocalCom().sendNewTextMsg(value, idSender, idChannel);

                    // Clean the input field
                    inputMessageField.setText("");
                }
            }
        });

        // Auto-scroll messages zone to bottom (newer messages)
        messagesBox.heightProperty().addListener(observable -> messagesScrollPane.setVvalue(1D));

        // Listener on list of channel messages
        ListChangeListener<Message> f = change -> {
            change.next();
            if (change.wasAdded()) {
                List<? extends Message> addedMessages = change.getAddedSubList();
                addedMessages.forEach(msg -> {
                    if (msg.getId() == null                                 // Display system messages
                            || msg.getId().equals(msg.getIdThread())) {     // Do not display replies in central messages box
                        MessageContainer msgContainer = new MessageContainer(msg, root);
                        msgContainer.fill(msg);
                        messagesBox.getChildren().add(msgContainer);
                    }
                });
            }
        };
        this.root.getModel().getMessageList().addListener(f);
    }

    /**
     * Clears all messages from the view
     */
    public void clearAllMessages() {
        messagesBox.getChildren().clear();
    }

}
