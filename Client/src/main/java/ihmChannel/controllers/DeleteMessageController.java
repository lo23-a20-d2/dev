package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import ihmChannel.models.TextMessage;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.control.TextArea;

import java.util.Date;

public class DeleteMessageController {
    private IhmChannel root;
    private Message msg;
    private HBox hoverBox;

    @FXML
    Button cancelButton;
    @FXML
    Button confirmButton;

    @FXML
    public void onCancel(ActionEvent ae) {
        cancelButton.getScene().getWindow().hide();
    }

    @FXML
    public void onConfirm(ActionEvent ae) {
        String idMessage = msg.getId();
        String idChannel = root.getModel().getIdChannel();
        root.getInterfaceToLocalCom().sendDeletedMsg(idMessage, idChannel);
        confirmButton.getScene().getWindow().hide();
        hoverBox.setVisible(false);
    }

    public void init(IhmChannel root, Message msg, HBox hoverBox) {
        this.root = root;
        this.msg = msg;
        this.hoverBox = hoverBox;
    }
}
