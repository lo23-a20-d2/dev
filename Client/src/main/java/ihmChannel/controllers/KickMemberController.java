package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KickMemberController {
    private IhmChannel root;
    private String idUserToKick;

    @FXML
    private Button cancelButton;
    @FXML
    private Button confirmButton;
    @FXML
    private ToggleGroup tgTypeKick;
    @FXML
    private RadioButton definitiveBan;
    @FXML
    private RadioButton simpleKick;
    @FXML
    private RadioButton temporaryBan;
    @FXML
    private ComboBox<String> endDateMenu;




    @FXML
    public void onCancel(ActionEvent ae) {
        this.cancelButton.getScene().getWindow().hide();
    }

    @FXML
    public void onConfirm(ActionEvent ae) {
        long endDate;
        RadioButton selected = (RadioButton)this.tgTypeKick.getSelectedToggle();
        if (selected == this.simpleKick) {
            endDate = 0;
        } else if (selected == this.definitiveBan) {
            endDate = -1;
        } else {
            String endDateItem;
            Date currentDate = new Date();
            long currentTimestamp = currentDate.getTime();
            endDateItem = endDateMenu.getValue();
            endDate = switch (endDateItem) {
                case "1h" -> currentTimestamp + 3600 * 1000;
                case "2h" -> currentTimestamp + 7200 * 1000;
                case "1 jour" -> currentTimestamp + 3600 * 1000 * 24;
                case "1 semaine" -> currentTimestamp + 3600 * 1000 * 24 * 7;
                default -> 0; // Safety default (should not be reached), simple kick
            };
        }
        root.getInterfaceToLocalCom().kickMember(root.getInterfaceToIhmMain().getIdConnectedUser(), root.getModel().getIdChannel(), idUserToKick, endDate);
        confirmButton.getScene().getWindow().hide();
    }

    public void init(IhmChannel root, String idUser) {
        this.root = root;
        this.idUserToKick = idUser;
        this.definitiveBan.setToggleGroup(tgTypeKick);
        this.simpleKick.setToggleGroup(tgTypeKick);
        this.definitiveBan.setToggleGroup(tgTypeKick);
        this.simpleKick.setSelected(true);

        ObservableList<String> timeOfBan = FXCollections.observableArrayList("1h", "2h", "1 jour", "1 semaine");
        endDateMenu.setItems(timeOfBan);
        endDateMenu.getSelectionModel().selectFirst();
    }
}
