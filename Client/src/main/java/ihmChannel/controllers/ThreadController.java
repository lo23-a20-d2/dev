package ihmChannel.controllers;

import ihmChannel.IhmChannel;
import ihmChannel.models.Message;
import ihmChannel.models.TextMessage;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class ThreadController {
    private IhmChannel root;
    private Message rootMessage;

    @FXML
    private VBox threadContainer;
    @FXML
    private TextField threadInputMessageField;
    @FXML
    private VBox threadMessagesBox;
    @FXML
    private ImageView threadCloseImageButton;
    @FXML
    private Label threadChannelNameLabel;

    public void init(IhmChannel root, Message rootMessage) {
        this.root = root;
        this.rootMessage = rootMessage;
        threadChannelNameLabel.setText(this.root.getModel().getChannelName());

        // Add root & replies to message box
        displayMessage(rootMessage);
        rootMessage.getRepliesList().forEach(this::displayMessage);

        // Listener for new replies
        ListChangeListener<Message> f = change -> {
            change.next();
            if (change.wasAdded()) {
                List<? extends Message> addedMessages = change.getAddedSubList();
                addedMessages.forEach(this::displayMessage);
            }
        };
        rootMessage.getRepliesList().addListener(f);

        threadCloseImageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                root.getCentralThreadContainer().getChildren().remove(threadContainer);
                root.getCentralController().setMsgOpenedInThread(null);
            }
        });
    }

    private void displayMessage(Message msg) {
        MessageContainer replyContainer = new MessageContainer(msg, root, true);
        replyContainer.fill(msg);
        threadMessagesBox.getChildren().add(replyContainer);
    }

    /**
     * Handles the sending of a reply whose content is written in inputMessageField
     *
     * @param ae event generated when the "Enter" key is pressed in inputMessageField
     */
    @FXML
    public void onEnter(ActionEvent ae) {
        String value = threadInputMessageField.getText();
        // Check if the inputField is empty
        if (!value.trim().equals("")) {
            // Get sender id, channel id, and thread id
            String idSender = root.getInterfaceToIhmMain().getIdConnectedUser();
            String idChannel = root.getModel().getIdChannel();
            String idThread = rootMessage.getIdThread();

            System.out.println("Root of reply : id = " + rootMessage.getId() + " idThread = " + rootMessage.getIdThread());
            // Send reply to LocalCom
            root.getInterfaceToLocalCom().sendNewTextReply(value, idSender, idChannel, idThread);

            // Clean the input field
            threadInputMessageField.setText("");
        }
    }
}
