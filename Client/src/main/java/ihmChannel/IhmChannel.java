package ihmChannel;

import CommonClasses.SharedChannel;
import CommonClasses.Subscription;
import CommonClasses.Subscription.Role;
import Interfaces.IhmChannelToIhmMain;
import Interfaces.IhmChannelToLocalCom;
import Interfaces.IhmMainToIhmChannel;
import Interfaces.LocalComToIhmChannel;
import ihmChannel.controllers.CentralController;
import ihmChannel.controllers.MenuController;
import ihmChannel.interfaces.impl.IhmMainToIhmChannelImpl;
import ihmChannel.interfaces.impl.LocalComToIhmChannelImpl;
import ihmChannel.models.Model;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.Date;

/**
 * Main controller for the IhmChannel module
 */
public class IhmChannel {
    @FXML
    private MenuController menuController;
    @FXML
    private CentralController centralController;
    @FXML
    private HBox centralThreadContainer;

    private LocalComToIhmChannel interfaceForLocalCom;
    private IhmMainToIhmChannel interfaceForIhmMain;
    private IhmChannelToLocalCom interfaceToLocalCom;
    private IhmChannelToIhmMain interfaceToIhmMain;
    private Model model;

    /**
     * Instantiates a new Ihm channel.
     * Also creates interfaces for the other modules.
     */
    public IhmChannel() {
        this.interfaceForLocalCom = new LocalComToIhmChannelImpl(this);
        this.interfaceForIhmMain = new IhmMainToIhmChannelImpl(interfaceForLocalCom);
    }

    /**
     * Initializes the interfaces to other modules.
     * Creates the local Model and initializes it.
     * Initializes the sub-controllers.
     * Creates the "system" pseudo-user (used to display system messages).
     *
     * @param interfaceToLocalCom the interface to local com
     * @param interfaceToIhmMain  the interface to ihm main
     */
    public void init(IhmChannelToLocalCom interfaceToLocalCom, IhmChannelToIhmMain interfaceToIhmMain) {
        this.interfaceToLocalCom = interfaceToLocalCom;
        this.interfaceToIhmMain = interfaceToIhmMain;

        this.model = new Model();
        this.model.init();
        this.menuController.init(this);
        this.centralController.init(this);
    }

    /**
     * Gets menu controller.
     *
     * @return the menu controller
     */
    public MenuController getMenuController() {
        return menuController;
    }

    /**
     * Gets central controller.
     *
     * @return the central controller
     */
    public CentralController getCentralController() {
        return centralController;
    }

    /**
     * Gets central thread container.
     *
     * @return the central thread container
     */
    public HBox getCentralThreadContainer() { return centralThreadContainer; }

    /**
     * Gets model.
     *
     * @return the model
     */
    public Model getModel() {
        return this.model;
    }

    /**
     * Gets interface for local com.
     *
     * @return the interface for local com
     */
    public LocalComToIhmChannel getInterfaceForLocalCom() {
        return interfaceForLocalCom;
    }

    /**
     * Gets interface for ihm main.
     *
     * @return the interface for ihm main
     */
    public IhmMainToIhmChannel getInterfaceForIhmMain() {
        return interfaceForIhmMain;
    }

    /**
     * Gets interface to local com.
     *
     * @return the interface to local com
     */
    public IhmChannelToLocalCom getInterfaceToLocalCom() {
        return interfaceToLocalCom;
    }

    /**
     * Gets interface to ihm main.
     *
     * @return the interface to ihm main
     */
    public IhmChannelToIhmMain getInterfaceToIhmMain() {
        return interfaceToIhmMain;
    }

    /**
     * Opens a test channel.
     */
    public void openTestChannel() {

        ArrayList<CommonClasses.User> users = new ArrayList<>();
        users.add(new CommonClasses.User("0", "user0", "pwd", "Charlie", "Chaplin", new Date(), null));
        users.add(new CommonClasses.User("1", "user1", "pwd", "David", "Bowie", new Date(), null));
        users.add(new CommonClasses.User("2", "user2", "pwd", "Lemmy", "Kilmister", new Date(), null));
        users.add(new CommonClasses.User("3", "user3", "pwd", "Chris", "Harms", new Date(), null));

        SharedChannel channel = new SharedChannel("1","Test Channel", "Test Description", false, users.get(0));

        ArrayList<CommonClasses.Message> messages = new ArrayList<>();
        messages.add(new CommonClasses.TextMessage("0", "0", new Date(1605398400000L), false, false, "0", "0", "Hello"));
        messages.add(new CommonClasses.TextMessage("1", "1", new Date(1605539003000L), false, false, "1", "0", "Hi"));
        messages.add(new CommonClasses.TextMessage("2", "2", new Date(1615539003000L), false, false, "3", "0", "Hey ! Doing good ?"));
        messages.add(new CommonClasses.TextMessage("3", "3", new Date(1625539003000L), false, false, "1", "0", "Meh"));
        messages.add(new CommonClasses.TextMessage("4", "4", new Date(1635539003000L), false, false, "2", "0", "Hi guys"));
        messages.add(new CommonClasses.TextMessage("5", "5", new Date(1645539003000L), false, false, "3", "0", "Hi !"));
        messages.add(new CommonClasses.TextMessage("6", "6", new Date(1655539003000L), false, false, "0", "0", "Hi"));

        ArrayList<Subscription> subscriptions = new ArrayList<>();
        subscriptions.add(new Subscription("0", users.get(0), channel, "Charlot", Role.ADMIN));
        subscriptions.add(new Subscription("1", users.get(1), channel, "Starman", Role.USER));
        subscriptions.add(new Subscription("2", users.get(2), channel, "Lemmy", Role.ADMIN));
        subscriptions.add(new Subscription("3", users.get(3), channel, "The Lord", Role.USER));

        this.interfaceForLocalCom.displayChannel(
                channel,
                messages,
                subscriptions);
    }
}
