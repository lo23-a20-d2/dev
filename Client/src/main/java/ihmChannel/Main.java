package ihmChannel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;

/**
 * Local Main class, only for testing purposes.
 */
public class Main extends Application {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Loads the root channel FXML file.
     *
     * @param primaryStage Displayed stage
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Locale.setDefault(Locale.FRANCE);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/windowView.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        IhmChannel ihmChannel = loader.getController();
//        ihmChannel.init(); // TODO: provide interfaces

        primaryStage.setTitle("Nom de l'application");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);

        primaryStage.show();

    }
}
