//package com;
//
//import CommonClasses.*;
//import data.impl.CommToLocalDataImpl;
//import ihmChannel.interfaces.impl.LocalComToIhmChannelImpl;
//import ihmMain.interfaces.impl.LocalComToIhmMainImpl;
//import message.client.*;
//import message.server.*;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import java.io.IOException;
//import java.sql.Date;
//import java.util.ArrayList;
//
//import static org.mockito.Mockito.*;
//
//public class ServerMessageHandlerTest {
//    private LocalCom com;
//    private CommToLocalDataImpl dataInterface;
//    private LocalComToIhmChannelImpl ihmChannelInterface;
//    private LocalComToIhmMainImpl ihmMainInterface;
//
//    static String content = "bonjour";
//    static String idMsg = "iiiiii";
//    static String idChannel = "cccccccc";
//    static String idSubscription = "ssssssssss";
//    static String idThread = "threeeeeadXASDSFADS";
//    static TextMessage txt = new TextMessage("ttttt", idThread,
//            new Date(System.currentTimeMillis()), false, false, idSubscription, idChannel, content);
//    static OwnedChannel ownedChannel = new OwnedChannel();
//    static SharedChannel sharedChannel = new SharedChannel("shareddddc");
//    static ArrayList<Channel> channelList = new ArrayList<>();
//    static ArrayList<User> userList = new ArrayList<>();
//    static ArrayList<Subscription> subList = new ArrayList<>();
//    static User u = new User("uuuu");
//    static Subscription.Role role = Subscription.Role.USER;
//    static ArrayList<Message> messageList = new ArrayList<>();
//
//
//    public ServerMessageHandlerTest() {
//        this.com = mock(LocalCom.class);
//        this.dataInterface = mock(CommToLocalDataImpl.class);
//        this.ihmChannelInterface = mock(LocalComToIhmChannelImpl.class);
//        this.ihmMainInterface = mock(LocalComToIhmMainImpl.class);
//    }
//
//    @BeforeEach
//    public void SetUp() {
//        when(com.getDataInterface()).thenReturn(dataInterface);
//        when(com.getIhmChannelInterface()).thenReturn(ihmChannelInterface);
//        when(com.getIhmMainInterface()).thenReturn(ihmMainInterface);
//    }
//
//    @Test
//    public void InviteToChannelServerMessageTest() throws IOException {
//        InviteToChannelServerMessage msg = new InviteToChannelServerMessage(u, idChannel, role);
//        msg.handle(this.com);
//        verify(dataInterface).addSubscription(idChannel, u, role);
//        verify(com).sendMessage(any(SupplyNewMemberClientMessage.class));
//        verify(com, times(1)).sendMessage(any(SupplyNewMemberClientMessage.class));
//        verify(com, times(1)).sendMessage(any(ClientMessage.class));
//    }
//
//    @Test
//    public void ModifyMsgServerMessageTest() throws IOException {
//        ModifyMsgServerMessage msg = new ModifyMsgServerMessage(content, idMsg, idChannel);
//        msg.handle(com);
//        verify(com, times(1)).sendMessage(any(SupplyModifiedMsgClientMessage.class));
//        verify(com, times(1)).sendMessage(any(ClientMessage.class));
//    }
//
//    @Test
//    public void PutModifiedMsgServerMessageTest() {
//        PutModifiedMsgServerMessage msg = new PutModifiedMsgServerMessage(txt);
//        msg.handle(com);
//        verify(ihmChannelInterface, times(1)).displayModifiedMsg(txt);
//    }
//
//    @Test
//    public void NotifyDeletedChannelServerMessageTest() {
//        //A completer
//    }
//
//    @Test
//    public void RequestDeleteOwnedChannelServerMessage() {
//        requestDeleteOwnedChannelServerMessage msg = new requestDeleteOwnedChannelServerMessage(idChannel);
//        msg.handle(com);
//        verify(com.getDataInterface(), times(1)).deleteOwnedChannel(idChannel);
//    }
//
//    @Test
//    public void SearchChannelServerMessage() {
//        channelList.add(ownedChannel);
//        channelList.add(sharedChannel);
//        SearchChannelServerMessage msg = new SearchChannelServerMessage(channelList);
//        msg.handle(com);
//        verify(com.getIhmMainInterface(), times(1)).displaySearchResult(channelList);
//    }
//
//    @Test
//    public void PutDeletedMsgServerMessage() {
//        PutDeleteMsgServerMessage msg = new PutDeleteMsgServerMessage(idMsg, idChannel);
//        when(com.getDataInterface().getChannelOwner(idChannel)).thenReturn(null);
//        //if data method returns null as creator
//        msg.handle(com);
//        verify(com.getDataInterface(), never()).deleteMessage(idMsg);
//        verify(com.getIhmChannelInterface(), times(1)).displayDeletedMsg(idMsg, idChannel);
//
//        //if returns a creator which id is not equal to the one of ihmMain method
//        when(com.getDataInterface().getChannelOwner(idChannel)).thenReturn(u);
//        msg.handle(com);
//        verify(com.getDataInterface(), times(0)).deleteMessage(idMsg);
//        verify(com.getIhmChannelInterface(), times(2)).displayDeletedMsg(idMsg, idChannel);
//
//        //creator id is equal to the one of ihmMain method
//        when(com.getIhmMainInterface().getIdConnectedUser()).thenReturn(u.getId());
//        msg.handle(com);
//        verify(com.getDataInterface(), times(1)).deleteMessage(idMsg);
//        verify(com.getIhmChannelInterface(), times(3)).displayDeletedMsg(idMsg, idChannel);
//    }
//
//    @Test
//    public void NotifyUserDeconnexionServerMessageTest() {
//        NotifyUserDeconnexionServerMessage msg = new NotifyUserDeconnexionServerMessage(u.getId());
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).notifyQuitChannel(u.getId());
//    }
//
//    @Test
//    public void NotifyClientDeconnexionServerMessageTest() {
//        NotifyClientDeconnexionServerMessage msg = new NotifyClientDeconnexionServerMessage(u.getId());
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).sendUserDisconnectedFromServer(u.getId());
//    }
//
//    @Test
//    public void SendUserChannelListServerMessageTest() {
//        channelList.add(ownedChannel);
//        channelList.add(sharedChannel);
//        userList.add(u);
//        userList.add(new User("ussssssss"));
//        subList.add(new Subscription());
//        subList.add(new Subscription());
//        SendUserChannelListServerMessage msg = new SendUserChannelListServerMessage(channelList, userList, subList);
//        msg.handle(com);
//        verify(com.getIhmMainInterface(), times(1)).updateConnectedChannelList(channelList, subList);
//        verify(com.getIhmChannelInterface(), times(1)).sendConnectedUsersServerList(userList);
//    }
//
//    @Test
//    public void NotifyNewUserConnectedToServerServerMessageTest() {
//        NotifyNewUserConnectedToServerServerMessage msg = new NotifyNewUserConnectedToServerServerMessage(u);
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).sendNewUserConnectedToServer(u);
//    }
//
//    @Test
//    public void JoinChannelServerMessageTest() throws IOException {
//        when(com.getDataInterface().getOwnedChannel(idChannel)).thenReturn(ownedChannel);
//        JoinChannelServerMessage msg = new JoinChannelServerMessage(idChannel, u.getId());
//        msg.handle(com);
//        verify(com.getDataInterface(), times(1)).getOwnedChannel(idChannel);
//        verify(com.getDataInterface(), times(1)).getMessages(idChannel);
//        verify(com, times(1)).sendMessage(any(SupplyChannelContentClientMessage.class));
//        verify(com, times(1)).sendMessage(any(ClientMessage.class));
//    }
//
//    @Test
//    public void NotifyChannelConnnectionServerMessageTest() {
//        NotifyChannelConnnectionServerMessage msg = new NotifyChannelConnnectionServerMessage(sharedChannel, messageList, subList);
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).displayChannel(sharedChannel, messageList, subList);
//    }
//
//    @Test
//    public void NewTextMsgServerMessageTest() throws IOException {
//        NewTextMsgServerMessage msg = new NewTextMsgServerMessage(content, u.getId(), idChannel);
//        msg.handle(com);
//        verify(com.getDataInterface(), times(1)).addTextMessage(content, u.getId(), idChannel);
//        verify(com, times(1)).sendMessage(any(SupplyNewTextMsgClientMessage.class));
//    }
//
//    @Test
//    public void PutNewTextMsgServerMessageTest() {
//        PutNewTextMsgServerMessage msg = new PutNewTextMsgServerMessage(txt);
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).displayNewTextMsg(txt);
//    }
//
//    @Test
//    public void NewChannelServerMessageTest() {
//        NewChannelServerMessage msg = new NewChannelServerMessage(sharedChannel, subList);
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).displayChannel(eq(sharedChannel), any(ArrayList.class), eq(subList));
//        verify(com.getIhmMainInterface()).displayNewChannel(sharedChannel);
//    }
//
//    @Test
//    public void NotifyNewChannelServerMessageTest() {
//        NotifyNewChannelServerMessage msg = new NotifyNewChannelServerMessage(sharedChannel);
//        msg.handle(com);
//        verify(com.getIhmMainInterface(), times(1)).newPublicChannel(sharedChannel);
//    }
//
//    @Test
//    public void NewSubscriptionServerMessageTest() {
////        NewSubscriptionServerMessage msg = new NewSubscriptionServerMessage(idChannel,u.getId());
////        msg.handle(com);
//
//    }
//
//    @Test
//    public void ProvideChannelToMemberServerMessageTest() {
//        ProvideChannelToMemberServerMessage msg = new ProvideChannelToMemberServerMessage(sharedChannel, messageList, subList);
//        msg.handle(com);
//        verify(com.getIhmMainInterface(), times(1)).displayNewChannel(sharedChannel);
//        verify(com.getIhmChannelInterface(), times(1)).displayChannel(sharedChannel, messageList, subList);
//    }
//
//    @Test
//    public void NotifyNewMemberServerMessageTest() {
//        Subscription sub = new Subscription();
//        NotifyNewMemberServerMessage msg = new NotifyNewMemberServerMessage(sub);
//        msg.handle(com);
//        verify(com.getIhmChannelInterface(), times(1)).displayNewMemberGreeting(sub);
//    }
//
//
//}
