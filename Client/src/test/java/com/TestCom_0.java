package com;

import CommonClasses.User;
import Interfaces.IClientManager;
import Interfaces.IServerCom;
import data.LocalData;
import ihmChannel.IhmChannel;
import ihmMain.IhmMain;
import message.client.NewTextMsgClientMessage;
import org.junit.jupiter.api.*;
import org.mockito.Mock;

import java.io.IOException;

public class TestCom_0 {
    static LocalCom com;
    @Mock IServerCom testServer;
    @Mock IClientManager testManager;
    static NewTextMsgClientMessage msg;

    static String host = "192.168.1.19";
    static User testUser;

    @BeforeAll
    @DisplayName("Setup")
    static void setup() throws IOException {
        // Create test user
        testUser = new User();
        testUser.setId("testIdUser");
        testUser.setLogin("testUserLogin");
        testUser.setPassword("testUserPwd");

        // Launch localCom
        com = new LocalCom();
    }

    @Test
    @DisplayName("Base assertion test")
    void assertTest(){
        Assertions.assertAll(
                () -> Assertions.assertEquals(msg.getMsgContent(), "test content"),
                () -> Assertions.assertEquals(msg.getIdSender(), "testIdSender"),
                () -> Assertions.assertEquals(msg.getIdChannel(), "testIdChannel")
        );
    }

    @Test
    @DisplayName("Server connection test")
    void connectionTest() throws IOException {
        // Connects to server
        com.getSocket();
        com.connectToServer(host);
    }

    @Test
    @DisplayName("Send message test")
    void sendMessageTest() throws IOException {
        // Creates message to test
        msg = new NewTextMsgClientMessage("test content", "testIdSender", "testIdChannel");

        // Send message
        //com.sendMessage(msg);
    }


}
