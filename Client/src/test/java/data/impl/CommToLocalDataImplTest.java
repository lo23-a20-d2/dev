package data.impl;

import CommonClasses.*;
import data.LocalData;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the CommToLocalDataImpl class
 * @author Benjamin Vaysse <benjamin.vaysse@etu.utc.fr>
 */
class CommToLocalDataImplTest {
    private final LocalData localData;
    private User tintin;
    private User haddock;
    private final OwnedChannel publicOwnedChannel;

    /**
     * Instantiates a new Comm to local data impl test.
     */
    public CommToLocalDataImplTest() {
        localData = new LocalData();
        tintin = localData.getIhmMainToLocalData().saveNewAccount("tintin", "milou", "Tintin", "Le célèbre reporter", null, null);
        if(tintin == null){
            tintin = localData.getIhmMainToLocalData().authenticate("tintin", "milou");
        }
        haddock = localData.getIhmMainToLocalData().saveNewAccount("haddock", "cognac", "Captain Archibald", "Haddock", null, null);
        if(haddock == null){
            haddock = localData.getIhmMainToLocalData().authenticate("haddock", "cognac");
        }
        publicOwnedChannel = localData.getIhmMainToLocalData().createOwnedChannel(
                "Le Trésor de Rackham le Rouge",
                "Red Rackham's Treasure (French: Le Trésor de Rackham le Rouge) is the twelfth volume of " +
                        "The Adventures of Tintin, the comics series by Belgian cartoonist Hergé. The story was" +
                        " serialised daily in Le Soir, Belgium's leading francophone newspaper, from February to " +
                        "September 1943 amidst the German occupation of Belgium during World War II. Completing an" +
                        " arc begun in The Secret of the Unicorn, the story tells of young reporter Tintin and his " +
                        "friend Captain Haddock as they launch an expedition to the Caribbean to locate the treasure" +
                        " of the pirate Red Rackham.",
                false,
                tintin
        );
        localData.getCommToLocalData().addSubscription(
                publicOwnedChannel.getId(),
                tintin,
                Subscription.Role.ADMIN
        );
    }
    /**
     * Add file message.
     */
    @Test
    void addFileMessage() {
        // v4
    }

    /**
     * Add file reply.
     */
    @Test
    void addFileReply() {
        // v4
    }

    /**
     * Gets message.
     */
    @Test
    void getMessage() {
        Message message = localData.getCommToLocalData().addTextMessage("Coucou", tintin.getId(), publicOwnedChannel.getId());
        Message retreivedMessage = localData.getCommToLocalData().getMessage(publicOwnedChannel.getId(), message.getId());
        assertEquals(message.getId(), retreivedMessage.getId());
    }

    /**
     * Modify message.
     */
    @Test
    void modifyMessage() {
        TextMessage textMessage = localData.getCommToLocalData().addTextMessage("Coucou", tintin.getId(), publicOwnedChannel.getId());
        TextMessage modifiedTextMessage = localData.getCommToLocalData().modifyMessage("Aurevoir", textMessage.getId());
        assertEquals( "Aurevoir", modifiedTextMessage.getTextContent());
        assertTrue(modifiedTextMessage.isEdited());
    }

    /**
     * Delete message.
     */
    @Test
    void deleteMessage() {
        Message message = localData.getCommToLocalData().addTextMessage("Coucou", tintin.getId(), publicOwnedChannel.getId());
        localData.getCommToLocalData().deleteMessage(message.getId());
        Message deletedMessage = localData.getCommToLocalData().getMessage(publicOwnedChannel.getId(), message.getId());
        assertTrue(deletedMessage.isErased());
    }

    /**
     * Deletes an owned channel.
     * First deletes all messages (CRUD delete)
     * Then deletes all subscriptions  (CRUD delete)
     */
    @Test
    void deleteOwnedChannel() {
        localData.getCommToLocalData().deleteOwnedChannel(publicOwnedChannel.getId());
        assertNull(localData.getCommToLocalData().getOwnedChannel(publicOwnedChannel.getId()));
    }

    /**
     * Gets all owned channel.
     */
    @Test
    void getAllOwnedChannel() {
        OwnedChannel publicOwnedChannel2 = localData.getIhmMainToLocalData().createOwnedChannel(
                "Tintin in America",
                "Tintin in America (French: Tintin en Amérique) is the third volume of The Adventures of Tintin," +
                        " the comics series by Belgian cartoonist Hergé. Commissioned by the conservative Belgian" +
                        " newspaper Le Vingtième Siècle for its children's supplement Le Petit Vingtième, it was " +
                        "serialized weekly from September 1931 to October 1932 before being published in a collected" +
                        " volume by Éditions du Petit Vingtième in 1932. The story tells of young Belgian reporter" +
                        " Tintin and his dog Snowy who travel to the United States, where Tintin reports on organized" +
                        " crime in Chicago. Pursuing a gangster across the country, he encounters a tribe of Blackfoot" +
                        " Native Americans before defeating the Chicago crime syndicate.",
                true,
                haddock
        );
        localData.getCommToLocalData().addSubscription(
                publicOwnedChannel2.getId(),
                haddock,
                Subscription.Role.ADMIN
        );
        List<OwnedChannel> ownedChannelList = localData.getCommToLocalData().getAllOwnedChannel(haddock.getId());
        assertFalse(ownedChannelList.isEmpty());
    }

    /**
     * Update likerslist.
     */
    @Test
    void updateLikerslist() {
        Message textMessage = localData.getCommToLocalData().addTextMessage("10% de réduction sur la carte jeune", tintin.getId(), publicOwnedChannel.getId());
        localData.getCommToLocalData().updateLikersList(textMessage.getId(), tintin.getId(), publicOwnedChannel.getId());
        Message textMessageUpdated = localData.getCommToLocalData().getMessage(publicOwnedChannel.getId(), textMessage.getId());
        assertTrue(textMessageUpdated.getLikersList().contains(tintin.getId()));
    }

    /**
     * Modify nickname.
     */
    @Test
    void modifyNickname() {
        Subscription firstSubscription = localData.getCommToLocalData().getOwnedChannel(publicOwnedChannel.getId()).getWhiteList().get(0);
        localData.getCommToLocalData().modifyNickname("Tin-Tin", firstSubscription.getUser().getId(), publicOwnedChannel.getId());
        Subscription modifiedSubscription = localData.getCommToLocalData().getOwnedChannel(publicOwnedChannel.getId()).getWhiteList().get(0);
        assertEquals("Tin-Tin", modifiedSubscription.getNickname());
    }

    /**
     * Add text message.
     */
    @Test
    void addTextMessage() {
        TextMessage textMessage = localData.getCommToLocalData().addTextMessage("1,2,3 viva Algérie", tintin.getId(), publicOwnedChannel.getId());
        assertEquals("1,2,3 viva Algérie", textMessage.getTextContent());
    }

    /**
     * Add text reply.
     */
    @Test
    void addTextReply() {
        TextMessage textMessage = localData.getCommToLocalData().addTextMessage("Un philosophe a dit un jour « le mystère des Pyramides, c'est le mystère de la conscience dans laquelle on n'entre pas ».", tintin.getId(), publicOwnedChannel.getId());
        localData.getCommToLocalData().addTextReply("Les pharaons se faisaient enterrer avec leurs serviteurs.", tintin.getId(), publicOwnedChannel.getId(), textMessage.getId());
        List<Message> threadContents = localData.getCommToLocalData().getThreadContents(publicOwnedChannel.getId(), textMessage.getId());
        assertEquals(textMessage.getIdThread(), threadContents.get(1).getIdThread());
    }

    /**
     * Gets thread contents.
     */
    @Test
    void getThreadContents() {
        TextMessage textMessage = localData.getCommToLocalData().addTextMessage("Un philosophe a dit un jour « le mystère des Pyramides, c'est le mystère de la conscience dans laquelle on n'entre pas ».", tintin.getId(), publicOwnedChannel.getId());
        localData.getCommToLocalData().addTextReply("Les pharaons se faisaient enterrer avec leurs serviteurs.", tintin.getId(), publicOwnedChannel.getId(), textMessage.getId());
        List<Message> threadContents = localData.getCommToLocalData().getThreadContents(publicOwnedChannel.getId(), textMessage.getId());
        assertEquals(textMessage.getIdThread(), threadContents.get(1).getIdThread());
    }

    /**
     * Gets channel owner.
     */
    @Test
    void getChannelOwner() {
        User channelOwner = localData.getCommToLocalData().getChannelOwner(publicOwnedChannel.getId());
        // only gets the user id, but all the other attributes (login, firstname, lastname, etc..) are null
        // maybe change getChannelOwner()
        assertEquals(tintin, channelOwner);
    }

    /**
     * Gets messages.
     */
    @Test
    void getMessages() {
        TextMessage firstMessage = localData.getCommToLocalData().addTextMessage("1st message", tintin.getId(), publicOwnedChannel.getId());
        TextMessage secondMessage = localData.getCommToLocalData().addTextMessage("2nd message", tintin.getId(), publicOwnedChannel.getId());
        List<Message> messageList = localData.getCommToLocalData().getMessages(publicOwnedChannel.getId());
        assertEquals(firstMessage.getId(), messageList.get(0).getId());
        assertEquals(secondMessage.getId(), messageList.get(1).getId());
    }

    /**
     * Add subscription.
     */
    @Test
    void addSubscription() {
        Subscription sub = localData.getCommToLocalData().addSubscription(publicOwnedChannel.getId(), haddock, Subscription.Role.USER);
        assertEquals("Haddock", sub.getUser().getLastName());
    }

    /**
     * Update adm list.
     */
    @Test
    void updateAdmList() {
        localData.getCommToLocalData().addSubscription(
                publicOwnedChannel.getId(),
                haddock,
                Subscription.Role.USER
        );
        localData.getCommToLocalData().updateAdmList(haddock.getId(), publicOwnedChannel.getId());
        System.out.println(localData.getCommToLocalData().getOwnedChannel(publicOwnedChannel.getId()).getWhiteList());
        Subscription subscription = localData.getCommToLocalData().getOwnedChannel(publicOwnedChannel.getId()).getWhiteList().get(1);
        assertEquals(Subscription.Role.ADMIN, subscription.getRole());
    }

    /**
     * Gets owned channel.
     */
    @Test
    void getOwnedChannel() {
        OwnedChannel ownedChannel1 = localData.getIhmMainToLocalData().createOwnedChannel(
                "Tintin in Brazil",
                "you get the thing",
                true,
                haddock
        );
        OwnedChannel ownedChannel2 = localData.getCommToLocalData().getOwnedChannel(ownedChannel1.getId());
        assertEquals(ownedChannel1.getId(), ownedChannel2.getId());
    }
}