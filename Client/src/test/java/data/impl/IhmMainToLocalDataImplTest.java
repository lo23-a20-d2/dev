package data.impl;

import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import data.LocalData;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class IhmMainToLocalDataImplTest {
    private final LocalData localData;
    private User tintin;
    private User haddock;
    private final OwnedChannel publicOwnedChannel;

    public IhmMainToLocalDataImplTest() {
        localData = new LocalData();
        tintin = localData.getIhmMainToLocalData().saveNewAccount("tintin", "milou", "Tintin", "Le célèbre reporter", null, null);
        if(tintin == null){
            tintin = localData.getIhmMainToLocalData().authenticate("tintin", "milou");
        }

        publicOwnedChannel = localData.getIhmMainToLocalData().createOwnedChannel(
                "Le Trésor de Rackham le Rouge",
                "Red Rackham's Treasure (French: Le Trésor de Rackham le Rouge) is the twelfth volume of " +
                        "The Adventures of Tintin, the comics series by Belgian cartoonist Hergé. The story was" +
                        " serialised daily in Le Soir, Belgium's leading francophone newspaper, from February to " +
                        "September 1943 amidst the German occupation of Belgium during World War II. Completing an" +
                        " arc begun in The Secret of the Unicorn, the story tells of young reporter Tintin and his " +
                        "friend Captain Haddock as they launch an expedition to the Caribbean to locate the treasure" +
                        " of the pirate Red Rackham.",
                false,
                tintin
        );

    }

    @Test
    void saveNewAccount() {
        String randomLogin = UUID.randomUUID().toString();
        User testUser = localData.getIhmMainToLocalData().saveNewAccount(randomLogin, "testUser", "test", "test", new Date(), null);
        assertNotNull(testUser);
        assertEquals(randomLogin, testUser.getLogin());
    }

    @Test
    void getUserData() {
        User tintinData = localData.getIhmMainToLocalData().getUserData(tintin.getId(), "tintin");
        assertNotNull(tintinData);
        assertEquals(tintin.getId(),tintinData.getId());
    }

    @Test
    void authenticate() {
        User tintinAuth = localData.getIhmMainToLocalData().authenticate("tintin", "milou");
        assertNotNull(tintinAuth);
        assertEquals(tintin.getId(),tintinAuth.getId());
    }

    @Test
    void createOwnedChannel() {
        OwnedChannel c = localData.getIhmMainToLocalData().createOwnedChannel(
                "TestChannel",
                "Test channel",
                false,
                tintin
        );
        assertNotNull(c);
    }

    @Test
    void addSubscription() {
        Subscription s = localData.getCommToLocalData().addSubscription(
                publicOwnedChannel.getId(),
                tintin,
                Subscription.Role.ADMIN
        );
        assertNotNull(s);
    }
}