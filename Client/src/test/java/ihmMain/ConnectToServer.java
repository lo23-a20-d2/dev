package ihmMain;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.hasText;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;


import java.io.IOException;

/**
 * ConnectToServer test class
 *
 * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
 */

class ConnectToServer extends ApplicationTest {
    // Test stage
    Stage mainStage;

    // Controller specific elements and data
    VBox mainRoot;
    ihmMain.controllers.authentication.ConnectToServer controller;
    Button connect;

    // Invalid mock data
    private final String invalidIP = "0".repeat(ihmMain.controllers.authentication.ConnectToServer.getMinInputLength());


    @Override
    public void start(Stage stage) throws IOException {
        // Load FXML view and setup stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/authentication/connectToServer.fxml"));
        mainRoot = loader.load();
        controller = loader.getController();
        mainStage = stage;
        stage.setScene(new Scene(mainRoot));
        stage.show();
        stage.toFront();

        // Find validation button used multiple times in the tests below
        this.connect = (Button) mainRoot.lookup("#connectButton");

    }
    /**
     * Disable connect button if field is empty
     * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
     */
    @Test
    void connectDisabledIfFieldsEmpty() {
        assertTrue(connect.disableProperty().get());
    }

    /**
     * Check if input is in the correct format
     * @author Maroua Mouhsine <maroua.mouhsine@etu.utc.fr>
     */
    @Test
    void addressFormatIncorrect() {
        // Find and fill IP address field with invalid data
        TextField input = (TextField) mainRoot.lookup("#userInput");
        input.setText(invalidIP);
        verifyThat("#userInput", hasText(invalidIP));
        // Click on connect button and check if error appeared
        clickOn(connect);
        verifyThat("#errorLabel", hasText("Le format de l'adresse IP saisie n'est pas valide"));
    }

}
