package ihmMain;
import CommonClasses.*;
import org.junit.jupiter.api.*;


/**
 * IhmMain test class
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public class IhmMainTests {
    private IhmMain ihmMain;
    private User user;

    private Channel ownedChannelPub;
    private Channel sharedChannelPub;
    private Channel ownedChannelPv;
    private Channel sharedChannelPv;

    private Subscription subOwnedPublic;
    private Subscription subSharedPublic;
    private Subscription subOwnedPrivate;
    private Subscription subSharedPrivate;


    @BeforeEach
    public void init(){
        ihmMain = new IhmMain();

        // Initiating the user
        user = new User();
        user.setId("user1");

        // Initiating public channels
        ownedChannelPub = new OwnedChannel("channelPub1");
        sharedChannelPub = new SharedChannel("channelPub2");

        // Initiating private channels
        ownedChannelPv = new OwnedChannel("channelPv1");
        ownedChannelPv.setPrivate(true);
        sharedChannelPv = new SharedChannel("channelPv2");
        sharedChannelPv.setPrivate(true);

        // Initiating subscriptions
        subOwnedPublic = new Subscription();
        subOwnedPublic.setUser(user);
        subOwnedPublic.setChannel(ownedChannelPub);

        subSharedPublic = new Subscription();
        subSharedPublic.setUser(user);
        subSharedPublic.setChannel(sharedChannelPub);

        subOwnedPrivate = new Subscription();
        subOwnedPrivate.setUser(user);
        subOwnedPrivate.setChannel(ownedChannelPv);

        subSharedPrivate = new Subscription();
        subSharedPrivate.setUser(user);
        subSharedPrivate.setChannel(sharedChannelPv);
    }

    /**
     * Fields connectedUser and idConnectedUser should be updated according to the new user.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("setConnectedUser")
    public void setConnectedUser(){
        ihmMain.setConnectedUser(user);
        Assertions.assertEquals(ihmMain.getConnectedUser(),user);
        Assertions.assertEquals(ihmMain.getIdConnectedUser(),user.getId());
    }

    /**
     * Fields connectedUser and idConnectedUser should be reset to "".
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("resetConnectedUser")
    public void resetConnectedUser(){
        ihmMain.resetConectedUser();

        Assertions.assertEquals(ihmMain.getConnectedUser().getId(),"");
        Assertions.assertEquals(ihmMain.getIdConnectedUser(),"");
    }

    /**
     * The method should return true because the connected user was defined with setConnectedUser.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("userIsConnected : true")
    public void userIsConnected_true(){
        ihmMain.setConnectedUser(user);

        Assertions.assertTrue(ihmMain.userIsConnected());
    }

    /**
     * The method should return false because no connected user was defined.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("userIsConnected : false")
    public void userIsConnected_False(){
        Assertions.assertFalse(ihmMain.userIsConnected());
    }

    /**
     * The user's channels list should be updated according to his subscriptions.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("setUserListChannel")
    public void setUserListChannel(){
        ihmMain.setConnectedUser(user);

        ihmMain.getObservableUserSubscription().add(subOwnedPublic);
        ihmMain.getObservableUserSubscription().add(subSharedPublic);
        ihmMain.getObservableUserSubscription().add(subOwnedPrivate);
        ihmMain.getObservableUserSubscription().add(subSharedPrivate);

        ihmMain.setUserListChannel();
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPublic().contains(ownedChannelPub));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPublic().contains(sharedChannelPub));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPv().contains(ownedChannelPub));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPv().contains(sharedChannelPub));

        Assertions.assertTrue(ihmMain.getObservableUserListChannelPv().contains(ownedChannelPv));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPv().contains(sharedChannelPv));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPublic().contains(ownedChannelPv));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPublic().contains(sharedChannelPv));

    }

    /**
     * Test add of a new owned private channel.
     *
     * User's subscriptions list should contain the new subscription.
     * User's private channels list should contain the new channel.
     * User's public channels list should not contain the new channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : private owned channel")
    public void addNewChannelToUser_OwnedPrivate(){
        ihmMain.addNewChannelToUser(ownedChannelPv, subOwnedPrivate);
        Assertions.assertTrue(ihmMain.getObservableUserSubscription().contains(subOwnedPrivate));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPv().contains(ownedChannelPv));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPublic().contains(ownedChannelPv));
    }

    /**
     * Test add of a new shared private channel.
     *
     * User's subscriptions list should contain the new subscription.
     * User's private channels list should contain the new channel.
     * User's public channels list should not contain the new channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : private shared channel")
    public void addNewChannelToUser_SharedPrivate(){
        ihmMain.addNewChannelToUser(sharedChannelPv, subSharedPrivate);
        Assertions.assertTrue(ihmMain.getObservableUserSubscription().contains(subSharedPrivate));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPv().contains(sharedChannelPv));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPublic().contains(sharedChannelPv));
    }

    /**
     * Test add of a new shared private channel in duplicate.
     *
     * User's subscriptions list should contain only one subscription.
     * User's private channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : private shared channel already in")
    public void addNewChannelToUser_DuplicateSharedPrivate(){
        ihmMain.addNewChannelToUser(sharedChannelPv, subSharedPrivate);
        ihmMain.addNewChannelToUser(sharedChannelPv, subSharedPrivate);
        Assertions.assertEquals(ihmMain.getObservableUserSubscription().size(),1);
        Assertions.assertEquals(ihmMain.getObservableUserListChannelPv().size(),1);
    }

    /**
     * Test add of a new owned private channel in duplicate.
     *
     * User's subscriptions list should contain only one subscription.
     * User's private channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : private owned channel already in")
    public void addNewChannelToUser_DuplicateOwnedPrivate(){
        ihmMain.addNewChannelToUser(ownedChannelPv, subOwnedPrivate);
        ihmMain.addNewChannelToUser(ownedChannelPv, subOwnedPrivate);
        Assertions.assertEquals(ihmMain.getObservableUserSubscription().size(),1);
        Assertions.assertEquals(ihmMain.getObservableUserListChannelPv().size(),1);
    }

    /**
     * Test add of a new owned public channel.
     *
     * User's subscriptions list should contain the new subscription.
     * User's public channels list should contain the new channel.
     * User's private channels list should not contain the new channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : public owned channel")
    public void addNewChannelToUser_OwnedPublic(){
        ihmMain.addNewChannelToUser(ownedChannelPub, subOwnedPublic);
        Assertions.assertTrue(ihmMain.getObservableUserSubscription().contains(subOwnedPublic));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPublic().contains(ownedChannelPub));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPv().contains(ownedChannelPub));
    }

    /**
     * Test add of a new shared public channel.
     *
     * User's subscriptions list should contain the new subscription.
     * User's public channels list should contain the new channel.
     * User's private channels list should not contain the new channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : public shared channel")
    public void addNewChannelToUser_SharedPublic(){
        ihmMain.addNewChannelToUser(sharedChannelPub, subSharedPublic);
        Assertions.assertTrue(ihmMain.getObservableUserSubscription().contains(subSharedPublic));
        Assertions.assertTrue(ihmMain.getObservableUserListChannelPublic().contains(sharedChannelPub));
        Assertions.assertFalse(ihmMain.getObservableUserListChannelPv().contains(sharedChannelPub));
    }

    /**
     * Test add of a new shared public channel in duplicate.
     *
     * User's subscriptions list should contain only one subscription.
     * User's public channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : public shared channel already in")
    public void addNewChannelToUser_DuplicateSharedPublic(){
        ihmMain.addNewChannelToUser(sharedChannelPub, subSharedPublic);
        ihmMain.addNewChannelToUser(sharedChannelPub, subSharedPublic);
        Assertions.assertEquals(ihmMain.getObservableUserSubscription().size(),1);
        Assertions.assertEquals(ihmMain.getObservableUserListChannelPublic().size(),1);
    }

    /**
     * Test add of a new owned public channel in duplicate.
     *
     * User's subscriptions list should contain only one subscription.
     * User's public channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("addNewChannelToUser : public owned channel already in")
    public void addNewChannelToUser_DuplicateOwnedPublic(){
        ihmMain.addNewChannelToUser(ownedChannelPub, subOwnedPublic);
        ihmMain.addNewChannelToUser(ownedChannelPub, subOwnedPublic);
        Assertions.assertEquals(ihmMain.getObservableUserSubscription().size(),1);
        Assertions.assertEquals(ihmMain.getObservableUserListChannelPublic().size(),1);
    }

    /**
     * Test add of a new owned public channel.
     *
     * Module's channels list should contain the channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : public owned channel")
    public void addNewPublicChannel_ownedPublic(){
        ihmMain.addNewPublicChannel(ownedChannelPub);

        Assertions.assertTrue(ihmMain.getObservableListChannel().contains(ownedChannelPub));
    }

    /**
     * Test add of a new shared public channel.
     *
     * Module's channels list should contain the channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : public shared channel")
    public void addNewPublicChannel_sharedPublic(){
        ihmMain.addNewPublicChannel(sharedChannelPub);

        Assertions.assertTrue(ihmMain.getObservableListChannel().contains(sharedChannelPub));
    }

    /**
     * Test add of a new shared private channel.
     *
     * Module's channels list should not contain the channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : shared private channel")
    public void addNewPublicChannel_sharedPrivate(){
        ihmMain.addNewPublicChannel(sharedChannelPv);

        Assertions.assertFalse(ihmMain.getObservableListChannel().contains(sharedChannelPv));
    }

    /**
     * Test add of a new owned private channel.
     *
     * Module's channels list should not contain the channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : owned private channel")
    public void addNewPublicChannel_ownedPrivate(){
        ihmMain.addNewPublicChannel(ownedChannelPv);

        Assertions.assertFalse(ihmMain.getObservableListChannel().contains(ownedChannelPv));
    }

    /**
     * Test add of a new shared channel in duplicate.
     *
     * Module's channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : shared channel already in")
    public void addNewPublicChannel_sharedDuplicate(){
        ihmMain.addNewPublicChannel(sharedChannelPub);
        ihmMain.addNewPublicChannel(sharedChannelPub);

        Assertions.assertEquals(ihmMain.getObservableListChannel().size(), 1);
    }

    /**
     * Test add of a new owned channel in duplicate.
     *
     * Module's channels list should contain only one channel.
     *
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    @Test
    @DisplayName("AddNewPublicChannel : owned channel already in")
    public void addNewPublicChannel_ownedDuplicate(){
        ihmMain.addNewPublicChannel(ownedChannelPub);
        ihmMain.addNewPublicChannel(ownedChannelPub);

        Assertions.assertEquals(ihmMain.getObservableListChannel().size(), 1);
    }
}
