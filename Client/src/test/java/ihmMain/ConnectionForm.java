package ihmMain;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import java.io.IOException;

import static ihmMain.Constants.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.hasText;

/**
 * ConnectionForm test class
 *
 * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
 */
public class ConnectionForm extends ApplicationTest {
    // Test stage
    Stage mainStage;

    // Controller specific elements and data
    VBox mainRoot;
    ihmMain.controllers.authentication.ConnectionForm controller;
    Button connect;

    // Invalid mock data
    private final String tooLong = "a".repeat(MAX_LOGIN_LENGTH+1);
    private final String pswtooShort = "a".repeat(MIN_PSW_LENGTH-1);
    private final String pswtooLong = "a".repeat(MAX_PSW_LENGTH+1);


    @Override
    public void start(Stage stage) throws IOException {
        // Load FXML view and setup stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/authentication/connectionForm.fxml"));
        mainRoot = loader.load();
        controller = loader.getController();
        mainStage = stage;
        stage.setScene(new Scene(mainRoot));
        stage.show();
        stage.toFront();

        // Find validation button used multiple times in the tests below
        this.connect = (Button) mainRoot.lookup("#connectButton");

    }

    /**
     * Verify if the button is really disabled if fields are empty.
     * In the beginning there are empty so no need to empty them
     *
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     */
    @Test
    void validateDisabledIfFieldsEmpty() {
        assertTrue(connect.disableProperty().get());
    }

    /**
     * Verify if the login is too long
     *
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     */
    @Test
    void loginTooLong(){
        // Find and fill login label
        TextField login = (TextField) mainRoot.lookup("#loginLabel");
        login.setText(tooLong);
        verifyThat("#loginLabel", hasText(tooLong));

        // Click on connect button and check if error appeared
        clickOn(connect);
        verifyThat("#errorLabel", hasText("Le login est trop long"));
    }

    /**
     * Checks if the password is too long
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     */
    @Test
    void passwordTooLong() {

        // Find and fill password label
        TextField password = (TextField) mainRoot.lookup("#passwordLabel");
        password.setText(pswtooLong);
        verifyThat("#passwordLabel", hasText(pswtooLong));

        // Click on connect button and check if error appeared
        clickOn(connect);
        verifyThat("#errorLabel", hasText("Le mot de passe est trop long"));
    }

    /**
     * Checks if the password is too short
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     */
    @Test
    void passwordTooShort() {

        // Find and fill password label
        TextField password = (TextField) mainRoot.lookup("#passwordLabel");
        password.setText(pswtooShort);
        verifyThat("#passwordLabel", hasText(pswtooShort));

        // Click on connect button and check if error appeared
        clickOn(connect);
        verifyThat("#errorLabel", hasText("Le mot de passe est trop court, saisissez un mot de passe avec plus de 6 caractères"));
    }

}

