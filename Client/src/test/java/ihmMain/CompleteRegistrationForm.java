package ihmMain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.hasText;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Complete registration form test class
 *
 * @author Anis Mikou <anis.mikou@etu.utc.fr>
 */
class CompleteRegistrationForm extends ApplicationTest {
    // Test stage
    Stage mainStage;

    // Controller specific elements and data
    VBox mainRoot;
    ihmMain.controllers.authentication.CompleteRegistrationForm controller;
    Button validate;
    int avatarWidth;

    // Invalid mock data
    private final String tooLong = "a".repeat(Constants.MAX_NAME_LENGTH+1);

    @Override
    public void start(Stage stage) throws IOException {
        // Load FXML view and setup stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/authentication/completeRegistrationForm.fxml"));
        mainRoot = loader.load();
        controller = loader.getController();
        mainStage = stage;
        stage.setScene(new Scene(mainRoot));
        stage.show();
        stage.toFront();

        // Find validation button used multiple times in the tests below
        this.validate = (Button) mainRoot.lookup("#validateButton");

        // Get avatar size from controller
        this.avatarWidth = Constants.AVATAR_WIDTH;
    }

    /**
     * Validate button should be disabled if fields are empty.
     * They are initially so no setup is required
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Test
    void validateDisabledIfFieldsEmpty() {
        assertTrue(validate.disableProperty().get());
    }

    /**
     * Check if there's an error when first name is too long.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Test
    void firstNameTooLong() {
        // Find and fill first name label with invalid data
        TextField firstname = (TextField) mainRoot.lookup("#firstNameLabel");
        firstname.setText(tooLong);
        verifyThat("#firstNameLabel", hasText(tooLong));

        // Click on validate button and check if error appeared
        clickOn(validate);
        verifyThat("#errorLabel", hasText("Le prénom entré est trop long"));
    }

    /**
     * Check if there's an error when last name is too long.
     *
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Test
    void lastNameTooLong() {
        // Find and fill last name label
        TextField lastName = (TextField) mainRoot.lookup("#lastNameLabel");
        lastName.setText(tooLong);
        verifyThat("#lastNameLabel", hasText(tooLong));

        // Click on validate button and check if error appeared
        clickOn(validate);
        verifyThat("#errorLabel", hasText("Le nom entré est trop long"));
    }

    /**
     * Check if image is cropped to the appropriate width and height.
     * Mock avatar is 500x500px
     *
     * @throws IOException if there was an error reading the mock avatar
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Test
    void isImageCropped() throws IOException {
        File mockAvatar = new File(getClass().getResource("/resources/mocks/avatar.png").getPath());
        BufferedImage cropped = controller.cropImage(mockAvatar);
        assertEquals(avatarWidth, cropped.getHeight());
        assertEquals(avatarWidth, cropped.getWidth());
    }

    /**
     * Check if image is centered.
     * Mock avatar has black point in the middle that should be in the middle of the cropped avatar
     *
     * @throws IOException if there was an error reading the mock avatar
     * @author Anis Mikou <anis.mikou@etu.utc.fr>
     */
    @Test
    void isImageCentered() throws IOException {
        File mockAvatar = new File(getClass().getResource("/resources/mocks/avatar.png").getPath());
        assertEquals(0xFF000000, ImageIO.read(mockAvatar).getRGB(250,250));
        BufferedImage cropped = controller.cropImage(mockAvatar);
        assertEquals(0xFF000000, cropped.getRGB(avatarWidth/2,avatarWidth/2));
    }
}
