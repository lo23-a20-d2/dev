package ihmMain;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.matcher.control.ComboBoxMatchers;

import java.io.IOException;

import static ihmMain.Constants.MIN_SEARCH_INPUT_LENGTH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.hasText;

/**
 * Search test class
 *
 * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
 */
class Search extends ApplicationTest {
    // Test stage
    Stage mainStage;

    // Controller specific elements and data
    VBox mainRoot;
    ihmMain.controllers.sidebar.defaultSidebar.Search controller;
    Button search;
    ComboBox<String> comboBox;

    private final String tooshort = "a".repeat(MIN_SEARCH_INPUT_LENGTH);


    @Override
    public void start(Stage stage) throws IOException {
        // Load FXML view and setup stage
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/sidebar/defaultSidebar/search.fxml"));
        mainRoot = loader.load();
        controller = loader.getController();
        mainStage = stage;
        stage.setScene(new Scene(mainRoot));
        stage.show();
        stage.toFront();

        // Find validation button used multiple times in the tests below
        this.search = (Button) mainRoot.lookup("#searchButton");

    }

    /**
     * hasItems
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Checks if the comboBox has the right amount of items (3)
     */
    @Test
    void hasItems() {
        assertThat(comboBox, ComboBoxMatchers.hasItems(3));
    }

    /**
     * containsItems
     * @author Maria IDRISSI <maria.idrissi-kaitouni@etu.utc.fr>
     * Checks if the comboBox has the Right items
     */
    @Test
    void containsItems() {
        // in order
        assertThat(comboBox, ComboBoxMatchers.containsItems("Nom channel", "Description","Utilisateur"));
    }
    @Test
    void searchQueryTooShort(){
        TextField searchInput = (TextField) mainRoot.lookup("#searchInput");
        searchInput.setText(tooshort);
        verifyThat("#searchInput", hasText(tooshort));

        // Click on search button and check if error appeared
        clickOn(search);
        verifyThat("#errorLabel", hasText("Le champ de recherche est trop court"));
    }

}

