# Projet LO23 - Code Source

Application de chat avec gestion de channels de discussion

## Structure du repo

Le repo se divise comme suit :

_La structure n'est pas exhaustive pour qu'elle reste lisible_

```
/
├── client/
│   ├── com
│   ├── data
│   │   ├── models
│   │   └── tests
│   ├── ihmChannel
│   │   ├── controllers
│   │   ├── tests
│   │   └── views
│   └── ihmMain
├── server/
│   ├── com
│   └── data
└── assets/
```

Les dossiers client et server contiendront respectivement tout le code lié à l'application client et serveur. Dans chacun de ces deux dossiers, on retrouvera un package par sous-équipe concernée. Dans ces packages, on pourra retrouver plusieurs des 4 packages suivant :

- views
- controllers
- models
- tests

Enfin, Le dossier assets contiendra toutes les images/icones utilisées dans le projet.

## Contribution

Merci de lire toutes les informations du fichier `CONTRIBUTING.md` avant de contribuer au projet.
