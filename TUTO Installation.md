# Tutoriel de prise en main du projet

## I. Pré-requis

### Open JDK
Pour ce projet nous avons choisis d'utiliser la dernière version open source de Java afin de pouvoir utiliser les dernières fonctionnalités et d'être sous licence libre.
[Télécharger](https://jdk.java.net/15/) OpenJDK 15 et le dézipper dans un dossier (Exemple : C:\Users\toto\\.jdks).

### JavaFX
Depuis Java 1.8, JavaFX a été enlevé du JDK de Java mais il reste toujours maintenu et mis à jour. 
[Télécharger](https://gluonhq.com/products/javafx/) le SDK version **15** de JavaFX (**Latest Release** et non la Long Term Support) et dézipper le dossier javafx-sdk-15 dans le même dossier contenant OpenJDK (Exemple  : C:\Users\toto\\.jdks).

### Scene Builder
Scene Builder est un outil qui permet de visualiser et formater les vues au format FXML. Nous verrons qu'il est également possible de l'intégrer dans un IDE (Eclipse ou IntelliJ).
[Télécharger](https://gluonhq.com/products/scene-builder/) la dernière version de Scene Builder et l'installer.

## II. Configurer le projet
Après avoir cloner le repository git, il faut configurer votre IDE pour exécuter les applications. Les étapes à suivre montre comment configurer l'application client avec intelliJ et Eclipse. Pour l'application serveur, il suffit de suivre les indication jusqu'à OpenJDK car il n'y a pas d'interface graphique. 
### Avec IntelliJ
JetBrain met à disposition la version Professionnel d'IntelliJ pour les étudiants. Il suffit de remplir le formulaire [ici](https://www.jetbrains.com/fr-fr/education/) et de télécharger intelliJ.
![](https://i.ibb.co/6BNHxFX/intel.png)Lancer IntelliJ et ouvrir le projet Client. 
Dans File->Project Structure->SDKs, cliquer sur le + , "ADD JDK..." et sélectionner le dossier d'OpenJDK téléchargé précédemment (Exemple : C:\Users\toto\\.jdks\openjdk-15).
Toujours dans Project Structure, aller dans Project et sélectionner le JDK ajouté dans "Project SDK" :

![](https://i.ibb.co/LCqnvdM/1.png)

Toujours dans Project Structure, aller dans Librairies, cliquer sur le +, sélectionner "Java" et aller dans le dossier lib du SDK de JavaFX téléchargé précédemment (Exemple : C:\Users\toto\\.jdks\javafx-sdk-15\lib).

![](https://i.ibb.co/3kg4Nww/2.png)
Cliquer sur Apply et Ok.

Aller dans Run->Edit Configurations... et créer une configuration de type Application.
Dans Main class, ajouter la class Main en cliquant sur les trois petits points.
Dans le champ VM options saisir : 

    --module-path PATH_TO_LIB --add-modules javafx.controls,javafx.fxml

En remplaçant `PATH_TO_LIB` par le chemin vers la lib JavaFX (Exemple:`--module-path C:\Users\toto\.jdks\javafx-sdk-15\lib --add-modules javafx.controls,javafx.fxml`)

![](https://i.ibb.co/F3FSjY5/3.png)

Enfin, pour ajouter Scene Builder à IntelliJ, aller dans File->Settings->Languages & Frameworks->JavaFX et ajouté le chemin vers l'executable de Scene Builder. Il sera donc possible de visualiser les fichiers FXML avec Scene Builder dans IntelliJ.

![](https://i.ibb.co/gPpDfhK/4.png)
Le projet est configuré avec IntelliJ et devrait compiler.

### Avec Eclipse

La dernière version d'Eclipse est disponible [ici](https://www.eclipse.org/downloads/).
Lancer Eclipse et ouvrir le projet Client. Faire un clic droit sur le projet et cliquer sur properties->Java Build Path et l'onglet Librairies.

![](https://i.ibb.co/NWCtdVD/1.png)Sélectionner "JRE System Library [jre]" et cliquer sur Edit.
Dans la nouvelle fenêtre, cocher Alternate JRE et cliquer sur Installed JREs.
Dans la nouvelle fenêtre cliquer sur Add.
Dans la nouvelle fenêtre choisir Standard VM, puis entrer le chemin d'OpenJDK précédemment téléchargé.
Cliquer sur Finish :
![](https://i.ibb.co/jh2kkkK/5.png)

Sélectionner OpenJDK et cliquer sur Apply and Close :
![](https://i.ibb.co/Y3yB9D7/6.png)

Dans Alternate JRE, sélectionner OpenJDK, puis cliquer sur Finish :
![](https://i.ibb.co/R3jqX6r/7.png)

Toujours dans l'onglet Librairies de Java Build Path, sélectionner Modulepath et cliquer sur Add Library.
Dans la nouvelle fenêtre sélectionner User Library puis Next.
![](https://i.ibb.co/9Ywhg4v/8.png)Dans la nouvelle fenêtre, cliquer sur User libraries puis New. Appeler la library JFX, la sélectionner et cliquer sur Add External JARs. Sélectionner tous les .jar situés dans le dossier lib du SDK de JavaFX (Exemple `C:\Users\toto\.jdks\javafx-sdk-15\lib`).
Cliquer sur Apply and Close puis Finish : 
![](https://i.ibb.co/hgmQPBG/10-cliquer-sur-apply-and-close.png)Cliquer sur Apply and Close.
Faire un clic droit sur le projet Client, Run as->Run Configurations.
Sélectionner Java Application->Main->Onglet Arguments.
Dans VM arguments, saisir : 

    --module-path PATH_TO_LIB --add-modules javafx.controls,javafx.fxml

En remplaçant `PATH_TO_LIB` par le chemin vers la lib JavaFX (Exemple:`--module-path C:\Users\toto\.jdks\javafx-sdk-15\lib --add-modules javafx.controls,javafx.fxml`)
Cliquer sur Apply et Run.
Le projet est configuré avec Eclipse et devrait compiler.

![](https://i.ibb.co/gzx7Pqx/12.png)

