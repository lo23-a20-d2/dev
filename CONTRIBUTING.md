# Contribuer au projet

:tada: Bienvenue sur le guide de contribution :tada:

Tout ce qui suit constitue l'ensemble des "règles" définies par les resp. dev & qualité pour s'assurer de produire un code de qualité, compréhensible par tous et gérable malgré le grand nombre de contributeurs. Si certains points vous semblent incompréhensibles sentez vous libres de créer une merge request avec des changements/améliorations.

## Git

### Tickets

Les tickets seront généralement créés par le développeur avant de commencer toute tache sur le repo. Les tickets devraient permettre de comprendre l'objectif de la tache et comment la fonctionnalité s'intègre au reste de l'application. Si la fonctionnalité fait appel à une librairie extérieure ou à une maquette visuelle, un lien vers la documentation la maquette devrait être fourni.

Ne pas oublier :

- d'ajouter les labels adéquats à vos tickets (labels de sous-équipe et de type de développement)
- de s'assigner le ticket et de le passer au label **Doing** lorsqu'on le commence
- de passer du label **Doing** à **Review** lorsqu'on ouvre une MR par rapport à un ticket

### Branches

On créera 1 branche par ticket. Celles-ci seront fusionnées dans `develop` grâce à des merge requests et après avoir été validées par le resp dev et qualité de l'équipe concernée (au moins).

On pourra alors fixer les developpements en versions stables en créant une branche `release` qui permettra à l'équipe de test de vérifier que l'application fonctionne correctement et de régler les derniers bugs. Cette branche `release` sera alors fusionnée dans master et supprimée créant ainsi la nouvelle version stable de l'application. On fusionnera aussi cette version dans `develop` pour que les nouvelles additions de codes se fassent sur la même base que la dernière version dans master.

Si besoin de régler un problème critique avant la démo, on pourra créer un branche `hotfix` directement dérivée de `master` et propager les changements dans `master` et `develop`.

Visualisation du process :

![alt text](https://pierrebelin.fr/wp-content/uploads/2020/07/image-3-1024x714.png "Process Git")

#### Noms de branche et commits

Chaque branche aura un nom qui permet de tout de suite savoir ce qu'elle apporte et le ticket qu'elle traite. Ce nom suivra le format :
`type/idticket-description`

##### Exemples

```
feat/34-chat-ui
bugfix/12-wrong-user-data
```

Pareillement, chaque commit aura un nom qui permet de savoir ce qui a été ajouté et où (si besoin). Ce nom aura le format :
`type(zone): description`.

La zone peut être un package, un composant ou vide quand le commit concerne tout le projet.

##### Exemples

```
feat (ihmChannel): send message button
refactor (auth): network connection
bugfix: black screen on startup
```

On aura 5 types de tickets, branches et commits :

- feat (pour feature)
- refactor
- bugfix
- docs
- test

Ce process Git a en grande partie été inspiré de [celui-là](https://pierrebelin.fr/blog/guide-complet-ameliorer-son-process-git/)

#### Commandes git utiles

```bash
# Créer et pousser un commit
git commit -m "type(zone): description" -m "details et explications"
git push

# Se déplacer sur la branche 'exemple'
git checkout exemple

# Création de branche à partir de develop
git checkout develop
git checkout -b type/idticket-nom

# Remettre sa branche à jour avec develop
# A faire avant d'ouvrir une merge request, risque de créer des conflits
git pull origin develop --rebase
## résolution de conflits
git push -f

# Annuler un rebase
git reflog # chercher la position du commit avant rebase
git reset --hard HEAD@{x} # insérer cette position à la place de x

```

Il existe aussi des clients Git avec interface graphique comme [GitHub Desktop](https://desktop.github.com/) et [Git Kraken](https://www.gitkraken.com/) (les repo privés sont disponible seulement avec Git Kraken Pro que vous pouvez vous procurez en demandant le pack étudiant de GitHub [ici](https://education.github.com/discount_requests/student_application))

## Qualité

Pour qu'une merge request soit acceptée, elle doit respecter quelques normes de qualité. Veuillez bien prendre en compte tous les élements décrits ici avant de commencer à développer.

### Guide de style

![](https://res.cloudinary.com/practicaldev/image/fetch/s--qjkpuQX6--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://imgs.xkcd.com/comics/code_quality.png)

Dans le cas où vous ne trouvez pas dans ce guide de style une réponse à votre interrogation, nous vous recommandons vivement de consulter le [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).

#### Formattage

Afin de ne pas perdre de temps à discuter du formattage du code lors des merge request, nous utiliseront [Prettier](https://prettier.io/). Cet outil est un _formatteur de code_ et il va donc permettre d'avoir un code très homogène et respectant partout les mêmes règles de formattage.

Lorsque _Maven_ sera configuré, **Prettier** sera ajouté directement au projet en tant que plugin et il ne nécessitera donc pas de configuration de votre côté (pour la configuration de prettier avec Maven, lire cet [article](https://dev.to/m4nu56/prettier-to-format-your-java-codebase-3f7i).)

En attendant, vous pouvez installer **Prettier** vous même en suivant ces [instructions](https://github.com/jhipster/prettier-java).

Configuration (the rest is left on default)

```
printWidth = 100
```

#### Appelation (Naming)

- `UpperCamelCase` pour les noms de classe
- `lowerCamelCase` pour les noms de méthodes
- `CONSTANT_CASE` pour les noms de constantes
- `lowerCamelCase` pour tout le reste

### Linter

Il est vivement recommandé d'installer [SonarLint](https://www.sonarlint.org/) dans votre IDE préféré, afin d'avoir des détections d'erreurs très pratiques.

### Documentation

Nous utilisons **JavaDoc** pour générer une documentation en HTML. **Cela implique que toutes les classes et méthodes que vous écrirez doivent être commentées. Avec notamment le `@author` qui doit être précisé.**

Il est conseillé de jeter un oeil à ce [tutoriel OpenClassRooms](https://openclassrooms.com/fr/courses/1115306-presentation-de-la-javadoc) pour comprendre pourquoi et comment nous allons utiliser JavaDoc.

Vous pouvez utiliser le plugin [JavaDoc pour IntelliJ](https://plugins.jetbrains.com/plugin/7157-javadoc), bien pratique pour générer de la documentation. (Une fois le plugin installé et activé, le raccourci clavier est **`Shift` + (`cmd` pour Mac ou `ctrl` pour Windows) + `alt` + `G``)**

Pour la java doc il faut mettre :

- sur la première ligne une description courte de la méthoode

- sur la deuxième ligne une description plus détaillée de la méthode 

- le nom des paramètres si c'est une méthode avec le tag @param

- le nom de tous les auteurs d’une méthode avec le tag @author, le premier nom est l’auteur référent qu'on pourra contacter en cas de problème, et les suivants qu'on contacte si le premier auteur n'est pas disponible ou n'a pas la réponse attendue.

-  L'auteur doit etre indiqué avec la forme suivante : * @author nom prenom \<prenom.nom@etu.utc.fr\>

Les différents tag possibles dans la Java doc sont :

- @param pour les paramètres de la méthode
- @return l'objet retourné par la méthode
- @throws les exceptions propagées
- @author l'auteur 
- @version le numéro de version de la classe
- @see réfère à une autre méthode, classe, etc.
- @since depuis quelle version
- @deprecated indique qu'une méthode est dépréciée


### Tests

Nous nous forçons à mettre en place des tests automatisés pour plusieurs raisons :

- faciliter la maintenance
- détecter au plus tôt des erreurs inattendues
- mieux communiquer (en écrivant des tests on décrit le comportement attendu d'un logiciel)

Différents types de tests sont mis en place:

- Les **tests unitaires** vérifient les classes, ou autres unités de code, rapidement et de façon exhaustive, pour s'assurer qu'elles tiennent leurs promesses.

- Les **tests d'intégration** vérifient que les classes et les parties de l'application qui doivent fonctionner ensemble le font en collaborant comme prévu.

- Les **tests fonctionnels** vérifient du point de vue de l'utilisateur final qu'on sera capable de résoudre ses problèmes en utilisant l'application lorsqu'elle est active.

Nous utiliserons le framework de tests Java [JUnit5](https://junit.org/junit5/).

Ce dernier sera directement intégré à _Maven_ donc il ne nécessitera pas de configuration de votre côté.

Si vous n'avez jamais fais de tests, il est recommandé de suivre ce [tutoriel](https://openclassrooms.com/fr/courses/6100311-testez-votre-code-java-pour-realiser-des-applications-de-qualite).

#### JUnit

**Qu’est ce que les tests unitaires ?**

Le test unitaire est un niveau de test de logiciel où les unités/composants individuels d'un logiciel sont testés. L'objectif est de valider que chaque unité du logiciel fonctionne comme prévu.
Lors de la création d'un logiciel, quelle que soit sa taille, ce logiciel doit être composé de classes Java. Tout comme la cellule est l'unité de base d'un organisme, une classe est l'unité de base d'un programme java. Le test unitaire consiste à tester toutes les classes individuelles d'un programme java pour s'assurer qu'elles fonctionnent comme prévu.

**Les avantages des tests unitaires ?** 

Le test unitaire vous donne l'assurance que votre code fonctionne correctement. Le test unitaire vous permet également de découvrir les bugs dès qu'ils apparaissent, car les bugs entraînent l'échec du test. Il y a beaucoup d'autres avantages des tests unitaires que je ne mentionnerai pas. Cela étant dit, parlons de JUnit.

**Qu’est-ce que JUnit ?** 

JUnit est un framework de test unitaire pour le langage Java. Vous pouvez en savoir plus sur JUnit [ici](https://junit.org/junit4/).

**Les annotations de JUnit** 

Les annotations JUnit sont utilisées pour contrôler le déroulement de nos tests. Elles donnent des instructions au testeur JUnit sur la façon d'exécuter nos tests. Les annotations JUnit de base sont :
 
`@Test, @Before, @After, @BeforeAll, @AfterClass, @Ignore, @Test(expected = Exception.class)
`
1. L’annotation `@Test` précise que la méthode est la méthode d’essai.
2. L'annotation `@Test(timeout=1000)` précise que la méthode sera rejetée si elle prend plus de 1000 millisecondes (1 seconde).
3. L'annotation `@BeforeAll` spécifie que la méthode ne sera invoquée qu'une seule fois, avant de commencer tous les tests.
4. L'annotation `@Before` spécifie que la méthode sera invoquée avant chaque test.
5. L'annotation `@After` spécifie que la méthode sera invoquée après chaque test.
6. L'annotation `@AfterClass` spécifie que la méthode ne sera invoquée qu'une seule fois, après avoir terminé tous les tests.

_Assert Classes :_

Assert est une méthode utilisée pour déterminer le statut d'un cas de test (réussite ou échec).
Les méthodes assert sont fournies par la classe org.junit.Assert qui étend la classe d'objets java.lang.

1. `void assertEquals(boolean expected, boolean actual)` : vérifie que deux primitives/objets sont égaux. Elle est surchargée.
2. `void assertTrue(boolean condition)` : vérifie qu'une condition est vraie.
3. `void assertFalse(boolean condition)` : vérifie qu'une condition est fausse.
4. `void assertNull(Object obj)` : vérifie que l'objet est nul.
5. `void assertNotNull(Object obj)` : vérifie que l'objet n'est pas nul.


#### Test avec JavaFX

Afin de tester vos vues FXML voici un tutoriel rapide de TestFX

Voici son [répot git](https://github.com/TestFX/TestFX/blob/master/README.md) 

Voici un tuto https://medium.com/@mglover/java-fx-testing-with-testfx-c3858b571320

Il faut mettre l'ensemble des imports suivants :
```java
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import java.io.IOException;

```
Vous aurez certainement à rajouter des imports en fonction de vos tests par exemple :

```java
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import static org.junit.jupiter.api.Assertions.assertTrue;
```

Chaque classe de test doit hériter de ApplicationTest. 

Vous pouvez utiliser des méthodes de test de UI comme : assertEquals(), clickOn(), verifyThat(), Assertions.assertThat(), lookup(), ainsi que d'autres méthodes pour simuler les interactions des utilisateurs.


Voici un exemple de méthode test où on verifie que le boutton est bien grisé c'est à dire qu'on peut pas appuyer dessus avant d'avoir rempli tous les champs:


```java
@Test
    void validateDisabledIfFieldsEmpty() {
        assertTrue(validate.disableProperty().get());
    }

```

Voila à quoi devrait ressembler votre fichier de test:


```java
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * SampleClass test class
 *
 * @author Prenom Nom <prenom.nom@etu.utc.fr>
 */
//La classe doit hériter de ApplicationTest
public class SampleClass extends ApplicationTest {
    // Test stage
    Stage mainStage;

    // Controller specific elements and data
    VBox mainRoot;
    //mettre le chemin vers votre contrôleur au lieu de "ihmMain.controllers.authentication.ConnectionForm »
    ihmMain.controllers.authentication.ConnectionForm controller;
    Button validate;


    @Override
    public void start(Stage stage) throws IOException {
        // Load FXML view and setup stage
        //Mettre le chemin vers votre FXML au lieu de "/ihmMain/views/authentication/connectionForm.fxml"
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ihmMain/views/authentication/connectionForm.fxml"));
        mainRoot = loader.load();
        controller = loader.getController();
        mainStage = stage;
        stage.setScene(new Scene(mainRoot));
        stage.show();
        stage.toFront();

        // Find validation button used multiple times in the tests below
        this.validate = (Button) mainRoot.lookup("#validateButton");

    }

    @Test
    void sampleMethod2() {
        assertTrue(validate.disableProperty().get());
    }

    @Test
    void sampleMethod2()

}
```
