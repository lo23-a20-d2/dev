package com;

import CommonClasses.SharedChannel;
import CommonClasses.TextMessage;
import CommonClasses.User;
import Interfaces.ServerCommToServerData;
import message.client.*;
import message.server.PutDeleteMsgServerMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class ClientMessageHandlerTest {
    private ClientManager manager;
    private ClientManager creatorManager;
    private ServerCom serverCom;
    private ServerCommToServerData dataInterface;

    private User channelCreator;
    private User client;
    private TextMessage textMsg;
    private SharedChannel sharedChannel;

    public ClientMessageHandlerTest(){
        this.manager = mock(ClientManager.class);
        this.creatorManager = mock(ClientManager.class);
        this.serverCom = mock(ServerCom.class);
        this.dataInterface = mock(ServerCommToServerData.class);
        this.textMsg = mock(TextMessage.class);
        this.sharedChannel = mock(SharedChannel.class);

        this.client = new User();
        this.channelCreator = new User();
    }

    @BeforeEach
    public void SetUp(){
        this.channelCreator.setId("creatorTest");
        this.client.setId("idClientTest");

        this.textMsg.setId("idMsgTest");
        this.textMsg.setTextContent("newContent");

        when(manager.getCom()).thenReturn(serverCom);
        when(serverCom.getDataInterface()).thenReturn(dataInterface);
        when(manager.getCom().getDataInterface().getChannelCreator("idChannelTest")).thenReturn(channelCreator);
        when(manager.getCom().getDataInterface().modifyMessage("newContent", "idMsgTest")).thenReturn(textMsg);
        when(manager.getCom().getClientById("creatorTest")).thenReturn(creatorManager);
        when(manager.getCom().getDataInterface().getSharedChannel("idChannelTest")).thenReturn(sharedChannel);
        when(manager.getUser()).thenReturn(client);
        when(manager.getCom().getDataInterface().createSharedChannel(client.getId(), "Channel test", "description", true)).thenReturn(sharedChannel);
        when(manager.getCom().getDataInterface().createSharedChannel(client.getId(), "Channel test", "description", false)).thenReturn(sharedChannel);
    }

    // ====================== Tests V1 ============================
    @Test
    @DisplayName("Quit channel test")
    public void ReqDeconnexionClientMessageTest() {
        String idChannel = "idChannelTest";
        ReqDeconnexionClientMessage msg = new ReqDeconnexionClientMessage(client.getId(), idChannel);
        msg.handle(manager);

        verify(dataInterface).removeConnectedUserServer(client.getId());
    }

//    @Test
//    @DisplayName("New Connection test")
//    public void NotifyNewConnectionClientMessageTest() throws IOException {
//        String idChannel = "idChannelTest";
//        String ipAddress = "ipAddressTest";
//
//        NotifyNewConnectionClientMessage msg = new NotifyNewConnectionClientMessage(client);
//        msg.handle(manager);
//
//        verify(dataInterface).addConnectedUserServer(client);
//        verify(dataInterface).getConnectedChannelList();
//    }

    @Test
    @DisplayName("Join channel test - isShared true")
    void JoinChannelClientMessageTest() {
        String idChannel = "idChannelTest";

        // Change the value of isShared to true
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(true);
        JoinChannelClientMessage msg = new JoinChannelClientMessage(idChannel, client.getId());

        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).addUserConnected(idChannel, client.getId());
        verify(dataInterface).getSharedChannel(idChannel);
        verify(dataInterface).getMessages(idChannel);
    }

    @Test
    @DisplayName("Join channel test - isShared false")
    void JoinChannelClientMessageTest2(){
        String idChannel = "idChannelTest";

        // Change the value of isShared to false
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(false);
        JoinChannelClientMessage msg = new JoinChannelClientMessage(idChannel, client.getId());

        msg.handle(manager);

        verify(dataInterface).getChannelCreator(idChannel);
    }

    @Test
    @DisplayName("New text message test - isShared true")
    void NewTextMsgClientMessageTest(){
        String msgContent = "content";
        String idMsg = "idMsgTest";
        String idChannel = "idChannelTest";

        // Change the value of isShared to true
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(true);
        NewTextMsgClientMessage msg = new NewTextMsgClientMessage(msgContent, client.getId(), idChannel);

        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).addTextMsg(msgContent, client.getId(), idChannel);
    }

    @Test
    @DisplayName("New text message test - isShared false")
    void NewTextMsgClientMessageTest2(){
        String msgContent = "content";
        String idMsg = "idMsgTest";
        String idChannel = "idChannelTest";

        // Change the value of isShared to false
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(false);
        NewTextMsgClientMessage msg = new NewTextMsgClientMessage(msgContent, idMsg, idChannel);

        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
    }

    @Test
    @DisplayName("New subscription message test - isShared true")
    void NewSubscriptionClientMessageTest(){
        String idChannel = "idChannelTest";

        // Change the value of isShared to true
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(true);
        NewSubscriptionClientMessage msg = new NewSubscriptionClientMessage(idChannel, client.getId());

        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).addUserConnected(idChannel, client.getId());
        verify(dataInterface).getMessages(idChannel);
    }

    @Test
    @DisplayName("New subscription message test - isShared false")
    void NewSubscriptionClientMessageTest2(){
        String idChannel = "idChannelTest";

        // Change the value of isShared to false
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(false);
        NewSubscriptionClientMessage msg = new NewSubscriptionClientMessage(idChannel, client.getId());

        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).getChannelCreator(idChannel);
    }

    @Test
    @DisplayName("New private channel test")
    void NewChannelClientMessageTest(){
        String idChannel = "idChannelTest";
        String channelName = "Channel test";
        String channelDescription = "description";
        NewChannelClientMessage msg = new NewChannelClientMessage(channelName, channelDescription, true);
        msg.handle(manager);

        verify(dataInterface).createSharedChannel(client.getId(), channelName, channelDescription, true);
    }

    @Test
    @DisplayName("New public channel test")
    void NewChannelClientMessageTest2(){
        String channelName = "Channel test";
        String channelDescription = "description";
        NewChannelClientMessage msg = new NewChannelClientMessage(channelName, channelDescription, false);
        msg.handle(manager);

        verify(dataInterface).createSharedChannel(client.getId(), channelName, channelDescription, false);
    }

    // ====================== Tests V2 ============================
    @Test
    @DisplayName("Modify message client test - isShared = true")
    public void ModifyMessageClientMessageTest(){
        String newContent = "newContent";
        String idMsg = "idMsgTest";
        String idChannel = "idChannelTest";

        // Change the value of isShared to true
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(true);

        ModifyMsgClientMessage msg = new ModifyMsgClientMessage(newContent, idMsg, idChannel);

        msg.handle(this.manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).modifyMessage(newContent, idMsg);
    }

    @Test
    @DisplayName("Modify message client test - isShared = false")
    public void ModifyMessageClientMessageTest2(){
        String newContent = "newContent";
        String idMsg = "idMsgTest";
        String idChannel = "idChannelTest";

        // Change the value of isShared to false
        when(manager.getCom().getDataInterface().isSharedChannel("idChannelTest")).thenReturn(false);

        ModifyMsgClientMessage msg = new ModifyMsgClientMessage(newContent, idMsg, idChannel);

        msg.handle(this.manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).getChannelCreator(idChannel);
    }

    @Test
    public void SendModifyUserClientMessageTest() {
        //To be completed
    }

    @Test
    public void DeleteMsgClientMessageTest() throws IOException {
        String idChannel = "idChannellllll";
        String idMsg = "idMsgfgggggg";
        //channel proprietaire
        when(manager.getCom().getDataInterface().isSharedChannel(idChannel)).thenReturn(false);
        DeleteMsgClientMessage msg = new DeleteMsgClientMessage(idMsg, idChannel);
        msg.handle(manager);
        verify(manager.getCom().getDataInterface(), never()).deleteMessage(idMsg);
        verify(manager.getCom(), times(1)).sendMessageToChannel(eq(idChannel), any(PutDeleteMsgServerMessage.class));

        //channel partage
        when(manager.getCom().getDataInterface().isSharedChannel(idChannel)).thenReturn(true);
        msg.handle(manager);
        verify(manager.getCom().getDataInterface(), times(1)).deleteMessage(idMsg);
        verify(manager.getCom(), times(2)).sendMessageToChannel(eq(idChannel), any(PutDeleteMsgServerMessage.class));
    }

//    @Test
//    @DisplayName("Search channel test")
//    void SearchChannelClientMessageTest(){
//        String input = "test channel";
//        SearchChannelClientMessage msg = new SearchChannelClientMessage(input);
//        msg.handle(manager);
//
//        verify(dataInterface).searchChannel(input);
//    }

    @Test
    @DisplayName("Delete channel test - isShared true")
    void SendDeleteChannelClientMessageTest() {
        String idChannel = "idChannelTest";
        when(manager.getCom().getDataInterface().isSharedChannel(idChannel)).thenReturn(true);
        SendDeleteChannelClientMessage msg = new SendDeleteChannelClientMessage(idChannel);
        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).deleteChannel(idChannel);
    }

    @Test
    @DisplayName("Delete channel test - isShared false")
    void SendDeleteChannelClientMessageTest2(){
        String idChannel = "idChannelTest";
        when(manager.getCom().getDataInterface().isSharedChannel(idChannel)).thenReturn(false);
        SendDeleteChannelClientMessage msg = new SendDeleteChannelClientMessage(idChannel);
        msg.handle(manager);

        verify(dataInterface).isSharedChannel(idChannel);
        verify(dataInterface).getChannelCreator(idChannel);
    }
}
