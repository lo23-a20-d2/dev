package data.impl;

import CommonClasses.*;
import data.ServerData;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ServerCommToServerDataImplTest {
    private final ServerData serverData;
    private Subscription subscription;
    private User tintin = new User("1","tintin", "milou", "Tintin", "Le célèbre reporter", null, null);
    private final Channel publicSharedChannel;
    private final OwnedChannel publicOwnedChannel;

    /**
     * Instantiates a new Server Comm To Server Data impl test.
     */
    public ServerCommToServerDataImplTest() {
        serverData = new ServerData();
        serverData.serverCommToServerData().addConnectedUserServer(tintin);
        List<Channel> channels = serverData.serverCommToServerData().getConnectedChannelList();
        publicOwnedChannel  = new OwnedChannel("1", "owned_channel", "this is a desc", false, tintin);

        if (channels.stream().filter(c -> c.getName().equals("Le Trésor de Rackham le Rouge")).findFirst().orElse(null) != null) {
            publicSharedChannel = channels.stream().filter(c -> c.getName().equals("Le Trésor de Rackham le Rouge")).findFirst().orElse(null);
        }
        else {
            publicSharedChannel = serverData.serverCommToServerData().createSharedChannel(
                    tintin.getId(),
                    "Le Trésor de Rackham le Rouge",
                    "Red Rackham's Treasure (French: Le Trésor de Rackham le Rouge)",
                    false);
            subscription = serverData.serverCommToServerData().addSubscription(
                    publicSharedChannel.getId(),
                    tintin.getId(),
                    Subscription.Role.ADMIN
            );
        }
        if (channels.stream().filter(c -> c.getName().equals("owned_channel")).findFirst().orElse(null) == null) {
            serverData.serverCommToServerData().addConnectedOwnedChannel(publicOwnedChannel);
        }

    }

    @Test
    void addConnectedUserServer() {
        serverData.serverCommToServerData().addConnectedUserServer(tintin);
        User userFromServer = serverData.serverCommToServerData().getConnectedUserById(tintin.getId());
        assertEquals(tintin.getId(), userFromServer.getId());
    }

    @Test
    void removeConnectedUserServer() {
    }

    @Test
    void getConnectedUserServer() {
    }

    @Test
    void getConnectedUsers() {
    }

    @Test
    void isSharedChannel() {
        assertTrue(serverData.serverCommToServerData().isSharedChannel(publicSharedChannel.getId()));
    }

    @Test
    void getChannelCreator() {
        OwnedChannel retrievedOwnedChannel = serverData.serverCommToServerData().getConnectedOwnedChannel(publicOwnedChannel.getId());
        assertEquals(serverData.serverCommToServerData().getChannelCreator(retrievedOwnedChannel.getId()).getId(), tintin.getId());
    }

    @Test
    void getConnectedChannelList() {
        List<Channel> connectedChannels = serverData.serverCommToServerData().getConnectedChannelList();
        Channel channel1 =  connectedChannels.stream().filter(c -> c.getName().equals(publicSharedChannel.getName())).findFirst().orElse(null);
        Channel channel2 =  connectedChannels.stream().filter(c -> c.getName().equals(publicOwnedChannel.getName())).findFirst().orElse(null);
        assertNotNull(channel1);
        assertNotNull(channel2);
    }

    @Test
    void getConnectedOwnedChannel() {
        OwnedChannel o = serverData.serverCommToServerData().getConnectedOwnedChannel("1");
        assertEquals(o.getName(), publicOwnedChannel.getName());
    }

    @Test
    void getUserSubscriptionList() {
    }

    @Test
    void modifyNickname() {
    }

    @Test
    void getSharedChannel() {
    }

    @Test
    void addTextMsg() {
    }

    @Test
    void addFileMsg() {
        // v4
    }

    @Test
    void modifyMessage() {
    }

    @Test
    void deleteMessage() {
    }

    @Test
    void deleteChannel() {
        serverData.serverCommToServerData().deleteChannel(publicSharedChannel.getId());
        assertNull(serverData.serverCommToServerData().getSharedChannel(publicSharedChannel.getId()));
    }

    @Test
    void updateLikersList() {
        Message textMessage = serverData.serverCommToServerData().addTextMsg("10% de réduction sur la carte jeune", tintin.getId(), publicSharedChannel.getId());
        serverData.serverCommToServerData().updateLikersList(textMessage.getId(), tintin.getId(), publicSharedChannel.getId());
        Message textMessageUpdated = serverData.serverCommToServerData().getMessage(publicSharedChannel.getId(), textMessage.getId());
        assertTrue(textMessageUpdated.getLikersList().contains(tintin.getId()));
    }

    @Test
    void getThreadContents() {
        TextMessage textMessage = serverData.serverCommToServerData().addTextMsg("Un philosophe a dit un jour « le mystère des Pyramides, c'est le mystère de la conscience dans laquelle on n'entre pas ».", tintin.getId(), publicSharedChannel.getId());
        serverData.serverCommToServerData().addTextReply("Les pharaons se faisaient enterrer avec leurs serviteurs.", tintin.getId(), publicSharedChannel.getId(), textMessage.getId());
        List<Message> threadContents = serverData.serverCommToServerData().getThreadContents(publicSharedChannel.getId(), textMessage.getId());
        assertEquals(textMessage.getIdThread(), threadContents.get(1).getIdThread());
    }

    @Test
    void createSharedChannel() {
    }

    @Test
    void getMessages() {
    }

    @Test
    void getSubscriptions() {
    }

    @Test
    void addUserConnected() {
    }

    @Test
    void getConnectedUserById() {

    }

    @Test
    void blockUserChannel() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.USER);

        serverData.serverCommToServerData()
                .blockUserChannel(publicSharedChannel.getId(), tintin.getId());

        Subscription retrievedSubscription = serverData.serverCommToServerData()
                .getSubscription(publicSharedChannel.getId(), tintin.getId());

        assertEquals(-1, retrievedSubscription.getEndDate());

    }

    @Test
    void suspendMemberChannel() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.USER);
        long date = 1609329000;

        serverData.serverCommToServerData()
                .suspendMemberChannel(publicSharedChannel.getId(), tintin.getId(), date);

        Subscription retrievedSubscription = serverData.serverCommToServerData()
                .getSubscription(publicSharedChannel.getId(), tintin.getId());

        assertEquals(date, retrievedSubscription.getEndDate());

    }

    @Test
    void updateAdmList() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.USER);

        serverData.serverCommToServerData().updateAdmList(tintin.getId(), publicSharedChannel.getId());

        Subscription retrievedSubscription = serverData.serverCommToServerData()
                .getSubscription(publicSharedChannel.getId(), tintin.getId());

        assertEquals(retrievedSubscription.getRole(), Subscription.Role.ADMIN);

    }

    @Test
    void removeFromUserList() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.ADMIN);

        serverData.serverCommToServerData().removeFromUserList(tintin.getId(), publicSharedChannel.getId());
        assertTrue(!serverData.serverCommToServerData()
                .getConnectedUsers(publicSharedChannel.getId()).contains(tintin.getId()));
    }

    @Test
    void getSubscriptionRole() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.ADMIN);

        Subscription retrievedSubscription = serverData.serverCommToServerData()
                .getSubscription(publicSharedChannel.getId(), tintin.getId());

        assertEquals(retrievedSubscription.getRole(), Subscription.Role.ADMIN);
    }

    @Test
    void addSubscription() {
        serverData.serverCommToServerData().addSubscription(
                publicSharedChannel.getId(), tintin.getId(), Subscription.Role.USER);

        Subscription retrievedSubscription = serverData.serverCommToServerData()
                .getSubscription(publicSharedChannel.getId(), tintin.getId());

        assertTrue(retrievedSubscription != null);
    }

    @Test
    void addConnectedOwnedChannel() {
        serverData.serverCommToServerData().addConnectedOwnedChannel(publicOwnedChannel);
        OwnedChannel retrievedOwnedChannel = serverData.serverCommToServerData().getConnectedOwnedChannel(publicOwnedChannel.getId());
        assertEquals(publicOwnedChannel.getId(), retrievedOwnedChannel.getId());

    }

    @Test
    void addFileReply() {
        //v4
    }
}