package com;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;

public class ClientConnectionThread extends Thread {
    private ServerCom com;

    public ClientConnectionThread(ServerCom com){
        this.com = com;
    }

    @Override
    public void run() {
        // Accept client connections
        while(true) {
            try {
                // Accept new client socket
                Socket clientSocket = com.getServerSocket().accept();
                com.log(Level.INFO, "Nouveau client accepté");

                // Create new client
                ClientManager clientManager = new ClientManager(clientSocket, com);
                com.addClient(clientManager);

            } catch (IOException exc){
                exc.printStackTrace();
            }
        }
    }
}
