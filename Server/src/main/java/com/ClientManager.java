package com;

import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.NotifyClientDeconnexionServerMessage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;

public class ClientManager implements IClientManager {
    // Network
    private Socket clientSocket;
    private ObjectOutputStream output;
    private ClientListener listener;
    private ServerCom com;

    private User user = null;


    public ClientManager(Socket clientS, ServerCom com) throws IOException {
        this.clientSocket = clientS;
        this.com = com;
        output = new ObjectOutputStream(clientSocket.getOutputStream());

        // Create new client listener
        listener = new ClientListener(clientSocket, this);
        listener.start();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User u){
        user = u;
    }

    public ServerCom getCom() {
        return com;
    }

    /**
     * Sending a server message to the client
     * @param msg message to send 
     * @throws IOException  if an error occurs while sending the message via the socket
     */
    public void sendMessage(message.server.ServerMessage msg) throws IOException {
        output.writeObject(msg);
    }

    /**
     * Management of client disconnection + communication to other users
     */
    public void clientDeconnexion(){
        com.removeClient(this);

        //Notification of connection to all other users
        NotifyClientDeconnexionServerMessage msg = new NotifyClientDeconnexionServerMessage(user.getId());
        com.sendMessageToServer(msg, user.getId());

        //Closing communication objects
        try {
            output.close();
            clientSocket.close();
        } catch (IOException ex) {
            com.log("Erreur de fermeture de la socket", ex);
        }

        com.log(Level.INFO, "Client deconnecte (" + user.getLogin() + ")");
    }
}

