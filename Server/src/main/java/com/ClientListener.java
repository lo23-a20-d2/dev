package com;

import message.client.ClientMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;

public class ClientListener extends Thread {
    // Network
    private ObjectInputStream input;
    private Socket clientSocket;
    private ClientManager clientManager;

    public ClientListener(Socket clientS, ClientManager manager){
        this.clientSocket = clientS;
        this.clientManager = manager;
    }

    @Override
    public void run() {
        //Opening of the input object
        try {
            input = new ObjectInputStream(clientSocket.getInputStream());
        }
        catch (IOException ex){
            clientManager.getCom().log("Erreur d'ouverture de l'object input", ex);
            return;
        }

        //Loop to read all the messages 
        while(true) {
            try{
                ClientMessage msg = (ClientMessage) input.readObject();
                clientManager.getCom().log(Level.INFO,
                        "Nouveau message reçu : "
                            + msg.toString()
                            + " ("
                            + (clientManager.getUser() != null ? clientManager.getUser().getLogin() : "undefined")
                            + ")");
                msg.handle(clientManager);
            }
            //Reading message error
            catch (ClassNotFoundException ex){
                clientManager.getCom().log("Erreur de lecture d'un message client", ex);
            }
            //Input error = user disconnection 
            catch (IOException ex){
                //user disconnection 
                clientManager.clientDeconnexion();
                return;
            }
        }
    }
}
