package com;

import CommonClasses.User;
import Interfaces.IServerCom;
import Interfaces.ServerCommToServerData;
import data.impl.ServerCommToServerDataImpl;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerCom implements IServerCom {
    // Network
    private List<ClientManager> clientManagerList = new ArrayList<>();
    private ServerSocket serverSocket = null;
    private ClientConnectionThread connectionThread;

    static final int PORT = 1996;
    private Logger logger;

    //Interfaces
    private ServerCommToServerData dataInterface;

    //Constructor
    public ServerCom() {
        // Logger creation
        logger = Logger.getLogger("ServerCom");
        logger.setLevel(Level.ALL); //to send messages from all levels 
        logger.setUseParentHandlers(false); // to delete the default console 
        ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(Level.ALL); // to only accept message with INFO level
        logger.addHandler(ch);

        logger.log(Level.INFO, "Hello world!");
    }

    public void init(ServerCommToServerData data){
        dataInterface = data;
    }

    //Getters + Setters
    public ServerSocket getServerSocket(){
        return serverSocket;
    }

    public void addClient(ClientManager clientManager){
        clientManagerList.add(clientManager);
    }

    public void removeClient(ClientManager clientManager){
        clientManagerList.remove(clientManager);
        dataInterface.removeConnectedUserServer(clientManager.getUser().getId());
    }

    public ServerCommToServerData getDataInterface() {
        return dataInterface;
    }

    //Methods

    /**
     * Saving a message in the ServerCom log
     * @param lvl Message importance level identifier
     * @param msg Logged message
     */
    public void log(Level lvl, String msg){
        logger.log(lvl, msg);
    }

    /**
     * Saving an exception in the ServerCom log
     * @param msg Logged message 
     * @param ex Exception to deal with 
     */
    public void log(String msg, Exception ex){
        logger.log(Level.SEVERE, msg, ex);
    }

    /**
     * Starts the acceptance of new connections
     */
    public void launchClientsConnexion(){
        try {
            serverSocket = new ServerSocket(PORT);
            connectionThread = new ClientConnectionThread(this);
            connectionThread.start();
            logger.log(Level.INFO, "Acceptation des connexions");
        }
        catch (IOException exc){
            exc.printStackTrace();
        }
    }

    public ClientManager getClientById(String id){
        return clientManagerList.stream().filter(clientManager -> clientManager.getUser() != null && clientManager.getUser().getId().equals(id)).findFirst().orElse(null);
    }

    public void sendMessageToChannel(String channelId, message.server.ServerMessage msg){
        List<User> connectedUsers = dataInterface.getConnectedUsers(channelId);

        connectedUsers.forEach(u -> {
            try {
                getClientById(u.getId()).sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendMessageToServer(message.server.ServerMessage msg){
        for (ClientManager cm : clientManagerList) {
            try {
                cm.sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessageToServer(message.server.ServerMessage msg, String blackListedId){
        for (ClientManager cm : clientManagerList) {
            if(cm.getUser() != null && !cm.getUser().getId().equals(blackListedId)) {
                try {
                    cm.sendMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
