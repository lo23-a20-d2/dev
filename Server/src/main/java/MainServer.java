import com.ServerCom;
import data.ServerData;

public class MainServer {

    public static void main(String[] args) {

        ServerCom com = new ServerCom();
        ServerData data = new ServerData();

        com.init(data.serverCommToServerData());
        com.launchClientsConnexion();


    }
}
