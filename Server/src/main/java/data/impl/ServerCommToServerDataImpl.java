package data.impl;

import CommonClasses.*;
import Interfaces.ServerCommToServerData;
import data.crud.MessageCRUD;
import data.crud.SharedChannelCRUD;
import data.crud.SubscriptionCRUD;
import data.utils.ImageSaver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;

/**
 * The implementation of the ServerCommToServerData interface.
 *
 * @author Benjamin Vaysse
 * @author Tom Bourg
 * @author Kenza Rifki
 * @author Rami Jerkaba
 * @author Mohamed Kuskusi
 */
public class ServerCommToServerDataImpl implements ServerCommToServerData {
    private Map<String,List<User>> userConnectedChannel;
    private List<User> connectedUserServer;
    private MessageCRUD messageCRUD;
    private SharedChannelCRUD sharedChannelCRUD;
    private SubscriptionCRUD subscriptionCRUD;
    private List<OwnedChannel> connectedOwnedChannelsList;
    private String avatarDirectoryPath;

    private void saveUserAvatar(User u){
        String filePath = avatarDirectoryPath + File.separator +u.getId() + "." +ImageSaver.getFileFormat();
        ImageSaver.saveImage(u.getAvatar(),filePath);
    }
    private BufferedImage getUserAvatar(User u){
        String filePath = avatarDirectoryPath + File.separator +u.getId() + "." +ImageSaver.getFileFormat();
        return ImageSaver.readImage(filePath);
    }

    /**
     * Instantiates a new Server comm to server data.
     *
     * @param messagePath      the message path
     * @param sharedChannel    the shared channel
     * @param subscriptionPath the subscription path
     */
    public ServerCommToServerDataImpl(String messagePath, String sharedChannel, String subscriptionPath, String avatarDirectoryPath) {
        messageCRUD = new MessageCRUD(messagePath);
        sharedChannelCRUD = new SharedChannelCRUD(sharedChannel);
        subscriptionCRUD = new SubscriptionCRUD(subscriptionPath);

        connectedUserServer = new ArrayList<>();
        userConnectedChannel = new HashMap<>();
        connectedOwnedChannelsList = new ArrayList<>();
        this.avatarDirectoryPath = avatarDirectoryPath;
    }

    @Override
    public void addConnectedUserServer( User user) {
        saveUserAvatar(user);
        connectedUserServer.add(user);
    }

    @Override
    public void removeConnectedUserServer(String userId) {
        connectedUserServer.removeIf(u -> u.getId().equals(userId));
        userConnectedChannel.forEach((k, users) -> {
            users.removeIf(u -> u.getId().equals(userId));
        });
        connectedOwnedChannelsList.removeIf(c -> c.getOwner().getId().equals(userId));
    }

    @Override
    public List<User> getConnectedUserServer() {
        return connectedUserServer;
    }

    @Override
    public List<User> getConnectedUsers(String channelId) {
        if(userConnectedChannel.containsKey(channelId)){
            return userConnectedChannel.get(channelId);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean isSharedChannel(String channelId) {
        SharedChannel channel = this.sharedChannelCRUD.read(channelId);
        if (channel == null){
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public User getChannelCreator(String channelId) {
        User channelCreator = null;

        for (OwnedChannel c : this.connectedOwnedChannelsList) {
            if (c.getId().equals(channelId)){
                channelCreator = (User) c.getOwner();
            }
        }

        return channelCreator;
    }

    @Override
    public List<Channel> getConnectedChannelList() {
        List<Channel> allChannels = new ArrayList<>();
        allChannels.addAll(sharedChannelCRUD.readAll());
        allChannels.addAll(connectedOwnedChannelsList);
        return allChannels;
    }

    @Override
    public OwnedChannel getConnectedOwnedChannel(String channelId){
        return connectedOwnedChannelsList.stream().filter(c -> c.getId().equals(channelId)).findFirst().orElse(null);
    }

    @Override
    public List<Subscription> getUserSubscriptionList(User u) {
        List<Subscription> subscriptions = subscriptionCRUD.readByIdUser(u.getId());
        subscriptions.forEach(s ->{
            String channelId = subscriptionCRUD.getChannelIdBySubscriptionId(s.getId());
            Channel c = sharedChannelCRUD.read(channelId);
            s.setChannel(c);
        });

        //Get subscriptions from ownedChannelList
        connectedOwnedChannelsList.forEach(c ->{
            c.getWhiteList().forEach(s ->{
                if(s.getUser().equals(u)){
                    subscriptions.add(s);
                }
            });
        });
        return subscriptions;
    }

    @Override
    public void modifyNickname(String nickname, String userId, String channelId) {
        SharedChannel c = sharedChannelCRUD.read(channelId);
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(channelId, userId);
        if(s!=null){
            s.setChannel(c);
            s.setNickname(nickname);
            subscriptionCRUD.update(s);
        }
    }

    @Override
    public SharedChannel getSharedChannel(String idChannel){
        SharedChannel channel = sharedChannelCRUD.read(idChannel);
        if(channel == null){
            return null;
        }
        List<Subscription> subscriptions = getSubscriptions(idChannel);
        channel.setUserList(new ArrayList<>(subscriptions));
        return channel;
    }

    @Override
    public TextMessage addTextMsg(String msgContent, String senderId, String channelId) {
        Date date = new Date();
        Subscription subscription = subscriptionCRUD.readByIdChannelAndIdUser(channelId,senderId);
        String id = UUID.randomUUID().toString();
        TextMessage message = new TextMessage(id,id,date,false,false,subscription.getId(),channelId,msgContent);
        return (TextMessage) messageCRUD.create(message);
    }

    @Override
    public TextMessage addTextReply(String msgContent, String senderId, String channelId, String parentMessageId) {
        Message parent = messageCRUD.read(parentMessageId);
        if(parent != null){
            String id = UUID.randomUUID().toString();
            String idThread = parent.getIdThread();
            Date date = new Date();
            Subscription subscription = subscriptionCRUD.readByIdChannelAndIdUser(channelId,senderId);
            TextMessage message = new TextMessage(id,idThread,date,false,false,subscription.getId(),channelId,msgContent);
            return (TextMessage) messageCRUD.create(message);
        }
        return null;
    }

    @Override
    public FileMessage addFileReply(String msgContent, String senderId, String channelId, String parentMessageId) {
        return null;
    }

    @Override
    public Message addFileMsg(String content, String senderId, String channelId) {
        return null;
    }

    @Override
    public TextMessage modifyMessage(String newContent, String messageId){
        Message message = this.messageCRUD.read(messageId);
        message.setEdited(true);
        if (message instanceof TextMessage) {
            ((TextMessage) message).setTextContent(newContent);
            this.messageCRUD.update(message);
            return (TextMessage) message;
        }
        else {
            return null;
        }
    }

    @Override
    public void deleteMessage(String messageId) {
        Message message = this.messageCRUD.read(messageId);
        message.setErased(true);
        this.messageCRUD.update(message);
    }

    @Override
    public void deleteChannel(String channelId){
        List<Message> msgs = this.messageCRUD.readByIdChannel(channelId);
        List<Subscription> subs = this.subscriptionCRUD.readByIdChannel(channelId);
        for(Message m : msgs){
            this.messageCRUD.delete(m.getId());
        }
        for(Subscription s : subs){
            this.subscriptionCRUD.delete(s.getId());
        }
        this.sharedChannelCRUD.delete(channelId);
    }

    @Override
    public void updateLikersList(String msgId, String senderId, String channelId) {
        Message m = messageCRUD.read(msgId);
        if (m.getLikersList() == null) {
            m.setLikersList(new ArrayList<>());
        }
        if (m.getLikersList().contains(senderId)) {
            m.getLikersList().remove(senderId);
        } else {
            m.getLikersList().add(senderId);
        }
        messageCRUD.update(m);
    }

    @Override
    public List<Message> getThreadContents(String channelId, String parentMessageId) {

        List<Message> thread = new ArrayList<>();
        this.messageCRUD.readByIdChannel(channelId).forEach(c ->{
            if(c.getIdThread().equals(parentMessageId)){
                thread.add(c);
            }
        });
        return thread;
    }


    @Override
    public List<Channel> searchChannel(String input, Category category) {
        List<Channel> list = new ArrayList<>();
        switch(category){

            case CHANNEL_NAME :
                list.addAll(sharedChannelCRUD.readByName(input));

                connectedOwnedChannelsList.forEach(c ->{
                    if(c.getName().toLowerCase().contains(input.toLowerCase())){
                        list.add(c);
                    }
                });
                break;

            case CHANNEL_DESC :
                list.addAll(sharedChannelCRUD.readByDescription(input));

                connectedOwnedChannelsList.forEach(c ->{
                    if(c.getDescription().toLowerCase().contains(input.toLowerCase())){
                        list.add(c);
                    }
                });
                break;

            case MEMBER :

                sharedChannelCRUD.readAll().forEach(c ->{

                    if (!subscriptionCRUD.readByIdChannelAndNickname(c.getId(), input).isEmpty()) {
                        list.add(c);
                    }

                });

                connectedOwnedChannelsList.forEach(c ->{

                    for (Subscription s : c.getWhiteList()) {
                        if (s.getNickname().toLowerCase().contains(input.toLowerCase())){
                            list.add(c);
                            break;
                        }
                    }

                });


        }
        return list;
    }

    @Override
    public Channel createSharedChannel(String creatorId, String name, String desc, boolean isPrivate) {
        String id = UUID.randomUUID().toString();
        User creator = null;
        for(User u : connectedUserServer){
            if(u.getId().equals(creatorId)){
                creator = u;
                break;
            }
        }
        SharedChannel newSharedChannel = new SharedChannel(id, name, desc, isPrivate, creator);
        return sharedChannelCRUD.create(newSharedChannel);
    }

    @Override
    public List<Message> getMessages(String idChannel) {
        return this.messageCRUD.readByIdChannel(idChannel);
    }

    @Override
    public Message getMessage(String idChannel, String idMessage){
        List<Message> messages = messageCRUD.readByIdChannel(idChannel);
        Optional<Message> message = messages.stream().filter(m -> m.getId().equals(idMessage)).findFirst();
        return message.orElse(null);
    }

    @Override
    public List<Subscription> getSubscriptions(String idChannel) {
        Channel c = sharedChannelCRUD.read(idChannel);
        List<Subscription> subscriptions = subscriptionCRUD.readByIdChannel(idChannel);
        subscriptions.forEach(s ->{
            s.setChannel(c);
            s.getUser().setAvatar(getUserAvatar(s.getUser()));
        });
        return subscriptions;
    }

    @Override
    public void addUserConnected(String idChannel, String idUser) {
        for (User u : connectedUserServer){
            if (u.getId().equals(idUser)){
                if(userConnectedChannel.containsKey(idChannel)){
                    if(!userConnectedChannel.get(idChannel).contains(u)){
                        userConnectedChannel.get(idChannel).add(u);
                    }
                }
                else{
                    userConnectedChannel.put(idChannel,new ArrayList<>());
                    userConnectedChannel.get(idChannel).add(u);
                }
            }
        }
    }

    @Override
    public User getConnectedUserById(String idUser) {
        return  connectedUserServer.stream().filter(u -> u.getId().equals(idUser)).findFirst().orElse(null);
    }

    @Override
    public void blockUserChannel(String idChannel, String idUserKicked) {

        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUserKicked);
        s.setChannel(sharedChannelCRUD.read(idChannel));
        s.setEndDate(-1); //Permanent kick = -1
        subscriptionCRUD.update(s);

    }

    @Override
    public void suspendMemberChannel(String idChannel, String idUserKicked, Long endDate) {
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUserKicked);
        s.setChannel(sharedChannelCRUD.read(idChannel));
        s.setEndDate(endDate);
        subscriptionCRUD.update(s);
    }


    @Override
    public void updateAdmList(String idUser, String idChannel) {
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setRole(Subscription.Role.ADMIN);
        s.setChannel(sharedChannelCRUD.read(idChannel));
        subscriptionCRUD.update(s);
    }

    @Override
    public void removeFromUserList(String idUser, String idChannel) {

        //Set Subscription isBanned attribute to TRUE
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setChannel(sharedChannelCRUD.read(idChannel));
        s.setIsBanned(true);
        s.setEndDate(0); //Simple kick = 0
        subscriptionCRUD.update(s);

        //Remove from connected user list
        userConnectedChannel.forEach((k, users) -> {
            users.removeIf(u -> u.getId().equals(idUser) && k.equals(idChannel));
        });

    }

    @Override
    public void cancelKick(String idUser, String idChannel){
        //Set Subscription isBanned attribute to FALSE
        Subscription s = subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
        s.setChannel(sharedChannelCRUD.read(idChannel));
        s.setIsBanned(false);
        s.setEndDate(0);
        subscriptionCRUD.update(s);
    }

    @Override
    public Subscription.Role getSubscriptionRole(String subscriptionId) {
        return subscriptionCRUD.read(subscriptionId).getRole();
    }

    @Override
    public Subscription addSubscription(String idChannel, String idUser, Subscription.Role role){
        SharedChannel channel = this.sharedChannelCRUD.read(idChannel);
        User u = null;
        for(User e : connectedUserServer){
            if(e.getId().equals(idUser)){
                u = e;
                break;
            }
        }
        if (channel != null && this.subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser) == null){

            String id = UUID.randomUUID().toString();
            Subscription s = new Subscription(id, u, channel, u.getLogin(), role);
            this.subscriptionCRUD.create(s);
            return s;

        }

        return null;

    }

    @Override
    public void addConnectedOwnedChannel(OwnedChannel channel) {
        if(!connectedOwnedChannelsList.contains(channel)){
            connectedOwnedChannelsList.add(channel);
            if(!userConnectedChannel.containsKey(channel.getId())){
                userConnectedChannel.put(channel.getId(),new ArrayList<>());
            }
        }
    }

    @Override
    public void removeConnectedOwnedChannel(String channelId) {
        connectedOwnedChannelsList.removeIf(c -> c.getId().equals(channelId));
    }

    @Override
    public void addConnectedOwnedChannel(List<OwnedChannel> channels) {
        for(OwnedChannel c : channels){
            addConnectedOwnedChannel(c);
        }
    }

    @Override
    public void updateConnectedUser(User u) {
        removeConnectedUserServer(u.getId());
        addConnectedUserServer(u);
    }

    @Override
    public Subscription getSubscription(String idChannel, String idUser) {
        return subscriptionCRUD.readByIdChannelAndIdUser(idChannel, idUser);
    }

    @Override
    public void updateSubscriptionOwnedChannel(String idChannel, Subscription sub) {
        connectedOwnedChannelsList.stream().filter(c -> c.getId().equals(idChannel)).findFirst().ifPresent(
                c -> c.getWhiteList().stream().filter(s -> s.getId().equals(sub.getId())).findFirst().ifPresentOrElse(
                        s -> s.setNickname(sub.getNickname()),
                        () -> c.getWhiteList().add(sub)
                )
        );
    }
}
