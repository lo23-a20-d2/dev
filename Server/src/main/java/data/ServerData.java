package data;

import Interfaces.ServerCommToServerData;
import data.impl.ServerCommToServerDataImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Server data.
 * Create files and storage directory if not exist
 * Instance interfaces implementations
 *
 * @author Tom Bourg
 * @author Kenza Rifki
 * @author Rami Jerbaka
 * @author Benjamin Vaysse
 */
public class ServerData {
    private final String DIRECTORY_NAME = "storage";
    private final String AVATAR_DIRECTORY_NAME = "img";
    private final String SHARED_CHANNELS_FILENAME = "shared_channels.json";
    private final String MESSAGE_FILENAME = "messages.json";
    private final String SUBSCRIPTION_FILENAME = "subscriptions.json";
    private File directory;
    private File avatarDirectory;
    private Map<String,File> files;

    private ServerCommToServerData serverCommToServerData;

    /**
     * Instantiates a new Local data.
     */
    public ServerData() {
        String directoryPath = this.getClass().getResource("").getPath()+DIRECTORY_NAME;

        //Storage Directory Creation
        directory = new File(directoryPath);
        if(! directory.exists()){
            directory.mkdir();
        }

        //Avatar Directory Creation
        String avatarDirectoryPath = directoryPath + File.separator + AVATAR_DIRECTORY_NAME;
        avatarDirectory = new File(avatarDirectoryPath);
        if(! avatarDirectory.exists()){
            avatarDirectory.mkdir();
        }

        files = new HashMap();
        files.put("messages",new File( directoryPath + File.separator + MESSAGE_FILENAME));
        files.put("shared_channels",new File( directoryPath + File.separator + SHARED_CHANNELS_FILENAME));
        files.put("subscriptions",new File( directoryPath + File.separator + SUBSCRIPTION_FILENAME));
        files.values().forEach(file -> {
            if(!file.exists()){
                try {
                    file.createNewFile();
                    FileWriter myWriter = new FileWriter(file);
                    myWriter.write("[]");
                    myWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        serverCommToServerData = new ServerCommToServerDataImpl(
                files.get("messages").getPath(),
                files.get("shared_channels").getPath(),
                files.get("subscriptions").getPath(),
                avatarDirectoryPath
        );
    }

    /**
     * Gets directory.
     *
     * @return the directory
     */
    public File getDirectory() {
        return directory;
    }

    /**
     * Gets files.
     *
     * @return the files
     */
    public Map<String, File> getFiles() {
        return files;
    }

    /**
     * Server comm to server data server comm to server data.
     *
     * @return the server comm to server data
     */
    public ServerCommToServerData serverCommToServerData() {
        return serverCommToServerData;
    }
}
