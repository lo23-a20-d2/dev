package data;


import CommonClasses.*;
import data.crud.MessageCRUD;

public class MainServerData {
    public static void main(String[] args) {
        ServerData data = new ServerData();
        User u1 = new User("1","test1","test1","test1","test",null,null);
//        User u2 = new User("2","test2","test2","test2","test",null,null);
//        User u2clone = new User("2","test233","test2","test2","test",null,null);
//        data.serverCommToServerData().addConnectedUserServer(u1);
//        data.serverCommToServerData().addConnectedUserServer(u2);
//        System.out.println(data.serverCommToServerData().getConnectedUserServer().size());
//        data.serverCommToServerData().removeConnectedUserServer(u2clone.getId());
//        System.out.println(data.serverCommToServerData().getConnectedUserServer().size());
        data.serverCommToServerData().addConnectedUserServer(u1);
        Channel c = data.serverCommToServerData().createSharedChannel(u1.getId(),"test","desc",false);
        System.out.println(u1.getId());
        Subscription s =data.serverCommToServerData().addSubscription(c.getId(),u1.getId(), Subscription.Role.ADMIN);
        //System.out.println(s.getId());
        TextMessage t = data.serverCommToServerData().addTextMsg("test","1",c.getId());
        data.serverCommToServerData().modifyMessage("modified",t.getId());
        //data.serverCommToServerData().deleteChannel(c.getId());
    }
}
