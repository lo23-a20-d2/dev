package data.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordHasherTest {
    @Test
    void testPasswordHasheur() {
        String password1 = "veryStrongPassword";
        String password2 = "weakPassword";
        String hashedPassword1 = PasswordHasher.hashPassword(password1);
        String hashedPassword2 = PasswordHasher.hashPassword(password1);
        String hashedPassword3 = PasswordHasher.hashPassword(password2);
        assertEquals(hashedPassword1, hashedPassword2);
        assertNotEquals(hashedPassword1, hashedPassword3);
    }
}