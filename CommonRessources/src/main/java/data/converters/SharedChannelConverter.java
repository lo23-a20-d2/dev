package data.converters;

import CommonClasses.OwnedChannel;
import CommonClasses.SharedChannel;
import CommonClasses.User;
import data.entities.SharedChannelEntity;
import data.entities.UserEntity;

/**
 * The type SharedChannel converter.
 *
 * SharedChannelConverter is used to convert SharedChannelEntity to SharedChannel or SharedChannel to SharedChannelEntity.
 * SharedChannelConverter should only be used by SharedChannelCRUD.
 *
 */
public class SharedChannelConverter {


    /**
     * Conver to dto message.
     *
     * @param entity the entity
     * @return the SharedChannel
     */
    public static SharedChannel converToDto(SharedChannelEntity entity){
        SharedChannel s = null;


        if(entity !=null){
            User u = new User();
            u.setId(entity.getCreatorId());
            s = new SharedChannel(
                    entity.getId(),
                    entity.getName(),
                    entity.getDescription(),
                    entity.isPrivate(),
                    u
            );
        }
        return s;
    }

    /**
     * Conver to entity SharedChannel entity.
     *
     * @param dto the dto
     * @return the SharedChannel entity
     */
    public static SharedChannelEntity converToEntity(SharedChannel dto){
        SharedChannelEntity s = null;
        if(dto != null){
            s = new SharedChannelEntity(
                    dto.getId(),
                    dto.getName(),
                    dto.getDescription(),
                    dto.isPrivate(),
                    dto.getCreator().getId()
            );
        }
        return s;
    }
}
