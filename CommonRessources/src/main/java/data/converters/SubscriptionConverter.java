package data.converters;

import CommonClasses.Channel;
import CommonClasses.SharedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import data.entities.SubscriptionEntity;

public class SubscriptionConverter {


    public static Subscription convertToDto(SubscriptionEntity entity){
        Subscription s = null;
        User u = null;
        Channel c = null;

        if(entity !=null){
            u = new User(entity.getIdUser());
            c = new SharedChannel(entity.getIdChannel());
            s = new Subscription(
                    entity.getId(),
                    u,
                    c,
                    entity.getNickname(),
                    entity.getRole(),
                    entity.getIsBanned(),
                    entity.getEndDate()
            );

        }

        return s;
    }

    public static SubscriptionEntity converToEntity(Subscription dto){
        SubscriptionEntity entity  = null;
        if(dto != null){
            entity = new SubscriptionEntity(
                    dto.getId(),
                    dto.getUser().getId(),
                    dto.getChannel().getId(),
                    dto.getNickname(),
                    dto.getRole(),
                    dto.getIsBanned(),
                    dto.getEndDate()
            );
        }
        return entity;
    }
}
