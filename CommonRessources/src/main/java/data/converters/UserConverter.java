package data.converters;

import CommonClasses.Message;
import CommonClasses.User;
import data.entities.MessageEntity;
import data.entities.UserEntity;
import data.utils.ImageSaver;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class UserConverter {

    public static User converToDto(UserEntity entity){
        User u = null;
        BufferedImage avatar = ImageSaver.readImage(entity.getAvatarPath());

        if(entity !=null){
            u = new User(
                    entity.getId(),
                    entity.getLogin(),
                    entity.getPassword(),
                    entity.getFirstName(),
                    entity.getLastName(),
                    entity.getBirthDate(),
                    avatar
            );
        }
        return u;
    }

    public static UserEntity converToEntity(User dto, String avatarPath){
        UserEntity u = null;
        if(dto != null){
            u = new UserEntity(
                    dto.getId(),
                    dto.getLogin(),
                    dto.getPassword(),
                    dto.getFirstName(),
                    dto.getLastName(),
                    dto.getBirthDate(),
                    avatarPath
            );
        }
        return u;
    }
}
