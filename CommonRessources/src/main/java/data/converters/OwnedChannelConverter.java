package data.converters;

import CommonClasses.OwnedChannel;
import CommonClasses.User;
import data.entities.OwnedChannelEntity;

public class OwnedChannelConverter {

    public static OwnedChannel converToDto(OwnedChannelEntity entity){
        OwnedChannel s = null;

        if(entity !=null){
            User u = new User();
            u.setId(entity.getOwnerId());
            s = new OwnedChannel(
                    entity.getId(),
                    entity.getName(),
                    entity.getDescription(),
                    entity.isPrivate(),
                    u
            );
        }
        return s;
    }

    public static OwnedChannelEntity converToEntity(OwnedChannel dto){
        OwnedChannelEntity s = null;
        if(dto != null){
            s = new OwnedChannelEntity(
                    dto.getId(),
                    dto.getName(),
                    dto.getDescription(),
                    dto.isPrivate(),
                    dto.getOwner().getId()
            );
        }
        return s;
    }

}
