package data.converters;

import CommonClasses.FileMessage;
import CommonClasses.Message;
import CommonClasses.TextMessage;
import data.entities.MessageEntity;

/**
 * The type Message converter.
 *
 * MessageConverter is used to convert MessageEntity to Message or Message to MessageEntity.
 * MessageConverter should only be used by MessageCRUD.
 * @author Tom Bourg
 */
public class MessageConverter {
    /**
     * Conver to dto message.
     *
     * @param entity the entity
     * @return the message
     */
    public static Message converToDto(MessageEntity entity){
        Message m = null;
        if(entity.isFileMessage()){
            m = new FileMessage(
                    entity.getId(),
                    entity.getIdThread(),
                    entity.getDatetime(),
                    entity.isEdited(),
                    entity.isErased(),
                    entity.getIdSubscription(),
                    entity.getIdChannel(),
                    entity.getContent()
            );
        }
        else{
            m = new TextMessage(
                    entity.getId(),
                    entity.getIdThread(),
                    entity.getDatetime(),
                    entity.isEdited(),
                    entity.isErased(),
                    entity.getIdSubscription(),
                    entity.getIdChannel(),
                    entity.getContent()
            );
        }
        m.setLikersList(entity.getLikersList());
        return m;
    }

    /**
     * Conver to entity message entity.
     *
     * @param dto the dto
     * @return the message entity
     */
    public static MessageEntity converToEntity(Message dto){
        MessageEntity m = new MessageEntity(
                dto.getId(),
                dto.getIdThread(),
                dto.getDatetime(),
                dto.isEdited(),
                dto.isErased(),
                dto.getIdChannel(),
                dto.getIdSubscription()
        );
        if(dto instanceof FileMessage){
            m.setFileMessage(true);
            m.setContent(((FileMessage) dto).getFilePath());
        }
        else{
            m.setFileMessage(false);
            m.setContent(((TextMessage) dto).getTextContent());
        }
        m.setLikersList(dto.getLikersList());
        return m;
    }
}
