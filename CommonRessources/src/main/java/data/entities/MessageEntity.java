package data.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Message entity.
 *
 * MessageEntity should only be used by MessageCRUD.
 * It corresponds to a saved Message in Json.
 * Message entity is instanced when we deserialize from Json.
 * We use MessageConverter to convert a MessageEntity to a TextMessage or a FileMessage.
 * Without this class, we won't be able to save TextMessage and FileMessage in the same file.
 * @author Tom Bourg
 * @author Mohamed Kuskusi
 */
public class MessageEntity {
    private String id;
    private String idThread;
    private Date datetime;
    private boolean edited;
    private boolean erased;
    private String idChannel;
    private String idSubscription;
    private String content;
    private List<String> likersList;
    private boolean fileMessage;

    /**
     * Instantiates a new empty Message entity.
     * (empty constructor is mandatory for Jackson)
     */
    public MessageEntity() {
    }



    /**
     * Instantiates a new Message entity.
     *
     * @param id             the id
     * @param idThread       the id thread
     * @param datetime       the datetime
     * @param edited         the edited
     * @param erased         the erased
     * @param idChannel      the id channel
     * @param idSubscription the id subscription
     */
    public MessageEntity(String id, String idThread, Date datetime, boolean edited, boolean erased, String idChannel, String idSubscription) {
        this.id = id;
        this.idThread = idThread;
        this.datetime = datetime;
        this.edited = edited;
        this.erased = erased;
        this.idChannel = idChannel;
        this.idSubscription = idSubscription;
        this.likersList = null;
    }

    public List<String> getLikersList() {
        return likersList;
    }

    public void setLikersList(List<String> likersList) {
        this.likersList = likersList;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets id thread.
     *
     * @return the id thread
     */
    public String getIdThread() {
        return idThread;
    }

    /**
     * Sets id thread.
     *
     * @param idThread the id thread
     */
    public void setIdThread(String idThread) {
        this.idThread = idThread;
    }

    /**
     * Gets datetime.
     *
     * @return the datetime
     */
    public Date getDatetime() {
        return datetime;
    }

    /**
     * Sets datetime.
     *
     * @param datetime the datetime
     */
    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    /**
     * Is edited boolean.
     *
     * @return the boolean
     */
    public boolean isEdited() {
        return edited;
    }

    /**
     * Sets edited.
     *
     * @param edited the edited
     */
    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    /**
     * Is erased boolean.
     *
     * @return the boolean
     */
    public boolean isErased() {
        return erased;
    }

    /**
     * Sets erased.
     *
     * @param erased the erased
     */
    public void setErased(boolean erased) {
        this.erased = erased;
    }

    /**
     * Gets id channel.
     *
     * @return the id channel
     */
    public String getIdChannel() {
        return idChannel;
    }

    /**
     * Sets id channel.
     *
     * @param idChannel the id channel
     */
    public void setIdChannel(String idChannel) {
        this.idChannel = idChannel;
    }

    /**
     * Gets id subscription.
     *
     * @return the id subscription
     */
    public String getIdSubscription() {
        return idSubscription;
    }

    /**
     * Sets id subscription.
     *
     * @param idSubscription the id subscription
     */
    public void setIdSubscription(String idSubscription) {
        this.idSubscription = idSubscription;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Is file message boolean.
     *
     * @return the boolean
     */
    public boolean isFileMessage() {
        return fileMessage;
    }

    /**
     * Sets file message.
     *
     * @param fileMessage the file message
     */
    public void setFileMessage(boolean fileMessage) {
        fileMessage = fileMessage;
    }
}
