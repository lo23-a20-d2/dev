package data.entities;

import CommonClasses.Subscription;


/**
 * The type Subscription entity.
 * SubscriptionEntity should only be used by SubscriptionCRUD.
 * It corresponds to a saved Subscription in Json.
 * Subscription entity is instanced when we deserialize from Json.
 * We use SubscriptionConverter to convert a SubscriptionEntity to a Subscription.
 * @author Tom Bourg
 */
public class SubscriptionEntity {

    private String id;
    private String idUser;
    private String idChannel;
    private String nickname;
    private Subscription.Role role;
    private Boolean isBanned;
    private long endDate;

    /**
     * Instantiates a new Subscription entity.
     * (empty constructor is mandatory for Jackson)
     */
    public SubscriptionEntity(){
    }

    /**
     * Instantiates a new Subscription entity.
     *
     * @param id        the id
     * @param idUser    the id user
     * @param idChannel the id channel
     * @param nickname  the nickname
     * @param role      the role
     * @param isBanned  the is banned
     * @param endDate   the end date
     */
    public SubscriptionEntity(String id, String idUser, String idChannel, String nickname, Subscription.Role role, Boolean isBanned, long endDate) {
        this.id = id;
        this.idUser = idUser;
        this.idChannel = idChannel;
        this.nickname = nickname;
        this.role = role;
        this.isBanned = isBanned;
        this.endDate = endDate;
    }

    /**
     * Gets nickname.
     *
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets nickname.
     *
     * @param nickname the nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Subscription.Role getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Subscription.Role role) {
        this.role = role;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets id user.
     *
     * @return the id user
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * Gets id channel.
     *
     * @return the id channel
     */
    public String getIdChannel() {
        return idChannel;
    }

    /**
     * Gets is banned.
     *
     * @return the is banned
     */
    public Boolean getIsBanned() {
        return isBanned;
    }

    /**
     * Sets is banned.
     *
     * @param banned the banned
     */
    public void setIsBanned(Boolean banned) {
        isBanned = banned;
    }

    /**
     * Gets end date.
     *
     * @return the end date
     */
    public long getEndDate() {
        return endDate;
    }

    /**
     * Sets end date.
     *
     * @param endDate the end date
     */
    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
