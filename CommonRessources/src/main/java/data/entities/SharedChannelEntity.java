package data.entities;



/**
 * The type SharedChannel entity.
 *
 * SharedChanneEntity should only be used by SharedChannelCRUD.
 * It corresponds to a saved SharedChannel in Json.
 * SharedChannel entity is instanced when we deserialize from Json.
 * We use SharedChannelConverter to convert a SharedChannelEntity to a SharedChannel.
 * @author sbrahiti
 */
public class SharedChannelEntity {
    private String id;
    private String name;
    private String description;
    private boolean isPrivate;
    private String creatorId;


    /**
     * Instantiates a new SharedChannel entity.
     * (empty constructor is mandatory for Jackson)
     */
    public SharedChannelEntity() {
    }

    /**
     * Instantiates a new SharedChannel entity.
     *
     * @param id            The id
     * @param name          The name
     * @param description   The description
     * @param isPrivate     is Private
     * @param creatorId     The creator id
     */
    public SharedChannelEntity(String id, String name, String description, boolean isPrivate, String creatorId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
        this.creatorId = creatorId;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Is private boolean.
     *
     * @return the boolean
     */
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * Sets private.
     *
     * @param aPrivate the a private
     */
    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    /**
     * Gets creator id.
     *
     * @return the creator id
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * Sets creator id.
     *
     * @param creatorId the creator id
     */
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

}