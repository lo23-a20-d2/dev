package data.entities;

import java.util.Date;

/**
 * The type User entity.
 *
 * UserEntity should only be used by UserCRUD.
 * It corresponds to a saved User in Json.
 * User entity is instanced when we deserialize from Json.
 * We use UserConverter to convert a UserEntity to a User.
 * @author Tom Bourg
 */
public class UserEntity {

    private String id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String avatarPath;

    /**
     * Instantiates a new User entity.
     * (empty constructor is mandatory for Jackson)
     */
    public UserEntity() {
    }

    /**
     * Instantiates a new User entity.
     *
     * @param id         the id
     * @param login      the login
     * @param password   the password
     * @param firstName  the first name
     * @param lastName   the last name
     * @param birthDate  the birth date
     * @param avatarPath the avatar path
     */
    public UserEntity(String id, String login, String password, String firstName, String lastName, Date birthDate, String avatarPath) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.avatarPath = avatarPath;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets avatar path.
     *
     * @return the avatar path
     */
    public String getAvatarPath() {
        return avatarPath;
    }

    /**
     * Sets avatar path.
     *
     * @param avatarPath the avatar path
     */
    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }
}
