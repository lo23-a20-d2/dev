package data.entities;

/**
 * The type Owned channel entity.
 *
 * OwnedChannelEntity should only be used by OwnedChannelCRUD.
 * It corresponds to a saved OwnedChannel in Json.
 * OwnedChannel entity is instanced when we deserialize from Json.
 * We use OwnedChannelConverter to convert a OwnedChannelEntity to a OwnedChannel.
 * @author Tom Bourg
 */
public class OwnedChannelEntity {
    private String id;
    private String name;
    private String description;
    private boolean isPrivate;
    private String ownerId;

    /**
     * Instantiates a new Owned channel entity.
     * (empty constructor is mandatory for Jackson)
     */
    public OwnedChannelEntity() {
    }

    /**
     * Instantiates a new Owned channel entity.
     *
     * @param id          the id
     * @param name        the name
     * @param description the description
     * @param isPrivate   the is private
     * @param ownerId     the owner id
     */
    public OwnedChannelEntity(String id, String name, String description, boolean isPrivate, String ownerId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
        this.ownerId = ownerId;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Is private boolean.
     *
     * @return the boolean
     */
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * Sets private.
     *
     * @param aPrivate the a private
     */
    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    /**
     * Gets owner id.
     *
     * @return the owner id
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Sets owner id.
     *
     * @param ownerId the owner id
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
