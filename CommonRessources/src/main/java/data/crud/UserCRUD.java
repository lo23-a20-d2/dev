package data.crud;

import CommonClasses.Message;
import CommonClasses.User;
import com.fasterxml.jackson.core.type.TypeReference;
import data.converters.MessageConverter;
import data.converters.UserConverter;
import data.entities.MessageEntity;
import data.entities.UserEntity;
import data.utils.ImageSaver;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type User crud.
 *
 * @author Tom Bourg
 * @author Kenza Rifki
 * @author Rami Jerbaka
 */
public class UserCRUD extends ICRUD<User> {
    private String imageDirectoryPath;
    /**
     * Instantiates a new User crud.
     *
     * @param filePath the file path
     */
    public UserCRUD(String filePath, String imageDirectoryPath) {
        super(filePath);
        this.imageDirectoryPath = imageDirectoryPath;
    }

    private List<UserEntity> getAllEntities(){
        List<UserEntity> list = null;
        try {
            list = objectMapper.readValue(new File(filePath), new TypeReference<List<UserEntity>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private String getImageFilePath(User u){
        return imageDirectoryPath + File.separator + u.getId() + "."+ ImageSaver.getFileFormat();
    }


    @Override
    public User read(String id) {
        List<UserEntity> entities = getAllEntities();
        if(entities != null){
            for (UserEntity e : entities) {
                if(e.getId().equals(id)){
                    return UserConverter.converToDto(e);
                }
            }
        }
        return null;
    }

    public User readWithLogin(String login) {
        List<UserEntity> entities = getAllEntities();
        if(entities != null){
            for (UserEntity e : entities) {
                if(e.getLogin().equals(login)){
                    return UserConverter.converToDto(e);
                }
            }
        }
        return null;
    }

    @Override
    public List<User> readAll() {
        List<User> list = new ArrayList<>();
        List<UserEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> list.add(UserConverter.converToDto(e)));
        }
        return list;
    }



    @Override
    public User update(User elementUpdated) {
        List<UserEntity> entities = getAllEntities();
        String imageFilePath = getImageFilePath(elementUpdated);
        ImageSaver.saveImage(elementUpdated.getAvatar(),imageFilePath);
        UserEntity entity = UserConverter.converToEntity(elementUpdated,imageFilePath);

        for (UserEntity e : entities) {
            if (e.getId().equals(entity.getId())) {
                e.setAvatarPath(entity.getAvatarPath());
                e.setBirthDate(entity.getBirthDate());
                e.setFirstName(entity.getFirstName());
                e.setLastName(entity.getLastName());
                e.setLogin(entity.getLogin());
                e.setPassword(entity.getPassword());
                break;
            }
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return elementUpdated;
    }

    @Override
    public void delete(String id) {
        List<UserEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getId().equals(id));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User create(User elementNew) {
        List<UserEntity> entities = getAllEntities();
        String imageFilePath = getImageFilePath(elementNew);
        ImageSaver.saveImage(elementNew.getAvatar(), imageFilePath);
        if(entities != null){
            entities.add(UserConverter.converToEntity(elementNew, imageFilePath));
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementNew;
    }

}
