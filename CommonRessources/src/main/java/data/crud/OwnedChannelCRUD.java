package data.crud;

import CommonClasses.OwnedChannel;
import CommonClasses.SharedChannel;
import com.fasterxml.jackson.core.type.TypeReference;
import data.converters.OwnedChannelConverter;
import data.converters.SharedChannelConverter;
import data.entities.OwnedChannelEntity;
import data.entities.SharedChannelEntity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Owned channel crud.
 *
 * @author Benjamin Vaysse
 */
public class OwnedChannelCRUD extends ICRUD<OwnedChannel> {

    /**
     * Instantiates a new Owned channel crud.
     *
     * @param filePath the file path
     */
    public OwnedChannelCRUD(String filePath) {
        super(filePath);
    }
    private List<OwnedChannelEntity> getAllEntities(){
        List<OwnedChannelEntity> list = null;
        try {
            list = objectMapper.readValue(new File(filePath), new TypeReference<List<OwnedChannelEntity>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
    @Override
    public OwnedChannel read(String id) {
        List<OwnedChannelEntity> entities = getAllEntities();
        if(entities != null){
            for (OwnedChannelEntity e : entities) {
                if(e.getId().equals(id)){
                    return OwnedChannelConverter.converToDto(e);
                }
            }
        }
        return null;
    }

    public List<OwnedChannel> readByUserId(String idUser) {
        List<OwnedChannelEntity> entities = getAllEntities();
        List<OwnedChannel> channels = new ArrayList<>();
        if(entities != null){
            for (OwnedChannelEntity e : entities) {
                if(e.getOwnerId().equals(idUser)){
                    channels.add(OwnedChannelConverter.converToDto(e));
                }
            }
        }
        return channels;
    }
    @Override
    public List<OwnedChannel> readAll() {
        List<OwnedChannel> list = new ArrayList<>();
        List<OwnedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> list.add(OwnedChannelConverter.converToDto(e)));
        }
        return list;
    }

    @Override
    public OwnedChannel update(OwnedChannel elementUpdated) {
        List<OwnedChannelEntity> entities = getAllEntities();

        OwnedChannelEntity entity = OwnedChannelConverter.converToEntity(elementUpdated);

        for (OwnedChannelEntity e : entities) {
            if (e.getId().equals(entity.getId())) {
                e.setOwnerId(entity.getOwnerId());
                e.setDescription(entity.getDescription());
                e.setName(entity.getName());
                e.setPrivate(entity.isPrivate());
                break;
            }
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementUpdated;
    }

    @Override
    public void delete(String channelID) {
        List<OwnedChannelEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getId().equals(channelID));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public OwnedChannel create(OwnedChannel elementNew) {
        List<OwnedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.add(OwnedChannelConverter.converToEntity(elementNew));
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementNew;
    }
}
