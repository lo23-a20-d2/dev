package data.crud;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * The type Icrud. It is abstract class, used to create all the other CRUD (MessageCRUD, UserCRUD, etc...)
 *
 * @param <T> the type parameter
 *
 * @author Tom Bourg
 * @author Kenza Rifki
 * @author Rami Jerbaka
 */
public abstract class ICRUD<T> {
    /**
     * The File path.
     */
    protected String filePath;
    /**
     * The Object mapper.
     */
    protected ObjectMapper objectMapper;

    /**
     * Instantiates a new Icrud.
     *
     * @param filePath the file path
     */
    public ICRUD(String filePath) {
        this.filePath = filePath;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Create t.
     *
     * @param elementNew the element new
     * @return the t
     */
    public abstract T create(T elementNew);

    /**
     * Read t.
     *
     * @param id the id
     * @return the t
     */
    public abstract T read(String id);

    /**
     * Read all list.
     *
     * @return the list
     */
    public abstract List<T> readAll();

    /**
     * Update t.
     *
     * @param elementUpdated the element updated
     * @return the t
     */
    public abstract T update(T elementUpdated);

    /**
     * Delete t.
     *
     * @param id the id
     */
    public abstract void delete(String id);
}
