package data.crud;

import CommonClasses.Subscription;

import CommonClasses.User;
import com.fasterxml.jackson.core.type.TypeReference;
import data.converters.MessageConverter;
import data.converters.SubscriptionConverter;

import data.entities.MessageEntity;
import data.entities.SharedChannelEntity;
import data.entities.SubscriptionEntity;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Subscription crud.
 *
 * @author Rami Jerbaka
 */
public class SubscriptionCRUD extends ICRUD<Subscription>{


    /**
     * Instantiates a new Subscription crud.
     *
     * @param filePath the file path
     */
    public SubscriptionCRUD(String filePath) {
        super(filePath);
    }

    private List<SubscriptionEntity> getAllEntities(){
        List<SubscriptionEntity> list = null;
        try {
            list = objectMapper.readValue(new File(filePath), new TypeReference<List<SubscriptionEntity>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Subscription read(String id) {
        return null;
    }

    /**
     * Read by id channel and id user subscription.
     *
     * @param idChannel the id channel
     * @param idUser    the id user
     * @return the subscription
     */
    public Subscription readByIdChannelAndIdUser(String idChannel, String idUser) {
        List<SubscriptionEntity> list = getAllEntities();

        if(list != null){
            for(SubscriptionEntity s : list){
                if(s.getIdChannel().equals(idChannel) && s.getIdUser().equals(idUser) ){
                    return SubscriptionConverter.convertToDto(s);
                }
            }
        }
        return null;
    }

    /**
     * Read by id channel list.
     *
     * @param id the id
     * @return the list
     */
    public List<Subscription> readByIdChannel(String id) {
        List<SubscriptionEntity> list = getAllEntities();
        List<Subscription> newList = new ArrayList<>();

        if(list != null){
            for(SubscriptionEntity s : list){
                if(s.getIdChannel().equals(id)){
                    newList.add(SubscriptionConverter.convertToDto(s));
                }
            }
        }
        return newList;
    }

    /**
     * Read by id channel list and nickname.
     *
     * @param id the id
     * @param nickname the nickname
     * @return the list
     */
    public List<Subscription> readByIdChannelAndNickname(String id, String nickname) {
        List<SubscriptionEntity> list = getAllEntities();
        List<Subscription> newList = new ArrayList<>();

        if(list != null){
            for(SubscriptionEntity s : list){
                if(s.getIdChannel().equals(id) && s.getNickname().toLowerCase().contains(nickname.toLowerCase())){
                    newList.add(SubscriptionConverter.convertToDto(s));
                }
            }
        }
        return newList;
    }

    /**
     * Read subscription list by idUser.
     *
     * @param idUser the user id
     * @return the list
     */
    public List<Subscription> readByIdUser(String idUser) {
        List<SubscriptionEntity> list = getAllEntities();
        List<Subscription> newList = new ArrayList<>();

        if(list != null){
            for(SubscriptionEntity s : list){
                if(s.getIdUser().equals(idUser)){
                    newList.add(SubscriptionConverter.convertToDto(s));
                }
            }
        }
        return newList;
    }

    @Override
    public List<Subscription> readAll() {
        List<Subscription> list = new ArrayList<>();
        List<SubscriptionEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> list.add(SubscriptionConverter.convertToDto(e)));
        }
        return list;
    }

    @Override
    public Subscription create(Subscription elementNew) {

        List<SubscriptionEntity> entities = getAllEntities();

        if(entities != null){
            entities.add(SubscriptionConverter.converToEntity(elementNew));
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementNew;
    }

    @Override
    public Subscription update(Subscription elementUpdated) {
        List<SubscriptionEntity> entities = getAllEntities();

        SubscriptionEntity entity = SubscriptionConverter.converToEntity(elementUpdated);

        for (SubscriptionEntity e : entities) {
            if (e.getId().equals(entity.getId())) {
                e.setNickname(entity.getNickname());
                e.setRole(entity.getRole());
                e.setIsBanned(entity.getIsBanned());
                e.setEndDate(entity.getEndDate());
                break;
            }
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return elementUpdated;
    }

    @Override
    public void delete(String id) {
        List<SubscriptionEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getId().equals(id));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteByIdChannelAndIdUser(String idChannel, String idUser){



        List<SubscriptionEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getIdChannel().equals(idChannel) && e.getIdUser().equals(idUser));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getChannelIdBySubscriptionId(String idSubscription){
        List<SubscriptionEntity> list = getAllEntities();
        for (SubscriptionEntity s : list){
            if(s.getId().equals(idSubscription)){
                return s.getIdChannel();
            }
        }
        return null;
    }
}
