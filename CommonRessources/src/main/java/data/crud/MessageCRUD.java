package data.crud;

import CommonClasses.Message;
import com.fasterxml.jackson.core.type.TypeReference;
import data.converters.MessageConverter;
import data.entities.MessageEntity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Message crud.
 *
 * @author Mohamed Kuskusi
 * @author Tom Bourg
 */
public class MessageCRUD extends ICRUD<Message> {


    /**
     * Instantiates a new Message crud.
     *
     * @param filePath the file path
     */
    public MessageCRUD(String filePath) {
        super(filePath);
    }


    private List<MessageEntity> getAllEntities(){
        List<MessageEntity> list = null;
        try {
            list = objectMapper.readValue(new File(filePath), new TypeReference<List<MessageEntity>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Message read(String id) {
        List<MessageEntity> entities = getAllEntities();
        if(entities != null){
            for (MessageEntity e : entities) {
                if(e.getId().equals(id)){
                    return MessageConverter.converToDto(e);
                }
            }
        }
        return null;
    }

    /**
     * Read by id channel list.
     *
     * @param id the id
     * @return a list of Message
     */
    public List<Message> readByIdChannel(String id) {
        List<Message> channelList = new ArrayList<>();
        List<MessageEntity> entities = getAllEntities();
        if(entities != null){
            for (MessageEntity e : entities) {
                if(e.getIdChannel().equals(id)){
                    channelList.add(MessageConverter.converToDto(e));
                }
            }
        }
        return channelList;
    }

    /**
     * Read by id subscription list.
     *
     * @param id the id
     * @return the list
     */
    public List<Message> readByIdSubscription(String id) {
//        List<Message> list = readAll();
//        List<Message> newList = new ArrayList<>();
//
//        if(list != null){
//            for(Message m : list){
//                if(m.getIdSubscription().equals(id)){
//                    newList.add(m);
//                }
//            }
//        }
//        return newList;
        List<Message> userList = new ArrayList<>();
        List<MessageEntity> entities = getAllEntities();
        if(entities != null){
            for (MessageEntity e : entities) {
                if(e.getIdSubscription().equals(id)){
                    userList.add(MessageConverter.converToDto(e));
                }
            }
        }
        return userList;
    }

    @Override
    public List<Message> readAll() {
        List<Message> list = new ArrayList<>();
        List<MessageEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> list.add(MessageConverter.converToDto(e)));
        }
        return list;
    }

    @Override
    public Message update(Message elementUpdated) {
        List<MessageEntity> entities = getAllEntities();

        MessageEntity entity = MessageConverter.converToEntity(elementUpdated);

        for (MessageEntity e : entities) {
            if (e.getId().equals(entity.getId())) {
                e.setContent(entity.getContent());
                e.setFileMessage(entity.isFileMessage());
                e.setEdited(entity.isEdited());
                e.setErased(entity.isErased());
                e.setIdThread(entity.getIdThread());
                e.setLikersList(entity.getLikersList());
                break;
            }
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return elementUpdated;
    }

    @Override
    public void delete(String id) {
        List<MessageEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getId().equals(id));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Message create(Message elementNew) {
        List<MessageEntity> entities = getAllEntities();
        if(entities != null){
            entities.add(MessageConverter.converToEntity(elementNew));
        }
        try {
                objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
                e.printStackTrace();
        }
        return elementNew;
    }
}
