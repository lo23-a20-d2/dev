package data.crud;

import CommonClasses.Message;
import CommonClasses.SharedChannel;
import com.fasterxml.jackson.core.type.TypeReference;
import data.converters.MessageConverter;
import data.converters.SharedChannelConverter;
import data.entities.MessageEntity;
import data.entities.SharedChannelEntity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Shared channel crud.
 *
 * @author Benjamin Vaysse
 */
public class SharedChannelCRUD extends ICRUD<SharedChannel> {

    /**
     * Instantiates a new Shared channel crud.
     *
     * @param filePath the file path
     */
    public SharedChannelCRUD(String filePath) {
        super(filePath);
    }

    private List<SharedChannelEntity> getAllEntities(){
        List<SharedChannelEntity> list = null;
        try {
            list = objectMapper.readValue(new File(filePath), new TypeReference<List<SharedChannelEntity>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public SharedChannel read(String id) {
        List<SharedChannelEntity> entities = getAllEntities();
        if(entities != null){
            for (SharedChannelEntity e : entities) {
                if(e.getId().equals(id)){
                    return SharedChannelConverter.converToDto(e);
                }
            }
        }
        return null;
    }

    public  List<SharedChannel> readByName(String name){
        List<SharedChannel> list = new ArrayList<>();
        List<SharedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> {
                if(e.getName().toLowerCase().contains(name.toLowerCase())){
                    list.add(SharedChannelConverter.converToDto(e));
                }
            });
        }
        return list;
    }

    public  List<SharedChannel> readByDescription(String description){
        List<SharedChannel> list = new ArrayList<>();
        List<SharedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> {
                if(e.getDescription().toLowerCase().contains(description.toLowerCase())){
                    list.add(SharedChannelConverter.converToDto(e));
                }
            });
        }
        return list;
    }

    @Override
    public List<SharedChannel> readAll() {
        List<SharedChannel> list = new ArrayList<>();
        List<SharedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.forEach(e -> list.add(SharedChannelConverter.converToDto(e)));
        }
        return list;
    }


    @Override
    public SharedChannel update(SharedChannel elementUpdated) {
        List<SharedChannelEntity> entities = getAllEntities();

        SharedChannelEntity entity = SharedChannelConverter.converToEntity(elementUpdated);

        for (SharedChannelEntity e : entities) {
            if (e.getId().equals(entity.getId())) {
                e.setCreatorId(entity.getCreatorId());
                e.setDescription(entity.getDescription());
                e.setName(entity.getName());
                e.setPrivate(entity.isPrivate());
                break;
            }
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementUpdated;
    }

    @Override
    public void delete(String channelID) {
        List<SharedChannelEntity> entities = getAllEntities();
        entities.removeIf(e -> e.getId().equals(channelID));
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SharedChannel create(SharedChannel elementNew) {
        List<SharedChannelEntity> entities = getAllEntities();
        if(entities != null){
            entities.add(SharedChannelConverter.converToEntity(elementNew));
        }
        try {
            objectMapper.writeValue(new File(filePath), entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementNew;
    }
}
