package data.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * We use this class to save images.
 *
 * All images are stored as ".png"
 *
 * @author Tom Bourg
 */
public class ImageSaver {
    private static final String FILE_FORMAT = "png";

    /**
     * Gets file format (always png)
     *
     * @return the file format
     */
    public static String getFileFormat() {
        return FILE_FORMAT;
    }

    /**
     * Saves the image at the desired file path.
     *
     * If an image at the desired file path already exists, it is deleted before saving the new one.
     *
     * @param image        the image
     * @param fullFilePath the full file path (path + file + extension)
     */
    public static void saveImage(BufferedImage image, String fullFilePath){
        if(image != null){
            File imageFile = new File(fullFilePath);
            if(imageFile.exists()){
                imageFile.delete();
            }
            try {
                ImageIO.write(image,FILE_FORMAT,imageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Read image buffered image.
     *
     * @param fullFilePath the full file path (path + file + extension)
     * @return the buffered image
     */
    public static BufferedImage readImage(String fullFilePath){
        BufferedImage image = null;
        File imageFile = new File(fullFilePath);
        if(imageFile.exists()){
            try {
                image = ImageIO.read(imageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return image;
    }
}
