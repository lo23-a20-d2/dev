package data.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Password hasher class
 *
 * The password is hashed at two distinct moments :
 * - Once when the user creates his account, or modifies his password. Before being stored locally.
 * - Every time the user logs-in, to compare the entered password with the one stored in db.
 *
 * @author Benjamin Vaysse <benjamin.vaysse@etu.utc.fr>
 */
public class PasswordHasher {
    /**
     * Hash password string.
     *
     * Using MD5 for hashing the password.
     *
     * Ideally, we should be using another hash function because MD5 has known security issues.
     *
     * @param passwordToHash the password to hash (string)
     * @return the string
     */
    public static String hashPassword(String passwordToHash) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return (generatedPassword);
    }
}