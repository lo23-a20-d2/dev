package message.client;

import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.PutDeleteMsgServerMessage;
import message.server.PutNewTextMsgServerMessage;

import java.util.List;

/**
 * Client message sent when deleting a message.
 * Used to provide the server with the information of the message and request its deletion.
 * @author Yiwen Wang
 */
public class DeleteMsgClientMessage implements ClientMessage{
    private String idMsg;
    private String idChannel;

    public DeleteMsgClientMessage(String idMsg, String idChannel) {
        this.idMsg = idMsg;
        this.idChannel = idChannel;
    }

    @Override
    public void handle(IClientManager manager) {
        boolean isShared=manager.getCom().getDataInterface().isSharedChannel(idChannel);
        //If the channel is shared, we delete the message from the server
        if (isShared){
            manager.getCom().getDataInterface().deleteMessage(idMsg);
        }
        //Message initialization
        PutDeleteMsgServerMessage msg=new PutDeleteMsgServerMessage(idMsg,idChannel);
        //Sending the message to all channel users
        manager.getCom().sendMessageToChannel(idChannel,msg);
    }
}
