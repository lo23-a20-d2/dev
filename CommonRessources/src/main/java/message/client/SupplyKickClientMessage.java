package message.client;

import CommonClasses.Channel;
import Interfaces.IClientManager;
import message.server.NotifyKickSuccessServerMessage;
import message.server.NotifyUserKickedServerMessage;

import java.io.IOException;
/**
 * Client message sent when an admin kick an user
 *
 * @author Benoit RAMS
 */

public class SupplyKickClientMessage implements ClientMessage {
    String idUserKicked;
    Channel channel;
    long endDate;

    public SupplyKickClientMessage(String idUserKicked, Channel channel, long endDate) {
        this.idUserKicked = idUserKicked;
        this.channel = channel;
        this.endDate = endDate;
    }

    @Override
    public void handle(IClientManager manager) {
        NotifyKickSuccessServerMessage msg = new NotifyKickSuccessServerMessage(channel.getId(), idUserKicked, endDate);
        manager.getCom().sendMessageToChannel(channel.getId(), msg);
        NotifyUserKickedServerMessage kMsg = new NotifyUserKickedServerMessage(channel, idUserKicked, endDate);
        try {
            manager.getCom().getClientById(idUserKicked).sendMessage(kMsg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}