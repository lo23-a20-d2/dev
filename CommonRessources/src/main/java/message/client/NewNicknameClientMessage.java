package message.client;

import Interfaces.IClientManager;
import message.client.ClientMessage;
import message.server.PutNewNicknameServerMessage;

/**
 * Client message sent when modification of the nickname
 * @author Violette Quitral <violette.quitral@etu.utc.fr>
 */

public class NewNicknameClientMessage implements ClientMessage {
	private String nickname;
	private String idUser;
	private String idChannel;
	
	
	public String getNickname() {
		return nickname;
	}



	public String getIdUser() {
		return idUser;
	}



	public String getIdChannel() {
		return idChannel;
	}

	

	public NewNicknameClientMessage(String nickname, String idUser, String idChannel) {
		this.nickname = nickname;
		this.idUser = idUser;
		this.idChannel = idChannel;
	}



	public void handle(IClientManager manager) {
		boolean isShared = manager.getCom().getDataInterface().isSharedChannel(this.idChannel);
		//If the channel is shared, we modify the nickname
		if(isShared) {
			manager.getCom().getDataInterface().modifyNickname(this.nickname, this.idUser, this.idChannel);
		}
		//Initialization of the message
		PutNewNicknameServerMessage msg = new PutNewNicknameServerMessage(this.nickname, this.idUser, this.idChannel);
        //Notify all connected users about the new nickname
		manager.getCom().sendMessageToChannel(idChannel,msg);
	}

}
