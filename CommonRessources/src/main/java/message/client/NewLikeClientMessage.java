package message.client;

import CommonClasses.Subscription;
import Interfaces.IClientManager;
import message.server.NewLikeServerMessage;
import message.server.PutLikesServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Client message sent when adding a like to a message 
 * Notify the server of new likes and send information to users
 * @author Benoit RAMS
 */
public class NewLikeClientMessage implements ClientMessage {

    private String idMsg;
    private String idSender;
    private String idChannel;


    public NewLikeClientMessage(String msg, String sender, String channel) {
        this.idMsg = msg;
        this.idSender = sender;
        this.idChannel = channel;
    }

    @Override
    public void handle(IClientManager manager) {

        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);

        //If shared channel then we directly modify the likes 
        if (isShared) {
            manager.getCom().getDataInterface().updateLikersList(idMsg, idSender, idChannel);
            PutLikesServerMessage msg = new PutLikesServerMessage(idChannel, idSender, idMsg);
            manager.getCom().sendMessageToChannel(idChannel, msg);
        }
        //If Channel owner then we must modify the number of likes locally
        else {
            String idCreator = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
            NewLikeServerMessage msg = new NewLikeServerMessage(idChannel, idSender, idMsg);
            try {
                manager.getCom().getClientById(idCreator).sendMessage(msg);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }

        }

    }
}
