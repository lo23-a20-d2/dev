package message.client;

import CommonClasses.TextMessage;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.PutModifiedMsgServerMessage;

import java.util.List;

/**
 * Client message sent by the owner of a channel when modifying a message
 * Provides information about the modification to the server
 * @author Gabriel Souza e Silva
 */
public class SupplyModifiedMsgClientMessage implements ClientMessage{
    private TextMessage textMsg;

    public SupplyModifiedMsgClientMessage (TextMessage textMsg){
        this.textMsg = textMsg;
    }

    public void handle(IClientManager manager) {
        // Message initialization
        PutModifiedMsgServerMessage msg = new PutModifiedMsgServerMessage(textMsg);
        manager.getCom().sendMessageToChannel(textMsg.getIdChannel(), msg);
    }
}
