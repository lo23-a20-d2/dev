package message.client;

import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.PutLikesServerMessage;

import java.util.List;

/**
 * Client Message sent when you want to update the number of likes on a message in an owner channel.
 * Allows you to send a message to all channel users with the number of likes of a message
 *
 * @author Gauthier Cabot
 */
public class SupplyLikersListClientMessage implements ClientMessage {

    private String idChannel;
    private String idUser;
    private String idMsg;

    public SupplyLikersListClientMessage(String idChannel, String idUser, String idMsg) {
        this.idChannel = idChannel;
        this.idUser = idUser;
        this.idMsg = idMsg;
    }

    @Override
    public void handle(IClientManager manager) {
        // send message to all users of the channel
        PutLikesServerMessage msg = new PutLikesServerMessage(idChannel, idUser, idMsg);
        manager.getCom().sendMessageToChannel(idChannel, msg);
    }
}
