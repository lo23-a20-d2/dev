package message.client;

import Interfaces.IClientManager;
import message.server.NotifyPromotionServerMessage;

/**
 * Client message sent when promotion of an user
 *
 * @author Violette Quitral <violette.quitral@etu.utc.fr>
 */

public class RequestPromotionClientMessage implements ClientMessage {
	private String idUser;
	private String idChannel;

	public RequestPromotionClientMessage(String idUser, String idChannel) {
		this.idUser = idUser;
		this.idChannel = idChannel;
	}

	public void handle(IClientManager manager) {
		boolean isShared = manager.getCom().getDataInterface().isSharedChannel(this.idChannel);
		//If the channel is shared, we modify the nickname
		if (isShared) {
			manager.getCom().getDataInterface().updateAdmList(idUser, idChannel);
		}
		//Initialization of the message
		NotifyPromotionServerMessage msg = new NotifyPromotionServerMessage(this.idUser, this.idChannel);
		//Notify all connected users about the promotion
		manager.getCom().sendMessageToChannel(idChannel,msg);
	}

}
