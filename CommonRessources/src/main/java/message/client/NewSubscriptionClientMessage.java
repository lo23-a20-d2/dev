package message.client;

import CommonClasses.Message;
import CommonClasses.SharedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.NewSubscriptionServerMessage;
import message.server.NotifyNewMemberServerMessage;
import message.server.ProvideChannelToMemberServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Client message sent when subscription to a channel
 * Notifies the server of the subscription
 * @author Gabriel Souza e Silva
 */
public class NewSubscriptionClientMessage implements ClientMessage {
    private String idChannel;
    private String idUser;

    public String getIdChannel(){
        return idChannel;
    }

    public String getIdUser(){
        return idUser;
    }

    public NewSubscriptionClientMessage(String idChannel, String idUser){
        this.idChannel = idChannel;
        this.idUser = idUser;
    }

    public void handle(IClientManager manager){
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(this.idChannel);

        if (isShared){
            manager.getCom().getDataInterface().addUserConnected(this.idChannel, this.idUser);
            Subscription sub = manager.getCom().getDataInterface().addSubscription(idChannel, idUser, Subscription.Role.USER);
            SharedChannel ch = manager.getCom().getDataInterface().getSharedChannel(idChannel);

            List<Message> messages = manager.getCom().getDataInterface().getMessages(this.idChannel);

            //Sending channel information to the newly connected
            ProvideChannelToMemberServerMessage msg = new ProvideChannelToMemberServerMessage(ch, messages, ch.getUserList());
            try {
                manager.sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Notifies logged in users of the new channel connection
            NotifyNewMemberServerMessage msgNotify = new NotifyNewMemberServerMessage(sub);
            manager.getCom().sendMessageToChannel(idChannel, msgNotify);
        }
        else{
            // Prendre le createur du channel
            User creator = manager.getCom().getDataInterface().getChannelCreator(idChannel);
            // Initialization of server return message
            NewSubscriptionServerMessage msgSubServer = new NewSubscriptionServerMessage(idChannel, manager.getUser());

            // Get the client manager of the creator
            IClientManager creatorMng = manager.getCom().getClientById(creator.getId());

            // Sending of the message 
            try {
                creatorMng.sendMessage(msgSubServer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}