package message.client;

import CommonClasses.Channel;
import CommonClasses.SharedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.NewChannelServerMessage;
import message.server.NotifyNewChannelServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Client message sent when creating a shared channel
 * Allows to provide the channel information to the server and request its creation.
 * @author Yiwen Wang
 */
public class NewChannelClientMessage implements ClientMessage {
    private String name;
    private String description;
    private boolean isPrivate;

    public NewChannelClientMessage(String name, String description, boolean isPrivate) {
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
    }

    @Override
    public void handle(IClientManager manager) {
        //create channel and get the returned created channel
        Channel channel = manager.getCom().getDataInterface().createSharedChannel(manager.getUser().getId(), name, description, isPrivate);
        User u=manager.getUser();
        //add creator's subscription
        manager.getCom().getDataInterface().addSubscription(channel.getId(),u.getId(), Subscription.Role.ADMIN);//ADMIN role for creator?
        //get subscription list
        List<Subscription> subscriptions=manager.getCom().getDataInterface().getSubscriptions(channel.getId());
        NewChannelServerMessage msg = new NewChannelServerMessage(channel, subscriptions);
        try{
            manager.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        if (!channel.isPrivate()){
            NotifyNewChannelServerMessage nMsg=new NotifyNewChannelServerMessage(channel);
            manager.getCom().sendMessageToServer(nMsg, u.getId());
        }

    }
}
