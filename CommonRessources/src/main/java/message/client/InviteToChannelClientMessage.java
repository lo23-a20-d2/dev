package message.client;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.InviteToChannelServerMessage;
import message.server.NotifyNewMemberServerMessage;
import message.server.NotifyNewSubscriptionServerMessage;

import java.io.IOException;

/**
 * Client message sent when inviting a user to a channel
 * Used to process the invitation on the server
 * @author Gauthier Cabot.
 */
public class InviteToChannelClientMessage implements ClientMessage{
    private String idUser;
    private String idChannel;
    private Subscription.Role role;

    public InviteToChannelClientMessage(String idUser, String idChannel, Subscription.Role role) {
        this.idUser = idUser;
        this.idChannel = idChannel;
        this.role = role;
    }


    @Override
    public void handle(IClientManager manager) {
        //If the channel is shared
        if(manager.getCom().getDataInterface().isSharedChannel(idChannel)){
            //Adding the subscription to the channel
            Subscription sub = manager.getCom().getDataInterface().addSubscription(idChannel, idUser, role);
            //Notification of the new subscription to other users
            manager.getCom().sendMessageToServer(new NotifyNewMemberServerMessage(sub),idUser);

            //Sending channel information to the new subscriber
            try {
                Channel chan = manager.getCom().getDataInterface().getSharedChannel(idChannel);
                manager.getCom().getClientById(idUser).sendMessage(new NotifyNewSubscriptionServerMessage(chan, sub));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Channel owner
        else{
            try {
                String creatorId = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
                User user = manager.getCom().getDataInterface().getConnectedUserById(idUser);
                manager.getCom().getClientById(creatorId).sendMessage(new InviteToChannelServerMessage(user,idChannel,role));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
