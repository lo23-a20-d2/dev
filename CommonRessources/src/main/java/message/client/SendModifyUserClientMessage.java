package message.client;

import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.PutUpdatedUserServerMessage;

import java.util.List;

public class SendModifyUserClientMessage implements ClientMessage {
    private User user;

    public SendModifyUserClientMessage(User user) {
        this.user = user;
    }

    @Override
    public void handle(IClientManager manager) {
        // Update connected user in server data
        manager.getCom().getDataInterface().updateConnectedUser(user);

        // Create put message for updated user
        PutUpdatedUserServerMessage msg = new PutUpdatedUserServerMessage(user);

        // Send message to all users connected to server
        manager.getCom().sendMessageToServer(msg);
    }
}
