package message.client;

import CommonClasses.Channel;
import Interfaces.IClientManager;
import message.server.NotifyKickCanceledServerMessage;
import message.server.NotifyKickCanceledUserServerMessage;

import java.io.IOException;
/**
 * Client message sent when an admin cancel a kick
 *
 * @author Benoit RAMS
 */

public class SupplyCancelKickClientMessage implements ClientMessage {
    String idUser;
    Channel channel;

    public SupplyCancelKickClientMessage(String idUser, Channel channel) {
        this.idUser = idUser;
        this.channel = channel;
    }

    @Override
    public void handle(IClientManager manager) {
        NotifyKickCanceledServerMessage msg = new NotifyKickCanceledServerMessage(idUser, channel.getId());
        manager.getCom().sendMessageToChannel(channel.getId(), msg);
        NotifyKickCanceledUserServerMessage kcMsg = new NotifyKickCanceledUserServerMessage(idUser, channel);
        try {
            manager.getCom().getClientById(idUser).sendMessage(kcMsg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }
}
