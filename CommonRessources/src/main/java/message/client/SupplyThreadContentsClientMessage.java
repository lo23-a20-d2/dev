package message.client;


import CommonClasses.Message;
import Interfaces.IClientManager;
import message.server.PutThreadContentsServerMessage;

import java.util.List;

/**
 * Message client envoyé lorsqu'on veut voir le thread d'un message
 * Permet d'envoyer le thread à l'utilisateur qui a démandé de visualiser le thread
 *
 * @author Benoit Rams
 */
public class SupplyThreadContentsClientMessage implements ClientMessage {

    private String idChannel;
    private List<Message> threadContents;
    private String idUser;

    public SupplyThreadContentsClientMessage(String idChannel, List<Message> threadContents, String idUser) {
        this.idChannel = idChannel;
        this.threadContents = threadContents;
        this.idUser = idUser;
    }

    @Override
    public void handle(IClientManager manager) {
        //Remarque changer la conception
        //Doit-on afficher le thread pour tout les user ou seulement 1 seul
        PutThreadContentsServerMessage msg = new PutThreadContentsServerMessage(idChannel, threadContents);
        try {
            manager.getCom().getClientById(idUser).sendMessage(msg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
