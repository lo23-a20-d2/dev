package message.client;

import CommonClasses.Category;
import CommonClasses.Channel;
import Interfaces.IClientManager;
import message.server.SearchChannelServerMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Client message sent when searching for a channel
 *
 * @author Benoit Rams
 */

public class SearchChannelClientMessage implements ClientMessage {

    String input;
    Category category;

    public SearchChannelClientMessage(String input, Category category) {
        this.input = input;
        this.category = category;
    }

    @Override
    public void handle(IClientManager manager)  {
        List<Channel> channelList = manager.getCom().getDataInterface().searchChannel(input, category);

        SearchChannelServerMessage msg = new SearchChannelServerMessage(new ArrayList<>(channelList));

        try {
            manager.sendMessage(msg);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
