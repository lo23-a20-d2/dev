package message.client;

import Interfaces.IClientManager;
import message.server.KickMemberServerMessage;

import java.io.IOException;

/**
 * Client message sent when kicking an user
 * Used to send the necessary informations of the kicked person to the server
 *
 * @author Benoit RAMS
 */
public class KickMemberClientMessage implements ClientMessage {

    String idUser;
    String idChannel;
    String idUserKicked;
    long endDate;

    public KickMemberClientMessage(String idUser, String idChannel, String idUserKicked, long endDate) {
        this.idUser = idUser;
        this.idChannel = idChannel;
        this.idUserKicked = idUserKicked;
        this.endDate = endDate;
    }

    @Override
    public void handle(IClientManager manager) {
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);
        //If the channel is shared, then we modified it
        if (isShared) {
            manager.getCom().getDataInterface().removeFromUserList(idUserKicked, idChannel);
            if (endDate == -1) {
                //kick permanent
                manager.getCom().getDataInterface().blockUserChannel(idChannel, idUserKicked);
            } else if (endDate > 0) {
                manager.getCom().getDataInterface().suspendMemberChannel(idChannel, idUserKicked, endDate);
            }
            //Sending the message to all channel users
            SupplyKickClientMessage kMsg = new SupplyKickClientMessage(idUserKicked, manager.getCom().getDataInterface().getSharedChannel(idChannel), endDate);
            kMsg.handle(manager);

        } else {
            //If channel owner
            KickMemberServerMessage pkMsg = new KickMemberServerMessage(idUserKicked, idChannel, endDate);
            String idCreator = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
            try {
                manager.getCom().getClientById(idCreator).sendMessage(pkMsg);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }


    }
}
