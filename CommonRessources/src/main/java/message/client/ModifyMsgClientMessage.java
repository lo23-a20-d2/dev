package message.client;

import CommonClasses.TextMessage;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.ModifyMsgServerMessage;
import message.server.PutModifiedMsgServerMessage;

import java.io.IOException;

/**
 * Client message sent when modifying a message. 
 * Provides information about this message to the server.
 * @author Gabriel Souza e Silva
 */
public class ModifyMsgClientMessage implements ClientMessage{
    private String newContent;
    private String idMsg;
    private String idChannel;

    public ModifyMsgClientMessage(String newContent, String idMsg, String idChannel){
        this.newContent = newContent;
        this.idMsg = idMsg;
        this.idChannel = idChannel;
    }

    public void handle(IClientManager manager) {
        boolean isShared = false;
        isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);

        if(isShared){
            //Message creation
            TextMessage textMsg = manager.getCom().getDataInterface().modifyMessage(newContent, idMsg);

            // Notify channel users
            PutModifiedMsgServerMessage msg = new PutModifiedMsgServerMessage(textMsg);
            manager.getCom().sendMessageToChannel(textMsg.getIdChannel(), msg);
        }
        else {
            //Send message to channel owner 
            try {
                User creator = manager.getCom().getDataInterface().getChannelCreator(idChannel);
                ModifyMsgServerMessage msg = new ModifyMsgServerMessage(newContent, idMsg, idChannel);
                manager.getCom().getClientById(creator.getId()).sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
