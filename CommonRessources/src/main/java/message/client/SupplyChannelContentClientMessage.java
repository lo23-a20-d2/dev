package message.client;

import CommonClasses.*;
import Interfaces.IClientManager;
import message.server.NotifyChannelConnnectionServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Client message sent by the owner of a channel when a user connects to it.
 * Provide the channel information stored locally to the server.
 * @author Gabriel Souza e Silva
 */
public class SupplyChannelContentClientMessage implements ClientMessage{
    private OwnedChannel channel;
    private List<Message> messages;
    private String idUser;

    public Channel getChannel() {
        return channel;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public SupplyChannelContentClientMessage(OwnedChannel channel, List<Message> messages,String idUser){
        this.channel = channel;
        this.messages = messages;
        this.idUser=idUser;
    }

    public void handle(IClientManager manager) {
        // Feedback to the user
        NotifyChannelConnnectionServerMessage msgNotify = new NotifyChannelConnnectionServerMessage(channel, messages, channel.getWhiteList());
        try{
            manager.getCom().getClientById(idUser).sendMessage(msgNotify);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }
}
