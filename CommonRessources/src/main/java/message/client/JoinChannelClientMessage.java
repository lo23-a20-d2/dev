package message.client;

import CommonClasses.*;
import Interfaces.IClientManager;
import message.server.JoinChannelServerMessage;
import message.server.NotifyChannelConnnectionServerMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Client message sent when connecting to a channel.
 * Used to notify the server of this connection.
 * @author Gabriel Souza e Silva
 */
public class JoinChannelClientMessage implements ClientMessage{
    private String idChannel;
    private String idUser;

    public String getIdChannel(){
        return idChannel;
    }

    public String getIdUser(){
        return idUser;
    }

    public JoinChannelClientMessage(String idChannel, String idUser){
        this.idChannel = idChannel;
        this.idUser = idUser;
    }

    public void handle(IClientManager manager) {
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(this.idChannel);

        // Add user to the list of logged in users
        manager.getCom().getDataInterface().addUserConnected(idChannel, idUser);

        if (isShared){
            // Get channel info
            Channel channel = manager.getCom().getDataInterface().getSharedChannel(idChannel);

            // Get list message  
            List<Message> msgsChannel = manager.getCom().getDataInterface().getMessages(idChannel);

            // Get subscriptions 
            List<Subscription> subscriptions = manager.getCom().getDataInterface().getSubscriptions(idChannel);

            // Send feedback message to user
            try {
                NotifyChannelConnnectionServerMessage msgNotify = new NotifyChannelConnnectionServerMessage(channel, msgsChannel, subscriptions);
                manager.sendMessage(msgNotify);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            // Get the informations from the creator of the channel
            User creator = manager.getCom().getDataInterface().getChannelCreator(idChannel);

            // Send message to the channel owner
            if(creator != null){
                JoinChannelServerMessage msg = new JoinChannelServerMessage(idChannel, idUser);
                IClientManager cliMng = manager.getCom().getClientById(creator.getId());
                try {
                    cliMng.sendMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
