package message.client;

import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.NotifyDeletedChannelServerMessage;
import message.server.RequestDeleteOwnedChannelServerMessage;

import java.io.IOException;


/**
 * The class corresponding to methode sendDeleteChannel().
 *
 * @author Aurelien PHILIPPE <aurelien.philippe@etu.utc.fr>
 */
public class SendDeleteChannelClientMessage implements ClientMessage{
	
	private String idChannel;

	public void handle(IClientManager manager) {
		boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);
    			
		//To all connected users 
		NotifyDeletedChannelServerMessage msg = new NotifyDeletedChannelServerMessage(idChannel);
    	
		manager.getCom().sendMessageToServer(msg);
		
		if(isShared) {
			manager.getCom().getDataInterface().deleteChannel(idChannel);
		}else {
			User creator = manager.getCom().getDataInterface().getChannelCreator(idChannel);
			manager.getCom().getDataInterface().removeConnectedOwnedChannel(idChannel);
			RequestDeleteOwnedChannelServerMessage msg2 = new RequestDeleteOwnedChannelServerMessage(idChannel);
			try{
                manager.getCom().getClientById(creator.getId()).sendMessage(msg2); //Sending to the creator 
        	}catch(IOException ex) {
                manager.getCom().log("Erreur lors de l'execution d'un message de Connexion", ex);
        	}
		}
    }

	public SendDeleteChannelClientMessage(String idChannel) {
		this.idChannel = idChannel;
	}

	public String getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}
}
