package message.client;

import Interfaces.IClientManager;

import java.io.Serializable;

public interface ClientMessage extends Serializable {
    public abstract void handle(IClientManager manager);
}
