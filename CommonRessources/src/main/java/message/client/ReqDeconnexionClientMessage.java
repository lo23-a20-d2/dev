package message.client;

import Interfaces.IClientManager;
import message.server.NotifyUserDeconnexionServerMessage;

/**
 * The class corresponding to methode channelDeconnexion().
 *
 * @author Aurlien PHILIPPE <aurelien.philippe@etu.utc.fr>
 */

public class ReqDeconnexionClientMessage implements ClientMessage{

	private String idUser;
	private String idChannel;
	
    public void handle(IClientManager manager) {
    	manager.getCom().getDataInterface().removeConnectedUserServer(idUser);
		NotifyUserDeconnexionServerMessage msg = new NotifyUserDeconnexionServerMessage(idUser);
    	manager.getCom().sendMessageToChannel(idChannel, msg);
    	
    }

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}
	
	public ReqDeconnexionClientMessage(String idUser, String idChannel) {
		this.idUser = idUser;
		this.idChannel = idChannel;
	}

}
