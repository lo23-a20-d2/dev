package message.client;

import CommonClasses.Channel;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.IClientManager;
import message.server.ConnectionErrorServerMessage;
import message.server.NotifyNewUserConnectedToServerServerMessage;
import message.server.SendUserChannelListServerMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Client message sent when connecting to a server 
 * Allows you to notify the server of this connection and send it user information
 *
 * @author Gauthier Cabot
 */
public class NotifyNewConnectionClientMessage implements ClientMessage {
    User user;
    private List<OwnedChannel> ownedChannelList;

    public NotifyNewConnectionClientMessage(User u, List<OwnedChannel> ownedChannelList) {
        this.user = u;
        this.ownedChannelList = ownedChannelList == null ? new ArrayList<>() : ownedChannelList;
    }

    public void handle(IClientManager manager) {
        //If already connected, we refuse the connection 
        if(manager.getCom().getClientById(user.getId()) != null){
            try {
                manager.sendMessage(new ConnectionErrorServerMessage("Un utilisateur est deja connecte avec ce compte !"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        //Sinon
        manager.setUser(user);
        manager.getCom().getDataInterface().addConnectedUserServer(user);
        manager.getCom().getDataInterface().addConnectedOwnedChannel(ownedChannelList);

        try {
        	//Retrieving server info (channels, logged in users, subscriptions)
            List<Channel> channelList = manager.getCom().getDataInterface().getConnectedChannelList();
            List<User> connectedUserList = manager.getCom().getDataInterface().getConnectedUserServer();
            List<Subscription> subscriptionList = manager.getCom().getDataInterface().getUserSubscriptionList(user);

            //Sending server information to the new logged in user
            SendUserChannelListServerMessage msg = new SendUserChannelListServerMessage(channelList, connectedUserList, subscriptionList);
            manager.sendMessage(msg);

            //Sending a connection notification to all members of the server except the new connected user
            NotifyNewUserConnectedToServerServerMessage notifyMsg = new NotifyNewUserConnectedToServerServerMessage(user,ownedChannelList);
            manager.getCom().sendMessageToServer(notifyMsg, user.getId());
        } catch (IOException ex) {
            manager.getCom().log("Erreur lors de l'execution d'un message de connexion", ex);
        }
    }
}