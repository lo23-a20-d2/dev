package message.client;

import CommonClasses.Message;
import CommonClasses.TextMessage;
import Interfaces.IClientManager;
import message.server.NewTextMsgServerMessage;
import message.server.PutNewTextMsgServerMessage;

import java.io.IOException;

/**
 * Client message sent when publishing a new message 
 * Notifies the server when a new message is published 
 * @author Gauthier Cabot
 */
public class NewTextMsgClientMessage implements ClientMessage  {
    private String msgContent;
    private String idSender;
    private String idChannel;

    public String getMsgContent() {
        return msgContent;
    }

    public String getIdSender() {
        return idSender;
    }

    public String getIdChannel() {
        return idChannel;
    }

    public NewTextMsgClientMessage(String msgContent, String idSender, String idChannel) {
        this.msgContent = msgContent;
        this.idSender = idSender;
        this.idChannel = idChannel;
    }

    public void handle(IClientManager manager){
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);

        if(isShared){
            TextMessage txt = manager.getCom().getDataInterface().addTextMsg(msgContent, idSender, idChannel);
            PutNewTextMsgServerMessage msg = new PutNewTextMsgServerMessage(txt);
            manager.getCom().sendMessageToChannel(idChannel, msg);
        }
        else {
            try {
                NewTextMsgServerMessage msg = new NewTextMsgServerMessage(msgContent, idSender, idChannel);
                manager.sendMessage(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
