package message.client;

import CommonClasses.Message;
import CommonClasses.TextMessage;
import Interfaces.IClientManager;
import message.server.PutNewTextMsgServerMessage;

/**
 * Customer message sent by the owner of a channel when posting a new message
 * Provides information about this message to the server
 * @author Gauthier Cabot
 */
public class SupplyNewTextMsgClientMessage implements ClientMessage{

    TextMessage textMsg;
    String idChannel;

    public SupplyNewTextMsgClientMessage(TextMessage textMsg, String idChannel) {
        this.textMsg = textMsg;
        this.idChannel = idChannel;
    }

    public void handle(IClientManager manager){
        PutNewTextMsgServerMessage msg = new PutNewTextMsgServerMessage(textMsg);
        manager.getCom().sendMessageToChannel(idChannel, msg);
    }
}
