package message.client;

import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import Interfaces.IClientManager;
import message.server.NotifyNewChannelServerMessage;

/**
 * Client message sent when creating an owner channel.
 * Used to provide the channel information to the server.
 *
 * @author Yiwen Wang
 */
public class NotifyNewUnsharedChannelClientMessage implements ClientMessage {
    private final OwnedChannel channel;
    private final Subscription sub;

    public NotifyNewUnsharedChannelClientMessage(OwnedChannel channel, Subscription sub) {
        this.channel = channel;
        this.sub = sub;
    }

    @Override
    public void handle(IClientManager manager) {
        manager.getCom().getDataInterface().addConnectedOwnedChannel(channel);
        manager.getCom().getDataInterface().updateSubscriptionOwnedChannel(channel.getId(), sub);
        if(!channel.isPrivate())
        {
            NotifyNewChannelServerMessage msg = new NotifyNewChannelServerMessage(channel);
            manager.getCom().sendMessageToServer(msg,manager.getUser().getId());
        }
    }
}
