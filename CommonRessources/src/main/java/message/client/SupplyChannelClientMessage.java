package message.client;

import CommonClasses.Message;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import Interfaces.IClientManager;
import message.server.NotifyNewMemberServerMessage;
import message.server.ProvideChannelToMemberServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Client message sent by the owner of a channel when registering a user for it.
 * Provide the channel information stored locally to the server.
 *
 * @author Gabriel Souza e Silva
 */
public class SupplyChannelClientMessage implements ClientMessage {
    private final OwnedChannel channel;
    private final List<Message> messages;
    private final Subscription sub;

    public SupplyChannelClientMessage(OwnedChannel channel, List<Message> messages, Subscription sub) {
        this.channel = channel;
        this.messages = messages;
        this.sub = sub;
    }

    public void handle(IClientManager manager) {
        // Sending to all connected users
        manager.getCom().getDataInterface().updateSubscriptionOwnedChannel(channel.getId(), sub);
        NotifyNewMemberServerMessage msgNotify = new NotifyNewMemberServerMessage(sub);
        manager.getCom().sendMessageToChannel(channel.getId(), msgNotify);

        ProvideChannelToMemberServerMessage msgProvide = new ProvideChannelToMemberServerMessage(this.channel, messages, channel.getWhiteList());
        // Sending to the new user
        try {
            manager.getCom().getClientById(sub.getUser().getId()).sendMessage(msgProvide);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}