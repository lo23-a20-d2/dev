package message.client;

import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import Interfaces.IClientManager;
import message.server.NotifyNewMemberServerMessage;
import message.server.NotifyNewSubscriptionServerMessage;

import java.io.IOException;

/**
 * Client message sent by the owner of a channel when inviting a user to his channel
 * Allows you to provide the channel information to the server
 *
 * @author Gauthier Cabot
 */
public class SupplyNewMemberClientMessage implements ClientMessage {

    private final String idUser;
    private final OwnedChannel channel;
    private final Subscription sub;

    public SupplyNewMemberClientMessage(String idUser, OwnedChannel channel, Subscription sub) {
        this.idUser = idUser;
        this.channel = channel;
        this.sub = sub;
    }

    @Override
    public void handle(IClientManager manager) {
        //Sending channel information to the new subscriber
        manager.getCom().getDataInterface().updateSubscriptionOwnedChannel(channel.getId(), sub);
        try {
            manager.getCom().sendMessageToServer(new NotifyNewMemberServerMessage(sub),idUser);
            manager.getCom().getClientById(idUser).sendMessage(new NotifyNewSubscriptionServerMessage(channel,sub));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
