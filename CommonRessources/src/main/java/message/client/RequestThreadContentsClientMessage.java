package message.client;

import CommonClasses.Message;
import Interfaces.IClientManager;
import message.server.PutThreadContentsServerMessage;
import message.server.RequestThreadContentsServerMessage;

import java.io.IOException;
import java.util.List;

/**
 * Message Client envoyé lorsqu'on veut voir le thread d'un message
 * Permet d'accéder à la discussion lié à un message
 *
 * @author Benoit Rams
 */

public class RequestThreadContentsClientMessage implements ClientMessage {

    private String idChannel;
    private String idThread;

    public RequestThreadContentsClientMessage(String idChannel, String idThread) {
        this.idChannel = idChannel;
        this.idThread = idThread;
    }

    @Override
    public void handle(IClientManager manager) {
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);
        //si le channel est partagé on affiche le thread à l'utilisateur
        if (isShared) {
            // voir comment on stocke les threads
            List<Message> threadContents = manager.getCom().getDataInterface().getThreadContents(idChannel, idThread);
            PutThreadContentsServerMessage msg = new PutThreadContentsServerMessage(idChannel, threadContents);
            try {
                manager.sendMessage(msg);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
        //sinon on demande les données aux propriétaire du channel
        else {
            String idUser = manager.getUser().getId();
            String idCreator = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
            RequestThreadContentsServerMessage msg = new RequestThreadContentsServerMessage(idChannel, idThread, idUser);
            try {
                manager.getCom().getClientById(idCreator).sendMessage(msg);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
