package message.client;

import Interfaces.IClientManager;
import message.server.CancelKickServerMessage;
import message.server.NotifyKickCanceledServerMessage;
import message.server.NotifyKickCanceledUserServerMessage;

import java.io.IOException;

/**
 * Client message sent when cancelling a kick
 * Notifies the server to cancel a user's kick
 *
 * @author Benoit RAMS
 */
public class ReqCancelKickClientMessage implements ClientMessage {

    String idUser;
    String idChannel;

    public ReqCancelKickClientMessage(String idUser, String idChannel) {
        this.idUser = idUser;
        this.idChannel = idChannel;
    }

    @Override
    public void handle(IClientManager manager) {
        //If the channel is shared then we modify it
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);
        if (isShared) {
            manager.getCom().getDataInterface().cancelKick(idUser, idChannel);
            //Send message to all users
            NotifyKickCanceledServerMessage msg = new NotifyKickCanceledServerMessage(idUser, idChannel);
            manager.getCom().sendMessageToChannel(idChannel, msg);

            //Send message to the kicked user
            NotifyKickCanceledUserServerMessage msgKick = new NotifyKickCanceledUserServerMessage(idUser, manager.getCom().getDataInterface().getSharedChannel(idChannel));
            try {
                manager.getCom().getClientById(idUser).sendMessage(msgKick);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {//If it is a owned channel
            CancelKickServerMessage ckMsg = new CancelKickServerMessage(idUser, idChannel);
            String idCreator = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
            try {
                manager.getCom().getClientById(idCreator).sendMessage(ckMsg);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }

        }

    }
}
