package message.client;

import CommonClasses.Subscription;
import Interfaces.IClientManager;

public class SupplyModifyNickNameClientMessage implements ClientMessage {
    private String idChannel;
    private Subscription sub;

    public SupplyModifyNickNameClientMessage(String idChannel, Subscription sub) {
        this.idChannel = idChannel;
        this.sub = sub;
    }

    @Override
    public void handle(IClientManager manager) {
        manager.getCom().getDataInterface().updateSubscriptionOwnedChannel(idChannel, sub);
    }
}
