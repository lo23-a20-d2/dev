package message.client;

import CommonClasses.TextMessage;
import Interfaces.IClientManager;
import message.server.NewTextReplyServerMessage;
import message.server.PutNewTextReplyServerMessage;

/**
 * Message Client pour renvoyer un texte
 * Permet de notifier le serveur qu'un utilisateur répond à un message
 *
 * @author Benoit RAMS
 */
public class NewTextReplyClientMessage implements ClientMessage {

    String msgContent;
    String idSender;
    String idChannel;
    String idThread;

    public NewTextReplyClientMessage(String msgContent, String idSender, String idChannel, String idThread) {
        this.msgContent = msgContent;
        this.idSender = idSender;
        this.idChannel = idChannel;
        this.idThread = idThread;
    }

    @Override
    public void handle(IClientManager manager) {
        boolean isShared = manager.getCom().getDataInterface().isSharedChannel(idChannel);
        //si le channel est partagé
        if(isShared) {
            TextMessage txt = manager.getCom().getDataInterface().addTextReply(msgContent, idSender, idChannel, idThread);
            PutNewTextReplyServerMessage msg = new PutNewTextReplyServerMessage(txt);
            manager.getCom().sendMessageToChannel(idChannel, msg);
        }
        else {
            String idCreator = manager.getCom().getDataInterface().getChannelCreator(idChannel).getId();
            NewTextReplyServerMessage msg = new NewTextReplyServerMessage(msgContent, idSender, idChannel, idThread);
            try{
                manager.getCom().getClientById(idCreator).sendMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
