package message.client;

import CommonClasses.TextMessage;
import Interfaces.IClientManager;
import message.server.PutNewTextReplyServerMessage;


/**
 * Message Client pour renvoyer un texte
 * Permet d'envoyer le nouveau message à tout les utilisateurs
 *
 * @author Benoit RAMS
 */
public class SupplyTextReplyClientMessage implements ClientMessage {

    TextMessage textMsg;
    String idChannel;

    public SupplyTextReplyClientMessage(TextMessage textMsg, String idChannel) {
        this.textMsg = textMsg;
        this.idChannel = idChannel;
    }

    @Override
    public void handle(IClientManager manager) {
        PutNewTextReplyServerMessage msg = new PutNewTextReplyServerMessage(textMsg);
        manager.getCom().sendMessageToChannel(idChannel,msg);
    }
}
