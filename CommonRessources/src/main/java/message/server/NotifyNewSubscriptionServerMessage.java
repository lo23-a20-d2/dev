package message.server;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import Interfaces.ILocalCom;

/**
 * Server message sent when a new user join a channel
 * Allows to notify all 
 * @author Gauthier Cabot
 */

public class NotifyNewSubscriptionServerMessage implements ServerMessage{
    private Channel channel;
    private Subscription sub;

    public NotifyNewSubscriptionServerMessage(Channel channel, Subscription sub) {
        this.channel = channel;
        this.sub = sub;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().newInvitation(channel, sub);
    }
}
