package message.server;

import Interfaces.ILocalCom;

/**
 * Server message sent to channel users to update channel members
 * Notifies users of a channel that a user is kicked
 *
 * @author Benoit RAMS
 */
public class NotifyKickSuccessServerMessage implements ServerMessage {

    String idChannel;
    String idUserKicked;
    long endDate;

    public NotifyKickSuccessServerMessage(String idChannel, String idUserKicked, long endDate) {
        this.idChannel = idChannel;
        this.idUserKicked = idUserKicked;
        this.endDate = endDate;
    }

    @Override
    public void handle(ILocalCom com) {
        //notify the kick to channel
        com.getIhmChannelInterface().notifyChannelKick(idUserKicked, idChannel, endDate);
    }
}
