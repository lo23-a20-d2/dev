package message.server;

import CommonClasses.Channel;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Server message sent when a new user is connected
 * Allows to notify all and gives the own channel of the new user 
 * @author Gauthier Cabot
 */
public class NotifyNewUserConnectedToServerServerMessage implements ServerMessage {
    private User u;
    private List<OwnedChannel> ownedChannelList;


    public NotifyNewUserConnectedToServerServerMessage(User u, List<OwnedChannel> ownedChannelList) {
        this.u = u;
        this.ownedChannelList = ownedChannelList;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().sendNewUserConnectedToServer(u);
        //Transmission of the owner channels of the new user and of the user's subscriptions
        if(!ownedChannelList.isEmpty()){
            List<Subscription> subList = new ArrayList<>();
            ownedChannelList.forEach(channel -> channel.getWhiteList().stream()
                    .filter(s -> s.getUser().getId().equals(com.getIhmMainInterface().getIdConnectedUser()))
                    .findFirst()
                    .ifPresent(subList::add)
            );
            com.getIhmMainInterface().updateConnectedChannelList(ownedChannelList.stream().map(c -> (Channel) c).collect(Collectors.toList()),subList);
        }
    }
}
