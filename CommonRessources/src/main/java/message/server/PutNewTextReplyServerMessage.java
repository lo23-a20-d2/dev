package message.server;

import CommonClasses.TextMessage;
import Interfaces.ILocalCom;

/**
 * Message Server pour renvoyer un texte
 * Permet de notifier tout les utilisateurs connecté au channel
 *
 * @author Benoit RAMS
 */
public class PutNewTextReplyServerMessage implements  ServerMessage{

    TextMessage textMsg;

    public PutNewTextReplyServerMessage(TextMessage textMsg) {
        this.textMsg = textMsg;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().displayTextReply(textMsg);
    }
}
