package message.server;

import CommonClasses.Channel;
import Interfaces.ILocalCom;


/**
 * Server message sent to the user to cancel his kick
 * Allows to notify the user that his kick is canceled
 *
 * @author Benoit RAMS
 */
public class NotifyKickCanceledUserServerMessage implements ServerMessage {

    String idUser;
    Channel channel;

    public NotifyKickCanceledUserServerMessage(String idUser, Channel channel) {
        this.idUser = idUser;
        this.channel = channel;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().notifyKickCanceled(idUser, channel);
    }
}
