package message.server;

import Interfaces.ILocalCom;
import CommonClasses.*;

/**
 * Server message sent when registering a user to a channel.
 * Allows you to notify other channel members of this registration.
 * @author Gabriel Souza e Silva
 */
public class NotifyNewMemberServerMessage implements ServerMessage{
    private Subscription sub;

    public NotifyNewMemberServerMessage(Subscription sub){
        this.sub = sub;
    }

    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().displayNewMemberGreeting(sub);
    }
}
