package message.server;

import CommonClasses.Message;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;
import message.client.SupplyChannelClientMessage;

import java.io.IOException;
import java.util.List;

/**
 * Server message sent to the owner of a channel when registering a user for it.
 * Used to notify the owner of this listing.
 * @author Gabriel Souza e Silva
 */
public class NewSubscriptionServerMessage implements ServerMessage{
    private String idChannel;
    private User user;

    public NewSubscriptionServerMessage(String idChannel, User user) {
        this.idChannel = idChannel;
        this.user = user;
    }

    public void handle(ILocalCom com) {
        Subscription sub = com.getDataInterface().addSubscription(idChannel, user, Subscription.Role.USER);
        OwnedChannel channel = com.getDataInterface().getOwnedChannel(idChannel);
        List<Message> messages = com.getDataInterface().getMessages(idChannel);

        SupplyChannelClientMessage msg = new SupplyChannelClientMessage(channel, messages, sub);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}