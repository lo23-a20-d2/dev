package message.server;

import CommonClasses.User;
import Interfaces.ILocalCom;

/**
 * Server message sent when promotion of an user
 *
 * @author Violette Quitral <violette.quitral@etu.utc.fr>
 */

public class NotifyPromotionServerMessage implements ServerMessage {
	private String idUser;
	private String idChannel;

	public String getIdUser() {
		return idUser;
	}

	public String getIdChannel() {
		return idChannel;
	}
	
	public NotifyPromotionServerMessage(String idUser, String idChannel) {
		this.idUser = idUser;
		this.idChannel = idChannel;
	}
	
	public void handle(ILocalCom com) {
		//If we own the channel
		User creator = com.getDataInterface().getChannelOwner(this.idChannel);
		if (creator != null && creator.getId().equals(com.getIhmMainInterface().getIdConnectedUser())) {
			com.getDataInterface().updateAdmList(this.idUser,this.idChannel);
		}
		com.getIhmChannelInterface().newAdmin(this.idUser,this.idChannel);
	}

	
}
