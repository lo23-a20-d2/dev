package message.server;

import CommonClasses.Channel;
import Interfaces.ILocalCom;

public class NotifyUserKickedServerMessage implements ServerMessage {
    Channel channel;
    String idUserKicked;
    long endDate;

    public NotifyUserKickedServerMessage(Channel channel, String idUserKicked, long endDate) {
        this.channel= channel;
        this.idUserKicked = idUserKicked;
        this.endDate = endDate;
    }


    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().notifyKickedChannel(idUserKicked, channel, endDate);
    }
}
