package message.server;

import CommonClasses.Message;
import CommonClasses.TextMessage;
import Interfaces.ILocalCom;

/**
 * Server message sent when publishing a new message
 * Allows to notify users connected to the channel that a new message is published.
 * @author Gauthier Cabot
 */
public class PutNewTextMsgServerMessage implements ServerMessage {
    TextMessage textMsg;

    public PutNewTextMsgServerMessage(TextMessage msg) {
        this.textMsg = msg;
    }

    public void handle(ILocalCom com){
        com.getIhmChannelInterface().displayNewTextMsg(textMsg);
    }

}
