package message.server;

import Interfaces.ILocalCom;
import message.client.SupplyKickClientMessage;

import java.io.IOException;
/**
 * Server message sent when an admin cancel a kick
 * Kick a member from a owned channel
 * @author Benoit RAMS
 */

public class KickMemberServerMessage implements ServerMessage {
    String idUserKicked;
    String idChannel;
    long endDate;

    public KickMemberServerMessage(String idUserKicked, String idChannel, long endDate) {
        this.idUserKicked = idUserKicked;
        this.idChannel = idChannel;
        this.endDate = endDate;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getDataInterface().removeFromUserList(idUserKicked, idChannel);
        if (endDate == -1) {
            com.getDataInterface().blockUserChannel(idChannel,idUserKicked);
        } else if (endDate > 0) {
            com.getDataInterface().suspendMemberChannel(idChannel,idUserKicked,endDate);
        }
        SupplyKickClientMessage msg = new SupplyKickClientMessage(idUserKicked, com.getDataInterface().getOwnedChannel(idChannel), endDate);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }
}
