package message.server;

import CommonClasses.User;
import Interfaces.ILocalCom;

import java.util.List;

/**
 * Server message sent when you want to display the number of likes on a message.
 * Allows you to visually modify the number of likes of a message for a person.
 *
 * @author Benoit RAMS
 */
public class PutLikesServerMessage implements ServerMessage {

    private String idChannel;
    private String idUser;
    private String idMsg;

    public PutLikesServerMessage(String idChannel, String idUser, String idMsg) {
        this.idChannel = idChannel;
        this.idUser = idUser;
        this.idMsg = idMsg;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().displayLikes(idMsg, idUser, idChannel);
    }
}
