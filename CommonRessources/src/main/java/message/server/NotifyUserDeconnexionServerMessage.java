package message.server;

import Interfaces.ILocalCom;
/**
 * The class corresponding to methode notifyUserDeconnexion().
 *
 * @author Aurlien PHILIPPE <aurelien.philippe@etu.utc.fr>
 */
public class NotifyUserDeconnexionServerMessage implements ServerMessage{

	private String idUser; // id of the user connecting 

	public void handle(ILocalCom com) {
        com.getIhmChannelInterface().notifyQuitChannel(idUser);
    }

	
    public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public NotifyUserDeconnexionServerMessage(String idUser) {
		this.idUser = idUser;
	}

}
