package message.server;

import CommonClasses.Channel;
import CommonClasses.User;
import Interfaces.ILocalCom;
import java.util.List;
import java.util.Map;

/**
 * Server message sent when creating a shared channel.
 * Allows to notify users of the creation of a new channel and provide information about it.
 * @author Yiwen Wang
 */
public class NotifyNewChannelServerMessage implements ServerMessage {
    Channel channel;

    public NotifyNewChannelServerMessage(Channel channel) {
        this.channel=channel;
    }


    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().newPublicChannel(channel);
    }
}
