package message.server;

import CommonClasses.Channel;
import CommonClasses.Message;
import CommonClasses.Subscription;
import Interfaces.ILocalCom;
import java.util.List;

/**
 * Server message sent to all clients of a channel to which a new user connects.
 * Used to notify users of this new connection and provide the new connection with channel information.
 * @author Gabriel Souza e Silva
 */
public class NotifyChannelConnnectionServerMessage implements ServerMessage{
    private Channel channel;
    private List<Message> messages;
    private List<Subscription> subscriptions;

    public NotifyChannelConnnectionServerMessage(Channel channel, List<Message> messages, List<Subscription> subscriptions){
        this.channel = channel;
        this.messages = messages;
        this.subscriptions = subscriptions;
    }

    public void handle(ILocalCom com) {
        // Display the channel for the connected user
        com.getIhmChannelInterface().displayChannel(channel, messages, subscriptions);
    }
}
