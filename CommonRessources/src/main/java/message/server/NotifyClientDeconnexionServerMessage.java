package message.server;

import CommonClasses.Channel;
import CommonClasses.OwnedChannel;
import Interfaces.ILocalCom;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Server message sent when user is disconnected 
 * Allows to notify the other users 
 * @author Gauthier Cabot 
 */
public class NotifyClientDeconnexionServerMessage implements ServerMessage {
    String userId;

    public NotifyClientDeconnexionServerMessage(String userId) {
        this.userId = userId;
    }

    @Override
    public void handle(ILocalCom com) {
        List<OwnedChannel> ownedChannelList = com.getDataInterface().getAllOwnedChannel(userId);
        if (!ownedChannelList.isEmpty()) {
            com.getIhmMainInterface().removeOwnedChannels(ownedChannelList.stream().map(Channel::getId).collect(Collectors.toList()));
        }
        com.getIhmChannelInterface().sendUserDisconnectedFromServer(userId);
    }
}
