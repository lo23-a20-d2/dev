package message.server;

import CommonClasses.User;
import Interfaces.ILocalCom;
/**
 * Server message sent when deleting a message
 * Allows to notify users connected to the channel that a message is deleted.
 * @author Yiwen WANG
 */
public class PutDeleteMsgServerMessage implements ServerMessage{
    private String idMsg;
    private String idChannel;

    public PutDeleteMsgServerMessage(String idMsg, String idChannel) {
        this.idMsg = idMsg;
        this.idChannel = idChannel;
    }

    @Override
    public void handle(ILocalCom com) {
        //If we are the channel owner, delete the message from local
        User creator = com.getDataInterface().getChannelOwner(idChannel);
        if (creator != null && creator.getId().equals(com.getIhmMainInterface().getIdConnectedUser())){
            com.getDataInterface().deleteMessage(idMsg);
        }
        com.getIhmChannelInterface().displayDeletedMsg(idMsg,idChannel);
    }
}
