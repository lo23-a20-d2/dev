package message.server;

import CommonClasses.TextMessage;
import Interfaces.ILocalCom;
import message.client.SupplyModifiedMsgClientMessage;

import java.io.IOException;

/**
 * Server message sent when modifying a message
 * Allows to notify the creator of the channel the modification of a message
 * @author Gabriel Souza e Silva
 */
public class ModifyMsgServerMessage implements ServerMessage{
    private String newContent;
    private String idMsg;
    private String idChannel;

    public ModifyMsgServerMessage(String newContent, String idMsg, String idChannel){
        this.newContent = newContent;
        this.idMsg = idMsg;
        this.idChannel = idChannel;
    }

    public void handle(ILocalCom com) {
        //Creation of the text message
        TextMessage textMsg = com.getDataInterface().modifyMessage(newContent, idMsg);

        // Client message initialization 
        SupplyModifiedMsgClientMessage msg = new SupplyModifiedMsgClientMessage(textMsg);

        // Sending of the client message
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
