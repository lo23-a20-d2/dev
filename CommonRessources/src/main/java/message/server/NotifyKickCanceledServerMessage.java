package message.server;

import Interfaces.ILocalCom;

/**
 * Server message sent when canceling kick to channel users
 * Allows to notify the users of a channel of the cancellation of a kick
 *
 * @author Benoit RAMS
 */
public class NotifyKickCanceledServerMessage implements ServerMessage {

    String idUser;
    String idChannel;

    public NotifyKickCanceledServerMessage(String idUser, String idChannel) {
        this.idChannel = idChannel;
        this.idUser = idUser;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().notifyKickCanceled(idUser, idChannel);
    }
}
