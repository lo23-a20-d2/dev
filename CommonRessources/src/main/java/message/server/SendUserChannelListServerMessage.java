package message.server;

import CommonClasses.Channel;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;
import java.util.List;

/**
 * Server message sent when a new user connect to a server, provides all the information 
 * Allows to get all informations
 * @author Gauthier Cabot
 */

public class SendUserChannelListServerMessage implements message.server.ServerMessage {
    private List<Channel> channelList;
    private List<User> connectedUserList;
    private List<Subscription> subscriptionList;

    public SendUserChannelListServerMessage(List<Channel> cL, List<User> uL, List<Subscription> sL) {
        channelList = cL;
        connectedUserList = uL;
        subscriptionList = sL;
    }

    public void handle(ILocalCom com) {
        com.getIhmMainInterface().initConnectedChannelList(channelList, subscriptionList);
        com.getIhmChannelInterface().sendConnectedUsersServerList(connectedUserList);
    }
}
