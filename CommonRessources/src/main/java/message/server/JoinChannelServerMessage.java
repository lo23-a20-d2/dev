package message.server;

import CommonClasses.Message;
import CommonClasses.OwnedChannel;
import Interfaces.ILocalCom;
import message.client.SupplyChannelContentClientMessage;

import java.io.IOException;
import java.util.List;

/**
 * Server message sent to the client owning a channel when a user connects to it.
 * Used to notify the owner of this connection.
 * @author Gabriel Souza e Silva
 */
public class JoinChannelServerMessage implements ServerMessage{
    private String idChannel;
    private String idUser;

    public String getIdChannel(){
        return idChannel;
    }

    public String getIdUser(){
        return idUser;
    }

    public JoinChannelServerMessage(String idChannel, String idUser){
        this.idChannel = idChannel;
        this.idUser = idUser;
    }

    public void handle(ILocalCom com) {
        // Get channel info
        OwnedChannel channel = com.getDataInterface().getOwnedChannel(idChannel);
        // Get channel messages
        List<Message> msgsChannel = com.getDataInterface().getMessages(idChannel);
        // Message initialization and sending to the server 
        SupplyChannelContentClientMessage msg = new SupplyChannelContentClientMessage(channel, msgsChannel,idUser);

        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
