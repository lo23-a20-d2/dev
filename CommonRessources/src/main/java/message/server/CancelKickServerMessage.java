package message.server;

import Interfaces.ILocalCom;
import message.client.SupplyCancelKickClientMessage;

import java.io.IOException;

/**
 * Server message sent when inviting a user to a channel
 * Allows you to notify the channel creator of the invitation
 * @author Benoit Rams
 */

public class CancelKickServerMessage implements ServerMessage {
    String idUser;
    String idChannel;

    public CancelKickServerMessage(String idUser, String idChannel) {
        this.idUser = idUser;
        this.idChannel = idChannel;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getDataInterface().cancelKick(idUser,idChannel);
        SupplyCancelKickClientMessage msg = new SupplyCancelKickClientMessage(idUser, com.getDataInterface().getOwnedChannel(idChannel));
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }
}
