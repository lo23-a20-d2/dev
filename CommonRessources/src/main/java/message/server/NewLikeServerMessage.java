package message.server;

import CommonClasses.User;
import Interfaces.ILocalCom;
import message.client.SupplyLikersListClientMessage;

import java.io.IOException;
import java.util.List;

/**
 * Server message sent when you want to add a like on a message from an owner channel.
 * Allows you to modify the number of likes of a message in an owner channel.
 *
 * @author Benoit RAMS
 */
public class NewLikeServerMessage implements ServerMessage {

    private String idChannel;
    private String idUser;
    private String idMsg;

    public NewLikeServerMessage(String idSender, String idUser, String idMsg) {
        this.idChannel = idSender;
        this.idUser = idUser;
        this.idMsg = idMsg;
    }

    @Override
    public void handle(ILocalCom com) {

        com.getDataInterface().updateLikersList(idMsg, idUser, idMsg);
        SupplyLikersListClientMessage msg = new SupplyLikersListClientMessage(idChannel, idUser, idMsg);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
