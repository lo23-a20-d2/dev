package message.server;

import CommonClasses.Message;
import Interfaces.ILocalCom;
import message.client.SupplyThreadContentsClientMessage;

import java.io.IOException;
import java.util.List;
/**
 * Message serveur envoyé lorsqu'on veut voir le thread d'un message
 * Permet d'accéder aux threads d'un channel propriétaire
 *
 * @author Benoit Rams
 */
public class RequestThreadContentsServerMessage implements ServerMessage{

    private String idChannel;
    private String idThread;
    private String idUser;

    public RequestThreadContentsServerMessage(String idChannel, String idThread, String idUser) {
        this.idChannel = idChannel;
        this.idThread = idThread;
        this.idUser = idUser;
    }

    @Override
    public void handle(ILocalCom com) {
        List<Message> threadContents = com.getDataInterface().getThreadContents(idChannel, idThread);
        SupplyThreadContentsClientMessage msg = new SupplyThreadContentsClientMessage(idChannel, threadContents, idUser);
        try {
            com.sendMessage(msg);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
