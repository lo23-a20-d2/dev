package message.server;

import CommonClasses.TextMessage;
import Interfaces.ILocalCom;
import message.client.SupplyTextReplyClientMessage;


/**
 * Message Serveur pour renvoyer un texte
 * Permet de changer les données du channel
 *
 * @author Benoit RAMS
 */
public class NewTextReplyServerMessage implements  ServerMessage{

    String msgContent;
    String idSender;
    String idChannel;
    String idThread;

    public NewTextReplyServerMessage(String msgContent, String idSender, String idChannel, String idThread) {
        this.msgContent = msgContent;
        this.idSender = idSender;
        this.idChannel = idChannel;
        this.idThread = idThread;
    }

    @Override
    public void handle(ILocalCom com) {
        TextMessage txt = com.getDataInterface().addTextReply(msgContent, idSender, idChannel, idThread);
        SupplyTextReplyClientMessage msg = new SupplyTextReplyClientMessage(txt, idChannel);
        try {
            com.sendMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
