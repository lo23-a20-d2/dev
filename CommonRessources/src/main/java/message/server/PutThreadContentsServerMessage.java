package message.server;

import CommonClasses.Message;
import Interfaces.ILocalCom;

import java.util.List;

/**
 * Message serveur envoyé lorsqu'on veut voir le thread d'un message
 * Affiche le thread d'un message
 *
 * @author Benoit Rams
 */
public class PutThreadContentsServerMessage implements ServerMessage {

    private String idChannel;
    private List<Message> threadContents;

    public PutThreadContentsServerMessage(String idChannel, List<Message> threadContents) {
        this.idChannel = idChannel;
        this.threadContents = threadContents;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().displayThread(idChannel, threadContents);
    }
}
