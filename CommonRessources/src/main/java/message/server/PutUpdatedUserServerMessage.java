package message.server;

import CommonClasses.User;
import Interfaces.ILocalCom;

public class PutUpdatedUserServerMessage implements ServerMessage{
    private User user;

    public PutUpdatedUserServerMessage(User user){
        this.user = user;
    }
    @Override
    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().updateConnectedUser(user);
    }
}
