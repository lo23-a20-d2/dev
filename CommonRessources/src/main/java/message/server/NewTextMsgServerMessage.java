package message.server;

import CommonClasses.Message;
import CommonClasses.TextMessage;
import Interfaces.ILocalCom;
import message.client.SupplyNewTextMsgClientMessage;

import java.io.IOException;

/**
 * Server message sent to the owner of a channel when posting a new message on it.
 * Allows to notify the owner of the channel that a new message is published.
 * @author Gauthier Cabot
 */
public class NewTextMsgServerMessage implements ServerMessage {
    String msgContent;
    String idSender;
    String idChannel;

    public NewTextMsgServerMessage(String msgContent, String idSender, String idChannel) {
        this.msgContent = msgContent;
        this.idSender = idSender;
        this.idChannel = idChannel;
    }

    public void handle(ILocalCom com) {
        TextMessage txt = com.getDataInterface().addTextMessage(msgContent, idSender, idChannel);

        try {
            SupplyNewTextMsgClientMessage msg = new SupplyNewTextMsgClientMessage(txt, idChannel);
            com.sendMessage(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
