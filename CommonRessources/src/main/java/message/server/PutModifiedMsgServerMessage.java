package message.server;

import CommonClasses.TextMessage;
import Interfaces.ILocalCom;

/**
 * Server message sent when modifying a message
 * Allows to notify the users connected to the channel the modification of a message
 * @author Gabriel Souza e Silva
 */
public class PutModifiedMsgServerMessage implements ServerMessage{
    private TextMessage textMsg;

    public PutModifiedMsgServerMessage(TextMessage textMsg){
        this.textMsg = textMsg;
    }

    public void handle(ILocalCom com) {
        com.getIhmChannelInterface().displayModifiedMsg(textMsg);
    }
}
