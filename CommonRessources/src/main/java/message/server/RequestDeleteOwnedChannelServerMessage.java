package message.server;

import Interfaces.ILocalCom;

/**
 * The class corresponding to methode notifyDeletedChannel().
 *
 * @author Aurelien PHILIPPE <aurelien.philippe@etu.utc.fr>
 */
public class RequestDeleteOwnedChannelServerMessage implements ServerMessage{
	private String idChannel;

	public String getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}

	public RequestDeleteOwnedChannelServerMessage(String idChannel) {
		this.idChannel = idChannel;
	}

	@Override
	public void handle(ILocalCom com) {
		com.getDataInterface().deleteOwnedChannel(idChannel);
	}
}
