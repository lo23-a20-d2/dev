package message.server;

import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;
import message.client.SupplyModifyNickNameClientMessage;

import java.io.IOException;

/**
 * Server message sent when modification of the nickname
 *
 * @author Violette Quitral <violette.quitral@etu.utc.fr>
 */

public class PutNewNicknameServerMessage implements ServerMessage {
	private final String nickname;
	private final String idUser;
	private final String idChannel;


	public PutNewNicknameServerMessage(String nickname, String idUser, String idChannel) {
		this.nickname = nickname;
		this.idUser = idUser;
		this.idChannel = idChannel;
	}


	public void handle(ILocalCom com) {
		//If we own the channel
		User creator = com.getDataInterface().getChannelOwner(this.idChannel);
		if (creator != null && creator.getId().equals(com.getIhmMainInterface().getIdConnectedUser())) {
			Subscription sub = com.getDataInterface().modifyNickname(this.nickname, this.idUser, this.idChannel);
			SupplyModifyNickNameClientMessage msg = new SupplyModifyNickNameClientMessage(idChannel, sub);
			try {
				com.sendMessage(msg);
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

		}
		com.getIhmChannelInterface().displayNewNickname(this.nickname,this.idUser,this.idChannel);
	}

}
