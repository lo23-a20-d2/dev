package message.server;

import Interfaces.ILocalCom;
import message.client.SupplyCancelKickClientMessage;

import java.io.IOException;

public class ConnectionErrorServerMessage implements ServerMessage {
    String errorMsg;

    public ConnectionErrorServerMessage(String msg) {
        this.errorMsg = msg;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().quitServer("Votre compte est déjà connecté à ce serveur");
    }
}
