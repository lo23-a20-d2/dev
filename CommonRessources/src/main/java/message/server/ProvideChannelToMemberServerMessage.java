package message.server;

import CommonClasses.Channel;
import CommonClasses.Message;
import CommonClasses.Subscription;
import Interfaces.ILocalCom;

import java.util.List;

/**
 * Server message sent when registering a user to a channel.
 * Used to provide the new subscriber with the channel information.
 * @author Gabriel Souza e Silva
 */
public class ProvideChannelToMemberServerMessage implements ServerMessage{
    private Channel channel;
    private List<Message> messages;
    private List<Subscription> subscriptions;

    public ProvideChannelToMemberServerMessage(Channel channel, List<Message> messages, List<Subscription> subscriptions){
        this.channel = channel;
        this.messages = messages;
        this.subscriptions = subscriptions;
    }

    public void handle(ILocalCom com) {
        Subscription sub = subscriptions.stream()
                .filter(s -> s.getUser().getId().equals(com.getIhmMainInterface().getIdConnectedUser()))
                .findFirst()
                .orElse(null);
        com.getIhmMainInterface().displayNewChannel(this.channel, sub);
        //com.getIhmChannelInterface().displayChannel(this.channel, this.messages, subscriptions);
    }
}
