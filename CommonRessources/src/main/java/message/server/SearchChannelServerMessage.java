package message.server;

import CommonClasses.Channel;
import Interfaces.ILocalCom;
import message.client.ClientMessage;

import java.util.ArrayList;

/**
 * Server message sent when searching a channel
 * @author Gauthier Cabot
 */

public class SearchChannelServerMessage implements ServerMessage  {

    ArrayList<Channel> channelList;
    public SearchChannelServerMessage(ArrayList<Channel> channelList) {
        this.channelList = channelList;
    }

    @Override
    public void handle(ILocalCom com) {
        com.getIhmMainInterface().displaySearchResult(channelList);
    }
}
