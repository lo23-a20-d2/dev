package message.server;

import Interfaces.ILocalCom;

/**
 * The class corresponding to methode notifyDeletedChannel().
 *
 * @author Aurelien PHILIPPE <aurelien.philippe@etu.utc.fr>
 */
public class NotifyDeletedChannelServerMessage implements ServerMessage{
	private String idChannel;

	public String getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}

	public NotifyDeletedChannelServerMessage(String idChannel) {
		super();
		this.idChannel = idChannel;
	}
	
	public void handle(ILocalCom com) {
		com.getIhmMainInterface().deleteChannel(idChannel);
	}
}
