package message.server;

import CommonClasses.Channel;
import CommonClasses.Message;
import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Server message sent when creating a shared channel.
 * Used to provide information about it to the new user.
 * @author Yiwen Wang
 */
public class NewChannelServerMessage implements ServerMessage {
    Channel channel;
    List<Subscription> subscriptions;

    public NewChannelServerMessage(Channel channel, List<Subscription> subscriptions) {
        this.channel = channel;
        this.subscriptions=subscriptions;
    }


    @Override
    public void handle(ILocalCom com) {
        Subscription sub = subscriptions.stream()
                .filter(s -> s.getUser().getId().equals(com.getIhmMainInterface().getIdConnectedUser()))
                .findFirst()
                .orElse(null);
        com.getIhmMainInterface().displayNewChannel(channel,sub);
        com.getIhmChannelInterface().displayChannel(channel, new ArrayList<>(),subscriptions);
    }
}
