package message.server;

import Interfaces.ILocalCom;
import java.io.Serializable;

public interface ServerMessage extends Serializable {
    public abstract void handle(ILocalCom com);
}
