package message.server;

import CommonClasses.Subscription;
import CommonClasses.User;
import Interfaces.ILocalCom;
import message.client.SupplyNewMemberClientMessage;

import java.io.IOException;

/**
 * Server message sent when inviting a user to a channel
 * Allows you to notify the channel creator of the invitation
 *
 * @author Gauthier Cabot
 */
public class InviteToChannelServerMessage implements ServerMessage {
    private final User user;
    private final String idChannel;
    private final Subscription.Role role;

    public InviteToChannelServerMessage(User user, String idChannel, Subscription.Role role) {
        this.idChannel = idChannel;
        this.user = user;
        this.role = role;
    }

    @Override
    public void handle(ILocalCom com) {
        Subscription sub = com.getDataInterface().addSubscription(idChannel, user, role);
        try {
            com.sendMessage(new SupplyNewMemberClientMessage(user.getId(), com.getDataInterface().getOwnedChannel(idChannel), sub));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
