package Interfaces;


import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;

import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * The interface Imh main to local data.
 *
 * @author Tom Bourg
 * @author Benjamin Vaysse
 */
public interface IhmMainToLocalData {
    /**
     * Save new account.
     *
     * @param login     the login
     * @param password  the password
     * @param firstName the first name
     * @param lastName  the last name
     * @param birthDate the birth date
     * @param avatar    the avatar
     * @return the user
     */
    User saveNewAccount(String login, String password, String firstName, String lastName, Date birthDate, BufferedImage avatar);

    /**
     * Gets user data.
     *
     * @param id    the id
     * @param login the login
     * @return the user data
     */
    User getUserData(String id, String login);


    /**
     * Authenticates a user.
     *
     * @param login    the login
     * @param password the password
     * @return the user data
     */
    User authenticate(String login,String password);

    User updateUser(String idUser, String firstName, String lastName, Date birthDate, BufferedImage avatar);

    Subscription addSubscription(String idChannel, User user, Subscription.Role role);


    /**
     * Creates an owned channel.
     *
     * @param name      the name
     * @param desc      the description of the channel
     * @param isPrivate specifies if the channel is private or not
     * @param owner     the owner of the channel
     * @return the channel
     */
    OwnedChannel createOwnedChannel(String name, String desc, boolean isPrivate, User owner);
}
