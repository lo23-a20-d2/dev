package Interfaces;

import CommonClasses.Channel;
import CommonClasses.Subscription;

import java.util.List;

/**
 * IhmMain's interface for LocalCom.
 * Contains set of methods needed by LocalCom.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public interface LocalComToIhmMain {
    /**
     * Getter for the connected user's id
     * @return String the connected user's id
     */
    public String getIdConnectedUser();

    /**
     * Display the user's search result
     *
     * @param channelList query's result
     */
    public void displaySearchResult(List<Channel> channelList);

    /**
     * Add a channel to the user ListChannel when the connected User joins it
     * @param channel channel joined by the user
     */
    public void displayNewChannel(Channel channel, Subscription subscription);

    /**
     * Add a channel to the user after being invited into it.
     *
     * @param channel the channel the user was invited to
     * @param subscription user's subscription to the new channel
     * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
     */
    public void newInvitation(Channel channel, Subscription subscription);

    /**
     * Called when a new public channel was created on the server
     *
     * @param channel the public channel created
     */
    public void newPublicChannel(Channel channel);

    /**
     * Notify the user he's been kicked from a channel.
     *
     * @param idUserKicked kicked user's id
     * @param channel the channel the user has been kicked from
     * @param endDate
     */
    public void notifyKickedChannel(String idUserKicked, Channel channel, long endDate);

    /**
     * Notify the user his kick has been canceled
     *
     * @param idUser kicked user's id
     * @param channel the channel the user used to be kicked from
     */
    public void notifyKickCanceled(String idUser, Channel channel);

    /**
     * Quit the current server
     * Reset the connected user and current channel to none. Show authentication page.
     */
    public void quitServer(String message);

    /**
     * Called on connexion, initiates all the user's channels and subscriptions
     *
     @param channelList       server's channels
     @param subscriptionList  user's subscriptions
     */
    public void initConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList);

    /**
     * Get the server's channels and the user's channels (joined and created) to update his homepage
     *
     * @param channelList      server's channel
     * @param subscriptionList user's subscriptions
     */
    public void updateConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList);

    /**
     * Remove owned channel when creator disconnected, and update his homepage
     *
     * @param ownedChannelsId list identifiants of owned channels
     */
    public void removeOwnedChannels(List<String> ownedChannelsId);

    /**
     * Delete channel from user and global channel lists
     * Hides ihmChannel if the deleted channel was the open one
     *
     * @param idChannel the id of the channel to delete
     */
    public void deleteChannel(String idChannel);
}
