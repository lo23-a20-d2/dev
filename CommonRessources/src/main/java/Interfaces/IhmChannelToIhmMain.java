package Interfaces;

/**
 * IhmMain's interface for IhmChannel.
 * Contains set of methods needed by ihmChannel.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public interface IhmChannelToIhmMain {
    /**
     * Getter for the connected user's id
     * @return String the connected user's id
     */
    public String getIdConnectedUser();
}
