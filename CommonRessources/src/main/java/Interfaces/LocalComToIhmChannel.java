package Interfaces;

import CommonClasses.*;

import java.util.List;

/**
 * The interface Local com to ihm channel.
 *
 * @author Guillaume Renaud
 */
public interface LocalComToIhmChannel {
    /**
     * Display new nickname.
     *
     * @param nickname  the nickname
     * @param idUser    the id user
     * @param idChannel the id channel
     */
    public void displayNewNickname(String nickname, String idUser, String idChannel);

    /**
     * Display new text msg.
     *
     * @param textMsg the text msg
     */
    public void displayNewTextMsg(TextMessage textMsg);

    /**
     * Display new file msg.
     *
     * @param fileMsg the file msg
     */
    public void displayNewFileMsg(FileMessage fileMsg);

    /**
     * Display modified msg.
     *
     * @param textMsg the text msg
     */
    public void displayModifiedMsg(TextMessage textMsg);

    /**
     * Display deleted msg.
     *
     * @param idMsg     the id msg
     * @param idChannel the id channel
     */
    public void displayDeletedMsg(String idMsg, String idChannel);

    /**
     * Display likes.
     *
     * @param idMsg     the id msg
     * @param idUser    the user
     * @param idChannel the id channel
     */
    public void displayLikes(String idMsg, String idUser, String idChannel);

    /**
     * Display text reply.
     *
     * @param textMsg the text msg
     */
    public void displayTextReply(TextMessage textMsg);

    /**
     * Display file reply.
     *
     * @param fileMsg the file msg
     */
    public void displayFileReply(FileMessage fileMsg);

    /**
     * Display thread.
     *
     * @param idChannel      the id channel
     * @param threadContents the thread contents
     */
    public void displayThread(String idChannel, List<Message> threadContents);

    /**
     * Display channel.
     *
     * @param channel       the channel
     * @param messages      the messages
     * @param subscriptions the subscriptions
     */
    public void displayChannel(Channel channel, List<Message> messages, List<Subscription> subscriptions);

    /**
     * Display new member greeting.
     *
     * @param sub the sub
     */
    public void displayNewMemberGreeting(Subscription sub);

    /**
     * Display new member.
     *
     * @param idChannel the id channel
     * @param newUser   the new user
     */
    public void displayNewMember(String idChannel, User newUser);

    /**
     * Notify channel kick.
     *
     * @param idUserKicked the id user kicked
     * @param idChannel    the id channel
     * @param endDate      the end date
     */
    public void notifyChannelKick (String idUserKicked, String idChannel, long endDate); //IHMChannel a changé le int en long pour pouvoir utiliser des timestamp

    /**
     * New admin.
     *
     * @param idUser    the id user
     * @param idChannel the id channel
     */
    public void newAdmin(String idUser, String idChannel);

    /**
     * Notify kick canceled.
     *
     * @param idUser    the id user
     * @param idChannel the id channel
     */
    public void notifyKickCanceled(String idUser, String idChannel);

    /**
     * Send connected users server list.
     *
     * @param connectedUsers the connected users
     */
    public void sendConnectedUsersServerList(List<User> connectedUsers);

    /**
     * Send new user connected to server.
     *
     * @param user the user
     */
    public void sendNewUserConnectedToServer(User user);

    /**
     * Notify quit channel.
     *
     * @param idUser the id user
     */
    public void notifyQuitChannel(String idUser);

    /**
     * Notify channel that a user disconnected from the server
     *
     * @param idUser the disconnected user id
     */
    public void sendUserDisconnectedFromServer(String idUser);

    /**
     * Update connected user.
     *
     * @param updatedUser the updated user
     */
    public void updateConnectedUser(User updatedUser);
}
