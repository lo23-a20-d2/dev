package Interfaces;

import message.client.ClientMessage;

import java.io.IOException;

public interface ILocalCom {
    /**
     * @return Interface IhmChannel
     */
    public LocalComToIhmChannel getIhmChannelInterface();

    /**
     * @return Interface LocalData
     */
    public CommToLocalData getDataInterface();

    /**
     * @return Interface IhmMain
     */
    public LocalComToIhmMain getIhmMainInterface();

    /**
     * Envoie d'un message client au serveur
     * @param msg Message client à envoyer
     * @throws IOException En cas d'erreur de connexion au serveur
     */
    public void sendMessage(ClientMessage msg) throws IOException;

}
