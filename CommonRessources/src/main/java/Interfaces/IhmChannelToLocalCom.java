package Interfaces;

import CommonClasses.Subscription;

import java.io.File;

public interface IhmChannelToLocalCom {
    public void changeNickname(String nickname, String idUser, String idChannel);

    /**
     * Communique la publication d'un message texte au serveur.
     * @param msgContent Contenu du message
     * @param idSender ID de l'auteur du message
     * @param idChannel ID du channel du message
     *
     * @author Gauthier Cabot
     */
    public void sendNewTextMsg(String msgContent, String idSender, String idChannel);
    public void sendNewFileMsg(File fileContent, String idSender, String idChannel);

    /**
     *
     * @param newContent Nouveau contenu de la message à modifier
     * @param idMsg ID du message
     * @param idChannel ID du channel
     *
     * @author Gabriel Souza e Silva
     */
    public void sendModifiedMsg(String newContent, String idMsg, String idChannel);

    public void sendDeletedMsg(String idMsg, String idChannel);

    public void sendLike(String idMsg, String idSender, String idChannel);

    public void sendNewTextReply(String msgContent, String idSender, String idChannel, String idThread);

    public void sendNewFileReply(File fileContent, String idSender, String idChannel, String idThread);

    public void requestThreadContents(String idChannel, String idThread);

    public void inviteUserToChannel(String idChannel, String idUser, Subscription.Role role);

    public void kickMember(String idUser, String idChannel, String idUserKicked, long endDate); // IHMChannel a changé le int en long pour pouvoir utiliser des timestamp (en accord avec le diagramme de séquence)

    public void promoteToAdmin(String idUser, String idChannel);

    public void cancelKick(String idUser, String idChannel);

    public void quitChannel(String idUser, String idChannel);

    public void deleteChannel(String idChannel);
}
