package Interfaces;

import CommonClasses.*;

import java.util.List;

/**
 * The interface Comm to local data.
 *
 * @author Kenza Rifki
 */
public interface CommToLocalData {
    /**
     * Modify a TextMessage from an owned channel.
     * We consider that the verification on the rights has been done beforehand.
     * (only the author of the message can modify a message).
     *
     * @param newContent the new content
     * @param messageId  the message id
     * @return the text message
     */
    public TextMessage modifyMessage(String newContent, String messageId);

    /**
     * Delete a message from an owned channel.
     * We consider that the verification on the rights has been done beforehand
     * (an admin of the channel or the author of the message can delete it).
     *
     * @param messageId the message id
     */
    public void deleteMessage(String messageId);

    /**
     * Modify nickname.
     *
     * @param nickname  the nickname
     * @param userId    the user id
     * @param channelId the channel id
     * @return Subscription
     */
    public Subscription modifyNickname(String nickname, String userId, String channelId);

    /**
     * Add text message.
     *
     * @param messageContent the message content
     * @param senderId       the sender id
     * @param channelId      the channel id
     * @return the message
     */
    public TextMessage addTextMessage(String messageContent, String senderId, String channelId);

    /**
     * Add file message.
     *
     * @param messageContent the message content
     * @param senderId       the sender id
     * @param channelId      the channel id
     * @return the message
     */
    public FileMessage addFileMessage(String messageContent, String senderId, String channelId);

    /**
     * Update likers list list.
     *
     * @param messageId the message id
     * @param senderId  the sender id
     * @param channelId the channel id
     * @return the list
     */
    public void updateLikersList(String messageId, String senderId, String channelId);

    //public FileMessage addFileMessage(String fileContent, String senderId, String channelId);

    /**
     * Add text reply message.
     *
     * @param messageContent the message content
     * @param senderId       the sender id
     * @param channelId      the channel id
     * @param threadId       the thread id
     * @return the message
     */
    public TextMessage addTextReply(String messageContent, String senderId, String channelId, String threadId);

    /**
     * Add file reply message.
     *
     * @param messageContent the message content
     * @param senderId       the sender id
     * @param channelId      the channel id
     * @param threadId       the thread id
     * @return the message
     */
    public FileMessage addFileReply(String messageContent, String senderId, String channelId, String threadId);

    /**
     * Gets thread contents.
     *
     * @param channelId the channel id
     * @param threadId  the thread id
     * @return the thread contents
     */
    public List<Message> getThreadContents(String channelId, String threadId);


    /**
     * Returns owned channel.
     *
     * @param idChannel the channel id
     * @return the owned channel
     */
    OwnedChannel getOwnedChannel(String idChannel);

    /**
     * Gets history.
     *
     * @param idChannel the id channel
     * @return the history
     */
    List<Message> getMessages(String idChannel);

    /**
     * Gets a specific message in a channel.
     *
     * @param idChannel the id channel
     * @param idMessage the id message
     * @return the message
     */
    Message getMessage(String idChannel, String idMessage);

    /**
     * Add subscription subscription.
     *
     * @param idChannel the id channel
     * @param user      the user
     * @param role      the role
     * @return the subscription
     */
    Subscription addSubscription(String idChannel, User user, Subscription.Role role);

    /**
     * Gets channel owner.
     *
     * @param channelId the channel id
     * @return the channel owner
     */
    User getChannelOwner(String channelId);

    /**
     * Delete an owned channel.
     * We consider that the verification on the rights has been done beforehand
     * (the owner of the channel can delete it).
     *
     * @param channelId the message id
     */
    void deleteOwnedChannel(String channelId);

    /**
     * Gets all owned channel.
     *
     * @param idUser the id user
     * @return the all owned channel
     */
    List<OwnedChannel> getAllOwnedChannel(String idUser);

    /**
     * Promotes a subscription to ADMIN
     *
     * @param idUser the id user
     * @param idChannel the id channel
     */
    void updateAdmList(String idUser, String idChannel);

    void removeFromUserList(String idUser, String idChannel);

    void cancelKick(String idUser, String idChannel);

    void blockUserChannel(String idChannel, String idUserKicked);

    void suspendMemberChannel(String idChannel, String idUserKicked, Long endDate);
}
