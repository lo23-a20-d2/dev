package Interfaces;

import CommonClasses.Channel;
import CommonClasses.Message;
import CommonClasses.Subscription;

import java.util.List;

/**
 * The interface Ihm main to ihm channel.
 * @author Guillaume Renaud
 */
public interface IhmMainToIhmChannel {
    /**
     * Display channel.
     *
     * @param channel       the channel
     * @param messages      the messages
     * @param subscriptions the subscriptions
     */
    public void displayChannel(Channel channel, List<Message> messages, List<Subscription> subscriptions);
}
