package Interfaces;

import CommonClasses.Channel;
import CommonClasses.Subscription;

import java.util.List;

/**
 * IhmMain's interface for LocalData.
 * Contains set of methods needed by LocalData.
 *
 * @author Sirine Ammar-Boudjelal <sirine.ammar-boudjelal@etu.utc.fr>
 */
public interface LocalDataToIhmMain {
    /**
     * Get the server's channels and the user's channels (joined and created) to update his homepage
     *
     * @param channelList server's channel
     * @param subscriptionList user's subscriptions
     */
    public void updateConnectedChannelList(List<Channel> channelList, List<Subscription> subscriptionList);
}
