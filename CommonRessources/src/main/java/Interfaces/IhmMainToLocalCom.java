package Interfaces;

import CommonClasses.Category;
import CommonClasses.OwnedChannel;
import CommonClasses.Subscription;
import CommonClasses.User;

import java.io.IOException;

public interface IhmMainToLocalCom {
    public void searchChannel(String input, Category category);

    /**
     * Rejoint un nouveaux channel (première connexion)
     * @param idChannel
     *        Integer representant l'id du channel
     * @param idUser
     *        Integer representant l'id de l'utilisateur
     * @throws IOException si une erreur survient lors de l'envoi du message via le socket
     *
     * @author Gabriel Souza e Silva
     */
    public void joinNewChannel(String idChannel, String idUser) throws IOException;

    /**
     * Passer le message de "Creer un channel" vers le serveur DATA
     *
     * @param name        nom du channel
     * @param description description du channel
     * @param isPrivate   channel public
     * @param isShared    channel partagé
     *
     * @author Yiwen Wang
     */
    public void createChannel(String name, String description, boolean isPrivate, boolean isShared);

    /**
     * Communique la création d'un channel propriétaire au serveur.
     *
     * @param channel Channel propriétaire créé.
     * @author Yiwen Wang
     */
    public void unsharedChannelCreated(OwnedChannel channel, Subscription sub);

    /**
     * Revenir sur un channel
     * @param idChannel Integer representant l'id du channel
     * @param idUser Integer representant l'id de l'utilisateur
     *
     * @author Gabriel Souza e Silva
     */
    public void enterChannel(String idChannel, String idUser);

    /**
     * Connecter le client au serveur et lui partager le compte de l'utilisateur
     * @param u
     *      Utilisateur
     * @param ipAddress
     *      Adresse IP du serveur auquel se connecter
     *
     * @author Gauthier Cabot
     */
    public void sendAccount(User u, String ipAddress) throws IOException;

    /**
     * Déconnecter le client du serveur
     * @author Gauthier Cabot
     */
    public void reqServerDisconnection() throws IOException;

    /**
     * Passer le message de "Modifier profile" vers le serveur DATA
     * @param u
     * @author Violette Quitral
     */
    void sendUpdateUser(User u);

}
