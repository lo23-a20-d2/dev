package Interfaces;

import CommonClasses.*;

import java.util.List;

/**
 * The interface containing the methods that server data is providing to server comm.
 *
 * @author Rami Jerbaka
 * @author Benjamin Vaysse
 */
public interface ServerCommToServerData {
    /**
     * Adds connected user to the server.
     *
     * @param user the user
     */
    void addConnectedUserServer(User user);

    /**
     * Removes connected user from the server.
     *
     * @param userId the user ID
     */
    void removeConnectedUserServer(String userId);

    /**
     * Gets all the connected users.
     *
     * @return the connected user server
     */
    List<User> getConnectedUserServer();

    /**
     * Gets a channel's connected users.
     *
     * @param channelId the channel id
     * @return the list of connected users
     */
    List<User> getConnectedUsers(String channelId);

    /**
     * Gets the is shared property of a channel
     *
     * @param channelId the channel id
     * @return true if the channel is shared, false if not.
     */
    boolean isSharedChannel(String channelId);

    /**
     * Gets a channel's creator.
     *
     * @param channelId the channel id
     * @return the channel's creator
     */
    User getChannelCreator(String channelId);

    /**
     * Gets all the connected channels available.
     *
     * @return the list of connected channels
     */
    List<Channel> getConnectedChannelList();

    /**
     * Gets a connected owned channel by id.
     * @param channelId the id of the channel
     * @return the connected owned channel
     */
    OwnedChannel getConnectedOwnedChannel(String channelId);

    /**
     * Gets a user's subscriptions list.
     *
     * @return the subscription list
     */
    List<Subscription> getUserSubscriptionList(User u);

    /**
     * Modifies a user's nickname.
     *
     * @param nickname  the nickname
     * @param userId    the user id
     * @param channelId the channel id
     */
    void modifyNickname(String nickname, String userId, String channelId);

    /**
     * Adds a text message to a channel.
     *
     * @param msgContent the message content
     * @param senderId   the sender id
     * @param channelId  the channel id
     * @return the text message
     */
    TextMessage addTextMsg(String msgContent, String senderId, String channelId);

    /**
     * Adds a text reply message.
     *
     * @param msgContent      the message content
     * @param senderId        the sender id
     * @param channelId       the channel id
     * @param parentMessageId the parent message id
     * @return the message
     */
    TextMessage addTextReply(String msgContent, String senderId, String channelId, String parentMessageId);

    /**
     * Adds a file reply message.
     *
     * @param msgContent      the message content
     * @param senderId        the sender id
     * @param channelId       the channel id
     * @param parentMessageId the parent message id
     * @return the message
     */
    FileMessage addFileReply(String msgContent, String senderId, String channelId, String parentMessageId);

    /**
     * Adds a file message.
     *
     * @param content   the content
     * @param senderId  the sender id
     * @param channelId the channel id
     * @return the message
     */
    Message addFileMsg(String content, String senderId, String channelId);

    /**
     * Modifies a text message from a shared channel.
     * We consider that the verification of the rights has been done beforehand.
     * (only the author of the message can modify a message).
     *
     * @param newContent the new content
     * @param msgId      the message id
     * @return the message
     */
    TextMessage modifyMessage(String newContent, String msgId);

    /**
     * Deletes a message from a shared channel.
     * We consider that the verification of the rights has been done beforehand
     * (an admin of the channel or the author of the message can delete it).
     *
     * @param messageId the message id
     */
    void deleteMessage(String messageId);

    /**
     * Deletes a shared channel.
     * We consider that the verification of the rights has been done beforehand
     * (the creator of the channel can delete it).
     *
     * @param channelId the message id
     */
    void deleteChannel(String channelId);

    /**
     * Updates the likers list of a message.
     *
     * @param msgId     the message id
     * @param senderId  the sender id
     * @param channelId the channel id
     */
    void updateLikersList(String msgId, String senderId, String channelId);

    /**
     * Gets a thread's content.
     *
     * @param channelId       the channel id
     * @param parentMessageId the parent message id
     * @return the thread's list of message
     */
    List<Message> getThreadContents(String channelId, String parentMessageId);

    /**
     * Searches in the channel list.
     *
     * @param input the search input
     * @param category the kind of input (channel description, nickname, channel name)
     * @return the list
     */
     List<Channel> searchChannel(String input, Category category);

    /**
     * Creates a new shared channel.
     *
     * @param creatorId  the creator id
     * @param name       the name of the channel
     * @param desc       the description of the channel
     * @param isPrivate  boolean telling us if the channel is private or not (public)
     */
    Channel createSharedChannel(String creatorId, String name, String desc, boolean isPrivate);

    /**
     * Returns a shared channel.
     *
     * @param idChannel    the channel id
     */
    SharedChannel getSharedChannel(String idChannel);

    /**
     * Gets a channel history of messages.
     *
     * @param idChannel the id channel
     * @return the list of the messages of the channel
     */
    List<Message> getMessages(String idChannel);

    /**
     * Gets a specific message in a channel.
     *
     * @param idChannel the id channel
     * @param idMessage the id message
     * @return the message
     */
    Message getMessage(String idChannel, String idMessage);

    /**
     * Gets all the subscriptions of a channel.
     *
     * @param idChannel the id channel
     * @return the list of subscriptions
     */

    List<Subscription> getSubscriptions(String idChannel);

    /**
     * Adds a connected user.
     *
     * @param idChannel the id channel
     * @param idUser    the id user
     */
    void addUserConnected(String idChannel, String idUser);

    /**
     * Blocks a user from a channel permanently.
     *
     * @param idChannel    the id channel
     * @param idUserKicked the id user kicked
     */
    void blockUserChannel(String idChannel, String idUserKicked);

    /**
     * Suspends a member of a channel temporarily.
     *
     * @param idChannel    the id channel
     * @param idUserKicked the id user kicked
     * @param endDate     the ban's end date
     */
    void suspendMemberChannel(String idChannel, String idUserKicked, Long endDate);

    /**
     * Adds a user to the administrators list of a channel.
     *
     * @param idUser the user id
     * @param idChannel the channel id
     */
    void updateAdmList(String idUser, String idChannel);

    /**
     * Removes a user from a channel.
     *
     * @param idUser    the id user
     * @param idChannel the id channel
     */
    void removeFromUserList(String idUser, String idChannel);

    /**
     * Cancel a kick/ban from a channel.
     *
     * @param idUser    the id user
     * @param idChannel the id channel
     */
    void cancelKick(String idUser, String idChannel);

    /**
     * Gets a subscription role.
     *
     * @param subscriptionId the subscription id
     * @return the subscription role (admin or user)
     */
    Subscription.Role getSubscriptionRole(String subscriptionId);

    /**
     * Adds a new subscription.
     *
     * @param idChannel the channel id
     * @param idUser the user id
     * @param role the user role in the channel
     * @return the subscription
     */
    Subscription addSubscription(String idChannel, String idUser,Subscription.Role role);

    /**
     * Adds a new connected owned channel.
     *
     * @param channel the owned channel to make available
     */
    void addConnectedOwnedChannel(OwnedChannel channel);

    /**
     * Removes a connected owned channel.
     *
     * @param channelId the channel id to remove for the connected channels list
     */
    void removeConnectedOwnedChannel(String channelId);

    /**
     * Gets a connected user by id.
     *
     * @param idUser the user id
     * @return the user
     */
    User getConnectedUserById(String idUser);

    /**
     * Adds connected owned channels.
     *
     * @param channels the list of owned channels to make available
     */
    void addConnectedOwnedChannel(List<OwnedChannel> channels);

    /**
     * Updates the list of connected users by adding a User.
     *
     * @param u the user
     */
    void updateConnectedUser(User u);

    /**
     * Gets a subscription.
     *
     * @param idChannel the channel id
     * @param idUser    the user id
     * @return the subscription role (admin or user)
     */
    Subscription getSubscription(String idChannel, String idUser);

    /**
     * Update dynamically the subscriptions of ownedChannels
     *
     * @param idChannel the channel id
     * @param sub       the subscription
     */
    void updateSubscriptionOwnedChannel(String idChannel, Subscription sub);
}
