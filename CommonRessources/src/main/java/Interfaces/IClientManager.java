package Interfaces;

import CommonClasses.User;
import message.server.ServerMessage;

import java.io.IOException;

public interface IClientManager {
    /**
     * Méthode permettant d'obtenir le module ServerCom
     * @return le Module Communication du serveur
     */
    public IServerCom getCom();

    /**
     * Envoi d'un message serveur à l'utilisateur géré par le ClientManager
     * @param msg Message Server à envoyer
     * @throws IOException
     */
    public void sendMessage(ServerMessage msg) throws IOException;

    /**
     * Affecter la valeur de l'ID de l'utilisateur géré par le ClientManager
     * @param user l'utilisateur
     */
    public void setUser(User user);

    /**
     * Affecter l'ID de l'utilisateur géré par le ClientManager
     * @return ID Utilisateur
     */
    public User getUser();
}
