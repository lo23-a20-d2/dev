package Interfaces;

import message.server.ServerMessage;

public interface IServerCom {
    /**
     * @return Interface ServerData
     */
    public ServerCommToServerData getDataInterface();

    /**
     * Envoie d'un message à tous les utilisateurs connectés au serveur
     * @param msg Message serveur à envoyer
     */
    public void sendMessageToServer(ServerMessage msg);

    /**
     * Envoie d'un message à tous les utilisateurs connectés au serveur excepté un, dont l'ID correspond à celle fournie.
     * @param msg Message serveur à envoyer
     * @param blackListedId ID de l'utilisateur auquel ne pas envoyer le messaege
     */
    public void sendMessageToServer(ServerMessage msg, String blackListedId);

    /**
     * Envoie d'un message à tous les utilisateurs connectés à un channel spécifique.
     * @param idChannel ID du channel
     * @param msg Messager serveur à envoyer
     */
    public void sendMessageToChannel(String idChannel, ServerMessage msg);

    /**
     * Récupère le ClientManager lié à un certain utilisateur
     * @param userId ID de l'utilisateur
     * @return ClientManager de l'utilisateur
     */
    public IClientManager getClientById(String userId);

    /**
     * Ecriture d'une chaine de caractères dans le log serveur
     * @param lvl Niveau d'importance
     * @param msg Chaine de caractères à écrire
     */
    public void log(java.util.logging.Level lvl, String msg);

    /**
     * Ecriture d'une exception dans le log serveur
     * @param msg Chaine de caractères descriptive
     * @param ex Exception à afficher
     */
    public void log(String msg, Exception ex);
}
