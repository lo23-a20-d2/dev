package CommonClasses;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;

/**
 * The type User.
 *
 * @author Mohamed KSUKUSI <mohamed.kuskusi@etu.utc.fr>
 */
public class User implements Serializable {
    private String id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private transient BufferedImage avatar;

    /**
     * Instantiates a new User.
     *  @param id       the id
     * @param login     the login
     * @param password  the password
     * @param firstName the first name
     * @param lastName  the last name
     * @param birthDate the birth date
     * @param avatar    the avatar image
     */
    public User(String id, String login, String password, String firstName, String lastName, Date birthDate, BufferedImage avatar) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.avatar = avatar;
    }

    public User(String id) {
        this.id = id;
        this.login = null;
        this.password = null;
        this.firstName = null;
        this.lastName = null;
        this.birthDate = null;
        this.avatar = null;
    }

    public User() {
        this.id = "";
        this.login = null;
        this.password = null;
        this.firstName = null;
        this.lastName = null;
        this.birthDate = null;
        this.avatar = null;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets birth date.
     *
     * @return the birth date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets birth date.
     *
     * @param birthDate the birth date
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets avatar.
     *
     * @return the avatar
     */
    public BufferedImage getAvatar() {
        return avatar;
    }

    /**
     * Sets avatar.
     *
     * @param avatar the avatar
     */
    public void setAvatar(BufferedImage avatar) {
        this.avatar = avatar;
    }


    @Override
    /**
     * Compares (==) the user with an object.
     *
     * @param obj Object to compare with
     *
     * @return true if the identifiers are equal, false if not.
     */
    public boolean equals(Object o) {
        if(o instanceof User){
            User user = (User) o;
            return user.getId().equals(this.getId());
        }
        return super.equals(o);
    }

    /**
     * Default serialization
     *
     * @param oos the object output stream
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();

        // write avatar


        //serializing avatar

        if (avatar != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(avatar, "png", baos);
            oos.writeObject(baos.toByteArray());
        } else {
            oos.writeInt(0);
        }
    }

    /**
     * Default deserialization
     *
     * @param ois the object input stream
     */
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();

        // read avatar
        /*
        try {
            int size = ois.readInt();
            if (size > 0) {
                byte[] bytes = new byte[size];
                ois.readFully(bytes);
                avatar = ImageIO.read(new ByteArrayInputStream(bytes));
            }
        } catch (OptionalDataException e) {
            avatar = null;
        } catch (IOException e) {}

        */


        //try deserializing avatar
        try {
            byte[] image = (byte[]) ois.readObject();
            avatar = ImageIO.read(new ByteArrayInputStream(image));
        }
        //if there's no avatar
        catch(OptionalDataException e){
            avatar = null;
        }
        catch (IOException | ClassNotFoundException e) {

            e.printStackTrace();
        }

    }
}
