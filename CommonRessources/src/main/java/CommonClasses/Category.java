package CommonClasses;

/**
 * Enum type Category, used to research a user or a channel.
 *
 * @author Tom BOURG
 */
public enum Category {
    CHANNEL_NAME ("Nom channel"),
    CHANNEL_DESC ("Description"),
    MEMBER ("Utilisateur");

    private String name;

    Category(String name){
        this.name = name;
    }

    public String toString(){
        return this.name;
    }
}
