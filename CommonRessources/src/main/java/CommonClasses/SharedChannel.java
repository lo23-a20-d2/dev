package CommonClasses;

import java.util.ArrayList;

/**
 * Shared channel class
 *
 * A shared channel exists as long as there is at least one user
 * on the channel, and any previous user of the channel can recreate it.
 *
 * @author Benjamin Vaysse <benjamin.vaysse@etu.utc.fr>
 * @author Kenza Rifki <kenza.rifki@etu.utc.fr>
 */
public class SharedChannel extends Channel {
    private ArrayList<Subscription> userList;
    private User creator;

    /**
     * Instantiates a new SharedChannel.
     *
     * @param id the id
     * @param name the name
     * @param description the description
     * @param isPrivate   the private property
     * @param creator the creator
     */
    public SharedChannel(String id, String name, String description, boolean isPrivate, User creator) {
        super(id, name, description, isPrivate);
        this.userList = new ArrayList<>();
        this.creator = creator;
    }

    public SharedChannel(String idChannel) { super(idChannel);}

    /**
     * Gets the channel's subscribers list.
     *
     * @return the user list
     */
    public ArrayList<Subscription> getUserList() {
        return userList;
    }

    /**
     * Sets the channel's subscribers list.
     *
     * @param userList the user list
     */
    public void setUserList(ArrayList<Subscription> userList) {
        this.userList = userList;
    }

    /**
     * Gets the channel's creator.
     *
     * @return the user creator
     */
    public User getCreator() {
        return creator;
    }

    /**
     * Sets the channel's creator.
     *
     * @param creator the user creator
     */
    public void setCreator(User creator) {
        this.creator = creator;
    }
}
