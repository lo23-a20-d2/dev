package CommonClasses;

import java.io.Serializable;

/**
 * The type Channel.
 * This is an abstract class which is used later on to create SharedChannel and OwnedChannel classes.
 * @author Benjamin Vaysse
 */
public abstract class Channel  implements Serializable {
    private String id;
    private String name;
    private String description;
    private boolean isPrivate;

    /**
     * Instantiates a new Channel.
     *
     * @param id the id
     * @param name the name
     * @param description the description
     * @param isPrivate   the private property
     */
    public Channel(String id, String name, String description, boolean isPrivate) {
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
        this.id = id;
    }

    public Channel() {
    }


    public Channel(String id) {
        this.id = id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the privacy level of the channel.
     *
     * @return true if the channel is private, false if not.
     */
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * Sets the private attribute of a channel.
     *
     * @param aPrivate true or false
     */
    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @Override
    public String toString() {
        return this.name;
    }
}