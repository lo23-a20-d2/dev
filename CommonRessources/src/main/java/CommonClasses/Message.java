package CommonClasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Tom Bourg
 * Class Message
 */
public abstract class Message implements Serializable {
    private String id;
    private String idThread;
    private Date datetime;
    private boolean edited;
    private boolean erased;
    private String idChannel;
    private String idSubscription;
    private List<String> likersList;

    /**
     * Instantiates a new Message.
     *  @param id       the id
     * @param idThread the thread id
     * @param datetime the datetime
     * @param edited   the edited property
     * @param erased   the erased property
     * @param idSubscription the assigned subscription id
     * @param idChannel the channel id
     */

    public Message(String id, String idThread, Date datetime, boolean edited, boolean erased, String idSubscription, String idChannel) {
        this.id = id;
        this.idThread = idThread;
        this.datetime = datetime;
        this.edited = edited;
        this.erased = erased;
        this.idSubscription = idSubscription;
        this.idChannel = idChannel;
        this.likersList = null;
    }

    public List<String> getLikersList() {
        return likersList;
    }

    public void setLikersList(List<String> likersList) {
        this.likersList = likersList;
    }

    public String getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(String idChannel) {
        this.idChannel = idChannel;
    }

    public String getIdSubscription() {
        return idSubscription;
    }

    public void setIdSubscription(String idSubscription) {
        this.idSubscription = idSubscription;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets id thread.
     *
     * @return the id thread
     */
    public String getIdThread() {
        return idThread;
    }

    /**
     * Sets id thread.
     *
     * @param idThread the id thread
     */
    public void setIdThread(String idThread) {
        this.idThread = idThread;
    }

    /**
     * Gets datetime.
     *
     * @return the datetime
     */
    public Date getDatetime() {
        return datetime;
    }

    /**
     * Sets datetime.
     *
     * @param datetime the datetime
     */
    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    /**
     * Gets the isEdited property.
     *
     * @return true if the message has been edited, false if not.
     */
    public boolean isEdited() {
        return edited;
    }

    /**
     * Sets edited property.
     *
     * @param edited true or false
     */
    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    /**
     * Gets the isErased property.
     *
     * @return true if the message has been erased, false if not.
     */
    public boolean isErased() {
        return erased;
    }

    /**
     * Sets erased property.
     *
     * @param erased true or false
     */
    public void setErased(boolean erased) {
        this.erased = erased;
    }

    @Override
    /**
     * Compares (==) the message with an object.
     *
     * @param obj Object to compare with
     *
     * @return true if the identifiers are equal, false if not.
     */
    public boolean equals(Object obj) {

        if(obj instanceof Message){
            Message m = (Message) obj;
            return this.id == m.id;
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Message id : "+ id;
    }
}
