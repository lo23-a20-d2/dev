package CommonClasses;

import java.util.Date;

/**
 * The type Text message, which inherits from message. It is a simple message, containing text.
 *
 * @author Mohamed KSUKUSI <mohamed.kuskusi@etu.utc.fr>
 */
public class TextMessage extends Message{


    private String textContent;

    /**
     * Instantiates a new Message.
     *
     * @param id             the id
     * @param idThread       the thread id
     * @param datetime       the datetime
     * @param edited         the edited property
     * @param erased         the erased property
     * @param idSubscription the assigned subscription id
     * @param idChannel      the channel id
     * @param textContent    the text content
     */
    public TextMessage(String id, String idThread, Date datetime, boolean edited, boolean erased, String idSubscription, String idChannel, String textContent) {
        super(id, idThread, datetime, edited, erased, idSubscription, idChannel);
        this.textContent = textContent;
    }

    /**
     * Gets text content.
     *
     * @return the text content
     */
    public String getTextContent() {
        return textContent;
    }

    /**
     * Sets text content.
     *
     * @param textContent the text content
     */
    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }


}
