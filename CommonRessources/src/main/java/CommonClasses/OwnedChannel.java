package CommonClasses;

import java.util.ArrayList;


/**
 * Owned channel class
 *
 * An owned channel is stored directly on the user's machine.
 * An owner channel only exists as long as its creator is connected; it
 * can be relaunched as soon as its creator reconnects or following its
 * request.
 *
 * @author Benjamin Vaysse <benjamin.vaysse@etu.utc.fr>
 * @author Kenza Rifki <kenza.rifki@etu.utc.fr>
 */
public class OwnedChannel extends Channel {
    private ArrayList<Subscription> whiteList;
    private User owner;

    /**
     * Instantiates a new OwnedChannel.
     *
     * @param id the id
     * @param name the name
     * @param description the description
     * @param isPrivate   the private property
     * @param owner the owner
     */
    public OwnedChannel(String id, String name, String description, boolean isPrivate, User owner) {
        super(id, name, description, isPrivate);
        this.whiteList = new ArrayList<>();
        this.owner = owner;
    }

    public OwnedChannel() {
        super();
    }

    public OwnedChannel(String idChannel) { super(idChannel);}

    /**
     * Gets the list of the channel's subscribers.
     *
     * @return the subscribers list
     */
    public ArrayList<Subscription> getWhiteList() {
        return whiteList;
    }

    /**
     * Sets the list of the channel's subscribers.
     *
     * @param whiteList the subscribers list
     */
    public void setWhiteList(ArrayList<Subscription> whiteList) {
        this.whiteList = whiteList;
    }

    /**
     * Gets the channel's owner.
     *
     * @return the user owner
     */
    public User getOwner() {
        return owner;
    }

    /**
     * Sets the channel's owner.
     *
     * @param owner the user owner
     */
    public void setOwner(User owner) {
        this.owner = owner;
    }
}
