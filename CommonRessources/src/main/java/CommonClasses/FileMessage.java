package CommonClasses;

import java.io.File;
import java.util.Date;

/**
 * The type File message, which inherits the Message class and contains a file.
 * @author Mohamed KSUKUSI <mohamed.kuskusi@etu.utc.fr>
 */
public class FileMessage extends Message{
    private String filePath;

    /**
     * Instantiates a new Message.
     *
     * @param id             the id
     * @param idThread       the thread id
     * @param datetime       the datetime
     * @param edited         the edited property
     * @param erased         the erased property
     * @param idSubscription the assigned subscription id
     * @param idChannel      the channel id
     * @param filePath       the file path
     */
    public FileMessage(String id, String idThread, Date datetime, boolean edited, boolean erased, String idSubscription, String idChannel, String filePath) {
        super(id, idThread, datetime, edited, erased, idSubscription, idChannel);
        this.filePath = filePath;
    }


    /**
     * Gets file path.
     *
     * @return the file path
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets file path.
     *
     * @param filePath the file path
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


}
