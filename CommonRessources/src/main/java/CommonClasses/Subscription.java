package CommonClasses;

import java.io.Serializable;

/**
 * The type Subscription. Contains the information of a user's subscription to a channel,
 * the user can either be an administrator of the channel, or only a simple user.
 *
 * @author Rami Jerbaka
 */
public class Subscription implements Serializable {

    /**
     * The enum Role : a user can be an administrator or a simple user.
     */
    public enum Role {
        ADMIN,
        USER
    }

    private String id;
    private User user;
    private Channel channel;
    private String nickname;
    private Role role;
    private Boolean isBanned;
    private long endDate;

    /**
     * Instantiates a new Subscription.
     * @param id the ID
     * @param user     the user
     * @param channel  the channel
     * @param nickname the user's nickname
     * @param role     the user's role in the channel
     * @param isBanned the banned property
     * @param endDate  the end date of the ban if isBanned is true
     */
    public Subscription(String id, User user, Channel channel, String nickname, Role role, Boolean isBanned, long endDate) {
        this.id = id;
        this.user = user;
        this.channel = channel;
        this.nickname = nickname;
        this.role = role;
        this.isBanned = isBanned;
        this.endDate = endDate;
    }

    /**
     * Instantiates a new Subscription.
     */
    public Subscription() {
        this.id = null;
        this.user = null;
        this.channel = null;
        this.nickname = null;
        this.role = null;
        this.isBanned = null;
        this.endDate = 0;
    }

    public Subscription(String id, User user, Channel channel, String nickname, Role role) {
        this.id = id;
        this.user = user;
        this.channel = channel;
        this.nickname = nickname;
        this.role = role;
        this.isBanned = false;
        this.endDate = 0;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Gets the channel.
     *
     * @return the channel
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Gets the user's nickname.
     *
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Gets the user's role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Gets the subscription ID.
     *
     * @return the ID
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the user's nickname.
     *
     * @param nickname the nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Sets the user's role.
     *
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Boolean getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Boolean banned) {
        isBanned = banned;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    @Override
    /**
     * Compares (==) the subscription with an object.
     *
     * @param obj Object to compare with
     *
     * @return true if user and channel references are equal, false if not.
     */
    public boolean equals(Object obj) {

        if (obj instanceof Subscription) //if obj is a Subscription object
        {
            Subscription m = (Subscription)obj;
            return ((user == m.user) && (channel == m.channel));
        }

        return false;
    }

}
